/*
 * This file contains guards that check for errors and incompabilities in the macro defines.
 *
 *  Created on: 30.11.2021
 *      Author: Benedikt Conze
 */

#ifndef GENERAL_CFG_GUARDS_H_
#define GENERAL_CFG_GUARDS_H_

/************
 ** GUARDS **
 ************/

#if ((USE_CAN_COMM) && (!HAL_USE_CAN))
#error "In order to use CAN, you have to set HAL_USE_CAN to TRUE."
#endif

#if !(USE_CAN_COMM || USE_ETH_COMM || USE_RTPS_COMM)
#error "Please activate at least one communication method."
#endif

#if ((CAN_FORWARD_TO_ETH == 1) || (ETH_FORWARD_TO_CAN == 1)) \
&& ((USE_CAN_COMM == 0) || (USE_CAN_COMM == 0))
#error Ethernet and Can needs to be enabled if messages shall be forwarded
#endif

#if defined(BOARD_ST_NUCLEO144_F767ZI) && (ASMO_USE_ADC || ASMO_USE_DAC )
#error "You can't use the external ADC or DAC on the nucleo board."
#endif

#if USE_USB_SERIAL && (!HAL_USE_USB || !HAL_USE_SERIAL_USB)
#error "To use serial communication over USB, activate HAL_USE_USB and HAL_USE_SERIAL_USB."
#endif

#if ASMO_DISPLAY_CON_STATUS && !USE_LED
#error "To display connection status on LEDs, LEDs have to be activated"
#endif

#endif /* GENERAL_CFG_GUARDS_H_ */
