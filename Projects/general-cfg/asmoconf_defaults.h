/*
 * Default settings for ASMO configuration options
 *
 *  Created on: 07.07.2022
 *      Author: Benedikt Conze
 */

#ifndef GENERAL_CFG_ASMOCONF_DEFAULTS_H_
#define GENERAL_CFG_ASMOCONF_DEFAULTS_H_

/*===========================================================================*/
/* Communication technology. Activate at least one                           */
/*===========================================================================*/

/**
 * @brief Use CAN communication?
 *
 * @note Activate HAL_USE_CAN in order to use this.
 */
#if !defined(USE_CAN_COMM) || defined(__DOXYGEN__)
#define USE_CAN_COMM            0
#endif

/**
 * @brief Use Ethernet communication?
 */
#if !defined(USE_ETH_COMM) || defined(__DOXYGEN__)
#define USE_ETH_COMM            1
#endif

/**
 * @brief Use RTPS communication?
 */
#if !defined(USE_RTPS_COMM) || defined(__DOXYGEN__)
#define USE_RTPS_COMM           0
#endif

/*===========================================================================*/
/* Forwarding                                                                */
/*===========================================================================*/

/**
 * @brief Forward incoming Ethernet messages to CAN?
 */
#if !defined(ETH_FORWARD_TO_CAN) || defined(__DOXYGEN__)
#define ETH_FORWARD_TO_CAN      0
#endif

/**
 * @brief Forward incoming CAN messages to Ethernet?
 */
#if !defined(CAN_FORWARD_TO_ETH) || defined(__DOXYGEN__)
#define CAN_FORWARD_TO_ETH      0
#endif

/*===========================================================================*/
/* Enable/Disable subsystems                                                 */
/*===========================================================================*/

/**
 * @brief Use serial communication over USB?
 *
 * @note Activate HAL_USE_USB and HAL_USE_SERIAL_USB in order to use this.
 */
#if !defined(USE_USB_SERIAL) || defined(__DOXYGEN__)
#define USE_USB_SERIAL          0
#endif

/**
 * @brief Use the ASMO buttons wrapper?
 */
#if !defined(USE_BUTTONS) || defined(__DOXYGEN__)
#define USE_BUTTONS             0
#endif

/**
 * @brief Use the ASMO LEDs on the STM32-ASMO or Nucleo board?
 */
#if !defined(USE_LED) || defined(__DOXYGEN__)
#define USE_LED                 0
#endif

/**
 * @brief Use the seven segment led display on the STM32-ASMO board?
 */
#if !defined(USE_SEVEN_SEG) || defined(__DOXYGEN__)
#define USE_SEVEN_SEG           0
#endif

/**
 * @brief Use extern buttons?
 */
#if !defined(EXTERN_BUTTONS) || defined(__DOXYGEN__)
#define EXTERN_BUTTONS          0
#endif

/**
 * @brief Use the ASMO external ADC?
 */
#if !defined(ASMO_USE_ADC) || defined(__DOXYGEN__)
#define ASMO_USE_ADC            0
#endif

/**
 * @brief Use the ASMO external DAC?
 */
#if !defined(ASMO_USE_DAC) || defined(__DOXYGEN__)
#define ASMO_USE_DAC            0
#endif

/**
 * @brief Use the led display subsystem.
 */
#if !defined(ASMO_USE_LED_DISPLAY) || defined(__DOXYGEN__)
#define ASMO_USE_LED_DISPLAY    0
#endif

/**
 * @brief Use the front board for the pump box.
 */
#if !defined(ASMO_USE_FRONTBOARD) || defined(__DOXYGEN__)
#define ASMO_USE_FRONTBOARD     0
#endif

/*===========================================================================*/
/* Ethernet communication options                                            */
/*===========================================================================*/

/**
 * @brief   The number of messages stored in the tx priority queue.
 * @details The larger the priority queue, the higher the maximal throughput.
 */
#if !defined(ASMO_ETH_MSG_BUF_SIZE) || defined(__DOXYGEN__)
#define ASMO_ETH_MSG_BUF_SIZE           8
#endif

/**
 * @details The amount of time in microseconds between the first message in the
 *          priority queue and the sending of all messages in the priority queue.
 */
#if !defined(ASMO_ETH_TX_DELAY_US) || defined(__DOXYGEN__)
#define ASMO_ETH_TX_DELAY_US            1000
#endif

/**
 * @details Maximum time in milliseconds to wait for the Ethernet connection to be
 *          established when board is turned on.
 */
#if !defined(ASMO_ETH_WAIT_FOR_CONNECTION) || defined(__DOXYGEN__)
#define ASMO_ETH_WAIT_FOR_CONNECTION    3000
#endif

/**
 * @brief   Enable this flag in your MDL_config to collect ethernet communication errors
 *          and also display those if ASMO seven segment display is available.
 */
#if !defined(ASMO_ETH_COLLECT_ERRORS) || defined(__DOXYGEN__)
#define ASMO_ETH_COLLECT_ERRORS         FALSE
#endif

/**
 * @brief   Enable or disable thread that periodically sends a heartbeat message.
 */
#if !defined(ASMO_SEND_HEARTBEAT) || defined(__DOXYGEN__)
#define ASMO_SEND_HEARTBEAT             USE_ETH_COMM
#endif

/**
 * @brief   The time in milliseconds between consecutive heartbeat messages.
 */
#if !defined(ASMO_HEARTBEAT_INTERVAL_MS) || defined(__DOXYGEN__)
#define ASMO_HEARTBEAT_INTERVAL_MS      2000
#endif

/**
 * @brief   Display Ethernet and RS232 link status on LEDs.
 */
#if !defined(ASMO_DISPLAY_CON_STATUS) || defined(__DOXYGEN__)
#define ASMO_DISPLAY_CON_STATUS         (USE_LED && (USE_ETH_COMM || USE_RTPS_COMM))
#endif

#endif /* GENERAL_CFG_ASMOCONF_DEFAULTS_H_ */