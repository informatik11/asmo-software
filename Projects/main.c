#include "SmartECLA_allHeaders.h"

/*
 * Application entry point.
 */
int main(int argc, char **argv) {

  (void)argc;
  (void)argv;

  halInit();
  chSysInit();

  /************************************
  ***  INITIALIZE PERIPHERALS START ***
  *************************************/

#if (USE_LED == true)
  LED_init();
#endif /* USE_LED == true */

#if defined(BOARD_STM32_ASMO)
  switch_init();
#endif /* defined(BOARD_STM32_ASMO) */

#if (USE_SEVEN_SEG == true)
  sevenSeg_init();
#endif /* USE_SEVEN_SEG == true */

  /*********************************
  *** INITIALIZE PERIPHERALS END ***
  **********************************/

  /* Signal successful peripheral init. */
#if (USE_LED == true)
  LED_switchMask(0x01);
#endif /* USE_LED == true */

#if (USE_SEVEN_SEG == true)
  sevenSeg_writeInt(0);

  /* Signal: Initialization done */
  sevenSeg_writeInt(1);
#endif /* USE_SEVEN_SEG == true */

  /* Initialize ASMO_ETH and CAN (if available). Used by safety thread. */
  ASMO_CommunicationsInit();

  /* Start safety thread. */
  ASMO_safetyInit();

  /* Initialization on the MD Layer. The MDL-Init will also start all MDL threads. */
  mdl_init();

  /* Start of the models. */
#ifdef MODELS_H_
  models_init();
#endif

  while (true) {
    chThdSleepSeconds(5);
  }

  /* Return to main exit handler. Is never reached. */
  return 0;
}
