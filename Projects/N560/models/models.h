/**
 * @file    models/models.h
 * @brief   Model header.
 *
 * @details Includes all model headers and additional USE_ID_ Defines
 * @{
 */

#ifndef MODELS_H_
#define MODELS_H_

//#include "MyModel_thread.h"      /* Model's header file */

/*===========================================================================*/
/* Model constants.                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Model pre-compile time settings.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Model data structures and types.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Model macros.                                                              */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

void models_init(void);
void models_dispatchCommand(MessageCommand *cmd);

#endif /* MODELS_H_ */

/** @} */

