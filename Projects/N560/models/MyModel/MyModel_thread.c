/**
 * @file    MyModel_thread.c
 * @brief   File to start the thread of the model.
 *
 * @{
 */

#include "SmartECLA_allHeaders.h"

/*===========================================================================*/
/* Model local definitions.                                                   */
/*===========================================================================*/
THD_WORKING_AREA(MYMODEL_THD_WA, 1024);

/*===========================================================================*/
/* Model exported variables.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* Model local variables and types.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Model local functions.                                                     */
/*===========================================================================*/
/**
 * @brief       MyModel thread.
 *
 * @param[in] p     No arguments
 */
THD_FUNCTION(MYMODEL_THD, p) {
  (void)p;
  chRegSetThreadName("MyModel");
  
  /* Run initialization of the model.*/
  MyModel_initialize();

  /* Wait until all necessary data may be there.*/
  chThdSleepSeconds(STARTUP_TIME);
  
  systime_t previousDeadline = chVTGetSystemTime(); // Current system time.
  while (!chThdShouldTerminateX()) {

    /* Run step of model.*/
    MyModel_step();

    previousDeadline = chThdSleepUntilWindowed(previousDeadline, chTimeAddX(previousDeadline, TIME_MS2I(STEP_TIME)));
  }
}

/*===========================================================================*/
/* Model interrupt handlers.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* Model exported functions.                                                  */
/*===========================================================================*/

/**
* @brief   Initializes and starts model thread.
*/
void models_myModel_start(void) {
  chThdCreateStatic(MYMODEL_THD_WA, sizeof(MYMODEL_THD_WA),
                    chThdGetPriorityX() - 20, (tfunc_t) MYMODEL_THD, NULL);
}

/**
* @brief   Command receiver function.
*/
void models_myModel_command(MessageCommand *cmd) {
  (void) cmd;
}

/** @} */

