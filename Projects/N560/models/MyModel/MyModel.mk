# Makefile for MyModel Model.

NEWMODEL_SRC :=	$(wildcard $(MODELSDIR)/$(NEWMODEL)/*.c)

NEWMODEL_INC :=	$(MODELSDIR)/$(NEWMODEL)

# Common
ALLCSRC += $(NEWMODEL_SRC)
ALLINC  += $(NEWMODEL_INC)
