# Makefile for Simulink Models.

# Include all model-specific makefiles:
NEWMODEL = MyModel
#include $(MODELSDIR)/$(NEWMODEL)/$(NEWMODEL).mk

# Add all source files to variable MODELSRC
MODELSRC = $(MODELSDIR)/models.c

# Add all include paths to variable MODELINC
MODELINC = $(MODELSDIR)

# Common
ALLCSRC += $(MODELSRC)
ALLINC  += $(MODELINC)
