/**
 * @file    models.c
 * @brief   File to start the models.
 *
 * @details This file contains ::models_init() which starts all models
 *          and the threads which trigger the model calculations.
 * @{
 */

#include "SmartECLA_allHeaders.h"

/*===========================================================================*/
/* Model local definitions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Model exported variables.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* Model local variables and types.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Model local functions.                                                     */
/*===========================================================================*/

/*===========================================================================*/
/* Model interrupt handlers.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* Model exported functions.                                                  */
/*===========================================================================*/

/**
* @brief   Starts the used models, each one in a separate thread.
*/
void models_init(void) {
  //models_myModel_start();
}

/**
* @brief   This function dispatches a model command.
* @note    Not sure what this is used for.
*/
void models_dispatchCommand(MessageCommand *cmd) {
  switch (cmd->messageId & DEVICE_ID_MASK) {
    (void) cmd;
  }
}

/** @} */

