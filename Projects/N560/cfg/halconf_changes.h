/*
 * This file contains project specific changes to the HAL configuration file
 * The default settings are found in general-cfg/halconf.h
 *
 *  Created on: 03-2022
 *      Author: Benedikt Conze
 */

#ifndef HALCONF_CHANGES_H
#define HALCONF_CHANGES_H

/*************************************************************
 *
 * Define project-specific changes to the default HAL configuration (general-cfg/halconf-defaults.h) here
 * DO NOT:
 *  - define functions or variables
 *  - include any files that define functions or variables
 *  - rely on system-specific or GCC-specific macros like __FLT_EPSILON__
 *  - define halconf-macros in the compiler options using -D
 *
 ************************************************************/

#endif /* HALCONF_CHANGES_H */
