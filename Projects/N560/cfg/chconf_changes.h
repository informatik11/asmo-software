/*
 * This file contains project specific changes to the application specific kernel settings
 * The default settings are found in general-cfg/chconf.h
 *
 *  Created on: 03-2022
 *      Author: Benedikt Conze
 */

#ifndef CHCONF_CHANGES_H
#define CHCONF_CHANGES_H

/*************************************************************
 *
 * Define project-specific changes to the default ChibiOS configuration (general-cfg/chconf-defaults.h) here
 * DO NOT:
 *  - define functions or variables
 *  - include any files that define functions or variables
 *  - rely on system-specific or GCC-specific macros like __FLT_EPSILON__
 *  - define chconf-macros in the compiler options using -D
 *
 ************************************************************/

/**
 * @brief   System tick frequency.
 * @details Frequency of the system timer that drives the system ticks. This
 *          setting also defines the system tick time unit.
 */
#define CH_CFG_ST_FREQUENCY                 5000

#endif /* CHCONF_CHANGES_H */
