# Medical Device Layer makefile

# Specify name of device subfolder
MDEVICE = N560

# Subdir to include 
MDLSUBDIR := $(MDLDIR)/$(MDEVICE)

# Include Device Subfolder's makefile
include $(MDLSUBDIR)/$(MDEVICE).mk

ALLINC += $(MDLDIR)