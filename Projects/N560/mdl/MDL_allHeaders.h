/*
 * This file encapsulates the Header management of the MDL and sets
 * the define for the currently used MD.
 *
 *  Created on: 05.07.2010
 *  Modified: 11.11.2010
 *      Author: Florian Goebe, Mathias Obster
 */

#ifndef MDL_ALLHEADERS_H_
#define MDL_ALLHEADERS_H_

//----------------------------------------------------------------------
// MDL CONFIG SECTION
//----------------------------------------------------------------------

// IMPORTANT: Set current MD (uncomment one of the following lines):
#define N560


// Choose if Extern Buttons used (uncommend the following line)
// #define EXTERN_BUTTONS



//----------------------------------------------------------------------
// All Headers section (don't edit please)
//----------------------------------------------------------------------

#ifdef ExampleProject
#include "ExampleProject_allHeaders.h"
#endif

#ifdef N560
#include "N560/N560_allHeaders.h"
#endif

// Functions which each project has to implement somewhere.
// If you don't need buttons, just write an empty function.
// Do NOT remove this declarations because the system tries to
// call the functions.

/**
 * Initializes the MDL Module
 */
void mdl_init(void);


#ifdef USE_SD_ENDRX_Interrupt
/**
 * Is called by the system when an Serial (USART) Receive Interrupt occurs.
 * Must be defined somewhere in MDL module in case we use "End Of Receive" Interrupts.
 */
void mdl_processEndOfReceiveInterrupt(void);
#endif

 #ifdef TIMEOUT_Interrupt
void mdl_processReceiverTimeoutInterrupt(void);
#endif

#endif /* MDL_ALLHEADERS_H_ */
