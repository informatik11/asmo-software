/**
 * \file	MDL_config.h
 * \brief	IL11: This file contains / includes all hardware/ASMO configuration (defines/structs) according to a certain MDL.
 *
 * You may exclusively include/set defines here that do NOT DEPEND on other headers or definitions,
 * since this header is always included first.
 * Include Level 11.
 *
 * \date	Oct 2020
 * \author	Moritz Huellmann
 */

#ifndef MDL_CONFIG_H_
#define MDL_CONFIG_H_

#include "N560/N560_defines.h"
#include "SmartECLA_allHeaders.h"

/*******************************
 ** MDL/USER-SPECIFIC DEFINES **
 *******************************/
#define DEVICE_NUMBER                   switch_readValue()
#define DEVICE_NUMBER_SHIFTER       DEFAULT_DEVNUMBER_SHIFT
#define DEVICE_ID                   DEVICE_ID_N560

/**
 * Use CAN communication?
 *
 * Activate HAL_USE_CAN in order to use this.
 */
#define USE_CAN_COMM              0

/**
 * Use Ethernet communication?
 */
#define USE_ETH_COMM              1

/**
 * Forward incoming Ethernet messages to CAN?
 */
#define ETH_FORWARD_TO_CAN        0

/**
 * Forward incoming CAN messages to Ethernet?
 */
#define CAN_FORWARD_TO_ETH        0

/**
 * Use serial communication over USB?
 *
 * Activate HAL_USE_USB and HAL_USE_SERIAL_USB in order to use this.
 */
#define USE_USB_SERIAL            1

/**
 * Use the ASMO buttons wrapper?
 */
#define USE_BUTTONS               0

/**
 * Use the ASMO LEDs on the STM32-ASMO or Nucleo board?
 */
#define USE_LED                   0

/**
 * Use extern buttons?
 */
#define EXTERN_BUTTONS            0

#endif /* MDL_CONFIG_H_ */
