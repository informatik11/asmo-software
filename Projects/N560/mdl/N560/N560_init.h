/**
 * \file 	N560_init.h
 * \date	06.12.2021
 * \brief	Header file for the initialization interface.
 * \author	Marc Wiartalla
 * \author	Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef _N560_INIT_H
#define _N560_INIT_H

// Begin of file -------------------------------------------------------------

// End of file -------------------------------------------------------------

#endif /* _N560_INIT_H */
