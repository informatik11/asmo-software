/**
 * \file 	N560_worker.c
 * \date	06.12.2021
 * \brief	N560 worker thread
 * \author	Marc Wiartalla
 * \author	Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#include "N560_allHeaders.h"

THD_FUNCTION(N560_workerThread, p) {
  (void)p;
  chRegSetThreadName("N560 Worker");

  ASMO_Measurement measurements[4] = { {SPO2, 0, TS_ZERO_VALUE}, {BPM, 0,
      TS_ZERO_VALUE},
                                      {PA, 0, TS_ZERO_VALUE}, {STATUS, 0,
                                          TS_ZERO_VALUE}};

  char tokenBuffer[64];
  uint8_t tokenWritePos = 0;
  uint8_t tokenCount = 0;

  bool isDataLine = false;
  timestamp_t timestamp = tsGetTS();

  sdStart(&SD2, &n560Config);

  while (true) {
    do {
      tokenBuffer[tokenWritePos++] = sdGet(&SD2);
    } while (tokenBuffer[tokenWritePos - 1] != '\n'
        && tokenBuffer[tokenWritePos - 1] != ' '
        && tokenBuffer[tokenWritePos - 1] != '\r');

    tokenCount++;

    if (tokenCount == 1) {
      isDataLine = tokenBuffer[0] >= 0x30 && tokenBuffer[0] <= 0x39;
      timestamp = tsGetTS();
    }
    else if (isDataLine) {
      uint16_t value = parseNumber(tokenBuffer);
      if (value != UINT16_MAX) {
        switch (tokenCount) {
        case TOKEN_POS_SPO2:
          measurements[0].value = value;
          measurements[0].timestamp = timestamp;
          ASMO_newMeasurement(measurements[0]);
          break;
        case TOKEN_POS_BPM:
          measurements[1].value = value;
          measurements[1].timestamp = timestamp;
          ASMO_newMeasurement(measurements[1]);
          break;
        case TOKEN_POS_PA:
          measurements[2].value = value;
          measurements[2].timestamp = timestamp;
          ASMO_newMeasurement(measurements[2]);
          break;
        }
      }
      else if (tokenCount == TOKEN_POS_STATUS) {
        measurements[3].value = parseStatus(tokenBuffer);
        measurements[3].timestamp = timestamp;
        ASMO_newMeasurement(measurements[3]);
      }

      if (tokenBuffer[tokenWritePos - 1] == '\n'
          || tokenBuffer[tokenWritePos - 1] == '\r') {
        tokenCount = 0;
        isDataLine = false;
      }

      tokenWritePos = 0;
      chThdSleepMilliseconds(10);
    }
    return;
  }
}

uint16_t parseNumber(const char *buffer) {
  uint16_t value = UINT16_MAX;
  uint8_t pos = 0;
  while (buffer[pos] != 0) {
    if (buffer[pos] >= 0x30 && buffer[pos] <= 0x39) {
      if (value == UINT16_MAX) {
        value = 0;
      }
      value = value * 10 + (buffer[pos] - 0x30);
    }
    pos++;
  }

  return value;
}

/*
 0: --- No Data Available
 1: * Alarm Parameter Being Violated
 2: AO Alarm Off
 3: AS Alarm Silence
 4: BU Battery in Use
 5: LB Low Battery
 6: LM Loss of Pulse with Interference
 7: LP Loss of Pulse
 8: MO Interference
 9: PH Pulse Rate Upper Limit Alarm
 10:    PL Pulse Rate Lower Limit Alarm
 11:    PS Pulse Search
 12:    SD Sensor Disconnect
 13:    SH Saturation Upper Limit Alarm
 14:    SL Saturation Lower Limit Alarm
 15:    SO Sensor Off
 */
uint16_t parseStatus(const char *buffer) {
  switch (buffer[0]) {
  case '-':
    return 0;
  case '*':
    return 1;
  case 'A':
    switch (buffer[1]) {
    case 'O':
      return 2;
    case 'S':
      return 3;
    default:
      return 0;
    }
  case 'B':
    return 4;
  case 'L':
    switch (buffer[1]) {
    case 'B':
      return 5;
    case 'M':
      return 6;
    case 'P':
      return 7;
    default:
      return 0;
    }
  case 'M':
    return 8;
  case 'P':
    switch (buffer[1]) {
    case 'H':
      return 9;
    case 'L':
      return 10;
    case 'S':
      return 11;
    default:
      return 0;
    }
  case 'S':
    switch (buffer[1]) {
    case 'D':
      return 12;
    case 'H':
      return 13;
    case 'L':
      return 14;
    case 'O':
      return 15;
    default:
      return 0;
    }
  default:
    return 0;
  }
}
