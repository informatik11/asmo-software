# List of all MDL (Medical Device Layer) files.
MDLSUBSRC = $(MDLSUBDIR)/N560_init.c \
			$(MDLSUBDIR)/N560_worker.c

# Common vars
ALLCSRC += $(MDLSUBSRC)
ALLINC  += $(MDLSUBDIR)