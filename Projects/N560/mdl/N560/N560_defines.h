/**
 * \file 	N560_defines.h
 * \date	06.12.2021
 * \brief	All N560-specific constants.
 * \author	Marc Wiartalla
 * \author	Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef _N560_DEFINES_H
#define _N560_DEFINES_H

#include "SmartECLA_allHeaders.h"

// Begin of file -------------------------------------------------------------

// Measurements
#define ACQUIRE_ID_SONOTT_FLOW1

// Measurement ids
#define SPO2    ID_N560_X_SPO2
#define BPM     ID_N560_X_BPM
#define PA      ID_N560_X_PA
#define STATUS  ID_N560_X_STATUS

//N560 byte positions in datastring
#define TOKEN_POS_SPO2      3
#define TOKEN_POS_BPM       4
#define TOKEN_POS_PA        5
#define TOKEN_POS_STATUS    6


// End of file ---------------------------------------------------------------

#endif /* _N560_DEFINES_H */
