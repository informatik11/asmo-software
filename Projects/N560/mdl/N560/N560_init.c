/**
 * \file 	N560_init.c
 * \date	06.12.2021
 * \brief	Main initialization interface for OS
 * \author	Marc Wiartalla
 * \author	Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#include "N560_allHeaders.h"

static THD_WORKING_AREA(N560_wa_workerThread, 512);

void mdl_init(void) {
  chThdCreateStatic(N560_wa_workerThread, sizeof(N560_wa_workerThread),
                    NORMALPRIO,
                    N560_workerThread,
                    NULL);

  ASMO_newMeasurement((ASMO_Measurement) {ID_N560_X_STARTUP, 1234, tsGetTS()});
}

