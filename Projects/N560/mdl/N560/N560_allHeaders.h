/**
 * \file 	N560_allHeaders.h
 * \date	06.12.2021
 * \brief	All N560-specific header files.
 * \author	Marc Wiartalla
 * \author	Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef _N560_ALLHEADERS_H
#define _N560_ALLHEADERS_H

// Begin of file -------------------------------------------------------------

#define APP_TITLE	"N560"

#include "SmartECLA_allHeaders.h"
#include "N560_defines.h"
#include "N560_init.h"
#include "N560_worker.h"

// End of file ---------------------------------------------------------------

#endif /* _N560_ALLHEADERS_H */
