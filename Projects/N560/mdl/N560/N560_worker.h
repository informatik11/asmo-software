/**
 * \file    N560_worker.h
 * \date    06.12.2021
 * \brief   N560 worker thread
 * \author  Marc Wiartalla
 * \author  Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef _N560_WORKER_H
#define _N560_WORKER_H

// Begin of file -------------------------------------------------------------

#define  N560_UART_BAUDRATE                 19200

static const SerialConfig n560Config = {N560_UART_BAUDRATE,
                                       0, USART_CR2_STOP1_BITS,
                                       0};

THD_FUNCTION(N560_workerThread, p);

uint16_t parseNumber(const char* buffer);
uint16_t parseStatus(const char* buffer);

// End of file -------------------------------------------------------------

#endif /* _N560_WORKER_H */
