# Projects

This folder contains an example project for the OxiMax N-560 pulse oxymeter. In addition a general configuration for all projects is included.

## Code Structure

```
+ general-cfg		Contains general configurations for all projects
+ N560			    The project for the OxiMax N-560 pulse oxymeter
+-- cfg				Project-specific configuration
+-- mdl				Medical device layer containing the actual actual data processing algorithms
+-- models			Generated code from Simulink models 	
```