/**
 * PTP Data Types.
 */
#include <inttypes.h>
#include <stdbool.h>
#include "ptp.h" /* for struct ptp_timestamp */

#ifndef _PTP_DATATYPES_H
#define _PTP_DATATYPES_H

#define PTP_MS_PER_S (1000LL)
#define PTP_US_PER_S (1000LL * PTP_MS_PER_S)
#define PTP_NS_PER_S (1000LL * PTP_US_PER_S)

struct ptp_clock_quality {
    uint8_t clockClass;
    uint8_t clockAccuracy;
    uint16_t offsetScaledLogVariance;
};

#define PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE 8
struct ptp_port_identity {
    uint8_t clockIdentity[PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE];
    uint16_t portNumber;
};

struct ptp_timeinterval {
    int64_t scaledNanoseconds;
};

/**
 * Subtract the given subtrahend from the given minuend and store the
 * result as a {@link struct ptp_timeinterval} in the given dest.
 *
 * @param[out] dest where the result of the subtraction will be stored
 * @param[in] minuend the minuend
 * @param[in] subtrahend the subtrahend
 */
void ptp_dt_subtract_ts_ts(struct ptp_timeinterval *dest, struct ptp_timestamp *minuend, struct ptp_timestamp *subtrahend);

/**
 * Subtract the given timeinterval (subtrahend) from the given 
 * timeinterval
 *
 * @param[out] dest where the result of the subtraction will be stored
 * @param[in] minuend the minuend, a ptp_timeinterval
 * @param[in] subtrahend the subtrahend, a ptp_timeinterval
 */
void ptp_dt_subtract_ti_ti(struct ptp_timeinterval *dest, struct ptp_timeinterval *minuend, struct ptp_timeinterval *subtrahend);

/**
 * Add the given timeinterval to the given timeinterval.
 *
 * @param[out] dest where to store the result
 * @param[in] summand1 first summand
 * @param[in] summand2 second summand
 */
void ptp_dt_add_ti_ti(struct ptp_timeinterval *dest, struct ptp_timeinterval *summand1, struct ptp_timeinterval *summand2);

/**
 * Divide the given timeinterval by the given divisor.
 *
 * @param[out] dest where to store the result
 * @param[in] divident the ti to divide
 * @param divisor the divisor 
 */
void ptp_dt_divide_ti(struct ptp_timeinterval *dest, struct ptp_timeinterval *divident, int64_t divisor);

/**
 * Check whether the given timeinterval is negative.
 *
 * @return true if it is negative, false if it is zero or positive.
 */
bool ptp_dt_ti_is_negative(struct ptp_timeinterval *dest);

/**
 * Compare the given {@link struct ptp_port_identity} structs.
 *
 * @return 0 if the structs are different, nonzero if they are equal.
 */
int ptp_dt_port_identity_equal(const struct ptp_port_identity *a, const struct ptp_port_identity *b);

/**
 * Compare the clockIdentities within the given 
 * {@link struct ptp_port_identity} structs.
 *
 * @return 0 if the structs are different, nonzero if they are equal.
 */
int ptp_dt_clock_identity_equal(const struct ptp_port_identity *a, const struct ptp_port_identity *b);

#endif /* _PTP_DATATYPES_H */
