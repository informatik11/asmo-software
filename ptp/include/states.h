#ifndef _PTP_STATES_H
#define _PTP_STATES_H

#include "engine.h"
#include "parser.h"

enum ptp_state {
    /* invalid state */
    __PTP_STATE_INVALID,
    /* indicates that no state transition shall be made */
    PTP_STATE_NO_TRANSITION,
    PTP_STATE_INITIALIZING,
    PTP_STATE_LISTENING,
    PTP_STATE_DISABLED,
    PTP_STATE_FAULTY,
    PTP_STATE_UNCALIBRATED,
    PTP_STATE_SLAVE
};

enum ptp_event {
    PTP_EVENT_INITIALIZE,
    /* custom event to signal initialization completion */
    PTP_EVENT_INITIALIZED,
    PTP_EVENT_DESIGNATED_ENABLED,
    PTP_EVENT_DESIGNATED_DISABLED,
    PTP_EVENT_FAULT_CLEARED,
    PTP_EVENT_FAULT_DETECTED,
    PTP_EVENT_STATE_DECISION_EVENT,
    PTP_EVENT_ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES,
    PTP_EVENT_SYNCHRONIZATION_FAULT,
    PTP_EVENT_MASTER_CLOCK_SELECTED,
    /* BMC result = BMC_MASTER */
    PTP_EVENT_BMC_MASTER,
    /* BMC result = BMC_PASSIVE */
    PTP_EVENT_BMC_PASSIVE,
    /* BMC result = BMC_SLAVE and new_master != old_master */
    PTP_EVENT_BMC_SLAVE_NEW_MASTER,
    /* BMC result = BMC_SLAVE and new_master == old_master */
    PTP_EVENT_BMC_SLAVE_SAME_MASTER
};

/** 
 * Struct of functions pointers that describe the behaviour in some
 * state of the state machine.
 */
struct ptp_state_hooks {
    /**
     * Hook that is called when this state is being entered. This hook
     * is always called when a state transition takes place, even if we 
     * remained in the current state.
     *
     * @param prev_state the previous state. If we remained in the same state,
     * prev_state equals the current state.
     * @param event the event that triggered the state change.
     * @param event_opaque opaque data passed by the event. Can be NULL.
     */
    void (*state_enter)(enum ptp_state prev_state, enum ptp_event event, void *event_opaque);

    /**
     * Called when an event happens. The current state shall perform
     * state transitions depending on the event.
     *
     * To make a transition, callees shall return a {@link enum ptp_state}.
     * If callees do not want to make a transition, they shall return the
     * special value PTP_STATE_NO_TRANSITION. In this case, state_enter 
     * will not be called and we will remain in the same state. Note that
     * this is different from returning the current state, which will call
     * state_enter.
     *
     * @param event the event that was triggered.
     * @param event_opaque opaque data passed by the event. Can be NULL.
     */
    enum ptp_state (*event_triggered)(enum ptp_event event, void *event_opaque);

    /**
     * Receive an Announce message.
     *
     * @param[in] hdr The parsed message header.
     * @param[in] msg The parsed message.
     * @param[in] ts The timestamp of the message. Maybe 
     * invalid, make sure to check valid flag!
     */
    int (*recv_announce)(struct ptp_proto_header *hdr, struct ptp_proto_announce *msg, struct ptp_engine_timestamp *ts);

    /**
     * Receive a Sync message.
     *
     * @param[in] hdr The parsed message header.
     * @param[in] msg The parsed message.
     * @param[in] ts The timestamp of the message. Maybe 
     * invalid, make sure to check valid flag!
     */
    int (*recv_sync)(struct ptp_proto_header *hdr, struct ptp_proto_sync *msg, struct ptp_engine_timestamp *ts);

    /**
     * Receive a Follow Up message.
     *
     * @param[in] hdr The parsed message header.
     * @param[in] msg The parsed message.
     * @param[in] ts The timestamp of the message. Maybe 
     * invalid, make sure to check valid flag!
     */
    int (*recv_follow_up)(struct ptp_proto_header *hdr, struct ptp_proto_follow_up *msg, struct ptp_engine_timestamp *ts);

    /**
     * Receive a Delay Resp message.
     *
     * @param[in] hdr The parsed message header.
     * @param[in] msg The parsed message.
     * @param[in] ts The timestamp of the message. Maybe
     * invalid, be sure to check valid flag!
     */
    int (*recv_delay_resp)(struct ptp_proto_header *hdr, struct ptp_proto_delay_resp *msg, struct ptp_engine_timestamp *ts);
};

#endif /* _PTP_STATES_H */
