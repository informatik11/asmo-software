#ifndef _PTP_DATASET_UPDATE_H
#define _PTP_DATASET_UPDATE_H

#include "parser.h"

/**
 * Update the PTP dataset according to state decision code S1.
 * 
 * @param[in] announce the announce message E_best
 * @param[in] hdr the headers of of E_best
 */
void ptp_dataset_update_s1(const struct ptp_proto_announce *announce, const struct ptp_proto_header *hdr);

/**
 * Update the PTP dataset according to state decision code M1.
 * 
 * @param[in] announce the announce message E_best
 * @param[in] hdr the headers of of E_best
 */
void ptp_dataset_update_m1(void);

#endif /* _PTP_DATASET_UPDATE_H */
