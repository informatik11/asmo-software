/**
 * Common datastructures for BMC modules.
 */
#ifndef _PTP_BMC_COMMON_H
#define _PTP_BMC_COMMON_H

#include "parser.h"

/* foreignMasterDS equivalent */
struct ptp_foreign_master {
    //struct ptp_port_identity portIdentity;
    struct ptp_proto_announce lastAnnounceMsg;
    struct ptp_proto_header lastAnnounceHeader;

    /* number of announce messages received in total */
    uint32_t announceMessageCount;

    int valid;
};

#endif /* _PTP_BMC_COMMON_H */
