#ifndef _PTP_CONFIG_H
#define _PTP_CONFIG_H

#define PTP_CONF_OUI_BYTE1 0xAC
#define PTP_CONF_OUI_BYTE2 0xDE
#define PTP_CONF_OUI_BYTE3 0x48

/* The announce interval. Should be uniform within a domain.
 * See 7.7.2.2. */
#define PTP_ANNOUNCE_INTERVAL 1

/* Number of announce message we must have received within
 * PTP_CONF_FOREIGN_MASTER_TIME_WINDOW to consider the master
 * qualified */
// no more than 254.
#define PTP_CONF_FOREIGN_MASTER_THRESHOLD 2
#define PTP_CONF_FOREIGN_MASTER_TIME_WINDOW (4 * PTP_ANNOUNCE_INTERVAL)

#define PTP_CONF_ANNOUNCE_RECEIPT_TIMEOUT 3

/* If 1, meanPathDelay will regularily be logged to console */
#define PTP_CONF_DELAY_LOG 1

/* If 1, the raw value of the most recent delay measurement will
 * be logged to console */
#define PTP_CONF_DELAY_LOG_RECENT 1

/* If measured delay is below the given value in microseconds, 
 * it is considered an outlier and the measurement is ignored. 
 * Negative delays are always considered outliers.*/
#define PTP_CONF_DELAY_MIN_US 20

/* If measured delay is above the given value in microseconds,
 * it is considered an outlier and the measurement is ignored.
 * Negative delays are always considered outliers. */
#define PTP_CONF_DELAY_MAX_US 10000

/* When we encountered this number of delay outlier measurements,
 * we assume a failure and enter failure state. 0 to disable,
 * no more than 254. */
#define PTP_CONF_DELAY_OUTLIER_THRESHOLD 4

/* Timeout for receiving a Delay_Resp in response to a Delay_Req */
#define PTP_CONF_DELAY_RESP_TIMEOUT 2000

/* How often to transmit Delay_Req packets before assuming failure,
 * 0 to retry indefinitely */
#define PTP_CONF_DELAY_RETRANSMIT_LIMIT 3

/* meanPathDelay is smoothed. It is calculated from the last 
 * measurement and the previous meanPathDelay. The most recent 
 * measurement is weighted with this factor, while the old 
 * measurement is weighted with 1 - PTP_CONF_DELAY_RECENT_WEIGHT. 
 *
 * Must be between 0 and 1.0. */
#define PTP_CONF_DELAY_RECENT_WEIGHT 0.6

/* If 1, offsetFromMaster will be logged on each clock sync */
#define PTP_CONF_SYNC_OFFSET_LOG 0

/* If 1, the control value of the PI controller will be logged */
#define PTP_CONF_SYNC_CONTROL_VALUE_LOG 0

#define PTP_CONF_SYNC_PI_FACTOR_P 0.6
#define PTP_CONF_SYNC_PI_FACTOR_I 0.3


#endif /* _PTP_CONFIG_H */
