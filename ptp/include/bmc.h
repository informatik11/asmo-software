/**
 * The Best Master Clock Algorithm module.
 *
 * This module manages a list of available masters, calculates
 * which is the best and feeds the apropriate events to the
 * state machine.
 */
#ifndef _PTP_BMC_H
#define _PTP_BMC_H

#include <stddef.h>
#include "stream.h"
#include "parser.h"
#include "datatypes.h"

/**
 * Add a new foreign master or update the record for an existing 
 * foreign master.
 *
 * @param[in] msg the announce message received by the master.
 * @param[in] hdr the header of the message.
 */
void ptp_bmc_add_foreign_master(struct ptp_proto_announce *msg, struct ptp_proto_header *hdr);

/**
 * Execute the BMC algorithm, causing emission of appropriate
 * events to the state machine.
 */
void ptp_bmc_execute(void);

/**
 * Clear the list of foreign masters.
 */
void ptp_bmc_clear_foreign_masters(void);

/**
 * Print debug info for the BMC module to the given
 * stream.
 *
 * @param stream the stream to write information to
 */
void ptp_bmc_print_debug(ptp_stream_t stream);

#endif /* _PTP_BMC_H */
