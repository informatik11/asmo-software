/**
 * Peek functions: peek values from packets and adjust byte ordering.
 * The inverse operation (putting values into packets) is provided 
 * by the poke module.
 */
#ifndef _PTP_PEEK_H
#define _PTP_PEEK_H

#include <inttypes.h>

uint8_t peek_uint8(const uint8_t *packet);

int8_t peek_int8(const uint8_t *packet);

uint16_t peek_uint16(const uint8_t *packet);

int16_t peek_int16(const uint8_t *packet);

uint32_t peek_uint32(const uint8_t *packet);

uint64_t peek_uint48(const uint8_t *packet);

int64_t peek_int64(const uint8_t *packet);
#endif /* _PTP_PEEK_H */
