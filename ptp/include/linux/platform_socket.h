#ifndef _PLATFORM_HEADERS_SOCKET_H
#define _PLATFORM_HEADERS_SOCKET_H

/**
 * a socket. all members are private and must not be accessed by
 * module users.
 */
typedef struct _ptp_socket {
    int __fd;
} ptp_socket_t;

/**
 * Metadata of a packet. All members are private and must
 * not be accessed by module users.
 */
typedef int ptp_packet_meta_t;

typedef int ptp_socket_ts_t;

#endif /*_PLATFORM_HEADERS_SOCKET_H */
