#ifndef _PTP_STATEMACHINE_H
#define _PTP_STATEMACHINE_H

#include "socket.h"
#include "engine.h"
#include "states.h"


/**
 * Initialize the state machine.
 */
void ptp_state_init(void);

/**
 * Trigger a given event.
 *
 * When the given event is not applicable to the current
 * state, we will automatically enter the PTP_STATE_FAULTY 
 * state.
 */
void ptp_state_triggerevt(enum ptp_event evt);

/**
 * Get the current state.
 *
 * @return the current state
 */
enum ptp_state ptp_state_get(void);

/**
 * Receive a packet.
 *
 * @param[in] msg The PTP message, as parsed by {@link ptp_engine_parse}.
 */
int ptp_state_recv(struct ptp_engine_message *msg);

/**
 * Get a string representation of the given state.
 *
 * Callers must not modify the returned string.
 *
 * @param state the state to get the string rep for.
 * can be a invalid or unknown state.
 * @return a string repr of the state
 */
const char *ptp_state_to_str(enum ptp_state state);

/**
 * Arm (start) the Announce receipt timeout. If the timer is 
 * currently armed, it will start from the beginning.
 */
void ptp_state_arm_announce_receipt_timeout(void);

/**
 * Disarm (stop) the Announce receipt timeout.
 */
void ptp_state_disarm_announce_receipt_timeout(void);


#endif /* _PTP_STATEMACHINE_H */
