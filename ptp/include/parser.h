/**
 * PTP protocol parser
 *
 * This module is responsible for parsing PTP protocol messages.
 * It expects the packets as received from the network and parses
 * them to structs that can then be used by other modules to 
 * interpret and handle the packets.
 *
 * Note that the structs defined in this header should not be 
 * sent over the network. They are purely meant for 
 * application-internal use.  The C compiler may pad the struct, 
 * unaligned memory accesses may take place or the byte order may 
 * be different from the network. Instead, please use one of the 
 * ptp_proto_pack_*() functions.
 */

#ifndef _PTP_PARSER_H
#define _PTP_PARSER_H

#include <inttypes.h>
#include <stddef.h>
#include "datatypes.h"
#include "proto.h"

/**
 * parse the given PTP protocol header. 
 *
 * @param[in] packet the packet to parse, including header, message and suffix.
 * @param[in] packet_size total size of the packet, including header, message and suffix.
 * @param[out] parsed_header where the parsed packet will be stored. Might have been modified on error.
 * @return 0 on success, otherwise an error ID
 */
int ptp_parser_header(
        const uint8_t *packet, 
        const size_t packet_size, 
        struct ptp_proto_header *parsed_header);

/**
 * parse the given PTP Announce message.
 *
 * @param[in] packet the packet to parse, including header, message and suffix.
 * @param[in] packet_size total size of the packet, including header, message and suffix.
 * @param[in] parsed_header parsed headers as returned by {@link ptp_parsed_header}.
 * @param[out] parsed where the parsed packet will be stored. Might have been modified on error.
 * @return 0 on success, otherwise an error ID
 */
int ptp_parser_announce(
        const uint8_t *packet, 
        const size_t packet_size, 
        const struct ptp_proto_header *parsed_header, 
        struct ptp_proto_announce *parsed);

/**
 * parse the given PTP Sync message
 *
 * @param[in] packet the packet to parse, including header, message and suffix.
 * @param[in] packet_size total size of the packet, including header, message and suffix.
 * @param[in] parsed_header parsed headers as returned by {@link ptp_parsed_header}.
 * @param[out] parsed where the parsed packet will be stored. Might have been modified on error.
 * @return 0 on success, otherwise an error ID
 */
int ptp_parser_sync(
        const uint8_t *packet,
        const size_t packet_size,
        const struct ptp_proto_header *parsed_header, 
        struct ptp_proto_sync *parsed);

/**
 * parse the given PTP Follow_Up message
 *
 * @param[in] packet the packet to parse, including header, message and suffix.
 * @param[in] packet_size total size of the packet, including header, message and suffix.
 * @param[in] parsed_header parsed headers as returned by {@link ptp_parsed_header}.
 * @param[out] parsed where the parsed packet will be stored. Might have been modified on error.
 * @return 0 on success, otherwise an error ID
 */
int ptp_parser_follow_up(
        const uint8_t *packet,
        const size_t packet_size,
        const struct ptp_proto_header *parsed_header, 
        struct ptp_proto_follow_up *parsed);

/**
 * parse the given PTP Delay_Resp message
 *
 * @param[in] packet the packet to parse, including header, message and suffix.
 * @param[in] packet_size total size of the packet, including header, message and suffix.
 * @param[in] parsed_header parsed headers as returned by {@link ptp_parsed_header}.
 * @param[out] parsed where the parsed packet will be stored. Might have been modified on error.
 * @return 0 on success, otherwise an error ID
 */
int ptp_parser_delay_resp(
        const uint8_t *packet,
        const size_t packet_size,
        const struct ptp_proto_header *parsed_header,
        struct ptp_proto_delay_resp *parsed);


/* first nibble encodes octet, last nibble encodes
   bit number */
#define PTP_FLAG_ALTERNATE_MASTER 0x00
#define PTP_FLAG_TWO_STEP 0x01
#define PTP_FLAG_UNICAST 0x02
#define PTP_FLAG_LEAP61 0x10
#define PTP_FLAG_LEAP59 0x11
#define PTP_FLAG_CURRENT_UTC_OFFSET_VALID 0x12
#define PTP_FLAG_PTP_TIMESCALE 0x13
#define PTP_FLAG_TIME_TRACEABLE 0x14
#define PTP_FLAG_FREQUENCY_TRACEABLE 0x15

/**
 * Check if the given header has the given flag set.
 *
 * If the given flag is not applicable to the message described
 * by the given header, it is treated as not set.
 *
 * @param[in] hdr a header
 * @param flag the flag to check, one of the PTP_FLAG_* defines
 */
int ptp_parser_header_has_flag(
        const struct ptp_proto_header *hdr,
        uint8_t flag);

#endif /* _PTP_PARSER_H */
