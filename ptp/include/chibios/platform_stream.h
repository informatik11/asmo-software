#ifndef _PLATFORM_HEADERS_STREAM_H
#define _PLATFORM_HEADERS_STREAM_H

#include <ch.h>
#include <hal.h>
#include <chprintf.h>
#include <memstreams.h>

typedef BaseSequentialStream* ptp_stream_t;

#endif /* _PLATFORM_HEADERS_STREAM_H */
