/**
 * The main thread. All PTP code executes from this thread.
 */
#ifndef _PTP_OS_MAIN_THD_H
#define _PTP_OS_MAIN_THD_H

/** 
 * This is a private struct that is not to be used by module users.
 */
struct ptp_os_main_thd_action {
    uint8_t _pool_free;
    int (*_handler_func)(void *opaque);
    void *_opaque;
};

void ptp_os_ch_startmain(void);

/**
 * Execute the given callback in a context where it is safe to execute
 * PTP code. It is guaranteed that no other PTP code will execute 
 * concurrently.
 *
 * This function is to be used to post to the main thread from ISRs.
 *
 * @param[in] callback The callback to execute. It shall return 0 if
 * it succeeded and nonzero otherwise. 
 * @param[in] opaque Opaque data passed to the callback as first 
 * argument. Do not pass stack-allocated data here, as callbacks
 * may be executed asynchronously.
 * @param is_highprio Whether the message is high priority.
 * @return 0 on success, PTP_ERROR_TIMEOUT on timeout, 
 * PTP_ERROR_RESET when the operation was aborted, 
 * PTP_ERROR_OUT_OF_MEMORY when memory allocation failed.
 */
int ptp_os_post_main_isr(int (*callback)(void *opaque), void *opaque, int is_highprio);

/**
 * Same as above, without aquiring the kernel lock (if it is already held)
 */
int _ptp_os_post_main_isr(int (*callback)(void *opaque), void *opaque, int is_highprio);

#endif /* _PTP_OS_MAIN_THD_H */
