#ifndef _PLATFORM_HEADERS_SOCKET_H
#define _PLATFORM_HEADERS_SOCKET_H

#include <lwip/api.h>
/**
 * a socket. all members are private and must not be accessed by
 * module users.
 */
typedef struct _ptp_socket {
    struct netconn *__netconn;
} ptp_socket_t;

/**
 * a timestamp
 */
typedef struct lwip_ts ptp_socket_ts_t;

/**
 * Metadata of a packet. All members are private and must
 * not be accessed by module users.
 */
typedef struct _ptp_packet_meta {
    struct netbuf *__netbuf;
} ptp_packet_meta_t;

#endif /*_PLATFORM_HEADERS_SOCKET_H */
