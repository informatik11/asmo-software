#ifndef _PLATFORM_HEADERS_OS_H
#define _PLATFORM_HEADERS_OS_H

#include <ch.h>
#include "platform_main_thd.h"

typedef mutex_t ptp_os_mutex_t;

/**
 * A timer. All members are private and must not be accessed or
 * modified by users.
 */
typedef struct _ptp_os_timer {
    virtual_timer_t _tmr;
    struct ptp_os_main_thd_action _action;
} ptp_os_timer_t;

#endif /* _PLATFORM_HEADERS_OS_H */
