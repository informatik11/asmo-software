/**
 * Clock synchronization module.
 *
 * This module is responsible for synchronizing the local clock
 * to the master.
 */
#ifndef _PTP_SYNC_H
#define _PTP_SYNC_H
#include "datatypes.h"

/**
 * Inhibit clock synchronization. The clock will not be synchronized
 * until {@link ptp_sync_uninhibit()} is called.
 *
 * Note that implementations MAY have a timeout that removes the 
 * inhibitation.
 */
void ptp_sync_inhibit(void);

/**
 * Un-inhibit clock synchronization.
 *
 * Implementations may synchronize the clock immediately when this
 * method is called.
 */
void ptp_sync_uninhibit(void);

/**
 * Update synchronization parameters.
 *
 * @param correction The correction value. In case the master
 * is a two-step clock, this should be the sum of the 
 * correction values of Sync and Follow_Up.
 * @param sync_recv_ts The timestamp when the Sync message was 
 * received.
 * @param sync_origin_ts In case the master is a two-step clock, the 
 * preciseOriginTimestamp of the Follow_Up message, otherwise
 * the originTimestamp of the Sync message.
 */
void ptp_sync_update(struct ptp_timeinterval correction, struct ptp_timestamp *sync_recv_ts, struct ptp_timestamp *sync_origin_ts);

#endif /* _PTP_SYNC_H */
