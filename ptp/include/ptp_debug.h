#include "parser.h"

#ifndef _PTP_DEBUG_H
#define _PTP_DEBUG_H
void ptp_debug_dump_header(struct ptp_proto_header *hdr);

void ptp_debug_dump_announce(struct ptp_proto_announce *announce);

void ptp_debug_dump_sync(struct ptp_proto_sync *sync);

void ptp_debug_dump_follow_up(struct ptp_proto_follow_up *follow_up);
#endif /* _PTP_DEBUG_H */
