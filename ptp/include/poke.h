/**
 * Poke Module. Poke values into packets and adjust byte ordering.
 * The inverse operation (taking values out of packets) is 
 * provided by the peek module.
 */

#ifndef _PTP_POKE_H
#define _PTP_POKE_H

#include <inttypes.h>

void poke_uint8(uint8_t *dest, uint8_t val);

void poke_uint16(uint8_t *dest, uint16_t val);

void poke_uint32(uint8_t *dest, uint32_t val);

void poke_uint48(uint8_t *dest, uint64_t val);

void poke_uint64(uint8_t *dest, uint64_t val);

void poke_int8(uint8_t *dest, int8_t val);

#endif /* _PTP_POKE_H */
