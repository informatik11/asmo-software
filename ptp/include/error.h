/**
 * PTP errors.
 */

#ifndef _SIMPLE_PTP_ERROR_H
#define _SIMPLE_PTP_ERROR_H

typedef enum PTP_ERROR_CODE {
  PTP_OK,                       // No error
  PTP_ERROR_PACKET_SIZE,        // Invalid size
  PTP_ERROR_PACKET_TYPE,        // Invalid packet type specified
  PTP_ERROR_OUT_OF_MEMORY,      // Out of memory
  PTP_ERROR_BIND,               // Could not bind to socket
  PTP_ERROR_IGMP_JOIN,          // Could not join multicast group
  PTP_ERROR_TIMEOUT,            // Timeout occurred
  PTP_ERROR_RESET,              // Operation was aborted or reset
  PTP_ERROR_NO_TIMESTAMP,       // No timestamp was received
  PTP_ERROR_SEND,               // Could not send PTP message
  PTP_ERROR_TIMESTAMP_OVERFLOW  // Timestamp overflow
} PTP_ERROR_CODE_T;

#endif /* _SIMPLE_PTP_ERROR_H */
