/**
 * BMC Dataset Comparison Module.
 *
 * This module compares datasets of foreign masters.
 */
#ifndef _PTP_BMC_COMPARE_H
#define _PTP_BMC_COMPARE_H

#include "bmc_common.h"

enum bmc_compare_result {
    BMC_COMPARE_RESULT_ERROR_1,
    BMC_COMPARE_RESULT_ERROR_2,
    BMC_COMPARE_RESULT_A_BETTER_THAN_B,
    BMC_COMPARE_RESULT_B_BETTER_THAN_A,
    BMC_COMPARE_RESULT_A_BETTER_THAN_B_BY_TOPOLOGY,
    BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY
};

/**
 * Compare the given foreign master datasets.
 *
 * @param[in] a the first master
 * @param[in] b the second master
 * @return the result of comparison
 */
enum bmc_compare_result ptp_bmc_compare(struct ptp_foreign_master *a, struct ptp_foreign_master *b);

/**
 * Compare the given foreign master to the local clock (D_0). 
 * The master passed as an argument is denoted "A" and the 
 * local master is "B".
 *
 * @param[in] a the master to compare to the local clock (A)
 * @return the result of comparison
 */
enum bmc_compare_result ptp_bmc_compare_to_local(struct ptp_foreign_master *a);

/**
 * Compare the given foreign master datasets and return the better one.
 *
 * @param[in] a the first master
 * @param[in] b the second master
 * @return pointer to A, if A is better, pointer to B if B is better, 
 *  NULL on error.
 */
struct ptp_foreign_master *ptp_bmc_compare_get_better(struct ptp_foreign_master *a, struct ptp_foreign_master *b);

#endif /* _PTP_BMC_COMPARE_H */
