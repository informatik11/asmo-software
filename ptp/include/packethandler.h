#ifndef _PTP_PACKETHANDLER_H
#define _PTP_PACKETHANDLER_H

#include "parser.h"
#include "engine.h"

int ptp_ph_discard_follow_up(struct ptp_proto_header *arg0, struct ptp_proto_follow_up *arg1, struct ptp_engine_timestamp *arg2);
int ptp_ph_discard_sync(struct ptp_proto_header *arg0, struct ptp_proto_sync *arg1, struct ptp_engine_timestamp *arg2);
int ptp_ph_discard_delay_resp(struct ptp_proto_header *arg0, struct ptp_proto_delay_resp *arg1, struct ptp_engine_timestamp *arg2);
int ptp_ph_discard_announce(struct ptp_proto_header *arg0, struct ptp_proto_announce *arg1, struct ptp_engine_timestamp *arg2);

int ptp_ph_announce(struct ptp_proto_header *hdr, struct ptp_proto_announce *msg, struct ptp_engine_timestamp *ts);

int ptp_ph_announce_reset_receipt_timeout(struct ptp_proto_header *hdr, struct ptp_proto_announce *msg, struct ptp_engine_timestamp *ts);

int ptp_ph_sync(struct ptp_proto_header *hdr, struct ptp_proto_sync *msg, struct ptp_engine_timestamp *ts);

int ptp_ph_follow_up(struct ptp_proto_header *hdr, struct ptp_proto_follow_up *msg, struct ptp_engine_timestamp *ts);

#endif /* _PTP_PACKETHANDLER_H */
