#ifndef _PLATFORM_OS_H
#define _PLATFORM_OS_H

#include <stddef.h>
#include <inttypes.h>
#include "error.h"
#include "platform_os.h"

/**
 * start PTP.
 *
 * @return 0 on success, error from {@link error.h} otherwise.
 */
int ptp_os_start(void);

void ptp_os_printf(const char *fmt, ...);

int ptp_os_snprintf(char *str, size_t size, const char *fmt, ...);

/**
 * Execute the given callback in a context where it is safe to execute
 * PTP code. It is guaranteed that no other PTP code will execute 
 * concurrently.
 *
 * It is implementation-specific whether the callback will execute 
 * synchronously or asynchronously.
 *
 * @param[in] callback The callback to execute. It shall return 0 if
 * it succeeded and nonzero otherwise. 
 * @param[in] opaque Opaque data passed to the callback as first 
 * argument. Do not pass stack-allocated data here, as callbacks
 * may be executed asynchronously.
 * @param is_highprio Whether the message is high priority.
 * @return 0 on success, PTP_ERROR_TIMEOUT on timeout, 
 * PTP_ERROR_RESET when the operation was aborted, 
 * PTP_ERROR_OUT_OF_MEMORY when memory allocation failed.
 */
int ptp_os_post_main(int (*callback)(void *opaque), void *opaque, int is_highprio);

/**
 * Initialize the given timer object.
 *
 * @param tmr the timer object to initialize. It must not have been
 * initialized before.
 */
void ptp_os_init_timer(ptp_os_timer_t *tmr);

/**
 * Execute the given callback delay milliseconds from now.
 *
 * The timer object must have been intialized using the
 * {@link ptp_os_init_timer} function.
 *
 * The callback will be executed in a context where it is safe to 
 * execute PTP code, so using {@link ptp_os_post_main} is not 
 * necessary.
 *
 * The callback will be executed at most once. For a periodic timer,
 * the timer must be re-armed in the handler function.
 *
 * The timer will be reset (stop and start with the new delay value)
 * if it is currently armed.
 *
 * The accuracy of the timer is implementation-specific. Limitations
 * on the values of delay are also implementation-specific.
 *
 * @param[in] tmr Caller-allocated timer structure.
 * @param delay The time in milliseconds from now when the callback 
 * will be called. 
 * @param[in] callback The callback to execute. 
 * @param[in] opaque Opaque data to pass to the callback as first 
 * argument. Do not pass stack-allocated data here.
 * @return 
 */
int ptp_os_set_timer_ms(ptp_os_timer_t *tmr, 
        uint32_t delay,
        int (*callback)(void *opaque), 
        void *opaque);

/**
 * Clear the given timer. This means that the callback will not be 
 * called if the timer was previously set.
 *
 * If the timer is currently not armed, this function will do nothing.
 *
 * The timer must have been initialized first. 
 *
 * @param[in] timer The timer.
 */
int ptp_os_clear_timer(ptp_os_timer_t *tmr);

/**
 * Get the current timestamp in milliseconds.
 *
 * The timestamp will overflow after about 3 years.
 * @return the timestamp
 */
uint32_t ptp_os_get_ts_ms(void);

void ptp_os_mutex_init(ptp_os_mutex_t *mtx);

void ptp_os_mutex_lock(ptp_os_mutex_t *mtx);

void ptp_os_mutex_unlock(ptp_os_mutex_t *mtx);

uint32_t ptp_os_rand(uint32_t *state);

#endif /* _PLATFORM_OS_H */
