/**
 * Platform-dependant stream functions.
 */
#ifndef _PLATFORM_STREAM_H
#define _PLATFORM_STREAM_H

#include "platform_stream.h"

void ptp_stream_printf(ptp_stream_t stream, const char *fmt, ...);

#endif /* _PLATFORM_STREAM_H */
