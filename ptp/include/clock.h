/**
 * The PTP clock.
 *
 * This module abstracts a PTP clock, which can be implemented in 
 * hardware or in software.
 */
#ifndef _PLATFORM_CLOCK_H
#define _PLATFORM_CLOCK_H

#include <stdbool.h>
#include "datatypes.h"

/**
 * Set the clock to the specified timestamp.
 *
 * The given argument must be an absolute timestamp.
 *
 * @param[in] ts absolute timestamp to set the clock to
 * @return 0 on success, error code otherwise
 */
int ptp_clock_set_ts(const struct ptp_timestamp *ts);

/**
 * Update the clock by subtracting the given timeinterval
 * from the current clock value. It is subtracted to make
 * it compatible with the definition of offsetFromMaster
 * in PTP, which is: 
 *   localClock = masterClock + offsetFromMaster
 *
 * @param[in] offsetFromMaster the offset. It is subtracted from the 
 * local clock, i.e. positive values are subtracted and negative
 * values are added.
 * @return 0 on success, error code otherwise.
 */
int ptp_clock_update_ts(const struct ptp_timeinterval *offsetFromMaster);

/**
 * Get the clocks current timestamp.
 *
 * @param[out] ts where to store the timestamp
 */
void ptp_clock_get_ts(struct ptp_timestamp *ts);
#endif /* _PLATFORM_CLOCK_H */
