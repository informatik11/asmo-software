#ifndef _PTP_DATASET_H
#define _PTP_DATASET_H

#include "datatypes.h"
#include "stream.h"

extern struct ptp_ds_default_ds ptp_ds_default;
extern struct ptp_ds_current_ds ptp_ds_current;
extern struct ptp_ds_parent_ds ptp_ds_parent;
extern struct ptp_ds_time_properties_ds ptp_ds_time_properties;
extern struct ptp_ds_port_ds ptp_ds_port;

/**
 * Initialize the datasets with their default values.
 */
void ptp_ds_init(void);

/**
 * Print debug info on the dataset module to the given stream.
 *
 * @param stream the stream to write to.
 */
void ptp_ds_print_debug(ptp_stream_t stream);

struct ptp_ds_default_ds {
    uint8_t twoStepFlag;
    struct ptp_port_identity clockIdentity;
    int numberPorts;
    struct ptp_clock_quality clockQuality;
    uint8_t priority1;
    uint8_t priority2;
    uint8_t domainNumber;
    uint8_t slaveOnly;
};

struct ptp_ds_current_ds {
    uint16_t stepsRemoved;
    struct ptp_timeinterval offsetFromMaster;
    struct ptp_timeinterval meanPathDelay;
};

struct ptp_ds_time_properties_ds {
    int32_t currentUtcOffset;
    uint8_t currentUtcOffsetValid;
    uint8_t leap59;
    uint8_t leap61;
    uint8_t timeTraceable;
    uint8_t frequencyTraceable;
    uint8_t ptpTimescale;
    uint8_t timeSource;
};

struct ptp_ds_parent_ds {
    struct ptp_port_identity parentPortIdentity;
    uint8_t parentStats;
    uint32_t observedParentOffsetScaledLogVariance;
    uint32_t observedParentClockPhaseChangeRate;
    uint8_t grandmasterIdentity[8];
    struct ptp_clock_quality grandmasterClockQuality;
    uint8_t grandmasterPriority1;
    uint8_t grandmasterPriority2;
};

struct ptp_ds_port_ds {
    struct ptp_port_identity portIdentity;
};

#endif /* _PTP_DATASET_H */
