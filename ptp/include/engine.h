/**
 * The PTP engine. This module accepts packets, hands them to the 
 * parser and then passes them to the statemachine, where they
 * are handled.
 */
#ifndef _PTP_ENGINE_H
#define _PTP_ENGINE_H

#include "socket.h"
#include "parser.h"

/**
 * Payload of a PTP message. 
 */
union ptp_engine_payload {
    struct ptp_proto_announce announce;
    struct ptp_proto_sync sync;
    struct ptp_proto_follow_up follow_up;
    struct ptp_proto_delay_resp delay_resp;
};

/** 
 * timestamp of a PTP message.
 */
struct ptp_engine_timestamp {
    uint32_t seconds;
    uint32_t nanoseconds; /**< max. 999 999 ns */
    uint8_t valid; /**< whether the timestamp is valid. */
};

/** 
 * a PTP message. Consists of a header and payload and
 * optionally a timestamp.
 */
struct ptp_engine_message {
    struct ptp_proto_header header;
    struct ptp_engine_timestamp timestamp;
    union ptp_engine_payload payload;
};

/**
 * Parse a PTP packet.
 *
 * @param[in] buf the packet data
 * @param bufsz the length of the packet data
 * @param[in] meta the packet metadata for obtaining the timestamp
 * @param[out] msg where to store the parses message.
 * @return 0 on success, otherwise error.
 */
int ptp_engine_parse(uint8_t *buf, size_t bufsz, ptp_packet_meta_t *meta, struct ptp_engine_message *msg);

/**
 * Handle a PTP message.
 *
 * @param[in] msg the parsed message
 * @param[in] meta the packet metadata
 * @return 0 on success, otherwise error.
 */
int ptp_engine_handle_msg(struct ptp_engine_message *msg);

#endif /* _PTP_ENGINE_H */
