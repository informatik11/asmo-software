/**
 * Packet Generator. This module generates PTP packets for sending over
 * the network. It is the dual operation to the packet parser.
 */

#ifndef _PTP_GENERATOR_H
#define _PTP_GENERATOR_H

#include "proto.h"

/**
 * Generate a protocol header. The provided buffer shall be at 
 * least PTP_PROTO_HEADER_SIZE bytes long.
 *
 * @param[out] generated_packet caller-allocated buffer where the
 * generated packet will be stored.
 * @param generated_packet_size the size of the provided buffer
 * @param[in] header pointer of the struct describing the header
 * to generate.
 * @return 0 if the packet was generated successfully, 
 * PTP_ERROR_PACKET_SIZE if the provided buffer was too small
 * to hold the packet
 */
int ptp_generator_header(uint8_t *generated_packet, size_t generated_packet_size, const struct ptp_proto_header *header);

/**
 * Generate a Delay Req Packet. The provided buffer shall be at least 
 * PTP_PROTO_DELAY_REQ bytes long.
 *
 * @param[out] generated_packet caller-allocated buffer where the
 * generated packet will be stored.
 * @param generated_packet_size the size of the provided buffer
 * @param[in] delay_req pointer to the struct describing the delay req
 * to generate.
 * @return 0 if the packet was generated successfully, 
 * PTP_ERROR_PACKET_SIZE if the provided buffer was too small
 * to hold the packet
 */
int ptp_generator_delay_req(uint8_t *generated_packet, size_t generated_packet_size, const struct ptp_proto_delay_req *delay_req);

#endif /* _PTP_GENERATOR_H */
