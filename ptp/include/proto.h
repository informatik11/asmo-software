/**
 * Protocol Structs.
 *
 * These structs represent PTP messges that are sent or received over
 * the network. They are used by both the parser and the generator.
 */

#ifndef _PTP_PROTO_H
#define _PTP_PROTO_H

#include <inttypes.h>
#include <stddef.h>
#include "datatypes.h"

#define PTP_PROTO_HEADER_SIZE 34
#define PTP_PROTO_HEADER_OFFSET_VERSION_PTP 1
#define PTP_PROTO_HEADER_OFFSET_MESSAGE_LENGTH 2
#define PTP_PROTO_HEADER_OFFSET_DOMAIN_NUMBER 4
#define PTP_PROTO_HEADER_OFFSET_FLAG_FIELD 6
#define PTP_PROTO_HEADER_OFFSET_CORRECTION_FIELD 8
#define PTP_PROTO_HEADER_OFFSET_SOURCE_PORT_IDENTITY 20
#define PTP_PROTO_HEADER_OFFSET_SEQUENCE_ID 30
#define PTP_PROTO_HEADER_OFFSET_CONTROL_FIELD 32
#define PTP_PROTO_HEADER_OFFSET_LOG_MESSAGE_INTERVAL 33

#define PTP_PROTO_TIMESTAMP_SIZE 10
#define PTP_PROTO_TIMESTAMP_OFFSET_NANOSECONDS 6

#define PTP_PROTO_CLOCK_QUALITY_SIZE 4
#define PTP_PROTO_CLOCK_QUALITY_OFFSET_CLOCK_ACCURACY 1
#define PTP_PROTO_CLOCK_QUALITY_OFFSET_OFFSET_SCALED_LOG_VARIANCE 2

#define PTP_PROTO_PORT_IDENTITY_SIZE 10
#define PTP_PROTO_PORT_IDENTITY_OFFSET_PORT_NUMBER 8

#define PTP_PROTO_ANNOUNCE_SIZE 30
#define PTP_PROTO_ANNOUNCE_OFFSET_ORIGIN_TIMESTAMP 34
#define PTP_PROTO_ANNOUNCE_OFFSET_CURRENT_UTC_OFFSET 44
#define PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_PRIORITY1 47
#define PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_CLOCK_QUALITY 48
#define PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_PRIO2 52
#define PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_IDENTITY 53
#define PTP_PROTO_ANNOUNCE_OFFSET_STEPS_REMOVED 61
#define PTP_PROTO_ANNOUNCE_OFFSET_TIME_SOURCE 63

#define PTP_PROTO_SYNC_SIZE 10
#define PTP_PROTO_SYNC_OFFSET_ORIGIN_TIMESTAMP 34

#define PTP_PROTO_DELAY_REQ_SIZE 10
#define PTP_PROTO_DELAY_REQ_OFFSET_ORIGIN_TIMESTAMP 34

#define PTP_PROTO_FOLLOW_UP_SIZE 10
#define PTP_PROTO_FOLLOW_UP_OFFSET_PRECISE_ORIGIN_TIMESTAMP 34

#define PTP_PROTO_DELAY_RESP_SIZE 20
#define PTP_PROTO_DELAY_RESP_OFFSET_RECEIVE_TIMESTAMP 34
#define PTP_PROTO_DELAY_RESP_OFFSET_REQUESTING_PORT_IDENTITY 44

enum ptp_proto_message_type {
    PTP_PROTO_MESSAGE_TYPE_SYNC = 0,
    PTP_PROTO_MESSAGE_TYPE_DELAY_REQ = 0x01,
    PTP_PROTO_MESSAGE_TYPE_ANNOUNCE = 0x0b,
    PTP_PROTO_MESSAGE_TYPE_FOLLOW_UP = 0x08,
    PTP_PROTO_MESSAGE_TYPE_DELAY_RESP = 0x09
};

/**
 * generic header of all PTP messages. 
 */
struct ptp_proto_header {
    /**
     * transport-specific data. only lower nibble is used (values 0 - 15)
     */
    uint8_t transportSpecific; 

    /**
     * type of the message. only lower nibble is used (values 0 - 15).
     */
    enum ptp_proto_message_type messageType;

    /**
     * the PTP version of the sender. only lower nibble is used (values 0 - 15).
     */
    uint8_t versionPTP;

    uint16_t messageLength;

    uint8_t domainNumber;

    uint8_t flagField[2];

    int64_t correctionField;

    struct ptp_port_identity sourcePortIdentity;

    uint16_t sequenceId;

    uint8_t controlField;

    int8_t logMessageInterval;
};

#define PTP_PROTO_GRANDMASTER_IDENTITY_SIZE 8
struct ptp_proto_announce {
    struct ptp_timestamp originTimestamp;

    int16_t currentUtcOffset;

    uint8_t grandmasterPriority1;

    struct ptp_clock_quality grandmasterClockQuality;

    uint8_t grandmasterPriority2;

    uint8_t grandmasterIdentity[PTP_PROTO_GRANDMASTER_IDENTITY_SIZE];

    uint16_t stepsRemoved;

    uint8_t timeSource;
};

struct ptp_proto_sync {
    struct ptp_timestamp originTimestamp;
};

struct ptp_proto_delay_req {
    struct ptp_timestamp originTimestamp;
};

struct ptp_proto_follow_up {
    struct ptp_timestamp preciseOriginTimestamp;
};

struct ptp_proto_delay_resp {
    struct ptp_timestamp receiveTimestamp;
    struct ptp_port_identity requestingPortIdentity;
};

#endif /* _PTP_PROTO_H */
