#ifndef _PLATFORM_SOCKET_H
#define _PLATFORM_SOCKET_H

#include <stddef.h>
#include <inttypes.h>
#include "platform_socket.h"
#include "error.h"

#define PTP_EVENT_PORT 319
#define PTP_GENERAL_PORT 320

/**
 * Initialize the socket module.
 *
 * Must be called before using any of the functions in this module.
 */
void socket_init(void);

/**
 * listen on the PTP UDP general port.
 *
 * callers provide a pointer to a caller-allocated {@link ptp_socket_t}. 
 * This structure is used to identify the socket in subsequent calls to
 * the socket platform API.
 *
 * Module users must not modify or access any of the ptp_socket_t 
 * members.
 *
 * On success, this method returns 0. Otherwise, it returns an error
 * code from {@link error.h}.
 */
PTP_ERROR_CODE_T socket_udp_listen_general(ptp_socket_t *sock);

/**
 * listen on the PTP UDP event port.
 *
 * callers provide a pointer to a caller-allocated {@link ptp_socket_t}. 
 * This structure is used to identify the socket in subsequent calls to
 * the socket platform API.
 *
 * Module users must not modify or access any of the ptp_socket_t 
 * members.
 *
 * On success, this method returns 0. Otherwise, it returns an error
 * code from {@link error.h}.
 */
PTP_ERROR_CODE_T socket_udp_listen_event(ptp_socket_t *sock);

/**
 * receive data from the given socket. This function will block
 * until data has been received.
 *
 * Callers must free the obtained buffers by means of the
 * {@link socket_free} function.
 *
 * Callers must also provide a pointer to a caller-allocated 
 * ptp_packet_meta_t that is used to identify the received 
 * buffer when freeing.
 *
 * @param[in] sock the ptp_socket_t that identifies the socket.
 * @param[out] pointer to a pointer where address of received 
 * data will be stored. 
 * @param[out] meta caller-allocated metadata for identifying the 
 * buffer.
 * @return negative values on error, correspond to a error code
 * from {@link errror.h}, on success the size of the received packet.
 */
int socket_recv(ptp_socket_t *sock, uint8_t **buf, ptp_packet_meta_t *meta);

/**
 * Allocate buffer for sending. Free using {@link socket_free}.
 *
 * @param size the number of bytes to allocate.
 * @param[out] meta caller-allocated metadata for identifying the buffer.
 * @return pointer to allocated buffer or NULL on error.
 */
uint8_t *socket_alloc(size_t size, ptp_packet_meta_t *meta);

/**
 * Send an event message.
 *
 * @param meta the packet to sent, allocated by {@link socket_alloc}
 * @param timestamp_callback a callback to execute when the packet 
 * was sent _and_ the send timestamp is available. Note that it is
 * possible that the callback is not called, for example when capturing
 * the timestamp has failed. The callback can be NULL.
 * @return 0 on success.
 */
int socket_send_event(ptp_packet_meta_t *meta, void (*timestamp_callback)(ptp_socket_ts_t *ts));

/**
 * Free a packet buffer.
 *
 * @param[in] pointer to ptp_packet_meta_t that was obtained from 
 * {@link socket_recv} or {@link socket_alloc}.
 */
void socket_free(ptp_packet_meta_t *meta);

/**
 * Get the receive timestamp of the given packet.
 *
 * To extract the timestamp, use {@link socket_ts_get_seconds},
 * {@link socket_ts_get_nanoseconds} and 
 * {@link socket_ts_is_negative}.
 *
 * If the given packet does not have a timestamp, NULL
 * is returned. This can for example happen if capturing
 * the timestamp failed.
 *
 * @return pointer to a ptp_socket_ts_t or NULL if the
 * given packet does not have a timestamp.
 */
const ptp_socket_ts_t *socket_packet_get_ts(const ptp_packet_meta_t *meta);

/**
 * Check if the given Packet has been timestamped.
 *
 * @param[in] meta pointer to a {@link ptp_packet_meta_t}
 * from a socket call, i.e. {@link sock_recv}.
 * @return true if the packet has a timestamp, false otherwise.
 */
int socket_packet_has_ts(ptp_packet_meta_t *meta);

/**
 * Get the second value of the timestamp.
 *
 * @param[in] meta pointer to {@link ptp_socket_ts_t}
 * @return the second value of the timestamp.
 */
uint32_t socket_ts_get_seconds(const ptp_socket_ts_t *ts);

/**
 * Get the nanosecond value of the timestamp.
 *
 * @param[in] meta pointer to {@link ptp_socket_ts_t}
 * @return the nanosecond value of the timestamp.
 */
uint32_t socket_ts_get_nanoseconds(const ptp_socket_ts_t *ts);

/**
 * Get the sign of the timestamp. 
 *
 * @param[in] meta pointer to {@link ptp_socket_ts_t}
 * @return 0 is the timestamp is nonnegative, nonzero if it is negative.
 */
int socket_ts_is_negative(const ptp_socket_ts_t *ts);

/**
 * Get a MAC address of this device.
 *
 * @param[out] macaddr pointer to 6-byte memory where MAC will
 * be stored.
 * @return 0 if getting the MAC succeeded, nonzero otherwise.
 */
int socket_get_mac(uint8_t macaddr[static 6]);

#endif /*_PLATFORM_SOCKET_H */
