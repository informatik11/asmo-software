/**
 * This module is responsible for measuring the delay to the
 * PTP master.
 *
 * It implements the Delay Request-Response Mechanism.
 *
 * TODO:
 * - timeouts for delay measurement!
 */
#ifndef _PTP_DELAY_H
#define _PTP_DELAY_H
#include "datatypes.h"
#include "engine.h"

/**
 * Initialize the delay module.
 */
void ptp_delay_init(void);

/**
 * Update delay data from a Sync message (and, if applicable, 
 * a Follow_Up message).
 *
 * A delay measurement is initiated by a Sync message (and, if the
 * master is a two-step clock, an associated Follow_Up message).
 *
 * @param correction the correction value to apply to the delay 
 * measurement. In case the master is a two-step clock, this
 * should be the sum of the correction values of the Sync and the
 * Follow_Up message.
 * @param sync_recv_ts The receive timestamp of the Sync message.
 * @param sync_origin_ts In case the master is a two-step clock,
 * this is the preciseOriginTimestamp in the Follow_Up message.
 * Otherwise, it is the originTimestamp in the Sync message.
 */
void ptp_delay_update_sync(
        struct ptp_timeinterval correction,
        struct ptp_timestamp *sync_recv_ts,
        struct ptp_timestamp *sync_origin_ts);

/**
 * Update delay data from a Delay_Resp message.
 *
 * @param[in] header the header of the message
 * @param[in] msg the delay resp message
 */
int ptp_delay_recv_delay_resp(struct ptp_proto_header *header,
        struct ptp_proto_delay_resp *msg,
        struct ptp_engine_timestamp *ts);


#endif /* _PTP_DELAY_H */
