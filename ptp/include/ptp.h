#ifndef _SIMPLE_PTP_PTP_H
#define _SIMPLE_PTP_PTP_H

#include <stddef.h>
#include "platform_stream.h"

/**
 * A PTP timestamp.
 */
struct ptp_timestamp {
    /**
     * seconds. only lower 48 bits are valid. rest should be 0.
     */
    uint64_t secondsField;

    uint32_t nanosecondsField;
};

int ptp_init(void);

/**
 * Get status information about the PTP engine.
 *
 * The status information is written to the given
 * stream in human-readable format.
 *
 * @param stream the stream to write to
 */
void ptp_ui_get_info(ptp_stream_t stream);

/**
 * Disable the PTP engine. 
 */
void ptp_disable(void);

/**
 * Enable the PTP engine. By default, the PTP engine is enabled. It is
 * only neccessary to call this function when it is desired to 
 * re-enable the engine after it has been disabled by 
 * {@link ptp_disable()}.
 */
void ptp_enable(void);

/**
 * Get the current PTP timestamp.
 *
 * @param[out] where to store the timestamp.
 */
void ptp_get_ts(struct ptp_timestamp *dest);

#endif /*_SIMPLE_PTP_PTP_H */
