# Simple PTP

A simple PTP implementation. It is meant to be run on ressource-constrained microcontroller nodes.

## Limitations

* *slave-only*. IEEE 1588 permits the implementation of slave-only clocks[^1]. Thus, for this software to be useful, you need a PTP implementation that is master-capable.
* no support for:
  * Management mechanism
  * Peer delay mechanism
  * a lot more...


## Code Structure

```
+ ptp                  The main source code
+-- include            Public Headers. This specifies the interface which 
|                      users of this library should use.
+-- src                Main Source.
  +-- platform         Headers for platform-specific code. If you want to 
  |                    port to a new platform, provide implementations for 
  |                    these headers.
  +-- platform-chibios Platform-specific code for ChibiOS + LWIP
    +-- include        Platform-specific headers for ChibiOS + LWIP
    +-- include-public Platform-specific public headers for ChibiOS + LWIP
```


[^1]: IEEE 1588. Precision Time Protocol, Chapter 9.2.2.
