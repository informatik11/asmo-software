#include <stdio.h>
#include <inttypes.h>

#include "ptp_debug.h"

void ptp_debug_dump_port_identity(struct ptp_port_identity *id)
{
    printf("[PTP Port Identity]\n");

    printf("clockIdentity = 0x");
    for (size_t i = 0; i < 8; i++) {
        printf("%" PRIx8, id->clockIdentity[i]);
    }
    printf("\n");

    printf("portNumber = %" PRIu16 "\n", id->portNumber);
}

void ptp_debug_dump_header(struct ptp_proto_header *hdr)
{
    printf("[PTP Protocol Header]\n");
    printf("transportSpecific = 0x%" PRIx8 "\n", hdr->transportSpecific);
    printf("messageType = 0x%" PRIx8 "\n", hdr->messageType);
    printf("versionPTP = %" PRIu8 "\n", hdr->versionPTP);
    printf("messageLength = %" PRIu16 "\n", hdr->messageLength);
    printf("domainNumber = %" PRIu8 "\n", hdr->domainNumber);
    printf("flagField = 0x%" PRIx8 " 0x%" PRIx8 "\n", hdr->flagField[0], hdr->flagField[1]);
    printf("correctionField = %" PRId64 "\n", hdr->correctionField);
    printf("sourcePortIdentity = {\n");
    ptp_debug_dump_port_identity(&hdr->sourcePortIdentity);
    printf("}\n");

    printf("sequenceId = %" PRIu16 "\n", hdr->sequenceId);
    printf("controlField = %" PRIu8 "\n", hdr->controlField);
    printf("logMessageInterval = %" PRId8 "\n", hdr->logMessageInterval);
}

void ptp_debug_dump_timestamp(struct ptp_timestamp *ts)
{
    printf("[PTP Timestamp]\n");
    printf("seconds = %" PRIu64 "\n", ts->secondsField);
    printf("nanoseconds = %" PRIu32 "\n", ts->nanosecondsField);
}

void ptp_debug_dump_announce(struct ptp_proto_announce *msg)
{
    printf("[PTP Announce Message]\n");
    printf("originTimestamp = {\n");
    ptp_debug_dump_timestamp(&msg->originTimestamp);
    printf("}\n");
    printf("currentUtcOffset = %" PRId16 "\n", msg->currentUtcOffset);
    printf("grandmasterPriority1 = %" PRIu8 "\n", msg->grandmasterPriority1);
    printf("grandmasterClockQuality = (TODO)\n");
    printf("grandmasterPriority2 = %" PRIu8 "\n", msg->grandmasterPriority2);
    printf("grandmasterIdentity = 0x");
    for (size_t i = 0; i < 8; i++) {
        printf("%" PRIx8, msg->grandmasterIdentity[i]);
    }
    printf("\n");
    printf("stepsRemoved = %" PRIu16 "\n", msg->stepsRemoved);
    printf("timeSource = %" PRIu8 "\n", msg->timeSource);
}

void ptp_debug_dump_sync(struct ptp_proto_sync *msg)
{
    printf("[PTP Sync Message]\n");
    printf("originTimestamp = {\n");
    ptp_debug_dump_timestamp(&msg->originTimestamp);
    printf("}\n");
}

void ptp_debug_dump_follow_up(struct ptp_proto_follow_up *msg)
{
    printf("[PTP Follow Up Message]\n");
    printf("preciseOriginTimestamp = {\n");
    ptp_debug_dump_timestamp(&msg->preciseOriginTimestamp);
    printf("}\n");
}
