#include <string.h>
#include <stdbool.h>
#include "dataset.h"
#include "dataset_update.h"

void ptp_dataset_update_s1(const struct ptp_proto_announce *announce, const struct ptp_proto_header *hdr) 
{
    ptp_ds_current.stepsRemoved = announce->stepsRemoved + 1;

    ptp_ds_parent.parentPortIdentity = hdr->sourcePortIdentity;
    memcpy(&ptp_ds_parent.grandmasterIdentity, &announce->grandmasterIdentity, PTP_PROTO_GRANDMASTER_IDENTITY_SIZE);
    ptp_ds_parent.grandmasterClockQuality = announce->grandmasterClockQuality;
    ptp_ds_parent.grandmasterPriority1 = announce->grandmasterPriority1;
    ptp_ds_parent.grandmasterPriority2 = announce->grandmasterPriority2;

    ptp_ds_time_properties.currentUtcOffset = announce->currentUtcOffset;
    ptp_ds_time_properties.currentUtcOffsetValid = ptp_parser_header_has_flag(hdr, PTP_FLAG_CURRENT_UTC_OFFSET_VALID);
    ptp_ds_time_properties.leap59 = ptp_parser_header_has_flag(hdr, PTP_FLAG_LEAP59);
    ptp_ds_time_properties.leap61 = ptp_parser_header_has_flag(hdr, PTP_FLAG_LEAP61);
    ptp_ds_time_properties.timeTraceable = ptp_parser_header_has_flag(hdr, PTP_FLAG_TIME_TRACEABLE);
    ptp_ds_time_properties.frequencyTraceable = ptp_parser_header_has_flag(hdr, PTP_FLAG_FREQUENCY_TRACEABLE);
    ptp_ds_time_properties.ptpTimescale = ptp_parser_header_has_flag(hdr, PTP_FLAG_PTP_TIMESCALE);
    ptp_ds_time_properties.timeSource = announce->timeSource;
}

void ptp_dataset_update_m1(void)
{
    ptp_ds_current.stepsRemoved = 0;
    ptp_ds_current.offsetFromMaster.scaledNanoseconds = 0;
    ptp_ds_current.meanPathDelay.scaledNanoseconds = 0;

    memcpy(&ptp_ds_parent.parentPortIdentity.clockIdentity, &ptp_ds_default.clockIdentity, PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE);
    ptp_ds_parent.parentPortIdentity.portNumber = 0;
    ptp_ds_parent.grandmasterClockQuality = ptp_ds_default.clockQuality;
    ptp_ds_parent.grandmasterPriority1 = ptp_ds_default.priority1;
    ptp_ds_parent.grandmasterPriority2 = ptp_ds_default.priority2;
    
    ptp_ds_time_properties.leap59 = false;
    ptp_ds_time_properties.leap61 = false;
    ptp_ds_time_properties.currentUtcOffset = 27;
    ptp_ds_time_properties.ptpTimescale = true;
    ptp_ds_time_properties.timeTraceable = false;
    ptp_ds_time_properties.frequencyTraceable = false;
    ptp_ds_time_properties.timeSource = 0xA0; /* INTERNAL_OSCILLATOR */
}
