#include <inttypes.h>
#include <stdbool.h>
#include "socket.h"
#include "dataset.h"
#include "config.h"
#include "datatypes.h"

struct ptp_ds_default_ds ptp_ds_default;
struct ptp_ds_current_ds ptp_ds_current;
struct ptp_ds_parent_ds ptp_ds_parent;
struct ptp_ds_time_properties_ds ptp_ds_time_properties;
struct ptp_ds_port_ds ptp_ds_port;

static void init_local_clock_identity(uint8_t *clockIdentity)
{
    uint8_t mac[6];
    socket_get_mac(mac);

    clockIdentity[0] = mac[0];
    clockIdentity[1] = mac[1];
    clockIdentity[2] = mac[2];
    clockIdentity[3] = 0xFF;
    clockIdentity[4] = 0xFE;
    clockIdentity[5] = mac[3];
    clockIdentity[6] = mac[4];
    clockIdentity[7] = mac[5];
}

static void init_local_clock_quality(struct ptp_clock_quality *qual)
{
    /* we are a slave-only clock */
    qual->clockClass = 255;
    qual->clockAccuracy = 248;
    qual->offsetScaledLogVariance = 0xFFFF;
}

static void init_local_port_identity(struct ptp_port_identity *id)
{
    init_local_clock_identity(id->clockIdentity);
    id->portNumber = 0;
}

static void init_default_ds(void)
{
    ptp_ds_default.twoStepFlag = false;
    ptp_ds_default.domainNumber = 0;
    init_local_port_identity(&ptp_ds_default.clockIdentity);
    ptp_ds_default.numberPorts = 1;
    init_local_clock_quality(&ptp_ds_default.clockQuality);
    ptp_ds_default.priority1 = 128;
    ptp_ds_default.priority2 = 128;
    ptp_ds_default.slaveOnly = true;
}

static void init_current_ds(void)
{
    ptp_ds_current.stepsRemoved = 0;
    ptp_ds_current.offsetFromMaster.scaledNanoseconds = 0;
    ptp_ds_current.meanPathDelay.scaledNanoseconds = 0;
}

static void init_parent_ds(void)
{
    init_local_port_identity(&ptp_ds_parent.parentPortIdentity);
    ptp_ds_parent.parentStats = false;
    ptp_ds_parent.observedParentOffsetScaledLogVariance = 0xFFFF;
    ptp_ds_parent.observedParentClockPhaseChangeRate = 0x7FFFFFFF;
    init_local_clock_identity(ptp_ds_parent.grandmasterIdentity);

    init_local_clock_quality(&ptp_ds_parent.grandmasterClockQuality);
    ptp_ds_parent.grandmasterPriority1 = 0xFF;
    ptp_ds_parent.grandmasterPriority2 = 0xFF;
}

static void init_time_properties_ds(void)
{
    ptp_ds_time_properties.currentUtcOffset = 27; /* as of 2018-08-22 */
    ptp_ds_time_properties.currentUtcOffsetValid = false;
    ptp_ds_time_properties.leap59 = false;
    ptp_ds_time_properties.leap61 = false;
    ptp_ds_time_properties.timeTraceable = false;
    ptp_ds_time_properties.frequencyTraceable = false;
    ptp_ds_time_properties.ptpTimescale = false;
    ptp_ds_time_properties.timeSource = 0xA0; /* INTERNAL_OSCILATOR */
}

static void init_port_ds(void)
{
    init_local_port_identity(&ptp_ds_port.portIdentity);
}

void ptp_ds_init(void)
{
    init_default_ds();
    init_current_ds();
    init_parent_ds();
    init_time_properties_ds();
    init_port_ds();
}

void ptp_ds_print_debug(ptp_stream_t stream)
{
    ptp_stream_printf(stream, "parentDS:\n");
    ptp_stream_printf(
        stream, 
        "    grandmasterIdentity = %02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x\n",
        ptp_ds_parent.grandmasterIdentity[0],
        ptp_ds_parent.grandmasterIdentity[1],
        ptp_ds_parent.grandmasterIdentity[2],
        ptp_ds_parent.grandmasterIdentity[3],
        ptp_ds_parent.grandmasterIdentity[4],
        ptp_ds_parent.grandmasterIdentity[5],
        ptp_ds_parent.grandmasterIdentity[6],
        ptp_ds_parent.grandmasterIdentity[7]
    );

    ptp_stream_printf(stream, "currentDS:\n");
    // TODO: ptp_stream_printf cannot handle 64-bit integers
    // and outputs wrong values for these.
    ptp_stream_printf(
        stream, 
        "    meanPathDelay = %U ns\n",
        ptp_ds_current.meanPathDelay.scaledNanoseconds >> 16
    );
    ptp_stream_printf(
        stream, 
        "    offsetFromMaster = %U ns\n",
        ptp_ds_current.offsetFromMaster.scaledNanoseconds >> 16
    );
}
