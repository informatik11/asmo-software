/**
 * Operating-System specific functionality. 
 *
 * The general idea of this implementation is as follows:
 * We have three threads, two listen on the PTP general and
 * event socket. They receive data from the socket, parse 
 * the packets and then put the parsed packets ("messages")
 * into a mailbox. The third thread ("ptp_main") picks up
 * messages from this mailbox and handles them.
 *
 * Messages are stored in a guarded Memory Pool. 
 */

#include "os.h"
#include "clock.h"
#include "socket.h"
#include "parser.h"
#include "engine.h"
#include "platform_main_thd.h"
#include "error.h"
#include "SmartECLA_allHeaders.h"
#include <ch.h>
#include <hal.h>
#include <chprintf.h>
#include <memstreams.h>

#define PTP_GENERAL_WA_SIZE 800
thread_t *ptp_general_thd;
THD_WORKING_AREA(ptp_general_wa, PTP_GENERAL_WA_SIZE);

#define PTP_EVENT_WA_SIZE 800
thread_t *ptp_event_thd;
THD_WORKING_AREA(ptp_event_wa, PTP_EVENT_WA_SIZE);

/* memory pool. General and Event thread place parsed PTP
 * messages into this pool. */
GUARDEDMEMORYPOOL_DECL(msg_pool, sizeof(struct ptp_engine_message), 0);
#define PTP_MSG_POOL_SIZE 3
struct ptp_engine_message __msg_pool_block[PTP_MSG_POOL_SIZE];

/* timeout waiting for a slot in the memory pool in milliseconds */
#define PTP_MSG_POOL_TIMEOUT 500

void ptp_os_printf(const char *fmt, ...) {

  va_list ap;
  va_start(ap, fmt);
  (void) asmoSerialPutParametrized(fmt, ap);
  va_end(ap);
}

static int handle_msg(void *opaque) {
  struct ptp_engine_message *msg;
  int r;

  msg = (struct ptp_engine_message*)opaque;

  r = ptp_engine_handle_msg(msg);

  chGuardedPoolFree(&msg_pool, (void*)msg);

  return r;
}

static int parse_and_post_to_main_thread(uint8_t *buf, size_t bufsz,
                                         ptp_packet_meta_t *meta,
                                         int is_highprio) {
  struct ptp_engine_message *msg;
  int ret, r;

  /* buffer is free'd by handler function */
  msg = (struct ptp_engine_message*)chGuardedPoolAllocTimeout(
      &msg_pool, TIME_MS2I(PTP_MSG_POOL_TIMEOUT));

  if (!msg) {
    ret = PTP_ERROR_OUT_OF_MEMORY;
    return ret;
  }

  r = ptp_engine_parse(buf, bufsz, meta, msg);
  if (r) {
    ret = r;
    /* only in error case. otherwise free'd by handle_msg() */
    chGuardedPoolFree(&msg_pool, (void*)msg);
    return ret;
  }

  r = ptp_os_post_main(&handle_msg, msg, is_highprio);
  if (r) {
    ret = r;
    /* only in error case. otherwise free'd by handle_msg() */
    chGuardedPoolFree(&msg_pool, (void*)msg);
    return ret;
  }

  return 0;
}

static void recv_and_parse(ptp_socket_t *sock, int is_highprio) {
  ptp_packet_meta_t meta;
  uint8_t *buf;
  int bufsz, r;

  while (true) {
    bufsz = socket_recv(sock, &buf, &meta);
    if (bufsz < 0) {
      ptp_os_printf("ptp: error receiving data: %d\n", bufsz);
      // TODO: handle errors better.
      continue;
    }

    r = parse_and_post_to_main_thread(buf, bufsz, &meta, is_highprio);
    if (r) {
      ptp_os_printf("ptp: error %d receiving packet.\n", r);
      /* no continue here, we have to free the socket data */
    }

    socket_free(&meta);
  }
}

/* this thread receives data from the general socket
 * and then posts them to the mailbox for processing 
 * by {@link ptp_thd}. */
static void general_thd(void *p) {
  (void) p;
  ptp_socket_t general_socket;
  PTP_ERROR_CODE_T r;

  chRegSetThreadName("ptp_general");

  r = socket_udp_listen_general(&general_socket);
  if (r) {
    ptp_os_printf("ptp: error listening: %d\n", r);
    chThdExit(r);
    return;
  }

  recv_and_parse(&general_socket, false);
}

/* this thread receives data from the event socket
 * and then posts them to the mailbox for processing 
 * by {@link ptp_thd}. */
static void event_thd(void *p) {
  (void)p;
  ptp_socket_t event_socket;
  PTP_ERROR_CODE_T r;

  chRegSetThreadName("ptp_event");

  r = socket_udp_listen_event(&event_socket);
  if (r) {
    ptp_os_printf("ptp: error listening on event socket: %d\n", r);
    chThdExit(r);
    return;
  }

  // TODO: only handle Sync messages with high prio
  recv_and_parse(&event_socket, true);
}

int ptp_os_start() {
  ptp_os_printf("ptp: os start\n");
  /* add slots to the main_thd_mempool pool */
  for (size_t i = 0; i < PTP_MSG_POOL_SIZE; i++) {
    chGuardedPoolAdd(&msg_pool, &__msg_pool_block[i]);
  }

  macPtpStart(&ETHD1);

  ptp_os_ch_startmain();
  ptp_general_thd = chThdCreateStatic(&ptp_general_wa, PTP_GENERAL_WA_SIZE,
                                      NORMALPRIO, &general_thd, NULL);
  ptp_event_thd = chThdCreateStatic(&ptp_event_wa, PTP_EVENT_WA_SIZE,
                                    NORMALPRIO, &event_thd, NULL);
  ptp_os_printf("ptp: thread started\n");

  return 0;
}

void ptp_os_mutex_init(ptp_os_mutex_t *mtx) {
  chMtxObjectInit(mtx);
}

void ptp_os_mutex_lock(ptp_os_mutex_t *mtx) {
  chMtxLock(mtx);
}

void ptp_os_mutex_unlock(ptp_os_mutex_t *mtx) {
  chMtxUnlock(mtx);
}

/* copied from ChibiOS' os/hal/lib/streams/chprintf.c 
 because there is no vsnprintf... :/  */
int ptp_os_snprintf(char *str, size_t size, const char *fmt, ...) {
  va_list ap;
  MemoryStream ms;
  BaseSequentialStream *chp;
  size_t size_wo_nul;
  int retval;

  if (size > 0)
    size_wo_nul = size - 1;
  else
    size_wo_nul = 0;

  /* Memory stream object to be used as a string writer, reserving one
   byte for the final zero.*/
  msObjectInit(&ms, (uint8_t*)str, size_wo_nul, 0);

  /* Performing the print operation using the common code.*/
  chp = (BaseSequentialStream*)(void*)&ms;
  va_start(ap, fmt);
  retval = chvprintf(chp, fmt, ap);
  va_end(ap);

  /* Terminate with a zero, unless size==0.*/
  if (ms.eos < size)
    str[ms.eos] = 0;

  /* Return number of bytes that would have been written.*/
  return retval;
}

/* this implements an Xorshift RNG from Marsaglia, George: Xorshift RNGs. */
uint32_t ptp_os_rand(uint32_t *state) {
  uint32_t r = *state;
  r ^= (r << 13);
  // on p. 4 of the paper, the ^ operator is missing.
  // p.3 however suggests it should be there.
  // code example from wikipedia also agrees on this:
  // <https://en.wikipedia.org/wiki/Xorshift>
  r ^= (r >> 17);
  r ^= (r << 5);
  *state = r;

  return r;
}

uint32_t ptp_os_get_ts_ms(void) {
  return TIME_MS2I(chVTGetSystemTime());
}
