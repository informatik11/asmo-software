#include "socket.h"
#include "platform_main_thd.h"
#include "error.h"
#include <string.h>
#include <lwip/api.h>
#include <lwip/netif.h>

struct socket_timestamp_callback {
    void (*callback)(ptp_socket_ts_t *ts);
    struct lwip_ts ts;
};

/* memory pool where callback and timestamp are stored for posting to
 * the main thread. */
#define SOCKET_TS_CALLBACK_MEMPOOL_SIZE 2
MEMORYPOOL_DECL(ts_callback_mempool, sizeof (struct socket_timestamp_callback), 0, NULL);
struct socket_timestamp_callback __ts_callback_mempool_block[SOCKET_TS_CALLBACK_MEMPOOL_SIZE];

void socket_init(void)
{
    for (size_t i = 0; i < SOCKET_TS_CALLBACK_MEMPOOL_SIZE; i++) {
        chPoolFree(&ts_callback_mempool, &__ts_callback_mempool_block[i]);
    }
}

static PTP_ERROR_CODE_T udp_listen_mc(uint16_t port, ip_addr_t *addr, ptp_socket_t *sock)
{
    PTP_ERROR_CODE_T ret = PTP_OK;
    err_t err;
    struct netconn *netconn;

	netconn = netconn_new(NETCONN_UDP);
    if (netconn == NULL) {
        ret = PTP_ERROR_OUT_OF_MEMORY;
        return ret;
    }

    err = netconn_bind(netconn, IP_ADDR_ANY, port);
    if (err != ERR_OK) {
        ret = PTP_ERROR_BIND;
        netconn_delete(netconn);
        return ret;
    }

    err = netconn_join_leave_group(netconn, addr, IP_ADDR_ANY, NETCONN_JOIN);
    if (err != ERR_OK) {
        ret = PTP_ERROR_IGMP_JOIN;
        netconn_delete(netconn);
        return ret;
    }

    sock->__netconn = netconn;

    return ret;
}

enum PTP_ERROR_CODE socket_udp_listen_general(ptp_socket_t *sock)
{
    ip_addr_t addr;
    IP4_ADDR(&addr, 224, 0, 1, 129);
    return udp_listen_mc(320, &addr, sock);
}

enum PTP_ERROR_CODE socket_udp_listen_event(ptp_socket_t *sock)
{
    ip_addr_t addr;
    IP4_ADDR(&addr, 224, 0, 1, 129);
    return udp_listen_mc(319, &addr, sock);
}

static int __timestamp_callback_main_thd(void *opaque)
{
    struct socket_timestamp_callback *cb_data = (struct socket_timestamp_callback *)opaque;
    ptp_socket_ts_t *ts = &cb_data->ts;

    cb_data->callback(ts);

    chPoolFree(&ts_callback_mempool, cb_data);

    return 0;
}

static void __timestamp_callback(struct lwip_ts *ts, void *opaque)
{
    struct socket_timestamp_callback *cb_data;
    int r;

    chSysLockFromISR();
    cb_data = chPoolAllocI(&ts_callback_mempool);
    if (cb_data == NULL) {
      chSysUnlockFromISR();
      return;
    }

    cb_data->callback = (void (*)(ptp_socket_ts_t *))opaque;
    /* copy the timestamp as main thd callback is executed async */
    cb_data->ts = *ts;

    r = _ptp_os_post_main_isr(__timestamp_callback_main_thd, cb_data, false);
    if (r) {
        /* only in case of error, otherwise free'd by main thd callback */
        chPoolFree(&ts_callback_mempool, cb_data);
    }

    chSysUnlockFromISR();
}

int socket_send_event(ptp_packet_meta_t *meta, void (*timestamp_callback)(ptp_socket_ts_t *ts))
{
    struct netconn *conn;
    struct netbuf *netbuf = meta->__netbuf;
    err_t res;
    int r = 0;
    ip_addr_t dest;
    IP4_ADDR(&dest, 224, 0, 1, 129); //TODO(MH) Warum ist das Hardcoded? Hier muss der Empfaenger dynamisch bestimmt werden (abhaengig von BTC).

    conn = netconn_new(NETCONN_UDP);
    if (conn == NULL) {
        r = PTP_ERROR_OUT_OF_MEMORY;
        return r;
    }

    if (timestamp_callback) {
        netbuf_set_ts_callback(netbuf, &__timestamp_callback, (void*) timestamp_callback);
    }

    res = netconn_sendto(conn, netbuf, &dest, 319);
    if (res != ERR_OK) {
        r = PTP_ERROR_SEND;
        netconn_delete(conn);
        return r;
    }

    r = 0;

    netconn_delete(conn);
    return r;

}

int socket_recv(ptp_socket_t *sock, uint8_t **buf, ptp_packet_meta_t *meta)
{
    err_t r;
    int ret;
    u16_t bufsz;

    r = netconn_recv(sock->__netconn, &meta->__netbuf);
    if (r != ERR_OK) {
        ret = -1;
        return ret;
    }

    r = netbuf_data(meta->__netbuf, (void **) buf, &bufsz);
    if (r != ERR_OK) {
        ret = -1;
        netbuf_delete(meta->__netbuf);
        return ret;
    }

    ret = bufsz;

    return ret;
}

inline const ptp_socket_ts_t *socket_packet_get_ts(const ptp_packet_meta_t *meta)
{
    return netbuf_get_ts(meta->__netbuf);
}

inline int socket_packet_has_ts(ptp_packet_meta_t *meta)
{
    return (NULL != netbuf_get_ts(meta->__netbuf));
}

inline uint32_t socket_ts_get_seconds(const ptp_socket_ts_t *ts)
{
    return ts->seconds;
}

inline uint32_t socket_ts_get_nanoseconds(const ptp_socket_ts_t *ts)
{
    /* mask out most significant bit, it is the sign */
    return ts->nanoseconds & ~(1 << 31);
}

inline int socket_ts_is_negative(const ptp_socket_ts_t *ts)
{
    /* most significant bit is the sign */
    return ts->nanoseconds & (1 << 31);
}

uint8_t *socket_alloc(size_t size, ptp_packet_meta_t *meta)
{
    struct netbuf *netbuf;
    uint8_t *buf = NULL;

    netbuf = netbuf_new();
    if (netbuf == NULL) {
        buf = NULL;
        return buf;
    }

    buf = (uint8_t *) netbuf_alloc(netbuf, size);
    if (!buf) {
        netbuf_delete(netbuf);
        return buf;
    }
    meta->__netbuf = netbuf;

    return buf;
}

void socket_free(ptp_packet_meta_t *meta)
{
    netbuf_delete(meta->__netbuf);
}

int socket_get_mac(uint8_t macaddr[static 6])
{
    for (struct netif* n = netif_list; n != NULL; n = n->next) {
        if (n->hwaddr_len >= 6 && n->hwaddr) {
            memcpy(macaddr, n->hwaddr, 6);
            return 0;
        }
    }

    return 1;
}
