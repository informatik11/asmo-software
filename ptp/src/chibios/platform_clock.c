#include "error.h"
#include <hal.h>
#include <stdlib.h>

#include "clock.h"
#include "datatypes.h"

int ptp_clock_set_ts(const struct ptp_timestamp *ts)
{
    struct mac_timestamp macTs;

    /* secondsField is 64-bit */
    if (ts->secondsField > UINT32_MAX) {
        return PTP_ERROR_TIMESTAMP_OVERFLOW;
    }

    if (ts->nanosecondsField > 999999999) {
        return PTP_ERROR_TIMESTAMP_OVERFLOW;
    }

    macTs.seconds = ts->secondsField;
    macTs.nanoseconds = ts->nanosecondsField;

    return macSetTimestamp(&macTs);

}

int ptp_clock_update_ts(const struct ptp_timeinterval *offsetFromMaster)
{
    int64_t offsetNanoseconds = offsetFromMaster->scaledNanoseconds >> 16;
    struct mac_timestamp macTs;

    // TODO: overflow/underflow should be handled properly.
    uint64_t absoluteNs;
    int32_t nanoseconds;
    uint32_t seconds;
    absoluteNs = llabs(offsetNanoseconds);
    seconds = absoluteNs / PTP_NS_PER_S;
    nanoseconds = absoluteNs % PTP_NS_PER_S;

    /* MAC driver expects sign in nanoseconds field.
     * We subtract positive values and add negative values */
    if (offsetNanoseconds > 0) {
        nanoseconds *= -1;
    }

    macTs.seconds = seconds;
    macTs.nanoseconds = nanoseconds;

    return macUpdateTimestamp(&macTs);
}

void ptp_clock_get_ts(struct ptp_timestamp *ts)
{
    struct mac_timestamp macTs;
    macGetTimestamp(&macTs);
    ts->secondsField = macTs.seconds;
    ts->nanosecondsField = macTs.nanoseconds;
}
