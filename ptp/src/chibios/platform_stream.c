#include "stream.h"
#include <ch.h>
#include <hal.h>
#include <chprintf.h>
#include <memstreams.h>

void ptp_stream_printf(ptp_stream_t stream, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	chvprintf(stream, fmt, ap);
	va_end(ap);
}
