#ifndef _PTP_STATE_FAULTY_H
#define _PTP_STATE_FAULTY_H

#include "packethandler.h"
#include "statemachine.h"
#include "states.h"

static enum ptp_state event_triggered(enum ptp_event event, void *event_opaque) {
  (void)event_opaque;

  switch (event) {
  case PTP_EVENT_INITIALIZE:
    return PTP_STATE_INITIALIZING;
  case PTP_EVENT_FAULT_DETECTED:
    return PTP_STATE_FAULTY;
  case PTP_EVENT_FAULT_CLEARED:
    return PTP_STATE_INITIALIZING;
  case PTP_EVENT_DESIGNATED_DISABLED:
    return PTP_STATE_DISABLED;
  default:
    return PTP_STATE_FAULTY;
  }
}

static void state_enter(enum ptp_state prev_state, enum ptp_event event,
                        void *event_opaque) {
  (void)prev_state;
  (void)event;
  (void)event_opaque;

  ptp_state_disarm_announce_receipt_timeout();
}

const struct ptp_state_hooks PTP_STATE_FAULTY_HOOKS = {
  .state_enter = &state_enter,
  .event_triggered = &event_triggered,
  .recv_announce = &ptp_ph_discard_announce,
  .recv_sync = &ptp_ph_discard_sync,
  .recv_follow_up = &ptp_ph_discard_follow_up,
  .recv_delay_resp = &ptp_ph_discard_delay_resp
};

#endif /* _PTP_STATE_FAULTY_H */
