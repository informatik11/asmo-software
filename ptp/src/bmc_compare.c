#include <string.h>
#include <stdbool.h>
#include "bmc_compare.h"
#include "dataset.h"
#include "datatypes.h"

static enum bmc_compare_result bmc_compare_part1(struct ptp_foreign_master *a, struct ptp_foreign_master *b)
{
    struct ptp_proto_announce *announce_a = &a->lastAnnounceMsg;
    struct ptp_proto_announce *announce_b = &b->lastAnnounceMsg;

    int res;

    struct ptp_clock_quality *qual_a = &announce_a->grandmasterClockQuality;
    struct ptp_clock_quality *qual_b = &announce_b->grandmasterClockQuality;

    /* priority1 */
    if (announce_a->grandmasterPriority1 > announce_b->grandmasterPriority1) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }

    if (announce_a->grandmasterPriority1 < announce_b->grandmasterPriority1) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    /* GM class */
    if (qual_a->clockClass > qual_b->clockClass) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }

    if (qual_a->clockClass < qual_b->clockClass) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    /* GM accuracy */
    if (qual_a->clockAccuracy > qual_b->clockAccuracy) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }

    if (qual_a->clockAccuracy < qual_b->clockAccuracy) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    /* GM offsetScaledLogVariance */
    if (qual_a->offsetScaledLogVariance > qual_b->offsetScaledLogVariance) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }

    if (qual_a->offsetScaledLogVariance < qual_b->offsetScaledLogVariance) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    /* GM priority 2 */
    if (announce_a->grandmasterPriority2 > announce_b->grandmasterPriority2) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }

    if (announce_a->grandmasterPriority2 < announce_b->grandmasterPriority2) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    res = memcmp(announce_a->grandmasterIdentity, announce_b->grandmasterIdentity, PTP_PROTO_GRANDMASTER_IDENTITY_SIZE);

    if (res > 0) { /* A > B */
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    } else {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }
}

static enum bmc_compare_result __bmc_compare_part2(struct ptp_foreign_master *a, struct ptp_foreign_master *b, struct ptp_port_identity *identity_of_receiver)
{

    struct ptp_proto_announce *announce_a = &a->lastAnnounceMsg;
    struct ptp_proto_announce *announce_b = &b->lastAnnounceMsg;
    struct ptp_proto_header *hdr_a = &a->lastAnnounceHeader;
    struct ptp_proto_header *hdr_b = &b->lastAnnounceHeader;
    int res;

    if (announce_a->stepsRemoved > (announce_b->stepsRemoved + 1)) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }
    if ((announce_a->stepsRemoved + 1) < announce_b->stepsRemoved) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    if (announce_a->stepsRemoved > announce_b->stepsRemoved) {
        res = memcmp(&identity_of_receiver->clockIdentity, &a->lastAnnounceHeader.sourcePortIdentity, PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE);

        if (res < 0) { /* receiver < sender */
            return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
        } else if (res > 0) { /* receiver > sender */
            return BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY;
        }

        if (identity_of_receiver->portNumber < hdr_a->sourcePortIdentity.portNumber) {
            return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
        } else if (identity_of_receiver->portNumber > a->lastAnnounceHeader.sourcePortIdentity.portNumber) {
            return BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY;
        }

        return BMC_COMPARE_RESULT_ERROR_1;
    } else if (announce_a->stepsRemoved < announce_b->stepsRemoved) {
        res = memcmp(&identity_of_receiver->clockIdentity, &b->lastAnnounceHeader.sourcePortIdentity.clockIdentity, PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE);

        if (res < 0) { /* receiver < sender */
            return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
        } else if (res > 0) { /* receiver > sender */
            return BMC_COMPARE_RESULT_A_BETTER_THAN_B_BY_TOPOLOGY;
        }

        if (identity_of_receiver->portNumber < hdr_a->sourcePortIdentity.portNumber) {
            return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
        } else if (identity_of_receiver->portNumber > hdr_a->sourcePortIdentity.portNumber) {
            return BMC_COMPARE_RESULT_A_BETTER_THAN_B_BY_TOPOLOGY;
        }

        return BMC_COMPARE_RESULT_ERROR_1;

    }

    res = memcmp(&hdr_a->sourcePortIdentity, &hdr_b->sourcePortIdentity, PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE);
    if (res < 0) { /* A < B */
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B_BY_TOPOLOGY;
    } else if (res > 0) { /* A > B */
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY;
    }

    /* we support only a single port. thus no need to compare the
     * identities of the receiving ports */
    return BMC_COMPARE_RESULT_ERROR_2;
}

static enum bmc_compare_result bmc_compare_part2(struct ptp_foreign_master *a, struct ptp_foreign_master *b)
{
    return __bmc_compare_part2(a, b, &ptp_ds_port.portIdentity);
}

enum bmc_compare_result ptp_bmc_compare(struct ptp_foreign_master *a, struct ptp_foreign_master *b)
{

    struct ptp_proto_announce *announce_a = &a->lastAnnounceMsg;
    struct ptp_proto_announce *announce_b = &b->lastAnnounceMsg;

    // TODO: better return code!
    if (a == NULL && b == NULL) {
        return BMC_COMPARE_RESULT_ERROR_1;
    }

    if (a == NULL && b != NULL) {
        return BMC_COMPARE_RESULT_B_BETTER_THAN_A;
    }

    if (b == NULL && a != NULL) {
        return BMC_COMPARE_RESULT_A_BETTER_THAN_B;
    }

    if (!memcmp(&announce_a->grandmasterIdentity, &announce_b->grandmasterIdentity, PTP_PROTO_GRANDMASTER_IDENTITY_SIZE)) {
        /* data set comparison, part 2 */
        return bmc_compare_part2(a, b);
    }

    return bmc_compare_part1(a, b);

}

enum bmc_compare_result ptp_bmc_compare_to_local(struct ptp_foreign_master *a)
{

    struct ptp_foreign_master local = {
        .lastAnnounceMsg = {
            .currentUtcOffset = ptp_ds_time_properties.currentUtcOffset,
            .grandmasterPriority1 = ptp_ds_parent.grandmasterPriority1,
            .grandmasterPriority2 = ptp_ds_parent.grandmasterPriority2,
            .stepsRemoved = ptp_ds_current.stepsRemoved,
            .timeSource = ptp_ds_time_properties.timeSource,
            .grandmasterClockQuality = ptp_ds_parent.grandmasterClockQuality
        },
        .lastAnnounceHeader = {
            .sourcePortIdentity = ptp_ds_port.portIdentity,
            .domainNumber = ptp_ds_default.domainNumber,
        },
        .valid = true
    };

    memcpy(&local.lastAnnounceMsg.grandmasterIdentity, &ptp_ds_parent.grandmasterIdentity, PTP_PROTO_GRANDMASTER_IDENTITY_SIZE);

    return ptp_bmc_compare(a, &local);
}

struct ptp_foreign_master *ptp_bmc_compare_get_better(struct ptp_foreign_master *a, struct ptp_foreign_master *b)
{
    enum bmc_compare_result res = ptp_bmc_compare(a, b);

    switch (res) {
        case BMC_COMPARE_RESULT_A_BETTER_THAN_B:
        case BMC_COMPARE_RESULT_A_BETTER_THAN_B_BY_TOPOLOGY:
            return a;
        case BMC_COMPARE_RESULT_B_BETTER_THAN_A:
        case BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY:
            return b;
        /* no default clause, so compiler can warn us if we missed
           an enum value (GCC will) */
        case BMC_COMPARE_RESULT_ERROR_1:
        case BMC_COMPARE_RESULT_ERROR_2:
            return NULL;
    }

    return NULL;
}
