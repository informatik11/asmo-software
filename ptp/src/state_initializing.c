#ifndef _PTP_STATE_INITIALIZING_H
#define _PTP_STATE_INITIALIZING_H

#include "packethandler.h"
#include "statemachine.h"
#include "states.h"
#include "delay.h"
#include "dataset.h"

static enum ptp_state event_triggered(enum ptp_event event, void *event_opaque) {
  (void)event_opaque;

  switch (event) {
  case PTP_EVENT_INITIALIZE:
    return PTP_STATE_INITIALIZING;
  case PTP_EVENT_INITIALIZED:
    return PTP_STATE_LISTENING;
  default:
    return PTP_STATE_INITIALIZING;
  }
}

static void state_enter(enum ptp_state prev_state, enum ptp_event event,
                        void *event_opaque) {
  (void)prev_state;
  (void)event;
  (void)event_opaque;

  ptp_ds_init();
  ptp_state_disarm_announce_receipt_timeout();

  ptp_state_triggerevt(PTP_EVENT_INITIALIZED);
}

const struct ptp_state_hooks PTP_STATE_INITIALIZING_HOOKS = {
    .state_enter = &state_enter, .event_triggered = &event_triggered,
    .recv_announce = &ptp_ph_discard_announce, .recv_sync = &ptp_ph_discard_sync,
    .recv_follow_up = &ptp_ph_discard_follow_up, .recv_delay_resp = &ptp_ph_discard_delay_resp};

#endif /* _PTP_STATE_INITIALIZING_H */
