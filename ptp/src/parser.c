#include <stdint.h>
#include <stdio.h>
#ifdef __unix__
#include <endian.h>
#else
#include <machine/endian.h>
#endif /* __unix__ */
#include <inttypes.h>
#include <string.h>

#include "error.h"

#include "peek.h"
#include "parser.h"
#include "datatypes.h"

#define HI_NIBBLE(b) (((b) >> 4) & 0x0F)
#define LO_NIBBLE(b) ((b) & 0x0F)

static inline int ptp_parser_timestamp(const uint8_t *timestamp_packet,
                                       const size_t size, /* size of timestamp packet */
                                       struct ptp_timestamp *timestamp_parsed) {
  if (size < PTP_PROTO_TIMESTAMP_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  const uint8_t *nanosecond_byte = timestamp_packet
      + PTP_PROTO_TIMESTAMP_OFFSET_NANOSECONDS;
  uint64_t seconds = peek_uint48(timestamp_packet);
  uint32_t nanoseconds = peek_uint32(nanosecond_byte);

  timestamp_parsed->secondsField = seconds;
  timestamp_parsed->nanosecondsField = nanoseconds;

  return 0;
}

static inline int ptp_parser_clock_quality(const uint8_t *packet,
                                           const size_t size, /* size of clock quality packet */
                                           struct ptp_clock_quality *parsed) {
  if (size < PTP_PROTO_CLOCK_QUALITY_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  parsed->clockClass = peek_uint8(packet);
  parsed->clockAccuracy = peek_uint8(
      packet + PTP_PROTO_CLOCK_QUALITY_OFFSET_CLOCK_ACCURACY);
  parsed->offsetScaledLogVariance = peek_uint16(
      packet + PTP_PROTO_CLOCK_QUALITY_OFFSET_OFFSET_SCALED_LOG_VARIANCE);

  return 0;
}

static inline int ptp_parser_port_identity(
    const uint8_t *port_identity_packet, const size_t size, /* size of port identity packet */
    struct ptp_port_identity *port_identity_parsed) {
  if (size < PTP_PROTO_PORT_IDENTITY_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  /* array of bytes, can be copied as-is */
  memcpy(&port_identity_parsed->clockIdentity, port_identity_packet, 8);
  port_identity_parsed->portNumber = peek_uint16(
      port_identity_packet + PTP_PROTO_PORT_IDENTITY_OFFSET_PORT_NUMBER);

  return 0;
}

int ptp_parser_header(const uint8_t *packet, const size_t packet_size,
                      struct ptp_proto_header *parsed_header) {
  if (packet_size < PTP_PROTO_HEADER_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  int r;

  /* transportSpecific and messageType fields share a byte */
  uint8_t transportSpecificAndMessageType = peek_uint8(packet);
  parsed_header->transportSpecific = HI_NIBBLE(transportSpecificAndMessageType);
  parsed_header->messageType = LO_NIBBLE(transportSpecificAndMessageType);

  /* high nibble in versionPTP is reserved */
  uint8_t versionPTP = peek_uint8(packet + PTP_PROTO_HEADER_OFFSET_VERSION_PTP);
  parsed_header->versionPTP = LO_NIBBLE(versionPTP);

  uint16_t message_length = peek_uint16(
      packet + PTP_PROTO_HEADER_OFFSET_MESSAGE_LENGTH);
  if (message_length != packet_size) {
    return -PTP_ERROR_PACKET_SIZE;
  }
  parsed_header->messageLength = message_length;

  parsed_header->domainNumber = peek_uint8(
      packet + PTP_PROTO_HEADER_OFFSET_DOMAIN_NUMBER);

  const uint8_t *flag_field = packet + PTP_PROTO_HEADER_OFFSET_FLAG_FIELD;
  parsed_header->flagField[0] = peek_uint8(flag_field);
  parsed_header->flagField[1] = peek_uint8(flag_field + 1);

  parsed_header->correctionField = peek_int64(
      packet + PTP_PROTO_HEADER_OFFSET_CORRECTION_FIELD);

  const uint8_t *source_port_id = packet
      + PTP_PROTO_HEADER_OFFSET_SOURCE_PORT_IDENTITY;
  const size_t source_port_id_len = packet_size
      - PTP_PROTO_HEADER_OFFSET_SOURCE_PORT_IDENTITY;
  r = ptp_parser_port_identity(source_port_id, source_port_id_len,
                               &parsed_header->sourcePortIdentity);
  if (r) {
    return r;
  }

  parsed_header->sequenceId = peek_uint16(
      packet + PTP_PROTO_HEADER_OFFSET_SEQUENCE_ID);
  parsed_header->controlField = peek_uint8(
      packet + PTP_PROTO_HEADER_OFFSET_CONTROL_FIELD);
  parsed_header->logMessageInterval = peek_int8(
      packet + PTP_PROTO_HEADER_OFFSET_LOG_MESSAGE_INTERVAL);

  return 0;
}

int ptp_parser_announce(const uint8_t *packet, const size_t packet_size,
                        const struct ptp_proto_header *parsed_header,
                        struct ptp_proto_announce *parsed) {
  if (packet_size < PTP_PROTO_HEADER_SIZE + PTP_PROTO_ANNOUNCE_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  // TODO check messageLength in header

  if (parsed_header->messageType != PTP_PROTO_MESSAGE_TYPE_ANNOUNCE) {
    return -PTP_ERROR_PACKET_TYPE;
  }

  int r;

  const size_t timestamp_size = packet_size
      - PTP_PROTO_ANNOUNCE_OFFSET_ORIGIN_TIMESTAMP;
  const uint8_t *timestamp_packet = packet
      + PTP_PROTO_ANNOUNCE_OFFSET_ORIGIN_TIMESTAMP;
  r = ptp_parser_timestamp(timestamp_packet, timestamp_size,
                           &parsed->originTimestamp);
  if (r) {
    return r;
  }

  parsed->currentUtcOffset = peek_int16(
      packet + PTP_PROTO_ANNOUNCE_OFFSET_CURRENT_UTC_OFFSET);
  parsed->grandmasterPriority1 = peek_uint8(
      packet + PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_PRIORITY1);

  const uint8_t *quality_packet = packet
      + PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_CLOCK_QUALITY;
  const size_t quality_size = packet_size
      - PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_CLOCK_QUALITY;
  r = ptp_parser_clock_quality(quality_packet, quality_size,
                               &parsed->grandmasterClockQuality);
  if (r) {
    return r;
  }
  parsed->grandmasterPriority2 = peek_uint8(
      packet + PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_PRIO2);

  /* 8-byte array, can be copied as-is */
  memcpy(&parsed->grandmasterIdentity,
         packet + PTP_PROTO_ANNOUNCE_OFFSET_GRANDMASTER_IDENTITY, 8);

  parsed->stepsRemoved = peek_uint16(
      packet + PTP_PROTO_ANNOUNCE_OFFSET_STEPS_REMOVED);
  parsed->timeSource = peek_uint8(
      packet + PTP_PROTO_ANNOUNCE_OFFSET_TIME_SOURCE);

  return 0;
}

int ptp_parser_sync(const uint8_t *packet, const size_t packet_size,
                    const struct ptp_proto_header *parsed_header,
                    struct ptp_proto_sync *parsed) {
  (void)parsed_header;
  if (packet_size < PTP_PROTO_SYNC_SIZE + PTP_PROTO_HEADER_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  struct ptp_timestamp *ts = &parsed->originTimestamp;
  const uint8_t *tsPacket = packet + PTP_PROTO_SYNC_OFFSET_ORIGIN_TIMESTAMP;
  return ptp_parser_timestamp(tsPacket, packet_size - PTP_PROTO_HEADER_SIZE, ts);

}

int ptp_parser_follow_up(const uint8_t *packet, const size_t packet_size,
                         const struct ptp_proto_header *parsed_header,
                         struct ptp_proto_follow_up *parsed) {
  (void)parsed_header;

  if (packet_size < PTP_PROTO_FOLLOW_UP_SIZE + PTP_PROTO_HEADER_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  struct ptp_timestamp *ts = &parsed->preciseOriginTimestamp;
  const uint8_t *tsPacket = packet
      + PTP_PROTO_FOLLOW_UP_OFFSET_PRECISE_ORIGIN_TIMESTAMP;
  return ptp_parser_timestamp(tsPacket, packet_size - PTP_PROTO_HEADER_SIZE, ts);

}

int ptp_parser_delay_resp(const uint8_t *packet, const size_t packet_size,
                          const struct ptp_proto_header *parsed_header,
                          struct ptp_proto_delay_resp *parsed) {
  (void)parsed_header;

  int r;

  if (packet_size < PTP_PROTO_DELAY_RESP_SIZE + PTP_PROTO_HEADER_SIZE) {
    return -PTP_ERROR_PACKET_SIZE;
  }

  struct ptp_timestamp *ts = &parsed->receiveTimestamp;
  const uint8_t *tsPacket = packet
      + PTP_PROTO_DELAY_RESP_OFFSET_RECEIVE_TIMESTAMP;
  r = ptp_parser_timestamp(tsPacket, packet_size - PTP_PROTO_HEADER_SIZE, ts);
  if (r) {
    return r;
  }

  struct ptp_port_identity *id = &parsed->requestingPortIdentity;
  const uint8_t *idPacket = packet
      + PTP_PROTO_DELAY_RESP_OFFSET_REQUESTING_PORT_IDENTITY;
  r = ptp_parser_port_identity(idPacket, packet_size - PTP_PROTO_HEADER_SIZE,
                               id);
  if (r) {
    return r;
  }

  return 0;
}

int ptp_parser_header_has_flag(const struct ptp_proto_header *hdr, uint8_t flag) {
  uint8_t octet = flag & 0xF0;
  uint8_t mask = 1 << (flag & 0x0F);

  /* only two flag octets available */
  if (octet > 1) {
    return 0;
  }

  return hdr->flagField[octet] & mask;
}
