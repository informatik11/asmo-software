#include "engine.h"
#include "parser.h"
#include "dataset.h"
#include "statemachine.h"
#include "os.h"

#include <string.h>

int ptp_engine_parse(uint8_t *buf, size_t bufsz, ptp_packet_meta_t *meta, struct ptp_engine_message *msg)
{
    int r = 0;
    struct ptp_proto_header *hdr;

    memset(msg, 0, sizeof (struct ptp_engine_message));

    r = ptp_parser_header(buf, bufsz, &msg->header);

    if (r) {
        return r;
    }

    hdr = &msg->header;

    switch(hdr->messageType) {
        case PTP_PROTO_MESSAGE_TYPE_ANNOUNCE:
            r = ptp_parser_announce(buf, bufsz, hdr, &msg->payload.announce);
            break;
        case PTP_PROTO_MESSAGE_TYPE_FOLLOW_UP:
            r = ptp_parser_follow_up(buf, bufsz, hdr, &msg->payload.follow_up);
            break;
        case PTP_PROTO_MESSAGE_TYPE_SYNC:
            r = ptp_parser_sync(buf, bufsz, hdr, &msg->payload.sync);
            break;
        case PTP_PROTO_MESSAGE_TYPE_DELAY_RESP:
            r = ptp_parser_delay_resp(buf, bufsz, hdr, &msg->payload.delay_resp);
            break;
        default:
          break;
    }

    /* we ignore the timestamp if it is negative */
    const ptp_socket_ts_t *ts = socket_packet_get_ts(meta);
    // TODO: what is the best way to handle negative tiemstamps?
    if (socket_packet_has_ts(meta) && !socket_ts_is_negative(ts)) {
        msg->timestamp.valid = 1;
        msg->timestamp.seconds = socket_ts_get_seconds(ts);
        msg->timestamp.nanoseconds = socket_ts_get_nanoseconds(ts);
    }

    return r;
}

int ptp_engine_handle_msg(struct ptp_engine_message *msg)
{
    if (msg->header.domainNumber != ptp_ds_default.domainNumber) {
        return 0;
    }

    return ptp_state_recv(msg);
}
