#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include "config.h"
#include "stream.h"
#include "os.h"
#include "bmc.h"
#include "bmc_common.h"
#include "bmc_compare.h"
#include "statemachine.h"
#include "dataset.h"
#include "dataset_update.h"

/* standard requires at least 5 (9.3.2.4.5) */
#define PTP_BMC_MAX_FOREIGN_MASTER_COUNT 5 

static struct ptp_foreign_master __foreign_master[PTP_BMC_MAX_FOREIGN_MASTER_COUNT];

/**
 * Find a foreign master wtih the given {@link struct ptp_port_identity}.
 *
 * If none could be found, NULL is returned.
 *
 * @param[in] id the port identity to search for.
 * @return NULL if no master could be found, a pointer to the 
 * {@link struct ptp_ds_foreign_master_ds} otherwise.
 */
static struct ptp_foreign_master* ptp_ds_foreign_master_find(
    const struct ptp_port_identity *id) {
  struct ptp_foreign_master *e;
  struct ptp_port_identity *e_id;

  for (size_t i = 0; i < PTP_BMC_MAX_FOREIGN_MASTER_COUNT; i++) {
    e = &__foreign_master[i];
    e_id = &e->lastAnnounceHeader.sourcePortIdentity;
    if (e->valid && ptp_dt_port_identity_equal(id, e_id)) {
      return e;
    }
  }

  return NULL;
}

static int is_qualified(const struct ptp_foreign_master *master) {
  // see 9.3.2.5
  // a) cannot happen, as we are a slave-only implementation
  //    and thus will never send out announce messages.

  // b) also not of concern, we only keep around the most
  //    recent messages.

  if (master->announceMessageCount < PTP_CONF_FOREIGN_MASTER_THRESHOLD) {
    return false;
  }

  if (master->lastAnnounceMsg.stepsRemoved >= 255) {
    return false;
  }

  return true;
}

/**
 * Add a new foreign master to the dataset.
 *
 * @param new_foreign_master data of the new foreign master
 * @return 0 on success, 1 if there was no space to create a new entry.
 */
static int ptp_ds_foreign_master_new(const struct ptp_proto_header *hdr,
                                     const struct ptp_proto_announce *msg) {
  struct ptp_foreign_master *e, *found = NULL;

  for (size_t i = 0; i < PTP_BMC_MAX_FOREIGN_MASTER_COUNT; i++) {
    e = &__foreign_master[i];
    if (!e->valid) {
      found = e;
      break;
    }
  }

  /* no space found */
  if (found == NULL) {
    return 1;
  }

  memset(found, 0, sizeof(struct ptp_foreign_master));

  found->lastAnnounceMsg = *msg;
  found->lastAnnounceHeader = *hdr;
  found->announceMessageCount = 0;
  found->valid = true;

  return 0;
}

void ptp_bmc_add_foreign_master(struct ptp_proto_announce *msg,
                                struct ptp_proto_header *hdr) {
  struct ptp_foreign_master *foreignMaster = ptp_ds_foreign_master_find(
      &hdr->sourcePortIdentity);

  // we already have a foreignMasterDS for this master
  if (foreignMaster) {
    foreignMaster->announceMessageCount++;
    foreignMaster->lastAnnounceMsg = *msg; /* copy */
  }
  // create foreignMasterDS for this master if possible
  else {
    int r;
    r = ptp_ds_foreign_master_new(hdr, msg);

    // no more capacity, drop the message
    if (r) {
      return;
    }
  }
}

void ptp_bmc_clear_foreign_masters(void) {
  for (size_t i = 0; i < PTP_BMC_MAX_FOREIGN_MASTER_COUNT; i++) {
    __foreign_master[i].valid = 0;
  }
}

static struct ptp_foreign_master* get_best_foreign_master(void) {
  /* build a array of all valid foreign masters.
   * this avoids having to check each entry for validity every time */
  struct ptp_foreign_master *valid_masters[PTP_BMC_MAX_FOREIGN_MASTER_COUNT];
  size_t foreign_master_count = 0;

  for (size_t i = 0; i < PTP_BMC_MAX_FOREIGN_MASTER_COUNT; i++) {
    struct ptp_foreign_master *e = &__foreign_master[i];
    if (e->valid) {
      valid_masters[foreign_master_count] = e;
      foreign_master_count++;
    }
  }

  if (!foreign_master_count) {
    return NULL;
  }

  if (foreign_master_count == 1) {
    return valid_masters[0];
  }

  struct ptp_foreign_master *max = valid_masters[0];
  for (size_t i = 1; i < foreign_master_count - 1; i++) {
    if (!is_qualified(valid_masters[i])) {
      continue;
    }

    max = ptp_bmc_compare_get_better(max, valid_masters[i]);
    /* error */
    if (max == NULL) {
      return NULL;
    }
  }

  return max;
}

/* clean up foreign masters: remove all masters with >1 announce msgs,
 * except the given best.
 * it is expected that the given best pointer points to an element of
 * __foreign_master so this function just has to compare pointers. 
 * best is also permitted to be NULL. */
static void cleanup_foreign_masters(struct ptp_foreign_master *best) {
  for (size_t i = 0; i < PTP_BMC_MAX_FOREIGN_MASTER_COUNT; i++) {
    struct ptp_foreign_master *e = &__foreign_master[i];
    if (e->valid && e->announceMessageCount > 1 && best != e) {
      e->valid = 0;
    }
  }
}

static void ds_update_m1(void) {
  ptp_dataset_update_m1();

  ptp_state_triggerevt(PTP_EVENT_BMC_MASTER);
}

static void ds_update_m2(void) {
  ds_update_m1();
}

static void ds_update_m3(void) {
  ptp_state_triggerevt(PTP_EVENT_BMC_MASTER);
}

static void ds_update_p1(struct ptp_foreign_master *best_master) {
  (void)best_master;
  ptp_state_triggerevt(PTP_EVENT_BMC_PASSIVE);
}

static void ds_update_p2(void) {
  ptp_state_triggerevt(PTP_EVENT_BMC_PASSIVE);
}

static void ds_update_s1(struct ptp_foreign_master *best_master) {
  struct ptp_proto_announce *announce = &best_master->lastAnnounceMsg;
  struct ptp_proto_header *hdr = &best_master->lastAnnounceHeader;

  /* we have to do this comparison before the dataset update, because
   * the update updates the ptp_ds_parent.parentPortIdentity. */
  /* TODO: when old_master == new_master? should we compare grandmasterIdentity? or port identity? */
  bool same_master = ptp_dt_port_identity_equal(
      &hdr->sourcePortIdentity, &ptp_ds_parent.parentPortIdentity);

  ptp_dataset_update_s1(announce, hdr);

  if (same_master) {
    ptp_state_triggerevt(PTP_EVENT_BMC_SLAVE_SAME_MASTER);
  }
  else {
    ptp_state_triggerevt(PTP_EVENT_BMC_SLAVE_NEW_MASTER);
  }

}

/* best_master may be NULL */
static void state_decision(struct ptp_foreign_master *best_master) {
  /* as we only have a single port, E_best = E_rbest (the best master
   * of this system is the best master on the single port we have).
   *
   * Also, we are a slave-only clock.
   */
  uint8_t my_clock_class = ptp_ds_default.clockQuality.clockClass;
  enum bmc_compare_result res;

  if (best_master == NULL && PTP_STATE_LISTENING == ptp_state_get()) {
    /* remain in LISTENING state */
    return;
  }

  res = ptp_bmc_compare_to_local(best_master);
  if (my_clock_class >= 1 && my_clock_class <= 127) {
    /* local clock is better */
    if (res == BMC_COMPARE_RESULT_B_BETTER_THAN_A
        || res == BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY) {
      /* BMC_MASTER - M1 */
      ds_update_m1();
    }
    else {
      /* BMC_PASSIVE - P1 */
      ds_update_p1(best_master);
    }
  }
  else {
    /* local clock is better */
    if (res == BMC_COMPARE_RESULT_B_BETTER_THAN_A
        || res == BMC_COMPARE_RESULT_B_BETTER_THAN_A_BY_TOPOLOGY) {
      /* BMC_MASTER - M2 */
      ds_update_m2();
    }
    else {
      /* E_best was always received on port r, as we only have
       a single port */
      ds_update_s1(best_master);
    }
  }

}

void ptp_bmc_execute(void) {
  /* may be NULL */
  struct ptp_foreign_master *best_master = get_best_foreign_master();

  state_decision(best_master);

  cleanup_foreign_masters(best_master);
}

void ptp_bmc_print_debug(ptp_stream_t stream) {
  // TODO: this code may not run in the main thread, thus
  // when a concurrent write is in progress, it might print
  // incorrect information
  struct ptp_foreign_master *e;

  ptp_stream_printf(stream, "Discovered PTP masters:\n");
  for (size_t i = 0; i < PTP_BMC_MAX_FOREIGN_MASTER_COUNT; i++) {
    e = &__foreign_master[i];
    if (e->valid) {
      ptp_stream_printf(
          stream,
          " * portIdentity = %02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x:%u (announceCount=%u)\n",
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[0],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[1],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[2],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[3],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[4],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[5],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[6],
          e->lastAnnounceHeader.sourcePortIdentity.clockIdentity[7],
          e->lastAnnounceHeader.sourcePortIdentity.portNumber,
          e->announceMessageCount);
    }
  }
}
