#ifndef _PTP_STATE_SLAVE_H
#define _PTP_STATE_SLAVE_H

#include "packethandler.h"
#include "statemachine.h"
#include "states.h"
#include "delay.h"
#include "bmc.h"

static void state_enter(enum ptp_state prev_state, enum ptp_event event,
                        void *event_opaque) {
  (void)prev_state;
  (void)event;
  (void)event_opaque;
  ptp_state_arm_announce_receipt_timeout();
}

static enum ptp_state event_triggered(enum ptp_event event, void *event_opaque) {
  (void)event_opaque;

  switch (event) {
  case PTP_EVENT_INITIALIZE:
    return PTP_STATE_INITIALIZING;
  case PTP_EVENT_STATE_DECISION_EVENT:
    ptp_bmc_execute();
    /* ptp_bmc_execute() initiates transition if needed */
    return PTP_STATE_NO_TRANSITION;
  case PTP_EVENT_ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES:
  case PTP_EVENT_BMC_MASTER:
  case PTP_EVENT_BMC_PASSIVE:
    return PTP_STATE_LISTENING;
  case PTP_EVENT_FAULT_DETECTED:
    return PTP_STATE_FAULTY;
  case PTP_EVENT_BMC_SLAVE_NEW_MASTER:
    return PTP_STATE_UNCALIBRATED;
  case PTP_EVENT_BMC_SLAVE_SAME_MASTER:
    return PTP_STATE_NO_TRANSITION;
  case PTP_EVENT_DESIGNATED_DISABLED:
    return PTP_STATE_DISABLED;
  case PTP_EVENT_SYNCHRONIZATION_FAULT:
    return PTP_STATE_UNCALIBRATED;
  default:
    return PTP_STATE_SLAVE;
  }
}

const struct ptp_state_hooks PTP_STATE_SLAVE_HOOKS = {
    .state_enter = &state_enter, .event_triggered = &event_triggered,
    .recv_announce = &ptp_ph_announce_reset_receipt_timeout, .recv_sync =
        &ptp_ph_sync,
    .recv_follow_up = &ptp_ph_follow_up, .recv_delay_resp =
        &ptp_delay_recv_delay_resp};

#endif /* _PTP_STATE_SLAVE_H */

