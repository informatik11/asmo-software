#include "statemachine.h"
#include "states.h"
#include "config.h"
#include "os.h"
#include "socket.h"

#define PRINT_ALL_TRANSITIONS 0

extern struct ptp_state_hooks PTP_STATE_INITIALIZING_HOOKS;
extern struct ptp_state_hooks PTP_STATE_LISTENING_HOOKS;
extern struct ptp_state_hooks PTP_STATE_DISABLED_HOOKS;
extern struct ptp_state_hooks PTP_STATE_FAULTY_HOOKS;
extern struct ptp_state_hooks PTP_STATE_UNCALIBRATED_HOOKS;
extern struct ptp_state_hooks PTP_STATE_SLAVE_HOOKS;

static enum ptp_state _state = __PTP_STATE_INVALID;
static const struct ptp_state_hooks *_state_hooks = NULL;

ptp_os_timer_t __state_decision_timer;

ptp_os_timer_t __announce_receipt_timer;

uint32_t __rand_seed;

static int __trigger_state_decision(void*);

static void __state_decision_timer_arm(void) {
  ptp_os_set_timer_ms(&__state_decision_timer, PTP_ANNOUNCE_INTERVAL * 1000,
                      &__trigger_state_decision, NULL);
}

static int __trigger_announce_receipt_timeout(void *opaque) {
  (void)opaque;
  ptp_state_triggerevt(PTP_EVENT_ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES);

  return 0;
}

static int __trigger_state_decision(void *opaque) {
  (void)opaque;
  if (PTP_STATE_INITIALIZING != ptp_state_get()) {
    ptp_state_triggerevt(PTP_EVENT_STATE_DECISION_EVENT);
  }

  __state_decision_timer_arm();

  return 0;
}

static const struct ptp_state_hooks* state_get_hook(enum ptp_state state) {
  switch (state) {
  case PTP_STATE_INITIALIZING:
    return &PTP_STATE_INITIALIZING_HOOKS;
    break;
  case PTP_STATE_LISTENING:
    return &PTP_STATE_LISTENING_HOOKS;
    break;
  case PTP_STATE_DISABLED:
    return &PTP_STATE_DISABLED_HOOKS;
    break;
  case PTP_STATE_FAULTY:
    return &PTP_STATE_FAULTY_HOOKS;
    break;
  case PTP_STATE_UNCALIBRATED:
    return &PTP_STATE_UNCALIBRATED_HOOKS;
    break;
  case PTP_STATE_SLAVE:
    return &PTP_STATE_SLAVE_HOOKS;
    break;
  default:
    return &PTP_STATE_FAULTY_HOOKS;
  }
}

static int state_exists(enum ptp_state s) {
  /* The following values have special meaning and thus do not
   * represent states:
   *  - __PTP_STATE_INVALID
   *  - PTP_STATE_NO_TRANSITION
   */
  return (s == PTP_STATE_INITIALIZING) || (s == PTP_STATE_LISTENING)
      || (s == PTP_STATE_DISABLED) || (s == PTP_STATE_FAULTY)
      || (s == PTP_STATE_UNCALIBRATED) || (s == PTP_STATE_SLAVE);
}

static int event_exists(enum ptp_event e) {
  return (e == PTP_EVENT_INITIALIZE) || (e == PTP_EVENT_INITIALIZED)
      || (e == PTP_EVENT_DESIGNATED_ENABLED)
      || (e == PTP_EVENT_DESIGNATED_DISABLED) || (e == PTP_EVENT_FAULT_CLEARED)
      || (e == PTP_EVENT_FAULT_DETECTED)
      || (e == PTP_EVENT_STATE_DECISION_EVENT)
      || (e == PTP_EVENT_ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES)
      || (e == PTP_EVENT_SYNCHRONIZATION_FAULT)
      || (e == PTP_EVENT_MASTER_CLOCK_SELECTED) || (e == PTP_EVENT_BMC_MASTER)
      || (e == PTP_EVENT_BMC_PASSIVE) || (e == PTP_EVENT_BMC_SLAVE_NEW_MASTER)
      || (e == PTP_EVENT_BMC_SLAVE_SAME_MASTER);
}

void ptp_state_arm_announce_receipt_timeout(void) {
  uint32_t rand, delay;

  rand = (ptp_os_rand(&__rand_seed) % 999) + 1; // rand in (0, 1000)
  delay = 1000 * PTP_CONF_ANNOUNCE_RECEIPT_TIMEOUT * PTP_ANNOUNCE_INTERVAL
      + rand;

  ptp_os_set_timer_ms(&__announce_receipt_timer, delay,
                      &__trigger_announce_receipt_timeout, NULL);
}

void ptp_state_disarm_announce_receipt_timeout(void) {
  ptp_os_clear_timer(&__announce_receipt_timer);
}

const char* ptp_state_to_str(enum ptp_state state) {
  static const char *state_names[] = {"<unknown>", /* 0 */
                                      "initializing", /* 1 */
                                      "listening", /* 2 */
                                      "disabled", /* 3 */
                                      "faulty", /* 4 */
                                      "uncalibrated", /* 5 */
                                      "slave" /* 6 */
  };

  const char *ret;
  switch (state) {
  case PTP_STATE_INITIALIZING:
    ret = state_names[1];
    break;
  case PTP_STATE_LISTENING:
    ret = state_names[2];
    break;
  case PTP_STATE_DISABLED:
    ret = state_names[3];
    break;
  case PTP_STATE_FAULTY:
    ret = state_names[4];
    break;
  case PTP_STATE_UNCALIBRATED:
    ret = state_names[5];
    break;
  case PTP_STATE_SLAVE:
    ret = state_names[6];
    break;
  default:
    ret = state_names[0];
  };

  return ret;
}

static const char* ptp_event_to_str(enum ptp_event evt) {
  static const char *event_names[] = {"<unknown>", "initialize", /* 1 */
                                      "initialized", /* 2 */
                                      "designated_enabled", /* 3 */
                                      "designated_disabled", /* 4 */
                                      "fault_cleared", /* 5 */
                                      "fault_detected", /* 6 */
                                      "state_decision_event", /* 7 */
                                      "announce_receipt_timeout_expires", /* 8 */
                                      "synchronization_fault", /* 9 */
                                      "master_clock_selected", /* 10 */
                                      "bmc_master", /* 11 */
                                      "bmc_passive", /* 12 */
                                      "bmc_slave_new_master", /* 13 */
                                      "bmc_slave_same_master" /* 14 */
  };
  const char *ret;

  switch (evt) {
  case PTP_EVENT_INITIALIZE:
    ret = event_names[1];
    break;
  case PTP_EVENT_INITIALIZED:
    ret = event_names[2];
    break;
  case PTP_EVENT_DESIGNATED_ENABLED:
    ret = event_names[3];
    break;
  case PTP_EVENT_DESIGNATED_DISABLED:
    ret = event_names[4];
    break;
  case PTP_EVENT_FAULT_CLEARED:
    ret = event_names[5];
    break;
  case PTP_EVENT_FAULT_DETECTED:
    ret = event_names[6];
    break;
  case PTP_EVENT_STATE_DECISION_EVENT:
    ret = event_names[7];
    break;
  case PTP_EVENT_ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES:
    ret = event_names[8];
    break;
  case PTP_EVENT_SYNCHRONIZATION_FAULT:
    ret = event_names[9];
    break;
  case PTP_EVENT_MASTER_CLOCK_SELECTED:
    ret = event_names[10];
    break;
  case PTP_EVENT_BMC_MASTER:
    ret = event_names[11];
    break;
  case PTP_EVENT_BMC_PASSIVE:
    ret = event_names[12];
    break;
  case PTP_EVENT_BMC_SLAVE_NEW_MASTER:
    ret = event_names[13];
    break;
  case PTP_EVENT_BMC_SLAVE_SAME_MASTER:
    ret = event_names[14];
    break;
  default:
    ret = event_names[0];
  }

  return ret;
}

static void state_change(enum ptp_state new_state, enum ptp_event event) {
  enum ptp_state old_state = _state;

  _state = new_state;
  _state_hooks = state_get_hook(new_state);

  if (old_state != new_state || PRINT_ALL_TRANSITIONS) {
    ptp_os_printf("entering state %s (%d) old_state=%s (%d) event=%s (%d)\n",
                  ptp_state_to_str(new_state), new_state,
                  ptp_state_to_str(old_state), old_state,
                  ptp_event_to_str(event), event);
  }

  if (!_state_hooks || !_state_hooks->state_enter) {
    ptp_os_printf("state: state hook invalid or state_enter not defined\n");
    return;
  }

  _state_hooks->state_enter(old_state, event, NULL);
}

inline void ptp_state_init(void) {
  /* MAC used to initialize seed */
  uint8_t macaddr[6];
  socket_get_mac(macaddr);

  __rand_seed = macaddr[5] | (macaddr[4] << 8) | (macaddr[3] << 16)
      | (macaddr[2] << 24);
  __rand_seed += ptp_os_get_ts_ms();

  ptp_os_init_timer(&__state_decision_timer);
  ptp_os_init_timer(&__announce_receipt_timer);
  __state_decision_timer_arm();

  state_change(PTP_STATE_INITIALIZING, PTP_EVENT_INITIALIZE);
}

inline enum ptp_state ptp_state_get() {
  return _state;
}

void ptp_state_triggerevt(enum ptp_event evt) {
  enum ptp_state new_state;

  if (!event_exists(evt)) {
    ptp_os_printf("state: nonexistant event %d\n", evt);
    return;
  }

  if (!_state_hooks || !_state_hooks->event_triggered) {
    ptp_os_printf("state: state hook invalid or event_triggered not defined\n");
    return;
  }

  new_state = _state_hooks->event_triggered(evt, NULL);

  if (new_state != PTP_STATE_NO_TRANSITION) {
    state_change(new_state, evt);
  }
}

int ptp_state_recv(struct ptp_engine_message *msg) {
  int r = 0;

  struct ptp_proto_header *hdr = &msg->header;
  union ptp_engine_payload *payload = &msg->payload;
  struct ptp_engine_timestamp *ts = &msg->timestamp;

  if (!_state_hooks || !_state_hooks->recv_announce || !_state_hooks->recv_sync
      || !_state_hooks->recv_follow_up) {
    return -1;
  }

  switch (hdr->messageType) {
  case PTP_PROTO_MESSAGE_TYPE_ANNOUNCE:
    r = _state_hooks->recv_announce(hdr, &payload->announce, ts);
    break;
  case PTP_PROTO_MESSAGE_TYPE_FOLLOW_UP:
    r = _state_hooks->recv_follow_up(hdr, &payload->follow_up, ts);
    break;
  case PTP_PROTO_MESSAGE_TYPE_SYNC:
    r = _state_hooks->recv_sync(hdr, &payload->sync, ts);
    break;
  case PTP_PROTO_MESSAGE_TYPE_DELAY_RESP:
    r = _state_hooks->recv_delay_resp(hdr, &payload->delay_resp, ts);
    break;
  case PTP_PROTO_MESSAGE_TYPE_DELAY_REQ:
    /* this message type exists purely for sending, but
     * not for receiving. This case exists to avoid
     * a compiler warning about an unhandled enum value. */
    r = 0;
    break;
  }

  return r;
}
