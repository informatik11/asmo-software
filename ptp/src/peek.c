#include <inttypes.h>
#include <string.h>
#include <stdio.h>

#include "peek.h"

static inline uint16_t ntohs(const uint64_t n) 
{
#if BYTE_ORDER == LITTLE_ENDIAN
    uint16_t res = 
        ((n & 0xFF) << 8)
        | ((n & 0xFF00) >> 8);

    return res;
#else
    return n;
#endif /* BYTE_ORDER */
}

static inline uint32_t ntohl(const uint64_t n)
{
#if BYTE_ORDER == LITTLE_ENDIAN
    return ((n & 0xFF) << 24) |
           ((n & 0xFF00) << 8) | 
           ((n & 0xFF0000) >> 8) |
           ((n & 0xFF000000) >> 24);
#else
    return n;
#endif /* BYTE_ORDER */
}

static inline uint64_t ntoh64(const uint64_t network_64bits)
{
#if BYTE_ORDER == LITTLE_ENDIAN
    uint8_t *p = (uint8_t *) &network_64bits;

    for (size_t i = 0; i < 8; i++) {
        p[7 - i] = p[i];
    }

    return *((uint64_t *) p);
#else
    return network_64bits;
#endif /* BYTE_ORDER */
}

inline uint8_t peek_uint8(const uint8_t *packet)
{
    return *packet;
}

inline int8_t peek_int8(const uint8_t *packet)
{
    return (int8_t) *packet;
}

inline uint16_t peek_uint16(const uint8_t *packet)
{
    uint16_t res;
    memcpy(&res, packet, sizeof(uint16_t));
    res = ntohs(res);
    return res;
}

inline int16_t peek_int16(const uint8_t *packet)
{
    int16_t res;
    memcpy(&res, packet, sizeof(int16_t));
    res = ntohs(res);
    return res;
}

inline uint32_t peek_uint32(const uint8_t *packet)
{
    uint32_t res;
    memcpy(&res, packet, sizeof(uint32_t));
    res = ntohl(res);
    return res;
}

inline uint64_t peek_uint48(const uint8_t *packet)
{
    uint64_t res = 0;

    const size_t bytes_to_read = 6; /* 48-bit uinsigned integer */
    for (size_t i = 0; i < bytes_to_read; i++) {
        /* the index of the byte to read in the given packet */
        const size_t byte_to_read = i;
        /* the bit index where the byte is written to in res */
        const size_t dest_bit_index = 
            /* data is big endian, so lowest byte contains most significant byte */
            (bytes_to_read - i - 1) * 8;
        res |= ((uint64_t) packet[byte_to_read]) << dest_bit_index;
    }

    return res;
}

inline int64_t peek_int64(const uint8_t *packet)
{
    uint64_t res = 0;

    const size_t bytes_to_read = 8; /* 64-bit integer */
    for (size_t i = 0; i < bytes_to_read; i++) {
        /* the index of the byte to read in the given packet */
        const size_t byte_to_read = i;
        /* the bit index where the byte is written to in res */
        const size_t dest_bit_index = 
            /* data is big endian, so lowest byte contains most significant byte */
            (bytes_to_read - i - 1) * 8;
        res |= ((uint64_t) packet[byte_to_read]) << dest_bit_index;
    }

    return res;
}
