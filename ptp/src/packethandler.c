#include <inttypes.h>
#include <stddef.h>
#include <stdbool.h>
#include "error.h"

#include "clock.h"
#include "os.h"
#include "packethandler.h"
#include "datatypes.h"
#include "dataset.h"
#include "delay.h"
#include "bmc.h"
#include "sync.h"
#include "statemachine.h"
#include "dataset_update.h"

/* last sync message received from the master.
 this is needed for two-step clocks, where
 we have to wait for the follow up message. */
// TODO: must reset __last_sync_valid after state change. how?
static struct ptp_proto_sync __last_sync;
static struct ptp_proto_header __last_sync_header;
static struct ptp_timestamp __last_sync_ts;
static int __last_sync_valid = 0;

int ptp_ph_discard_follow_up(struct ptp_proto_header *arg0, struct ptp_proto_follow_up *arg1, struct ptp_engine_timestamp *arg2) {
  (void)arg0;
  (void)arg1;
  (void)arg2;
  return 0;
}
int ptp_ph_discard_sync(struct ptp_proto_header *arg0, struct ptp_proto_sync *arg1, struct ptp_engine_timestamp *arg2) {
  (void)arg0;
  (void)arg1;
  (void)arg2;
  return 0;
}
int ptp_ph_discard_delay_resp(struct ptp_proto_header *arg0, struct ptp_proto_delay_resp *arg1, struct ptp_engine_timestamp *arg2) {
  (void)arg0;
  (void)arg1;
  (void)arg2;
  return 0;
}
int ptp_ph_discard_announce(struct ptp_proto_header *arg0, struct ptp_proto_announce *arg1, struct ptp_engine_timestamp *arg2) {
  (void)arg0;
  (void)arg1;
  (void)arg2;
  return 0;
}

static int msg_is_from_foreign_master(struct ptp_proto_header *hdr) {
  /* when we are not a slave, we do not have a master.
   thus the packet can only come from a foreign master. */
  if (PTP_STATE_SLAVE != ptp_state_get()) {
    return true;
  }

  struct ptp_port_identity *masterId = &ptp_ds_parent.parentPortIdentity;
  struct ptp_port_identity *msgId = &hdr->sourcePortIdentity;

  return ptp_dt_port_identity_equal(masterId, msgId);
}

static int msg_is_from_current_master(struct ptp_proto_header *hdr) {
  struct ptp_port_identity *masterId = &ptp_ds_parent.parentPortIdentity;
  struct ptp_port_identity *msgId = &hdr->sourcePortIdentity;

  return ptp_dt_port_identity_equal(masterId, msgId);
}

int ptp_ph_announce(struct ptp_proto_header *hdr,
                    struct ptp_proto_announce *msg,
                    struct ptp_engine_timestamp *ts) {
  (void)ts;
  // discard alternate master messages
  if (ptp_parser_header_has_flag(hdr, PTP_FLAG_ALTERNATE_MASTER)) {
    return 0;
  }

  // message is from a master which is not our current master
  if (msg_is_from_foreign_master(hdr)) {
    ptp_bmc_add_foreign_master(msg, hdr);
  }
  // message is from current master
  else {
    ptp_dataset_update_s1(msg, hdr);
  }

  return 0;
}

int ptp_ph_announce_reset_receipt_timeout(struct ptp_proto_header *hdr,
                                          struct ptp_proto_announce *msg,
                                          struct ptp_engine_timestamp *ts) {
  int r = ptp_ph_announce(hdr, msg, ts);

  struct ptp_port_identity *masterId = &ptp_ds_parent.parentPortIdentity;
  struct ptp_port_identity *msgId = &hdr->sourcePortIdentity;

  // message from current master
  if (ptp_dt_port_identity_equal(masterId, msgId)) {
    /* reset the timer */
    ptp_state_arm_announce_receipt_timeout();
  }

  return r;
}

static void sync_clock(struct ptp_timestamp *originTimestamp,
                       struct ptp_timeinterval correction) {
  if (!__last_sync_valid) {
    return;
  }

  ptp_delay_update_sync(correction, &__last_sync_ts, originTimestamp);
  ptp_sync_update(correction, &__last_sync_ts, originTimestamp);
}

int ptp_ph_sync(struct ptp_proto_header *hdr, struct ptp_proto_sync *msg,
                struct ptp_engine_timestamp *ts) {
  enum ptp_state state = ptp_state_get();

  if (state != PTP_STATE_SLAVE && state != PTP_STATE_UNCALIBRATED) {
    return 0;
  }

  if (!msg_is_from_current_master(hdr)) {
    return 0;
  }

  /* drop packet if timestamp is missing */
  // TODO: handle this better.
  if (!ts->valid) {
    return PTP_ERROR_NO_TIMESTAMP;
  }

  __last_sync = *msg;
  __last_sync_header = *hdr;
  __last_sync_ts.secondsField = ts->seconds;
  __last_sync_ts.nanosecondsField = ts->nanoseconds;
  __last_sync_valid = true;

  /* no two-step message, we can handle it directly without
   * having to wait for the Follow_Up */
  if (!ptp_parser_header_has_flag(hdr, PTP_FLAG_TWO_STEP)) {
    // TODO: use timeinterval directly.
    struct ptp_timeinterval correctionSync = {.scaledNanoseconds =
        __last_sync_header.correctionField};
    sync_clock(&__last_sync.originTimestamp, correctionSync);
  }

  return 0;
}

int ptp_ph_follow_up(struct ptp_proto_header *hdr,
                     struct ptp_proto_follow_up *msg,
                     struct ptp_engine_timestamp *ts) {
  (void)ts;
  struct ptp_timeinterval totalCorrection;

  if (!__last_sync_valid) {
    return 0;
  }

  if (!msg_is_from_current_master(hdr)) {
    return 0;
  }

  if (!ptp_dt_port_identity_equal(&hdr->sourcePortIdentity,
                                  &__last_sync_header.sourcePortIdentity)) {
    return 0;
  }

  if (hdr->sequenceId != __last_sync_header.sequenceId) {
    return 0;
  }

  // TODO: use timeinterval directly.
  totalCorrection.scaledNanoseconds = __last_sync_header.correctionField
      + hdr->correctionField;
  sync_clock(&msg->preciseOriginTimestamp, totalCorrection);

  __last_sync_valid = false;
  return 0;
}
