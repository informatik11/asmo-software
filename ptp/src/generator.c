#include "generator.h"
#include "poke.h"
#include "datatypes.h"
#include <string.h>
#include "error.h"

static void poke_port_identity(uint8_t *dest, const struct ptp_port_identity *id)
{
    for (size_t i = 0; i < PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE; i++) {
        poke_uint8(dest + i, id->clockIdentity[i]);
    }

    poke_uint16(dest + PTP_PROTO_PORT_IDENTITY_OFFSET_PORT_NUMBER, id->portNumber);
}

static void poke_timestamp(uint8_t *dest, const struct ptp_timestamp *ts)
{
    poke_uint48(dest, ts->secondsField);
    poke_uint32(dest + 6, ts->nanosecondsField);
}

int ptp_generator_header(uint8_t *generated_packet, size_t generated_packet_size, const struct ptp_proto_header *header)
{
    uint8_t v;

    if (generated_packet_size < PTP_PROTO_HEADER_SIZE) {
        return PTP_ERROR_PACKET_SIZE;
    }

    memset(generated_packet, 0, PTP_PROTO_HEADER_SIZE);

    v = (header->messageType & 0x0F) | ((header->transportSpecific & 0x0F) << 4);
    poke_uint8(generated_packet, v);

    poke_uint8(generated_packet + PTP_PROTO_HEADER_OFFSET_VERSION_PTP, header->versionPTP & 0x0F);

    poke_uint16(generated_packet + PTP_PROTO_HEADER_OFFSET_MESSAGE_LENGTH, header->messageLength);

    poke_uint8(generated_packet + PTP_PROTO_HEADER_OFFSET_DOMAIN_NUMBER, header->domainNumber);

    poke_uint8(generated_packet + PTP_PROTO_HEADER_OFFSET_FLAG_FIELD, header->flagField[0]);
    poke_uint8(generated_packet + PTP_PROTO_HEADER_OFFSET_FLAG_FIELD + 1, header->flagField[1]);

    poke_uint64(generated_packet + PTP_PROTO_HEADER_OFFSET_CORRECTION_FIELD, header->correctionField);

    poke_port_identity(generated_packet + PTP_PROTO_HEADER_OFFSET_SOURCE_PORT_IDENTITY, &header->sourcePortIdentity);

    poke_uint16(generated_packet + PTP_PROTO_HEADER_OFFSET_SEQUENCE_ID, header->sequenceId);

    poke_uint8(generated_packet + PTP_PROTO_HEADER_OFFSET_CONTROL_FIELD, header->controlField);

    poke_int8(generated_packet + PTP_PROTO_HEADER_OFFSET_LOG_MESSAGE_INTERVAL, header->logMessageInterval);

    return 0;

}

int ptp_generator_delay_req(uint8_t *generated_packet, size_t generated_packet_size, const struct ptp_proto_delay_req *delay_req)
{
    if (generated_packet_size < PTP_PROTO_DELAY_REQ_SIZE) {
        return PTP_ERROR_PACKET_SIZE;
    }

    poke_timestamp(generated_packet, &delay_req->originTimestamp);

    return 0;

}

