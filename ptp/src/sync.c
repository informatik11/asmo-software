#include "sync.h"
#include "statemachine.h"
#include "dataset.h"
#include "config.h"
#include "clock.h"
#include "os.h"

#include <stdbool.h>

/* private struct */
struct __ptp_sync_params {
    bool valid;
    struct ptp_timestamp sync_origin_ts;
    struct ptp_timestamp sync_recv_ts; 
    struct ptp_timeinterval correction;
};

struct __ptp_sync_params _sync_params = {
    .valid = false
};
bool _sync_inhibited = false;
float _sync_pi_iterm = 0;

static void pi_update(struct ptp_timeinterval *offset)
{
    float error, pterm;
    struct ptp_timeinterval output;
    int r;

    error = (float) (offset->scaledNanoseconds >> 16);
    pterm = PTP_CONF_SYNC_PI_FACTOR_P * error;
    _sync_pi_iterm += PTP_CONF_SYNC_PI_FACTOR_I * error;
    output.scaledNanoseconds = (uint64_t)(pterm + _sync_pi_iterm) << 16;

#if PTP_CONF_SYNC_CONTROL_VALUE_LOG == 1
    uint32_t low_word, high_word;
    high_word = output.scaledNanoseconds >> 32;
    low_word = output.scaledNanoseconds & 0xFFFFFFFF;
    ptp_os_printf("sync.controlValue = 0x%X 0x%X\n", high_word, low_word);
#endif

    r = ptp_clock_update_ts(&output);
    if (r) {
        ptp_os_printf("PTP: Warning: synchronizing clock (update) failed with %d\n", r);
        // TODO: trigger failure
    }
}

static void __sync(struct ptp_timestamp *sync_origin_ts, struct ptp_timestamp *sync_recv_ts, struct ptp_timeinterval *correction)
{
    struct ptp_timeinterval offset;
    int r;

    // If we are not in the SLAVE state, our clock is probably not 
    // accurate.
    // Then, we just set the timestamp and ignore meanPathDelay.
    if (PTP_STATE_SLAVE != ptp_state_get()) {
        r = ptp_clock_set_ts(sync_origin_ts);
        // on success, we are synchronized and can go to the SLAVE
        // state
        if (r == 0) {
            ptp_state_triggerevt(PTP_EVENT_MASTER_CLOCK_SELECTED);
        }
    } else {
        ptp_dt_subtract_ts_ts(&offset, sync_recv_ts, sync_origin_ts);
        ptp_dt_subtract_ti_ti(&offset, &offset, &ptp_ds_current.meanPathDelay);
        ptp_dt_subtract_ti_ti(&offset, &offset, correction);
        ptp_ds_current.offsetFromMaster = offset;

        pi_update(&offset);

#if PTP_CONF_SYNC_OFFSET_LOG == 1
        uint32_t high_word, low_word;
        high_word = offset.scaledNanoseconds >> 32;
        low_word = offset.scaledNanoseconds & 0xFFFFFFFF;
        ptp_os_printf("offsetFromMaster = 0x%X 0x%X\n", high_word, low_word);
#endif /* PTP_CONF_SYNC_OFFSET_LOG == 1 */
    }
}

static void __sync_saved_params(void)
{
    __sync(&_sync_params.sync_origin_ts, &_sync_params.sync_recv_ts, &_sync_params.correction);
}

void ptp_sync_inhibit(void)
{
    _sync_inhibited = true;
}

void ptp_sync_uninhibit(void)
{
    if (!_sync_inhibited) {
        return;
    }

    /* ptp_sync_update() was called while synchronization was 
     * inhibited. We synchronize now. */
    if (_sync_params.valid) {
        __sync_saved_params();
    }

    _sync_params.valid = false;
    _sync_inhibited = false;
}

void ptp_sync_update(struct ptp_timeinterval correction, struct ptp_timestamp *sync_recv_ts, struct ptp_timestamp *sync_origin_ts)
{
    /* save for later, we synchronize when ptp_sync_uninhibit() 
     * is called */
    if (_sync_inhibited) {
        _sync_params.sync_origin_ts = *sync_origin_ts;
        _sync_params.sync_recv_ts = *sync_recv_ts;
        _sync_params.correction = correction;
        _sync_params.valid = true;
    } else {
        __sync(sync_origin_ts, sync_recv_ts, &correction);
    }
}
