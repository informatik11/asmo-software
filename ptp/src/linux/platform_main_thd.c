#include "../platform/os.h"
#include "../platform/socket.h"
#include "../parser.h"
#include "../engine.h"
#include "main_thd.h"
#include <simple_ptp/error.h>
#include <ch.h>
#include <hal.h>
#include <chprintf.h>
#include <memstreams.h>
#include <string.h>

#define PTP_MAIN_WA_SIZE 1024
thread_t *ptp_main_thd;
THD_WORKING_AREA(ptp_main_wa, PTP_MAIN_WA_SIZE);

/* mailbox for main thread actions. */
#define PTP_MBOX_SIZE 4
static msg_t __mbox_buf[PTP_MBOX_SIZE]; /* pointers to struct ptp_os_main_thd_action */
MAILBOX_DECL(main_thd_mbox, __mbox_buf, PTP_MBOX_SIZE);

/* timeout waiting for a slot in the mailbox in milliseconds */
#define PTP_MBOX_TIMEOUT 500

/* memory pool. struct ptp_os_main_thd_action are placed in this Pool. */
GUARDEDMEMORYPOOL_DECL(main_thd_mempool, sizeof (struct ptp_os_main_thd_action));
#define PTP_MAIN_THD_POOL_SIZE PTP_MBOX_SIZE + 1
struct ptp_os_main_thd_action __main_thd_mempool_block[PTP_MAIN_THD_POOL_SIZE];

/* memory pool. used for storing struct ptp_os_main_thd_action when posting
 * to the main thread from an ISR. */
MEMORYPOOL_DECL(main_thd_isr_mempool, sizeof (struct ptp_os_main_thd_action), NULL);
struct ptp_os_main_thd_action __main_thd_isr_mempool_block[PTP_MAIN_THD_POOL_SIZE];

/* timeout waiting for a slot in the memory pool in milliseconds */
#define PTP_MAIN_THD_POOL_TIMEOUT 500

#define PTP_MAIN_THD_FREE_NONE 0
#define PTP_MAIN_THD_FREE_REGULAR_POOL 1
#define PTP_MAIN_THD_FREE_ISR_POOL 2

static void main_thd(void *p)
{
    msg_t r;
    int res;
    msg_t mailbox_msg;
    struct ptp_os_main_thd_action *action;
    
    chRegSetThreadName("ptp_main");

    while (true) {
        r = chMBFetch(&main_thd_mbox, &mailbox_msg, TIME_INFINITE);
        if (r != MSG_OK) {
            ptp_os_printf("ptp: error fetching from mailbox\n");
            // TODO: handle errors better
            continue;
        }

        action = (struct ptp_os_main_thd_action *) mailbox_msg;

        if (!action->_handler_func) {
            ptp_os_printf("ptp main thd: detected invalid handle_func\n");
            continue;
        }

        res = action->_handler_func(action->_opaque);

        if (res) {
            ptp_os_printf("ptp: error %d handling action\n", res);
        }

        switch (action->_pool_free) {
            case PTP_MAIN_THD_FREE_REGULAR_POOL:
                chGuardedPoolFree(&main_thd_mempool, (void *)mailbox_msg);
                break;
            case PTP_MAIN_THD_FREE_ISR_POOL:
                chPoolFree(&main_thd_isr_mempool, (void *)mailbox_msg);
                break;
        }
    }
}

/**
 * Initialize the given action struct.
 *
 * @param[out] action the action struct to initialize
 * @param[in] callback the callback to be used
 * @param[in] opaque opaque data to pass to the callback as first argument.
 * @param pool_free How to free the ptp_os_main_thd_action struct. Either
 * PTP_MAIN_THD_FREE_NONE, PTP_MAIN_THD_FREE_REGULAR_POOL or 
 * PTP_MAIN_THD_FREE_ISR_POOL.
 */
static void __init_action_struct(struct ptp_os_main_thd_action *action, int (*callback)(void *opaque), void *opaque, uint8_t pool_free)
{
    memset(action, 0, sizeof(struct ptp_os_main_thd_action));

    action->_handler_func = callback;
    action->_opaque = opaque;
    action->_pool_free = pool_free;
}

/**
 * Post the given action struct to the main thread.
 */
static int __post_main(struct ptp_os_main_thd_action *action, int is_highprio)
{
    msg_t mb_res;

    if (is_highprio) {
        mb_res = chMBPostAhead(&main_thd_mbox, (msg_t) action, PTP_MBOX_TIMEOUT);
    } else {
        mb_res = chMBPost(&main_thd_mbox, (msg_t) action, PTP_MBOX_TIMEOUT);
    }

    if (mb_res == MSG_RESET) {
        return PTP_ERROR_RESET;
    } else if (mb_res == MSG_TIMEOUT) {
        return PTP_ERROR_TIMEOUT;
    } else if (mb_res != MSG_OK) {
        return -1;
    } 

    return 0;
}

/**
 * Post the given action struct to the main thread, with immediate timeout.
 *
 * This makes this function suitable for calling from an ISR.
 *
 * This function expects it is being called with the system lock
 * held.
 */
static int __post_main_isr(struct ptp_os_main_thd_action *action, int is_highprio)
{
    msg_t mb_res;

    if (is_highprio) {
        mb_res = chMBPostAheadI(&main_thd_mbox, (msg_t) action);
    } else {
        mb_res = chMBPostI(&main_thd_mbox, (msg_t) action);
    }

    if (mb_res == MSG_RESET) {
        return PTP_ERROR_RESET;
    } else if (mb_res == MSG_TIMEOUT) {
        return PTP_ERROR_TIMEOUT;
    } else if (mb_res != MSG_OK) {
        return -1;
    } 

    return 0;
}

static void __tmr_handler(void *p)
{
    ptp_os_timer_t *tmr = (ptp_os_timer_t *) p;

    // TODO: what to do with the return value?
    chSysLockFromISR();
    __post_main_isr(&tmr->_action, false);
    chSysUnlockFromISR();
}

inline void ptp_os_init_timer(ptp_os_timer_t *tmr)
{
    chVTObjectInit(&tmr->_tmr);
}

int ptp_os_set_timer_ms(ptp_os_timer_t *tmr, uint32_t delay, int (*callback)(void *opaque), void *opaque)
{
    struct ptp_os_main_thd_action *action;

    action = &tmr->_action;
    __init_action_struct(action, callback, opaque, PTP_MAIN_THD_FREE_NONE);

    chVTSet(&tmr->_tmr, MS2ST(delay), &__tmr_handler, (void *)tmr);

    return 0;
}

int ptp_os_clear_timer(ptp_os_timer_t *tmr)
{
    chVTReset(&tmr->_tmr);

    return 0;
}

int ptp_os_post_main(int (*callback)(void *opaque), void *opaque, int is_highprio)
{
    int ret, r;
    struct ptp_os_main_thd_action *action;

    action = (struct ptp_os_main_thd_action *) chGuardedPoolAllocTimeout(&main_thd_mempool, MS2ST(PTP_MAIN_THD_POOL_TIMEOUT));

    if (!action) {
        ret = PTP_ERROR_OUT_OF_MEMORY;
        goto out;
    }

    __init_action_struct(action, callback, opaque, PTP_MAIN_THD_FREE_REGULAR_POOL);

    r = __post_main(action, is_highprio);
    if (r) {
        ret = r;
        goto out_pool_free;
    }

    return 0;

out_pool_free:
    /* only in error case. otherwise free'd by main thread */
    chGuardedPoolFree(&main_thd_mempool, (void *)action);

out:
    return ret;
}

int _ptp_os_post_main_isr(int (*callback)(void *opaque), void *opaque, int is_highprio)
{
    int ret, r;
    struct ptp_os_main_thd_action *action;

    action = (struct ptp_os_main_thd_action *) chPoolAllocI(&main_thd_isr_mempool);
    if (!action) {
        ret = PTP_ERROR_OUT_OF_MEMORY;
        goto out;
    }

    __init_action_struct(action, callback, opaque, PTP_MAIN_THD_FREE_ISR_POOL);

    r = __post_main_isr(action, is_highprio);
    if (r) {
        ret = r;
        goto out_pool_free;
    }

    return 0;

out_pool_free:
    /* only in error case. otherwise free'd by main thread */
    chPoolFree(&main_thd_isr_mempool, (void *)action);

out:
    return ret;
}

int ptp_os_post_main_isr(int (*callback)(void *opaque), void *opaque, int is_highprio)
{
    int r;
    chSysLockFromISR();
    r = _ptp_os_post_main_isr(callback, opaque, is_highprio);
    chSysUnlockFromISR();
    return r;
}

void ptp_os_ch_startmain(void)
{
    /* add slots to the main_thd_mempool pool */
    for (size_t i = 0; i < PTP_MAIN_THD_POOL_SIZE; i++) {
        chGuardedPoolAdd(&main_thd_mempool, &__main_thd_mempool_block[i]);
        chPoolAdd(&main_thd_isr_mempool, &__main_thd_isr_mempool_block[i]);
    }

    ptp_main_thd = chThdCreateStatic(&ptp_main_wa, PTP_MAIN_WA_SIZE, NORMALPRIO, &main_thd, NULL);
}
