#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "../platform/os.h"
#include "../platform/socket.h"
#include "../parser.h"
#include "../engine.h"

void ptp_os_printf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
}

int ptp_os_snprintf(char *str, size_t size, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vsnprintf(str, size, fmt, ap);
	va_end(ap);
}

uint32_t ptp_os_get_ts_ms(void)
{
    return 0;
}

void ptp_os_mutex_init(ptp_os_mutex_t *mtx)
{
}

void ptp_os_mutex_lock(ptp_os_mutex_t *mtx)
{
}

void ptp_os_mutex_unlock(ptp_os_mutex_t *mtx)
{
}

uint32_t ptp_os_rand(uint32_t *state)
{
    return rand();
}

int ptp_os_post_main(int (*callback)(void *opaque), void *opaque, int is_highprio)
{
    return 0;
}

int ptp_os_set_timer_ms(ptp_os_timer_t *tmr, uint32_t delay, int (*callback)(void *opaque), void *opaque)
{
    return 0;
}

void ptp_os_init_timer(ptp_os_timer_t *tmr)
{
}

int ptp_os_clear_timer(ptp_os_timer_t *tmr)
{
    return 0;
}


int ptp_os_start()
{
    return 0;
}
