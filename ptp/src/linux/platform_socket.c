#include "../platform/socket.h"
#include "platform-headers/socket.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int socket_udp_listen(uint16_t port, ptp_socket_t *sock)
{
    int r = 0;
    int fd = 0;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd == -1) {
        r = -1;
        goto out;
    }

    struct sockaddr_in bind_addr = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = htonl(INADDR_ANY),
        .sin_port = htons(port)
    };
    r = bind(fd, (struct sockaddr *) &bind_addr, sizeof (bind_addr));
    if (r == -1) {
        r = -1;
        goto out;
    }

    r = 0;
out:
    return r;
}

int socket_get_mac(uint8_t macaddr[static 6])
{
    macaddr[0] = 0xde;
    macaddr[1] = 0xad;
    macaddr[2] = 0x00;
    macaddr[3] = 0xbe;
    macaddr[4] = 0xef;
    macaddr[5] = 0x00;

    return 0;
}
