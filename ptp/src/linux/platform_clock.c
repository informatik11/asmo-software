#include <simple_ptp/error.h>

#include "../platform/clock.h"
#include "../datatypes.h"

int ptp_clock_set_ts(const struct ptp_timestamp *ts)
{
    (void)ts;

    return 0;
}

int ptp_clock_update_ts(const struct ptp_timeinterval *offsetFromMaster)
{
    return 0;
}

void ptp_clock_get_ts(struct ptp_timestamp *ts)
{
    ts->secondsField = 1234;
    ts->nanosecondsField = 99;
}
