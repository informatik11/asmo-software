#include "poke.h"
#include <stdio.h>
#include <stddef.h>

inline void poke_uint8(uint8_t *dest, uint8_t val)
{
    *dest = val;
}

void poke_uint16(uint8_t *dest, uint16_t val)
{
    dest[0] = (val >> 8) & 0xFF;
    dest[1] = val & 0xFF;
}

void poke_uint64(uint8_t *dest, uint64_t val)
{
    for (size_t i = 0; i < 8; i++) {
        dest[i] = (val >> (8 * (7 - i))) & 0xFF;
    }
}

void poke_uint48(uint8_t *dest, uint64_t val)
{
    for (size_t i = 0; i < 6; i++) {
        dest[i] = (val >> (8 * (5 - i))) & 0xFF;
    }
}

void poke_uint32(uint8_t *dest, uint32_t val)
{
    for (size_t i = 0; i < 4; i++) {
        dest[i] = (val >> (8 * (3 - i))) & 0xFF;
    }
}

void poke_int8(uint8_t *dest, int8_t val)
{
    *dest = (uint8_t) val;
}
