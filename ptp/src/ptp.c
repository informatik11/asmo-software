#include <stddef.h>
#include "ptp.h"
#include "socket.h"
#include "os.h"
#include "statemachine.h"
#include "bmc.h"
#include "dataset.h"
#include "delay.h"
#include "config.h"
#include "clock.h"

static int __main_thd_init(void *opaque) {
  (void)opaque;

  ptp_state_init();
  ptp_delay_init();

  return 0;
}

int ptp_init(void) {
  ptp_os_printf("ptp: startup.\n");

  /* initialize socket module first to make sure it is initialized
   * when main thread starts */
  socket_init();

  ptp_os_start();

  /* do most of the initialization on the main thread */
  ptp_os_post_main(&__main_thd_init, NULL, 0);

  return 0;
}

void ptp_ui_get_info(ptp_stream_t stream) {
  enum ptp_state state = ptp_state_get();
  const char *state_str = ptp_state_to_str(state);
  ptp_stream_printf(stream, "state = %s (%d)\n", state_str, state);

  ptp_bmc_print_debug(stream);
  ptp_ds_print_debug(stream);

}

static int __disable(void *opaque) {
  (void)opaque;
  ptp_state_triggerevt(PTP_EVENT_DESIGNATED_DISABLED);
  return 0;
}

static int __enable(void *opaque) {
  (void)opaque;

  ptp_state_triggerevt(PTP_EVENT_DESIGNATED_ENABLED);
  return 0;
}

void ptp_disable(void) {
  ptp_os_post_main(&__disable, NULL, 0);
}

void ptp_enable(void) {
  ptp_os_post_main(&__enable, NULL, 0);
}

void ptp_get_ts(struct ptp_timestamp *dest) {
  ptp_clock_get_ts(dest);
}
