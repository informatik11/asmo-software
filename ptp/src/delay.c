#include "delay.h"
#include "proto.h"
#include "dataset.h"
#include "sync.h"
#include "config.h"
#include "generator.h"
#include "statemachine.h"
#include "os.h"
#include "socket.h"

#include <math.h>
#include <stdbool.h>

/*
 * TODO:
 * - timing conditions for sending delay req
 */

uint16_t _delay_req_sequence = 0;
struct ptp_delay_req_data {
  bool in_progress;
  bool has_ts_tx_delay_req;
  bool has_ts_rx_delay_req;
  uint16_t sequenceId;
  /* how often we retransmited a delay req */
  uint8_t delay_retransmits;
  /* how many outlier measurement we encountered in a row */
  uint8_t outliers;
  /* slave -> master delay */
  struct ptp_timestamp ts_tx_delay_req;
  struct ptp_timestamp ts_rx_delay_req;
  /* master -> slave delay */
  struct ptp_timestamp ts_rx_sync;
  struct ptp_timestamp ts_origin_sync;
  /* total correction value until now in nanoseconds */
  struct ptp_timeinterval total_correction;
  ptp_os_timer_t tmr_resp_timeout;
};
static struct ptp_delay_req_data _delay_req;

/* validate config */
#if PTP_CONF_DELAY_MIN_US < 0
#error "PTP_CONF_DELAY_MIN_US must not be less than 0."
#endif

#if PTP_CONF_DELAY_MAX_US < 0
#error "PTP_CONF_DELAY_MAX_US must not be less than 0."
#endif

#if PTP_CONF_DELAY_MAX_US < PTP_CONF_DELAY_MIN_US
#error "PTP_CONF_DELAY_MAX_US must not be less then PTP_CONF_DELAY_MIN_US."
#endif

#if PTP_CONF_DELAY_OUTLIER_THRESHOLD > 254
#error "PTP_CONF_DELAY_OUTLIER_THRESHOLD must not be greate than 254."
#endif

#if PTP_CONF_DELAY_OUTLIER_THRESHOLD < 0
#error "PTP_CONF_DELAY_OUTLIER_THRESHOLD must not be less than 0."
#endif

#if PTP_CONF_DELAY_RESP_TIMEOUT < 0
#error "PTP_CONF_DELAY_RESP_TIMEOUT must not be less than 0."
#endif

#if PTP_CONF_DELAY_RETRANSMIT_LIMIT < 0
#error "PTP_CONF_DELAY_RETRANSMIT_LIMIT must not be less than 0."
#endif

static int msg_is_from_current_master(struct ptp_proto_header *hdr) {
  struct ptp_port_identity *masterId = &ptp_ds_parent.parentPortIdentity;
  struct ptp_port_identity *msgId = &hdr->sourcePortIdentity;

  return ptp_dt_port_identity_equal(masterId, msgId);
}

static int build_ptp_header(uint8_t *dest, size_t dest_size) {
  struct ptp_proto_header header = {.transportSpecific = 0, .messageType =
                                        PTP_PROTO_MESSAGE_TYPE_DELAY_REQ,
                                    .versionPTP = 2, .messageLength =
                                        PTP_PROTO_DELAY_REQ_SIZE
                                            + PTP_PROTO_HEADER_SIZE,
                                    .domainNumber = ptp_ds_default.domainNumber,
                                    .flagField = {0, 0}, .correctionField = 0,
                                    .sourcePortIdentity =
                                        ptp_ds_port.portIdentity,
                                    .sequenceId = _delay_req_sequence,
                                    .controlField = 0x01, .logMessageInterval =
                                        0x7F};
  int r;

  r = ptp_generator_header(dest, dest_size, &header);
  if (!r) {
    _delay_req.sequenceId = _delay_req_sequence;
    _delay_req_sequence++;
  }

  return r;
}

static bool is_outlier(struct ptp_timeinterval *ti) {
  uint64_t ti_us = (ti->scaledNanoseconds >> 16) / 1000LL;

  if (ti_us > PTP_CONF_DELAY_MAX_US) {
    return true;
  }

  if (ti_us < PTP_CONF_DELAY_MIN_US) {
    return true;
  }

  return false;
}

/* true if measurement is outlier, false otherwise */
static bool filter_outliers(struct ptp_timeinterval *ti) {
  if (is_outlier(ti)) {
    _delay_req.outliers++;

    if (_delay_req.outliers >= PTP_CONF_DELAY_OUTLIER_THRESHOLD) {
      ptp_os_printf("ptp ERROR: %d consecutive delay "
                    "measurements were outliers.\n",
                    _delay_req.outliers);

      ptp_state_triggerevt(PTP_EVENT_FAULT_DETECTED);
      _delay_req.outliers = 0;
    }

    return true;
  }
  else {
    _delay_req.outliers = 0;
  }

  return false;
}

static struct ptp_timeinterval smoothen_delay(
    struct ptp_timeinterval mean_path_delay) {
  float smoothened_delay, previous_delay, last_measurement, alpha;
  struct ptp_timeinterval res;

  last_measurement = (float)(mean_path_delay.scaledNanoseconds >> 16);
  previous_delay =
      (float)(ptp_ds_current.meanPathDelay.scaledNanoseconds >> 16);
  alpha = (float)PTP_CONF_DELAY_RECENT_WEIGHT;

  // previous delay measurement is available
  if (previous_delay > 0) {
    smoothened_delay = (alpha * last_measurement)
        + ((1 - alpha) * previous_delay);
  }
  else {
    // no previous measurement, just copy value
    smoothened_delay = last_measurement;
  }

  res.scaledNanoseconds = ((uint64_t)roundf(smoothened_delay)) << 16;
  return res;
}

static void __update_delay(void) {
  struct ptp_timeinterval mean_path_delay, d1, d2;
#if PTP_CONF_DELAY_LOG_RECENT == 1 || PTP_CONF_DELAY_LOG == 1
  uint32_t low_word, high_word;
#endif

  if (_delay_req.has_ts_tx_delay_req && _delay_req.has_ts_rx_delay_req) {
    /*
     * We have:
     *
     *   meanPathDelay =
     *     1/2 * [ (syncRecvTs - syncOriginTs)
     *     + (delayReqRecvTs - delayReqSendTs) ]
     *
     * syncRecvTs and delayReqSendTs are measured with the slave
     * clock, while delayReqRecv and syncOriginTs are measured with
     * the master clock.
     *
     * As master and slave may have a significant offset, the terms
     *     "syncRecvTs - syncOriginTs"
     *     and "delayReqRecvTs - delayReqSendTs"
     * can easily under-/overflow. Thus, we rearrange:
     *
     *   meanPathDelay =
     *     1/2 * [ (syncRecvTs - delayReqSendTs)
     *     + (delayReqRecvTs - syncOriginTs) ]
     *
     * Which is exactly the same, but it is less likely to overflow.
     */
    ptp_dt_subtract_ts_ts(&d1, &_delay_req.ts_rx_sync,
                          &_delay_req.ts_tx_delay_req);
    ptp_dt_subtract_ts_ts(&d2, &_delay_req.ts_rx_delay_req,
                          &_delay_req.ts_origin_sync);
    ptp_dt_add_ti_ti(&mean_path_delay, &d1, &d2);
    ptp_dt_divide_ti(&mean_path_delay, &mean_path_delay, 2);

#if PTP_CONF_DELAY_LOG_RECENT == 1
    high_word = mean_path_delay.scaledNanoseconds >> 32;
    low_word = mean_path_delay.scaledNanoseconds & 0xFFFFFFFF;
    ptp_os_printf("recentMeanPathDelay = 0x%X 0x%X\n", high_word, low_word);
#endif /* PTP_CONF_DELAY_LOG_RECENT == 1 */

    if (ptp_dt_ti_is_negative(&mean_path_delay)) {
      ptp_os_printf("ptp WARNING: encountered negative meanPathDelay!\n");
    }
    else if (!filter_outliers(&mean_path_delay)) {
      ptp_ds_current.meanPathDelay = smoothen_delay(mean_path_delay);
    }

#if PTP_CONF_DELAY_LOG == 1
    high_word = ptp_ds_current.meanPathDelay.scaledNanoseconds >> 32;
    low_word = ptp_ds_current.meanPathDelay.scaledNanoseconds & 0xFFFFFFFF;
    ptp_os_printf("meanPathDelay = 0x%X 0x%X\n", high_word, low_word);
#endif /* PTP_CONF_DELAY_LOG == 1 */

    _delay_req.in_progress = false;
    _delay_req.has_ts_tx_delay_req = false;
    _delay_req.has_ts_rx_delay_req = false;
    ptp_os_clear_timer(&_delay_req.tmr_resp_timeout);

    ptp_sync_uninhibit();
  }
}

static void __timestamp_callback(ptp_socket_ts_t *ts) {
  if (socket_ts_is_negative(ts)) {
    /* will in the end lead to the resp timeout expiring */
    return;
  }

  _delay_req.ts_tx_delay_req.secondsField = socket_ts_get_seconds(ts);
  _delay_req.ts_tx_delay_req.nanosecondsField = socket_ts_get_nanoseconds(ts);
  _delay_req.has_ts_tx_delay_req = true;
  __update_delay();
}

static int __delay_resp_timeout(void *opaque);

static int __send_delay_req(void) {
  uint8_t *buf;
  size_t bufsz;
  ptp_packet_meta_t meta;
  struct ptp_proto_delay_req packet;
  int r, ret;

  bufsz = PTP_PROTO_HEADER_SIZE + PTP_PROTO_DELAY_REQ_SIZE;
  buf = socket_alloc(bufsz, &meta);
  if (buf == NULL) {
    return -PTP_ERROR_OUT_OF_MEMORY;
  }

  r = build_ptp_header(buf, bufsz);
  if (r) {
    ret = r;
    socket_free(&meta);
    return ret;
  }

  packet.originTimestamp.secondsField = 0;
  packet.originTimestamp.nanosecondsField = 0;
  r = ptp_generator_delay_req(buf + PTP_PROTO_HEADER_SIZE,
                              bufsz - PTP_PROTO_HEADER_SIZE, &packet);
  if (r) {
    ret = r;
    socket_free(&meta);
    return ret;
  }

  r = socket_send_event(&meta, &__timestamp_callback);
  if (r) {
    ret = r;
    socket_free(&meta);
    return ret;
  }

  ret = 0;
  _delay_req.in_progress = true;

  ptp_os_set_timer_ms(&_delay_req.tmr_resp_timeout,
  PTP_CONF_DELAY_RESP_TIMEOUT,
                      &__delay_resp_timeout, NULL);

  socket_free(&meta);
  return ret;
}

static int __delay_resp_timeout(void *opaque) {
  (void)opaque;
  _delay_req.delay_retransmits++;
  if ((PTP_CONF_DELAY_RETRANSMIT_LIMIT > 0)
      && (_delay_req.delay_retransmits > PTP_CONF_DELAY_RETRANSMIT_LIMIT)) {

    ptp_os_printf("ptp ERROR: Giving up after having sent %u "
                  "Delay_Req without response.\n",
                  _delay_req.delay_retransmits);
    ptp_state_triggerevt(PTP_EVENT_FAULT_DETECTED);

    _delay_req.in_progress = false;

    return 0;

  }

  __send_delay_req();

  return 0;
}

void ptp_delay_init(void) {
  ptp_os_init_timer(&_delay_req.tmr_resp_timeout);
}

int ptp_delay_recv_delay_resp(struct ptp_proto_header *header,
                              struct ptp_proto_delay_resp *msg,
                              struct ptp_engine_timestamp *ts) {
  (void)ts;
  struct ptp_port_identity *requestingId = &msg->requestingPortIdentity;

  /* response was not for us, drop it. */
  if (!ptp_dt_port_identity_equal(requestingId, &ptp_ds_port.portIdentity)) {
    return 0;
  }

  /* in 11.3.2 the standard specifies Delay_Resp messages shall have the
   * same sequenceId as the associated Delay_Req.
   *
   * In 9.5.7, it specifies the sequenceId of the Delay_Req
   * message must match the requestingSequenceId field of the Delay_Resp
   * message. However, the Delay_Resp message does not have a
   * requestingSequenceId field.
   */
  if (header->sequenceId != _delay_req.sequenceId) {
    return 0;
  }

  if (!msg_is_from_current_master(header)) {
    return 0;
  }

  _delay_req.has_ts_rx_delay_req = true;
  _delay_req.ts_rx_delay_req = msg->receiveTimestamp;
  __update_delay();

  return 0;
}

void ptp_delay_update_sync(struct ptp_timeinterval correction,
                           struct ptp_timestamp *sync_recv_ts,
                           struct ptp_timestamp *sync_origin_ts) {
  if (_delay_req.in_progress) {
    return;
  }

  ptp_sync_inhibit();

  _delay_req.total_correction = correction;
  _delay_req.ts_rx_sync = *sync_recv_ts;
  _delay_req.ts_origin_sync = *sync_origin_ts;

  __send_delay_req();
}
