#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "datatypes.h"

inline int ptp_dt_port_identity_equal(const struct ptp_port_identity *a, const struct ptp_port_identity *b)
{
    if (a == b) {
        return true;
    }

    if (a == NULL || b == NULL) {
        return false;
    }

    if (!ptp_dt_clock_identity_equal(a, b)) {
        return false;
    }

    return a->portNumber == b->portNumber;
}

inline void ptp_dt_subtract_ts_ts(struct ptp_timeinterval *dest, struct ptp_timestamp *a, struct ptp_timestamp *b)
{
    // TODO: assert {a,b}.nanosecondsField <= 999.999.999

    int64_t diff_s, diff_ns;
    /* cast required, as secondsField and nanosecondsfield are unsigned */
    diff_s =  ((int64_t) a->secondsField) - ((int64_t) b->secondsField);
    diff_ns = ((int64_t) a->nanosecondsField) - ((int64_t) b->nanosecondsField);

    if (diff_ns < 0 && diff_s != 0) { /* carry over underflow */
        diff_ns = -diff_ns;
        diff_s--;
    }

    // TODO: handle overflow?
    dest->scaledNanoseconds = (diff_s * PTP_NS_PER_S + diff_ns) << 16;
}

inline void ptp_dt_subtract_ti_ti(struct ptp_timeinterval *dest, struct ptp_timeinterval *minuend, struct ptp_timeinterval *subtrahend)
{
    dest->scaledNanoseconds = minuend->scaledNanoseconds - subtrahend->scaledNanoseconds;
}

inline bool ptp_dt_ti_is_negative(struct ptp_timeinterval *dest)
{
    return (dest->scaledNanoseconds < 0);
}

inline void ptp_dt_divide_ti(struct ptp_timeinterval *dest, struct ptp_timeinterval *divident, int64_t divisor)
{
    dest->scaledNanoseconds = divident->scaledNanoseconds / divisor;
}

inline void ptp_dt_subtract_ts_ti(struct ptp_timestamp *dest, struct ptp_timestamp *minuend, struct ptp_timeinterval *subtrahend)
{
    int64_t interval_ns = subtrahend->scaledNanoseconds >> 16;
    int64_t s = interval_ns / PTP_NS_PER_S;
    int64_t ns = interval_ns - (s * PTP_NS_PER_S);

    dest->secondsField = minuend->secondsField - s;
    dest->nanosecondsField = minuend->nanosecondsField - ns;
}

inline void ptp_dt_add_ti_ti(struct ptp_timeinterval *dest, struct ptp_timeinterval *summand1, struct ptp_timeinterval *summand2)
{
    dest->scaledNanoseconds = summand1->scaledNanoseconds + summand2->scaledNanoseconds;
}

inline int ptp_dt_clock_identity_equal(const struct ptp_port_identity *a, const struct ptp_port_identity *b)
{
    return !(memcmp(&a->clockIdentity, &b->clockIdentity, PTP_PORT_IDENTITY_CLOCK_IDENTITY_SIZE));
}
