PTPROOT				:= $(PTPDIR)
PTPSRCDIR			:= $(PTPROOT)/src
PTPINCDIR			:= $(PTPROOT)/include

# Set to linux if specific linux headers should be used.
PTPPLATFORM			:= chibios

PTPPLATINCDIR		:= $(PTPINCDIR)/$(PTPPLATFORM)
PTPPLATSRCDIR		:= $(PTPSRCDIR)/$(PTPPLATFORM)

PTPROOTSRC := \
	$(PTPSRCDIR)/bmc_compare.c \
	$(PTPSRCDIR)/bmc.c \
	$(PTPSRCDIR)/dataset_update.c \
	$(PTPSRCDIR)/dataset.c \
	$(PTPSRCDIR)/datatypes.c \
	$(PTPSRCDIR)/delay.c \
	$(PTPSRCDIR)/engine.c \
	$(PTPSRCDIR)/generator.c \
	$(PTPSRCDIR)/packethandler.c \
	$(PTPSRCDIR)/parser.c \
	$(PTPSRCDIR)/peek.c \
	$(PTPSRCDIR)/poke.c \
	$(PTPSRCDIR)/ptp_debug.c \
	$(PTPSRCDIR)/ptp.c \
	$(PTPSRCDIR)/state_disabled.c \
	$(PTPSRCDIR)/state_faulty.c \
	$(PTPSRCDIR)/state_initializing.c \
	$(PTPSRCDIR)/state_listening.c \
	$(PTPSRCDIR)/state_slave.c \
	$(PTPSRCDIR)/state_uncalibrated.c \
	$(PTPSRCDIR)/statemachine.c \
	$(PTPSRCDIR)/sync.c

PTPPLATSRC := \
	$(PTPPLATSRCDIR)/platform_clock.c \
	$(PTPPLATSRCDIR)/platform_os.c \
	$(PTPPLATSRCDIR)/platform_socket.c \
	$(PTPPLATSRCDIR)/platform_stream.c \
	$(PTPPLATSRCDIR)/platform_main_thd.c
	
PTPCSRC := \
	$(PTPPLATSRC) \
	$(PTPROOTSRC)
	
PTPINCDIRS := \
	$(PTPINCDIR) \
	$(PTPPLATINCDIR)
	
ALLCSRC +=  $(PTPCSRC)
ALLINC  += 	$(PTPINCDIRS)
	