# Simulink Model

In this folder, you can find a template for a Simulink model designed for the ASMO Software. The main concept, is to create a Simulink model, which contains inputs and outputs that can directly interact with the data provisioning layer. C code can be generated from this model and inserted into an ASMO project. There, the model is executed stepwise with a desired sample time.

In this folder, you find:
- A Simulink template, which can be used as a starting point to create your own model
	- It contains output and input blocks, which directly interact with the data provisioning layer
	- The model also contains settings, which are important to successfully generate code from the model
- A Resource folder, which contains files, that are needed by the Simulink model
	- There is the _message_id.m_ file, which holds information from the communication matrix to successfully interact with the data provisioning layer
		- It is generated with the Codegenerator
	- Moreover, here you can also find Matlab wrapper functions, which define the behavior of the input and output blocks

Quick start:
- Add the Resource folder to your Matlab path
- Open the Simulink template model and change it to your needs
- Generate Code via `Ctrl+B`
	- The generated code will be available in two subdirectories
	- *ModelName_ert_rtw* contains generated source and header files for the model
	- *slprj* contains a *_sharedutils* folder, which contains important Matlab functions
- Copy the source and header files from the previously mentioned folders into a new model folder in the general model folder of your ASMO project
- Adjust the source, header and Makefiles in the model folder of the ASMO project to include the newly generated files

