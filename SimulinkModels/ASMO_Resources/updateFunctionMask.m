function [ ] = updateFunctionMask( mask )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    t = ?safety_function;
    num = numel(t.EnumerationMemberList);
    functions{num} = []; 
    for i=1:num
        functions{i} = t.EnumerationMemberList(i).Name; 
    end
    

    msk = Simulink.Mask.get(mask);
    num = str2double(msk.getParameter('pNumFunctions').Value);
    for i=1:5
        f = strcat('pFunction', num2str(i));
        fs = strcat('psFunction', num2str(i));
        p = msk.getParameter(f);
        ps = msk.getParameter(fs);
        p.TypeOptions = functions;
        if i<=num
            p.Visible = 'on';
            ps.Value = strcat('safety_function.', p.Value);
        else
            p.Value = 'SET_MIN';
            p.Visible = 'off';
            ps.Value = '';
        end
    end
end

