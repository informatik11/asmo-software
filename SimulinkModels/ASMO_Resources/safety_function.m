classdef(Enumeration) safety_function < Simulink.IntEnumType
	enumeration
        NOOP(0),
        SET_MIN(1),
        SET_MAX(2)
    end
	methods( Static = true)
		function retVal = addClassNameToEnumNames()
			retVal = true;
		end
		function retVal = getDefaultValue()
			retVal = safety_function.NOOP;
		end
	end
    
end

