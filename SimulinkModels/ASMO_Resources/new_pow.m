function [ new_num ] = next_pow(num)
    n_log = log2(num);
    new_num = num;
    if ~isinteger(n_log)
        new_num = (round(n_log)+1) * 2;
    end
end