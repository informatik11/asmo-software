function [ ] = updateGetMeasurement(mask)
%updateGetMeasurement update Outputs of GetMeasurement-Block
%   Create output if "Get Timestamp" is checked and remove output
%   otherwise.

    msk = Simulink.Mask.get(mask);
    
    cName = 'getMeasurement';
    ref_block = 'value';
    outName = 'timestamp';
    termName = 'termTS';
    getTS = strcmp(msk.getParameter('pTimestamp').Value, 'on');
    block_out = getSimulinkBlockHandle([mask '/' outName]);
    block_term = getSimulinkBlockHandle([mask '/' termName]);
    if block_out>-1 && ~getTS 
        replaceBlocks(mask, outName, 'built-in/Terminator', termName, cName, 2);
    elseif block_term>-1 && getTS
        replaceBlocks(mask, termName, [mask '/' ref_block], outName, cName, 2);
    end

end

function [ ] = replaceBlocks(mask, block_old, block_new, new_name, block_connect, pos_out)
    bo = getSimulinkBlockHandle([mask '/' block_old]);
    bc = getSimulinkBlockHandle([mask '/' block_connect]);
    pos = get_param(bo, 'Position');
    t = get_param(bc,'PortHandles');
    out = t.Outport;
    line = get_param(out(pos_out), 'Line');
    if line>-1
        delete_line(line);
    end
    delete_block(bo);
    add_block(block_new, [mask '/', new_name], 'Position',pos);
    add_line(mask, [block_connect '/' num2str(pos_out)], [new_name '/1']);
end
