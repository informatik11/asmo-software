function [ resultMap ] = getIDParts( strID )
    resultMap = containers.Map;
    c = strsplit( strID, '_');
    cn = numel(c);
    if cn>1
        x=0;
        for i=c
            x=x+1;
            if all(isstrprop(char(i),'digit')) && x<cn
                break;
            end
        end
        if x==cn
            resultMap('dev') = {char(c(2))};
            if cn>2
                s = c(3);
                for i=4:cn
                    s = strcat(s,'_',c(i));
                end
                resultMap('key') = {char(s)};
            else
                resultMap('key') = {char('empty')};
            end
            resultMap('devno') = {char('empty')};
        else
            s = c(2);
            for i=3:(x-1)
                s = strcat(s,'_',c(i));
            end
            resultMap('dev') = {char(s)};
            resultMap('devno') = {char(c(x))};

            if cn>x
                s = c(x+1);
                for i=(x+2):cn
                    s = strcat(s,'_',c(i));
                end
            else
                s = 'empty';
            end
            resultMap('key') = {char(s)};
        end
    end
end