
#define S_FUNCTION_NAME                AddDefine_sfun
#define S_FUNCTION_LEVEL               2

#include <limits.h>
#include <stddef.h>
#include "string.h"
/*
 * Need to include simstruc.h for the definition of the SimStruct and
 * its associated macro definitions.
 */
#include "simstruc.h"

#define EDIT_OK(S, P_IDX) \
(!((ssGetSimMode(S)==SS_SIMMODE_SIZES_CALL_ONLY) && mxIsEmpty(ssGetSFcnParam(S, P_IDX))))

/* Enable user-defined implementation of optional functions: */
#define MDL_CHECK_PARAMETERS
#define MDL_SET_INPUT_PORT_DIMENSION_INFO
#define MDL_SET_OUTPUT_PORT_DIMENSION_INFO
#define MDL_SET_DEFAULT_PORT_DIMENSION_INFO
#define MDL_SET_WORK_WIDTHS
#define MDL_START
#define MDL_RTW

/* Error strings: */
#define PARAMETER_ACCESS_ERR_MSG  "There was an error getting the parameter."
#define STRING_EXTRACTION_ERR_MSG "Extraction of a string from the block parameter failed."
#define BUFFER_ALLOCATION_ERR_MSG "Unable to allocate buffer memory to be used for putting the dialog parameter in the RTW structure."
#define RTW_STRUCT_WRITE_ERR_MSG  "There was an error writing to the RTW structure."
#define STRING_TOO_LONG_ERR_MSG   "The dialog parameter string is too long to copy."

#define MX_GET_STRING_SUCCESS ( (int)0 )

typedef enum
{
    STRING_PARAM_DIALOG_INDEX = 0
}DialogParamterIndexEnumType;

/* We need to take the address of this string, so it cannot be a #define. */
static const char * const RTW_STRUCT_STRING_PARAM_NAME = "StringParameter";



#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)

/* Function: mdlCheckParameters ===========================================
 * Abstract:
 *    mdlCheckParameters verifies new parameter settings whenever parameter
 *    change or are re-evaluated during a simulation. When a simulation is
 *    running, changes to S-function parameters can occur at any time during
 *    the simulation loop.
 */
static void mdlCheckParameters(SimStruct *S)
{
    /*
     * Check the parameter 1
     */
    if EDIT_OK(S, 0) {
        int_T *dimsArray = (int_T *) mxGetDimensions(ssGetSFcnParam(S, 0));
        bool isString = mxIsChar (ssGetSFcnParam(S, 0));
        
        /* Parameter 1 must be a vector */
        if ((dimsArray[0] > 1) && (dimsArray[1] > 1)) {
            ssSetErrorStatus(S,"Parameter 1 must be a vector");
            return;
        }
        /* Parameter 1 must be a string*/
         if (!(isString))
        {
             ssSetErrorStatus(S,"Parameter 1 must be a String");
             return;
         }
        
        /* Check the parameter attributes */
        // ssCheckSFcnParamValueAttribs(S, 0, "P1", DYNAMICALLY_TYPED, 2, dimsArray, 0);
    }
}

#endif

/* Function: mdlInitializeSizes ===========================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    /* Number of expected parameters */
    ssSetNumSFcnParams(S, 1);
    
#if defined(MATLAB_MEX_FILE)

if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
    /*
     * If the number of expected input parameters is not equal
     * to the number of parameters entered in the dialog box return.
     * Simulink will generate an error indicating that there is a
     * parameter mismatch.
     */
    mdlCheckParameters(S);
    if (ssGetErrorStatus(S) != NULL) {
        return;
    }
} else {
    /* Return if number of expected != number of actual parameters */
    return;
}

#endif

/* Set the parameter's tunability */
ssSetSFcnParamTunable(S, 0, 0);

/*
 * Set the number of pworks.
 */
ssSetNumPWork(S, 0);

/*
 * Set the number of dworks.
 */
if (!ssSetNumDWork(S, 0))
    return;

/*
 * Set the number of input ports.
 */
if (!ssSetNumInputPorts(S, 0))
    return;

/*
 * Configure the input port 1
 */
// ssSetInputPortDataType(S, 0, SS_UINT8);
// ssSetInputPortWidth(S, 0, DYNAMICALLY_SIZED);
// ssSetInputPortComplexSignal(S, 0, COMPLEX_NO);
// ssSetInputPortDirectFeedThrough(S, 0, 1);
// ssSetInputPortAcceptExprInRTW(S, 0, 0);
// ssSetInputPortOverWritable(S, 0, 0);
// ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
// ssSetInputPortRequiredContiguous(S, 0, 1);

/*
 * Set the number of output ports.
 */
if (!ssSetNumOutputPorts(S, 0))
    return;

/*
 * This S-function can be used in referenced model simulating in normal mode.
 */
ssSetModelReferenceNormalModeSupport(S, MDL_START_AND_MDL_PROCESS_PARAMS_OK);

/*
 * Set the number of sample time.
 */
ssSetNumSampleTimes(S, 1);

/*
 * All options have the form SS_OPTION_<name> and are documented in
 * matlabroot/simulink/include/simstruc.h. The options should be
 * bitwise or'd together as in
 *   ssSetOptions(S, (SS_OPTION_name1 | SS_OPTION_name2))
 */
ssSetOptions(S,
        SS_OPTION_USE_TLC_WITH_ACCELERATOR |
        SS_OPTION_CAN_BE_CALLED_CONDITIONALLY |
        SS_OPTION_EXCEPTION_FREE_CODE |
        SS_OPTION_WORKS_WITH_CODE_REUSE |
        SS_OPTION_SFUNCTION_INLINED_FOR_RTW |
        SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
}

/* Function: mdlInitializeSampleTimes =====================================
 * Abstract:
 *    This function is used to specify the sample time(s) for your
 *    S-function. You must register the same number of sample times as
 *    specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
    
#if defined(ssSetModelReferenceSampleTimeDefaultInheritance)

ssSetModelReferenceSampleTimeDefaultInheritance(S);

#endif

}


#if defined(MDL_SET_INPUT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)

/* Function: mdlSetInputPortDimensionInfo =================================
 * Abstract:
 *    This method is called with the candidate dimensions for an input port
 *    with unknown dimensions. If the proposed dimensions are acceptable, the
 *    method should go ahead and set the actual port dimensions.
 *    If they are unacceptable an error should be generated via
 *    ssSetErrorStatus.
 *    Note that any other input or output ports whose dimensions are
 *    implicitly defined by virtue of knowing the dimensions of the given
 *    port can also have their dimensions set.
 *
 */
static void mdlSetInputPortDimensionInfo(SimStruct *S, int_T portIndex, const
        DimsInfo_T *dimsInfo)
{
    /* /* Set input port dimension */
    if(!ssSetInputPortDimensionInfo(S, portIndex, dimsInfo)) return;
    
    
}
#endif



#if defined(MDL_SET_OUTPUT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
/* Function: mdlSetOutputPortDimensionInfo ================================
 * Abstract:
 *    This method is called with the candidate dimensions for an output port
 *    with unknown dimensions. If the proposed dimensions are acceptable, the
 *    method should go ahead and set the actual port dimensions.
 *    If they are unacceptable an error should be generated via
 *    ssSetErrorStatus.
 *    Note that any other input or output ports whose dimensions are
 *    implicitly defined by virtue of knowing the dimensions of the given
 *    port can also have their dimensions set.
 *
 */
static void mdlSetOutputPortDimensionInfo(SimStruct *S, int_T portIndex, const DimsInfo_T *dimsInfo)
{
    /* Set output port dimension */
    if(!ssSetOutputPortDimensionInfo(S, portIndex, dimsInfo)) return;
    
}
#endif


#if defined(MDL_SET_DEFAULT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
/* Function: mdlSetDefaultPortDimensionInfo ===============================
 * Abstract:
 *    This method is called when there is not enough information in your
 *    model to uniquely determine the port dimensionality of signals
 *    entering or leaving your block. When this occurs, Simulink's
 *    dimension propagation engine calls this method to ask you to set
 *    your S-functions default dimensions for any input and output ports
 *    that are dynamically sized.
 *
 *    If you do not provide this method and you have dynamically sized ports
 *    where Simulink does not have enough information to propagate the
 *    dimensionality to your S-function, then Simulink will set these unknown
 *    ports to the 'block width' which is determined by examining any known
 *    ports. If there are no known ports, the width will be set to 1.
 *
 */
static void mdlSetDefaultPortDimensionInfo(SimStruct *S)
{
    /* Set input port 1 default dimension */
    if (ssGetInputPortWidth(S, 0) == DYNAMICALLY_SIZED) {
        ssSetInputPortWidth(S, 0, 1);
    }
    
}
#endif
#if defined(MDL_SET_WORK_WIDTHS) && defined(MATLAB_MEX_FILE)
/* Function: mdlSetWorkWidths =============================================
 * Abstract:
 *      The optional method, mdlSetWorkWidths is called after input port
 *      width, output port width, and sample times of the S-function have
 *      been determined to set any state and work vector sizes which are
 *      a function of the input, output, and/or sample times.
 *
 *      Run-time parameters are registered in this method using methods
 *      ssSetNumRunTimeParams, ssSetRunTimeParamInfo, and related methods.
 */
static void mdlSetWorkWidths(SimStruct *S)
{
    /* Set number of run-time parameters */
    //if (!ssSetNumRunTimeParams(S, 1)) return;
    
// /*
//  * Register the run-time parameter 1
//  */
// ssRegDlgParamAsRunTimeParam(S, 0, 0, "p1", ssGetDataTypeId(S, "uint8"));
    
}
#endif

#if defined(MDL_START)
/* Function: mdlStart =====================================================
 * Abstract:
 *    This function is called once at start of model execution. If you
 *    have states that should be initialized once, this is the place
 *    to do it.
 */
static void mdlStart(SimStruct *S)
{
 const mxArray * const dialog_parameter =
            ssGetSFcnParam( S, STRING_PARAM_DIALOG_INDEX );
 if( NULL == dialog_parameter )
    {
        ssSetErrorStatus( S, PARAMETER_ACCESS_ERR_MSG );
    }
    else
    {
        /* Add one extra character's worth for the '\0' at the end. */
        const size_t buffer_length =
                ( ( mxGetN( dialog_parameter ) + 1 ) * sizeof( char ) );
        
        /* The buffer size must later be cast to an "int", so we need to
         * ensure that the cast is safe.
         */
        if( buffer_length < INT_MAX )
        {
            char * const dialog_parameter_string =
                    mxMalloc( buffer_length );
            
            if( NULL == dialog_parameter_string )
            {
                ssSetErrorStatus( S, BUFFER_ALLOCATION_ERR_MSG );
            }
            else
            {
                /* The check against "INT_MAX" ensures that the cast 
                 * is safe.
                 */
                const size_t string_extraction_status =
                        mxGetString( dialog_parameter,
                                     dialog_parameter_string,
                                     ( (int)buffer_length ) );
                
                if( MX_GET_STRING_SUCCESS == string_extraction_status )
                {
                    printf("Adding custom define to code: #define %s\n", dialog_parameter_string);
                }
                else
                {
                    ssSetErrorStatus( S, STRING_EXTRACTION_ERR_MSG );
                }
                mxFree( dialog_parameter_string );
                }
            }
        
        else
        {
            /* The cast from "size_t" to "int" isn't safe.*/
            ssSetErrorStatus( S, STRING_TOO_LONG_ERR_MSG )
        }
    }
    return;
}
#endif


/* Function: mdlOutputs ===================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block. Generally outputs are placed in the output vector(s),
 *    ssGetOutputPortSignal.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    /*
     * Get access to Parameter/Input/Output/DWork/size information
     */
    uint8_T *u1 = (uint8_T *) ssGetInputPortSignal(S, 0);
}

/* Function: mdlTerminate =================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.
 */
static void mdlTerminate(SimStruct *S)

   {
 const mxArray * const dialog_parameter =
            ssGetSFcnParam( S, STRING_PARAM_DIALOG_INDEX );
 if( NULL == dialog_parameter )
    {
        ssSetErrorStatus( S, PARAMETER_ACCESS_ERR_MSG );
    }
    else
    {
        /* Add one extra character's worth for the '\0' at the end. */
        const size_t buffer_length =
                ( ( mxGetN( dialog_parameter ) + 1 ) * sizeof( char ) );
        
        /* The buffer size must later be cast to an "int", so we need to
         * ensure that the cast is safe.
         */
        if( buffer_length < INT_MAX )
        {
            char * const dialog_parameter_string =
                    mxMalloc( buffer_length );
            
            if( NULL == dialog_parameter_string )
            {
                ssSetErrorStatus( S, BUFFER_ALLOCATION_ERR_MSG );
            }
            else
            {
                /* The check against "INT_MAX" ensures that the cast 
                 * is safe.
                 */
                const size_t string_extraction_status =
                        mxGetString( dialog_parameter,
                                     dialog_parameter_string,
                                     ( (int)buffer_length ) );
                
                if( MX_GET_STRING_SUCCESS == string_extraction_status )
                {
                    // printf("Terminate: The String that is a mask parameter is: %s\n", dialog_parameter_string);
                }
                else
                {
                    ssSetErrorStatus( S, STRING_EXTRACTION_ERR_MSG );
                }
                mxFree( dialog_parameter_string );
                }
            }
        
        else
        {
            /* The cast from "size_t" to "int" isn't safe.*/
            ssSetErrorStatus( S, STRING_TOO_LONG_ERR_MSG )
        }
    }
    return;
}


#if defined(MDL_RTW) && defined(MATLAB_MEX_FILE)
/* Function: mdlRTW ===========================================================
 * Abstract:
 * Assign the string from the dialog parameter into the RTW structure so
 * that it can be extracted by the TLC file.
 */
static void mdlRTW( SimStruct *S )
{
    const mxArray * const dialog_parameter =
            ssGetSFcnParam( S, STRING_PARAM_DIALOG_INDEX );
    
    if( NULL == dialog_parameter )
    {
        ssSetErrorStatus( S, PARAMETER_ACCESS_ERR_MSG );
    }
    else
    {
        /* Add one extra character's worth for the '\0' at the end. */
        const size_t buffer_length =
                ( ( mxGetN( dialog_parameter ) + 1 ) * sizeof( char ) );
        
        /* The buffer size must later be cast to an "int", so we need to
         * ensure that the cast is safe.
         */
        if( buffer_length < INT_MAX )
        {
            char * const dialog_parameter_string =
                    mxMalloc( buffer_length );
            
            if( NULL == dialog_parameter_string )
            {
                ssSetErrorStatus( S, BUFFER_ALLOCATION_ERR_MSG );
            }
            else
            {
                /* The check against "INT_MAX" ensures that the cast 
                 * is safe.
                 */
                const size_t string_extraction_status =
                        mxGetString( dialog_parameter,
                                     dialog_parameter_string,
                                     ( (int)buffer_length ) );
                
                if( MX_GET_STRING_SUCCESS == string_extraction_status )
                {
                    const int_T rtw_structure_assignment_result
                            = ssWriteRTWStrParam( S,
                                                  RTW_STRUCT_STRING_PARAM_NAME,
                                                  dialog_parameter_string );
                    
                    if( 1 == rtw_structure_assignment_result )
                    {
                        /* Things went well. */
                    }
                    else
                    {
                        ssSetErrorStatus( S, RTW_STRUCT_WRITE_ERR_MSG );
                    }
                }
                else
                {
                    ssSetErrorStatus( S, STRING_EXTRACTION_ERR_MSG );
                }
                mxFree( dialog_parameter_string );
            }
        }
        else
        {
            /* The cast from "size_t" to "int" isn't safe.*/
            ssSetErrorStatus( S, STRING_TOO_LONG_ERR_MSG )
        }
    }
    return;
}
#endif /* defined(MDL_RTW) && defined(MATLAB_MEX_FILE) */

/*
 * Required S-function trailer
 */
#ifdef    MATLAB_MEX_FILE
# include "simulink.c"
#else
# include "cg_sfun.h"
#endif
