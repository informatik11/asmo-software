function [ ] = updateMask( mask, use, param, dl, timeseries )

msk = Simulink.Mask.get(mask);
p = msk.Parameters;
dev = get_param(mask,'pDev');
devno = get_param(mask, 'pDevNo');
key = get_param(mask, 'pKey');
dp = get_param(mask, 'DialogParameters');

% Liste der Device-No aus der IDMap lesen 
map = evalin('base', strcat('IDMap(''',dev,''')'));
list = map('devnos');

s = '';
s2 = '';

% Dropdown-Liste Device wurde ge�ndert
if dl<1
    
    % Device-No Liste wird aktualisiert
    p(2).TypeOptions = list;

    % pr�fen, ob der eingetragene Wert in neuer Liste 
    % vorkommt
    b = true;
    
    l = p(2).TypeOptions;
    for i = 1:numel(l)
        s = char(l{i});
        if strcmp(s, devno)
            b = false;
            break;
        end
    end
    
    % wenn der Wert nicht in neuer Liste Device-No vorkommt auf den ersten
    % verf�gbaren Wert setzen
    if b
        devno = char(list{1});
        set_param(mask,'pDevNo',devno);
    end
end

map = map(char(devno));
list = map('keys');

% Dropdown-Liste Device oder Device-No wurde ge�ndert
if dl<2

    % Key Liste wird aktualisiert
    p(3).TypeOptions = list;

    % pr�fen, ob der eingetragene Wert in neuer Liste 
    % vorkommt
    b = true;

    l=p(3).TypeOptions;
    for i = 1:numel(l)
        if strcmp(char(l{i}), key)
            b = false;
            break;
        end
    end
    
    % wenn der Wert nicht in neuer Liste Key vorkommt auf den ersten
    % verf�gbaren Wert setzen
    if b
        key = char(list{1});
        set_param(mask, 'pKey', key);
    end
end

% eine der Dropdown-Listen wurde ge�ndert
if dl<3
    s = strcat('ID_', dev);
    s2 = s;
    if ~strcmp(devno, 'empty')
        s = strcat(s, '_', devno);
        s2 = strcat(s2, '_X');
    end
    if ~strcmp(key, 'empty')
        s = strcat(s, '_', key);
        s2 = strcat(s2, '_', key);
    end
    b=false;
    for i=evalin('base', strcat('IDMap(''IDs'')'))
        if strcmp(s, char(i))
            b=true;
            break;
        end
    end
else
    s = get_param(mask, 'pID');
    s2 = s;
    if ~strcmp(devno, 'empty')
        s2 = strrep(s,strcat('_', devno, '_'),'_X_');
    end
    if timeseries == 1
        s2 = get_param(mask, 'pID_X');
    end
    b = true;
end

% es wurde ein g�ltiger Eintrag erstellt
if b
    set_param(mask, 'pID', s);
    set_param(mask, 'IDType', strcat(use, s));
	if use == "ACQUIRE_" 
		set_param(mask, 'IDType', strrep(strcat(use, s),strcat('_', devno, '_'),'_X_'));
	end
    if isfield(dp, 'IDTypeX')
        set_param(mask, 'IDTypeX', strcat(use, s2));
    end
    set_param(mask, 'bValue', strcat('message_id.', s));
    
    if timeseries == 1 
        if ~strcmp(devno,'empty')
            dn = sscanf(devno, '%d');
            set_param(mask, 'pDevNoI', num2str(dn));
        end
        small = s(4:numel(s));
        set_param(mask, 'pID_Small', lower(small));
        if timeseries==2 
            set_param(mask, 'pID_X2', strcat(use, s2));
        end
        set_param(mask, 'pID_X', s2);
        
        clear s2;
        clear small;
    end
end

% update icon size to fit the text
if strcmp(param,'icon')
    v = get_param( mask, 'Position');
    l = length(strrep(s,'ID_',''));
    n = 8;
    if timeseries==1
        n = 9;
    end
    v(1) = v(3) - n*l;
    %v(3) = v(1) + 8*l;
    
    % update timeseries icon 
    if timeseries == 1  
        
        t = get_param(getSimulinkBlockHandle(strcat(mask,'/getTSMeasurement')),'PortHandles');
        out = t.Outport;
        for x = 3:numel(out) 
            line = get_param(out(x), 'Line');
            if line>-1
                n = get_param(line, 'DstBlockHandle');
                delete_line(line);
                delete_block(n);
            end
        end
        
        BlockPaths = find_system(mask,'SearchDepth',2,'LookUnderMasks','all','BlockType','Terminator');
        for x = 1:numel(BlockPaths)
            line = find_system(BlockPaths{x}, 'Type', 'line');
            for y = 1:numel(line)
                if line{y} >-1
                    delete_line(line{y});
                end
            end
            delete_block(BlockPaths{x});
        end
        
        t = get_param(getSimulinkBlockHandle(strcat(mask,'/timestamps')),'Position');
        xmin = strcmp(get_param(mask, 'pMin'), 'on');
        xmax = strcmp(get_param(mask, 'pMax'), 'on');
        xavg = strcmp(get_param(mask, 'pAvg'), 'on');
        ports = 0; 
        
        pos = [t(1) t(4)+16 t(3) t(4)+30];
        if xmin 
            add_block('built-in/Outport', [mask '/min'], 'Position',pos);
            add_line(mask, 'getTSMeasurement/3', 'min/1');
            ports = ports + 1;
        else
            add_block('built-in/Terminator', [mask '/TermMin'], 'Position',pos);
            add_line(mask, 'getTSMeasurement/3', 'TermMin/1');
        end
        
        pos = [t(1) t(4)+46 t(3) t(4)+60];
        if xmax 
            add_block('built-in/Outport', [mask '/max'], 'Position',pos);
            add_line(mask, 'getTSMeasurement/4', 'max/1');
            ports = ports + 1;
        else
            add_block('built-in/Terminator', [mask '/TermMax'], 'Position',pos);
            add_line(mask, 'getTSMeasurement/4', 'TermMax/1');
        end
        
        pos = [t(1) t(4)+76 t(3) t(4)+90];
        if xavg 
            add_block('built-in/Outport', [mask '/avg'], 'Position',pos);
            add_line(mask, 'getTSMeasurement/5', 'avg/1');
            ports = ports + 1;
        else
            add_block('built-in/Terminator', [mask '/TermAvg'], 'Position',pos);
            add_line(mask, 'getTSMeasurement/5', 'TermAvg/1');
        end
        
        v(1) = v(1) - 40;
        v(4) = v(2) + 30 + ports*8;
        
        clear ports;
        clear pos;
        clear t;
        clear out;
        clear line;
        clear n;
    elseif timeseries == 2  
        
        num = str2double(msk.getParameter('pNumFunctions').Value);
        ref_block = 'safetySet1';
        
        pos_block = get_param([mask '/' ref_block], 'Position');
        pos_in = get_param([mask '/In1'], 'Position'); 
        pos_func = get_param([mask '/Function1'], 'Position');
        
        for i = 2:5
            cName = strcat('safetySet',num2str(i));
            inName = strcat('In',num2str(i));
            funcName = strcat('Function',num2str(i));
            blocks = find_system(mask,'LookUnderMasks','on','FollowLinks','on','Name',cName);
            numBlocks = numel(blocks);
            if numBlocks == 1 && i>num
                t = get_param(getSimulinkBlockHandle([mask '/' cName]),'PortHandles');
                in = t.Inport;
                line = get_param(in(1), 'Line');
                if line>-1
                    delete_line(line);
                end
                line = get_param(in(2), 'Line');
                if line>-1
                    delete_line(line);
                end
                line = get_param(in(3), 'Line');
                if line>-1
                    delete_line(line);
                end
                delete_block(getSimulinkBlockHandle([mask '/' funcName]));
                delete_block(getSimulinkBlockHandle([mask '/' inName]));
                delete_block(blocks{1});
            elseif numBlocks==0 && i<=num
                add_block([mask '/' ref_block], [mask '/' cName], 'Position', [pos_block(1) pos_block(4)+30+60*(i-2) pos_block(3) pos_block(4)+60+60*(i-2)]);
                add_block([mask '/In1'], [mask '/' inName], 'Position', [pos_in(1) pos_in(4)+40+60*(i-2) pos_in(3) pos_in(4)+55+60*(i-2)]);
                add_block([mask '/Function1'], [mask '/' funcName], 'Position', [pos_func(1) pos_func(4)+40+60*(i-2) pos_func(3) pos_func(4)+55+60*(i-2)]);
                m = Simulink.Mask.get([mask '/' funcName]);
                p = m.getParameter('psFunction');
                p.Value = ['psFunction' num2str(i)];
                add_line(mask, [inName '/1'], [cName '/3'], 'autorouting','on');
                add_line(mask, [funcName '/1'], [cName '/2'], 'autorouting','on');
                add_line(mask, 'ID/1', [cName '/1'], 'autorouting','on');
            end
        end
        
        v(1) = v(1) - 40;
        v(4) = v(2) + 30 + num*8;
        
        clear ports;
        clear pos;
        clear t;
        clear in;
        clear line;
        clear n;
        clear num;
    end   
    
    set_param( mask, 'Position', v);
end

clear n;
clear v;
clear l;
clear b;
clear s;
clear s2;
clear msk;
clear p;
clear map;
clear dev;
clear devno;
clear key;
clear list;

end
