function [ ] = populateIDs( mask )
    
    msk = Simulink.Mask.get(mask);
    p = msk.getParameter('pID');
    map = evalin('base', strcat('IDMap(''IDs'')'));
    devs = evalin('base', 'IDMap(''devs'')');
    
    p.TypeOptions = map; 
    
    p = msk.getParameter('pDev');
    p.TypeOptions = devs;
    
    clear devs;
    clear map;
    clear p;
    clear msk;
    
end