function [  ] = toggleFunction( mask, add )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    msk = Simulink.Mask.get(mask);

    numParam = msk.getParameter('pNumFunctions');

    num = str2double(numParam.Value);
    
    if add == 1
        if num<5
            num = num + 1;
            paramName = strcat('pFunction', num2str(num));
            numParam.Value = num2str(num);
            msk.getParameter(paramName).Visible = 'on';
        end
    else
        if num>1
            
            paramName = strcat('pFunction', num2str(num));
            num = num - 1;
            numParam.Value = num2str(num);
            msk.getParameter(paramName).Visible = 'off';
        end
    end

end

