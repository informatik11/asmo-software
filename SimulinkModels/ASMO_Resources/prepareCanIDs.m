function [ ] = prepareCanIDs()
    if evalin('base', '~exist(''IDMap'',''var'');')
        resultMap = containers.Map;    
        resultMap('devs') = {};
        resultMap('numd') = 1;
        t = ?message_id;
        cntIDs=numel(t.EnumerationMemberList);
        ids{cntIDs} = [];
        numi = 1;
        for n = 1:cntIDs
            part = getIDParts(t.EnumerationMemberList(n).Name);
            ids{numi} = t.EnumerationMemberList(n).Name;
            numi = numi + 1;
            resultMap = addID( resultMap, part );
        end
        r = resultMap('devs');
        r = sort(r);
        ids = sort(ids);
        resultMap('devs') = r;
        resultMap('IDs') = ids;
        resultMap('numi') = numi;
        assignin('base', 'IDMap', resultMap);
    end
end

function [ resultMap ] = addID( resultMap, ID )
    if isKey(ID, 'dev')
        if ~isKey( resultMap, ID('dev'))
            r = containers.Map;
            r('devnos') = {};
            r('numdn') = 1;
            t = resultMap('devs');
            t{resultMap('numd')} = char(ID('dev'));
            resultMap('devs') = t;
            resultMap('numd') = resultMap('numd') + 1;
            resultMap(char(ID('dev'))) = r;
        end
        r = resultMap(char(ID('dev')));
        devno = char(ID('devno'));
        if ~isKey(r, devno)
            dn = r('devnos');
            ndn = r('numdn');
            dn{ndn} = char(ID('devno'));
            dn = sort_nat(dn);
            r('devnos') = dn;
            r('numdn') = ndn + 1;
            dnm = containers.Map;
            dnm('keys') = {};
            dnm('numk') = 1;
            r(devno) = dnm;
        end
        dnm = r(devno);
        k = dnm('keys');
        nk = dnm('numk');
        k{nk} = char(ID('key'));
        k = unique(k);
        k = sort_nat(k);
        nk = numel(k)+1;
        dnm('keys') = k;
        dnm('numk') = nk;
        r(devno) = dnm;
        resultMap(char(ID('dev'))) = r;
    end
end

