function [ ] = next_pow( mask )
    num = str2num(get_param(mask, 'pWindow'));
    old = str2num(get_param(mask, 'pWindowOld'));
    
    n_log = log2(num);
    dir = 0;
    if old<num
        dir = 1;
    elseif old>num
        %dir = -1;
    end
    if ~isinteger(n_log)
        num = 2^(floor(n_log)+dir);
    end
    
    set_param(gcb, 'pWindowOld', num2str(num));
    set_param(gcb, 'pWindow', num2str(num));
    
end