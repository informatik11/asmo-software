/**
 * \file ASMO_simulinkFactors.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#ifndef MATLAB_MEX_FILE
#include "ASMO_simulinkFactors.h"
#endif
uint32_t simulink_getFactor(uint32_t id, double value) {
    switch (id) {
#if defined(ACQUIRE_ID_PERFORMANCE_COUNTER) || defined (USE_ID_PERFORMANCE_COUNTER)
    case ID_PERFORMANCE_COUNTER:

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_TIME_BEACON) || defined (USE_ID_TIME_BEACON)
    case ID_TIME_BEACON:

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW) || defined (USE_ID_N560_0_MEASUREMENT_TOO_LOW)|| defined (USE_ID_N560_1_MEASUREMENT_TOO_LOW) 
    case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
    case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH) || defined (USE_ID_N560_0_MEASUREMENT_TOO_HIGH)|| defined (USE_ID_N560_1_MEASUREMENT_TOO_HIGH) 
    case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
    case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING) || defined (USE_ID_N560_0_ACQUIRE_MISSING)|| defined (USE_ID_N560_1_ACQUIRE_MISSING) 
    case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
    case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_SAFETY_SET) || defined (USE_ID_SAFETY_SET)
    case ID_SAFETY_SET:

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_SAFETY_TIMEOUT) || defined (USE_ID_SAFETY_TIMEOUT)
    case ID_SAFETY_TIMEOUT:

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_STARTUP) || defined (USE_ID_N560_0_STARTUP)|| defined (USE_ID_N560_1_STARTUP) 
    case (ID_N560_X_STARTUP | (0 << 12)):
    case (ID_N560_X_STARTUP | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_CARD_DISABLED) || defined (USE_ID_N560_0_CARD_DISABLED)|| defined (USE_ID_N560_1_CARD_DISABLED) 
    case (ID_N560_X_CARD_DISABLED | (0 << 12)):
    case (ID_N560_X_CARD_DISABLED | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_STATUS_REPLY) || defined (USE_ID_N560_0_STATUS_REPLY)|| defined (USE_ID_N560_1_STATUS_REPLY) 
    case (ID_N560_X_STATUS_REPLY | (0 << 12)):
    case (ID_N560_X_STATUS_REPLY | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_ALIVE) || defined (USE_ID_N560_0_ALIVE)|| defined (USE_ID_N560_1_ALIVE) 
    case (ID_N560_X_ALIVE | (0 << 12)):
    case (ID_N560_X_ALIVE | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_RESEND_STARTUP) || defined (USE_ID_RESEND_STARTUP)
    case ID_RESEND_STARTUP:

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_USED_IDS) || defined (USE_ID_N560_0_USED_IDS)|| defined (USE_ID_N560_1_USED_IDS) 
    case (ID_N560_X_USED_IDS | (0 << 12)):
    case (ID_N560_X_USED_IDS | (1 << 12)):

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_SEND_USED_IDS) || defined (USE_ID_SEND_USED_IDS)
    case ID_SEND_USED_IDS:

            return (unsigned long)(value / 1);
#endif
#if defined(ACQUIRE_ID_N560_X_SPO2) || defined (USE_ID_N560_0_SPO2)|| defined (USE_ID_N560_1_SPO2) 
    case (ID_N560_X_SPO2 | (0 << 12)):
    case (ID_N560_X_SPO2 | (1 << 12)):

            return (unsigned long)(value / 1.0);
#endif
#if defined(ACQUIRE_ID_N560_X_BPM) || defined (USE_ID_N560_0_BPM)|| defined (USE_ID_N560_1_BPM) 
    case (ID_N560_X_BPM | (0 << 12)):
    case (ID_N560_X_BPM | (1 << 12)):

            return (unsigned long)(value / 1.0);
#endif
#if defined(ACQUIRE_ID_N560_X_PA) || defined (USE_ID_N560_0_PA)|| defined (USE_ID_N560_1_PA) 
    case (ID_N560_X_PA | (0 << 12)):
    case (ID_N560_X_PA | (1 << 12)):

            return (unsigned long)(value / 1.0);
#endif
#if defined(ACQUIRE_ID_N560_X_STATUS) || defined (USE_ID_N560_0_STATUS)|| defined (USE_ID_N560_1_STATUS) 
    case (ID_N560_X_STATUS | (0 << 12)):
    case (ID_N560_X_STATUS | (1 << 12)):

            return (unsigned long)(value / 1.0);
#endif
        default:
            break;
    }
    return 0;
}
