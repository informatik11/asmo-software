/**
 * \file ASMO_measurements.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#include "SmartECLA_allHeaders.h"

//! Holds the current medical data.
ASMO_Data measurements = {
        #if defined(USE_ID_PERFORMANCE_COUNTER) || defined(ACQUIRE_ID_PERFORMANCE_COUNTER)
        { ID_PERFORMANCE_COUNTER, 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_TIME_BEACON) || defined(ACQUIRE_ID_TIME_BEACON)
        { ID_TIME_BEACON, 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
        { (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
        { (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
        { (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
        { (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
        { (ID_N560_X_ACQUIRE_MISSING | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
        { (ID_N560_X_ACQUIRE_MISSING | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_SAFETY_SET) || defined(ACQUIRE_ID_SAFETY_SET)
        { ID_SAFETY_SET, 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_SAFETY_TIMEOUT) || defined(ACQUIRE_ID_SAFETY_TIMEOUT)
        { ID_SAFETY_TIMEOUT, 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
        { (ID_N560_X_STARTUP | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
        { (ID_N560_X_STARTUP | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
        { (ID_N560_X_CARD_DISABLED | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
        { (ID_N560_X_CARD_DISABLED | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
        { (ID_N560_X_STATUS_REPLY | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
        { (ID_N560_X_STATUS_REPLY | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
        { (ID_N560_X_ALIVE | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
        { (ID_N560_X_ALIVE | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_RESEND_STARTUP) || defined(ACQUIRE_ID_RESEND_STARTUP)
        { ID_RESEND_STARTUP, 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
        { (ID_N560_X_USED_IDS | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
        { (ID_N560_X_USED_IDS | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_SEND_USED_IDS) || defined(ACQUIRE_ID_SEND_USED_IDS)
        { ID_SEND_USED_IDS, 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
        { (ID_N560_X_SPO2 | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
        { (ID_N560_X_SPO2 | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
        { (ID_N560_X_BPM | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
        { (ID_N560_X_BPM | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_PA) || defined(ACQUIRE_ID_N560_X_PA)
        { (ID_N560_X_PA | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_PA) || defined(ACQUIRE_ID_N560_X_PA)
        { (ID_N560_X_PA | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_0_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
        { (ID_N560_X_STATUS | (0 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
        #if defined(USE_ID_N560_1_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
        { (ID_N560_X_STATUS | (1 << 12)), 0, TS_ZERO_VALUE },
        0,
        4294967295,
        #endif
};

//! Holds the current medical data with Message configuration.
ASMO_Data_Config acquire_measurements = {
        #ifdef ACQUIRE_ID_PERFORMANCE_COUNTER
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_TIME_BEACON
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_ACQUIRE_MISSING
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_ACQUIRE_MISSING
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_SAFETY_SET
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_SAFETY_TIMEOUT
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_STARTUP
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_STARTUP
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_CARD_DISABLED
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_CARD_DISABLED
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_STATUS_REPLY
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_STATUS_REPLY
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_ALIVE
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_ALIVE
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_RESEND_STARTUP
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_USED_IDS
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_USED_IDS
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_SEND_USED_IDS
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_SPO2
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_SPO2
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_BPM
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_BPM
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_PA
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_PA
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_STATUS
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
        #ifdef ACQUIRE_ID_N560_X_STATUS
        { 
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            TIME_MS2I(0),
            TS_ZERO_VALUE,
            0,
            {},
            0
        },
        #endif
};

//! Stores the received ASMO_Measurement in ::measurements.
void ASMO_storeMeasurement(ASMO_Measurement *measurement) {

    switch(measurement->messageId) {
        #if defined(USE_ID_PERFORMANCE_COUNTER) || defined(ACQUIRE_ID_PERFORMANCE_COUNTER)
            case ID_PERFORMANCE_COUNTER:
                if(ASMO_safetyCheck(measurement, measurements.performance_counter_minimum, measurements.performance_counter_maximum)) {
                    measurements.performance_counter = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_TIME_BEACON) || defined(ACQUIRE_ID_TIME_BEACON)
            case ID_TIME_BEACON:
                if(ASMO_safetyCheck(measurement, measurements.time_beacon_minimum, measurements.time_beacon_maximum)) {
                    measurements.time_beacon = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_measurement_too_low_minimum, measurements.n560_0_measurement_too_low_maximum)) {
                    measurements.n560_0_measurement_too_low = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_measurement_too_low_minimum, measurements.n560_1_measurement_too_low_maximum)) {
                    measurements.n560_1_measurement_too_low = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_measurement_too_high_minimum, measurements.n560_0_measurement_too_high_maximum)) {
                    measurements.n560_0_measurement_too_high = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_measurement_too_high_minimum, measurements.n560_1_measurement_too_high_maximum)) {
                    measurements.n560_1_measurement_too_high = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
            case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_acquire_missing_minimum, measurements.n560_0_acquire_missing_maximum)) {
                    measurements.n560_0_acquire_missing = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
            case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_acquire_missing_minimum, measurements.n560_1_acquire_missing_maximum)) {
                    measurements.n560_1_acquire_missing = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_SAFETY_SET) || defined(ACQUIRE_ID_SAFETY_SET)
            case ID_SAFETY_SET:
                if(ASMO_safetyCheck(measurement, measurements.safety_set_minimum, measurements.safety_set_maximum)) {
                    measurements.safety_set = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_SAFETY_TIMEOUT) || defined(ACQUIRE_ID_SAFETY_TIMEOUT)
            case ID_SAFETY_TIMEOUT:
                if(ASMO_safetyCheck(measurement, measurements.safety_timeout_minimum, measurements.safety_timeout_maximum)) {
                    measurements.safety_timeout = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
            case (ID_N560_X_STARTUP | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_startup_minimum, measurements.n560_0_startup_maximum)) {
                    measurements.n560_0_startup = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
            case (ID_N560_X_STARTUP | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_startup_minimum, measurements.n560_1_startup_maximum)) {
                    measurements.n560_1_startup = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
            case (ID_N560_X_CARD_DISABLED | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_card_disabled_minimum, measurements.n560_0_card_disabled_maximum)) {
                    measurements.n560_0_card_disabled = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
            case (ID_N560_X_CARD_DISABLED | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_card_disabled_minimum, measurements.n560_1_card_disabled_maximum)) {
                    measurements.n560_1_card_disabled = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
            case (ID_N560_X_STATUS_REPLY | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_status_reply_minimum, measurements.n560_0_status_reply_maximum)) {
                    measurements.n560_0_status_reply = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
            case (ID_N560_X_STATUS_REPLY | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_status_reply_minimum, measurements.n560_1_status_reply_maximum)) {
                    measurements.n560_1_status_reply = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
            case (ID_N560_X_ALIVE | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_alive_minimum, measurements.n560_0_alive_maximum)) {
                    measurements.n560_0_alive = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
            case (ID_N560_X_ALIVE | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_alive_minimum, measurements.n560_1_alive_maximum)) {
                    measurements.n560_1_alive = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_RESEND_STARTUP) || defined(ACQUIRE_ID_RESEND_STARTUP)
            case ID_RESEND_STARTUP:
                if(ASMO_safetyCheck(measurement, measurements.resend_startup_minimum, measurements.resend_startup_maximum)) {
                    measurements.resend_startup = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
            case (ID_N560_X_USED_IDS | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_used_ids_minimum, measurements.n560_0_used_ids_maximum)) {
                    measurements.n560_0_used_ids = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
            case (ID_N560_X_USED_IDS | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_used_ids_minimum, measurements.n560_1_used_ids_maximum)) {
                    measurements.n560_1_used_ids = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_SEND_USED_IDS) || defined(ACQUIRE_ID_SEND_USED_IDS)
            case ID_SEND_USED_IDS:
                if(ASMO_safetyCheck(measurement, measurements.send_used_ids_minimum, measurements.send_used_ids_maximum)) {
                    measurements.send_used_ids = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
            case (ID_N560_X_SPO2 | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_spo2_minimum, measurements.n560_0_spo2_maximum)) {
                    measurements.n560_0_spo2 = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
            case (ID_N560_X_SPO2 | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_spo2_minimum, measurements.n560_1_spo2_maximum)) {
                    measurements.n560_1_spo2 = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
            case (ID_N560_X_BPM | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_bpm_minimum, measurements.n560_0_bpm_maximum)) {
                    measurements.n560_0_bpm = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
            case (ID_N560_X_BPM | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_bpm_minimum, measurements.n560_1_bpm_maximum)) {
                    measurements.n560_1_bpm = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_PA) || defined(ACQUIRE_ID_N560_X_PA)
            case (ID_N560_X_PA | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_pa_minimum, measurements.n560_0_pa_maximum)) {
                    measurements.n560_0_pa = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_PA) || defined(ACQUIRE_ID_N560_X_PA)
            case (ID_N560_X_PA | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_pa_minimum, measurements.n560_1_pa_maximum)) {
                    measurements.n560_1_pa = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_0_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
            case (ID_N560_X_STATUS | (0 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_0_status_minimum, measurements.n560_0_status_maximum)) {
                    measurements.n560_0_status = *measurement;
                }
            break;
        #endif
        #if defined(USE_ID_N560_1_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
            case (ID_N560_X_STATUS | (1 << 12)):
                if(ASMO_safetyCheck(measurement, measurements.n560_1_status_minimum, measurements.n560_1_status_maximum)) {
                    measurements.n560_1_status = *measurement;
                }
            break;
        #endif
        default:
            break;
    }
    ASMO_newTSMeasurement(measurement);
}

//! Returns true, if copied the stored ASMO_Measurement in ::measurements.
bool ASMO_getMeasurement(uint32_t ID, ASMO_Measurement **measurement, ASMO_MeasurementConfig **config) {

    bool result = false;

    switch(ID) {
        #ifdef ACQUIRE_ID_PERFORMANCE_COUNTER
            case 0x00000010:
                *measurement = &measurements.performance_counter;
                *config = &acquire_measurements.performance_counter_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_TIME_BEACON
            case 0x00000100:
                *measurement = &measurements.time_beacon;
                *config = &acquire_measurements.time_beacon_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
                *measurement = &measurements.n560_0_measurement_too_low;
                *config = &acquire_measurements.n560_0_measurement_too_low_config;
                result = true;
                break;
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
                *measurement = &measurements.n560_1_measurement_too_low;
                *config = &acquire_measurements.n560_1_measurement_too_low_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
                *measurement = &measurements.n560_0_measurement_too_high;
                *config = &acquire_measurements.n560_0_measurement_too_high_config;
                result = true;
                break;
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
                *measurement = &measurements.n560_1_measurement_too_high;
                *config = &acquire_measurements.n560_1_measurement_too_high_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
                *measurement = &measurements.n560_0_acquire_missing;
                *config = &acquire_measurements.n560_0_acquire_missing_config;
                result = true;
                break;
            case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
                *measurement = &measurements.n560_1_acquire_missing;
                *config = &acquire_measurements.n560_1_acquire_missing_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_SAFETY_SET
            case 0x07000001:
                *measurement = &measurements.safety_set;
                *config = &acquire_measurements.safety_set_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_SAFETY_TIMEOUT
            case 0x07000002:
                *measurement = &measurements.safety_timeout;
                *config = &acquire_measurements.safety_timeout_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_STARTUP
            case (ID_N560_X_STARTUP | (0 << 12)):
                *measurement = &measurements.n560_0_startup;
                *config = &acquire_measurements.n560_0_startup_config;
                result = true;
                break;
            case (ID_N560_X_STARTUP | (1 << 12)):
                *measurement = &measurements.n560_1_startup;
                *config = &acquire_measurements.n560_1_startup_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (0 << 12)):
                *measurement = &measurements.n560_0_card_disabled;
                *config = &acquire_measurements.n560_0_card_disabled_config;
                result = true;
                break;
            case (ID_N560_X_CARD_DISABLED | (1 << 12)):
                *measurement = &measurements.n560_1_card_disabled;
                *config = &acquire_measurements.n560_1_card_disabled_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (0 << 12)):
                *measurement = &measurements.n560_0_status_reply;
                *config = &acquire_measurements.n560_0_status_reply_config;
                result = true;
                break;
            case (ID_N560_X_STATUS_REPLY | (1 << 12)):
                *measurement = &measurements.n560_1_status_reply;
                *config = &acquire_measurements.n560_1_status_reply_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_ALIVE
            case (ID_N560_X_ALIVE | (0 << 12)):
                *measurement = &measurements.n560_0_alive;
                *config = &acquire_measurements.n560_0_alive_config;
                result = true;
                break;
            case (ID_N560_X_ALIVE | (1 << 12)):
                *measurement = &measurements.n560_1_alive;
                *config = &acquire_measurements.n560_1_alive_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_RESEND_STARTUP
            case 0x0cffffff:
                *measurement = &measurements.resend_startup;
                *config = &acquire_measurements.resend_startup_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_USED_IDS
            case (ID_N560_X_USED_IDS | (0 << 12)):
                *measurement = &measurements.n560_0_used_ids;
                *config = &acquire_measurements.n560_0_used_ids_config;
                result = true;
                break;
            case (ID_N560_X_USED_IDS | (1 << 12)):
                *measurement = &measurements.n560_1_used_ids;
                *config = &acquire_measurements.n560_1_used_ids_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_SEND_USED_IDS
            case 0x0dffffff:
                *measurement = &measurements.send_used_ids;
                *config = &acquire_measurements.send_used_ids_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_SPO2
            case (ID_N560_X_SPO2 | (0 << 12)):
                *measurement = &measurements.n560_0_spo2;
                *config = &acquire_measurements.n560_0_spo2_config;
                result = true;
                break;
            case (ID_N560_X_SPO2 | (1 << 12)):
                *measurement = &measurements.n560_1_spo2;
                *config = &acquire_measurements.n560_1_spo2_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_BPM
            case (ID_N560_X_BPM | (0 << 12)):
                *measurement = &measurements.n560_0_bpm;
                *config = &acquire_measurements.n560_0_bpm_config;
                result = true;
                break;
            case (ID_N560_X_BPM | (1 << 12)):
                *measurement = &measurements.n560_1_bpm;
                *config = &acquire_measurements.n560_1_bpm_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_PA
            case (ID_N560_X_PA | (0 << 12)):
                *measurement = &measurements.n560_0_pa;
                *config = &acquire_measurements.n560_0_pa_config;
                result = true;
                break;
            case (ID_N560_X_PA | (1 << 12)):
                *measurement = &measurements.n560_1_pa;
                *config = &acquire_measurements.n560_1_pa_config;
                result = true;
                break;
        #endif
        #ifdef ACQUIRE_ID_N560_X_STATUS
            case (ID_N560_X_STATUS | (0 << 12)):
                *measurement = &measurements.n560_0_status;
                *config = &acquire_measurements.n560_0_status_config;
                result = true;
                break;
            case (ID_N560_X_STATUS | (1 << 12)):
                *measurement = &measurements.n560_1_status;
                *config = &acquire_measurements.n560_1_status_config;
                result = true;
                break;
        #endif
    }
    
    return result;
}

// No longer used. Just here for compatibility.
unsigned long message_mailbox_mask = 0;

