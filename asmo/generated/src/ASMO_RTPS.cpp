/**
 * \file ASMO_RTPS.cpp
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

/*
 *  Created on: Aug 2021 - Aug 2022
 *      Author: sven, Benedikt Conze
 */

#include "ASMO_RTPS.hpp"
#include "SmartECLA_allHeaders.h"
#include "ASMO_message.h"
#include "ASMO_stream.h"
#include "rtps/rtps.h"
#include <lwip/api.h>
#include <lwip/netif.h>
#include <string.h>

// extern variables from config.h
std::array<uint8_t, 4> rtps::Config::IP_ADDRESS;
rtps::GuidPrefix_t rtps::Config::BASE_GUID_PREFIX;

// helper macro
#define IP_ADDR_INT(a, b, c, d) ((d << 24) | (c << 16) | (b << 8) | a)

// One participant for every MC
rtps::Participant* part;

// store readers and writers
#if ASMO_ACQUIRE_STREAM_INTERNAL_MAIN_0
rtps::Writer* INTERNAL_MAIN_0_writer;
#endif
#if ASMO_TOUCH_STREAM_INTERNAL_MAIN_0
rtps::Reader* INTERNAL_MAIN_0_reader;
#endif
#if ASMO_ACQUIRE_STREAM_N560_MAIN_0
rtps::Writer* N560_MAIN_0_writer;
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_0
rtps::Reader* N560_MAIN_0_reader;
#endif
#if ASMO_ACQUIRE_STREAM_N560_MAIN_1
rtps::Writer* N560_MAIN_1_writer;
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_1
rtps::Reader* N560_MAIN_1_reader;
#endif
#if ASMO_ACQUIRE_STREAM_OTHERS_MAIN_0
rtps::Writer* OTHERS_MAIN_0_writer;
#endif
#if ASMO_TOUCH_STREAM_OTHERS_MAIN_0
rtps::Reader* OTHERS_MAIN_0_reader;
#endif

/*
 * Is given to callback, to reconstruct CAN ID
 */
void message_callback_RTPS(void* arg, const rtps::ReaderCacheChange& cacheChange){
  (void)arg;

  AsmoMessage msg;
  ucdrBuffer buffer;

  if(cacheChange.getDataSize() < 4) {
    return;
  }

  if(cacheChange.getData()[0] != 0 || cacheChange.getData()[1] != 1 ||
     cacheChange.getData()[2] != 0 || cacheChange.getData()[3] != 0) {
    return;
    //Broken ucdr header
  }
  ucdr_init_buffer(&buffer, &cacheChange.getData()[4], cacheChange.getDataSize()-4);

  if(!AsmoMessage_deserialize_topic(&buffer, &msg)) {
    return;//something went wrong here
  }

  // read and clear messageType
  uint8_t msgType = msg.id >> 29;
  msg.id &= ~(1 << 30);
  msg.id &= ~(1 << 29);

  switch(msgType)
  {
  case ASMO_NET_MESSAGE_TYPE_MEASUREMENT:
  {
    ASMO_Measurement meas;
    meas.timestamp.nanosecondsField = msg.nanoseconds;
    meas.timestamp.secondsField = msg.seconds;
    meas.messageId = msg.id;
    meas.value = msg.value;

    ASMO_storeMeasurement(&meas);

#ifdef ETHPERF_LONG_MEASUREMENT_LENGTH
    // doesn't make sense in RTPS
    chSysHalt("Ignoring long msg in RTPS mode");
#endif

    // BC: To accurately identify the number of new measurements,
    //     we count them here instead of waiting for changes in the struct
    models_dispatchMeasurement(&measurement);
  }
    break;
  case ASMO_NET_MESSAGE_TYPE_SIMPLECAN:
  {
    uint8_t blockId = asmo_block_id_from_msgid(msg.id) << 24;

    MessageCommand cmd = {
        .messageId = msg.id,
        .command = msg.value
    };

    switch(blockId) {
        case BLOCK_ID_MEDICAL_ALERT:
            models_dispatchCommand(&cmd);
            break;
        case BLOCK_ID_MODEL_COMMAND:
            models_dispatchCommand(&cmd);
            break;
        case BLOCK_ID_SAFETY_COMMAND:
            cmd.command &= 0xFFFFFFFF;
            ASMO_safetyDispatch(&cmd);
            break;
    }
  }
    break;
  default:
    chSysHalt("Unknown msgType");
    break;
  }
}

/*
 * Get writer from message id
 * \returns writer corresponding to id
 */
rtps::Writer* getWriter(uint32_t id)
{
  switch((id & 0xFFF000) >> 12) {
#if ASMO_ACQUIRE_STREAM_INTERNAL_MAIN_0
    case 0x000:
      return INTERNAL_MAIN_0_writer;
#endif
#if ASMO_ACQUIRE_STREAM_N560_MAIN_0
    case 0x070:
      return N560_MAIN_0_writer;
#endif
#if ASMO_ACQUIRE_STREAM_N560_MAIN_1
    case 0x071:
      return N560_MAIN_1_writer;
#endif
#if ASMO_ACQUIRE_STREAM_OTHERS_MAIN_0
    case 0xff0:
      return OTHERS_MAIN_0_writer;
#endif
  }

  return NULL;
}

/*
 * Send measurement via writer
 */
msg_t sendLocalMeasurementToRTPS(ASMO_Measurement* measurement){
  AsmoMessage msg;

  msg.value       = measurement->value;
  msg.id          = measurement->messageId;
  msg.seconds     = measurement->timestamp.secondsField;
  msg.nanoseconds = measurement->timestamp.nanosecondsField;

  // write message type (simpleMessage, 001) into id
  msg.id &= ~(1 << 31);
  msg.id &= ~(1 << 30);
  msg.id |= (1 << 29);

  ucdrBuffer buff;
  static uint8_t data[100]; // todo
  memset(data, 0,100);
  ucdr_init_buffer(&buff, data, 100);

  //Write CDR information into buffer (rtps specification chapter 10)
  const uint16_t zero_options = 0; //currently not used
  ucdr_serialize_array_uint8_t(&buff,
                                rtps::SMElement::SCHEME_CDR_LE.data(),
                                rtps::SMElement::SCHEME_CDR_LE.size());//Little endian flag
  ucdr_serialize_uint16_t(&buff, zero_options);
  size_t len = ucdr_buffer_length(&buff);
  //Re init buffer, otherwise there are some alignment issues
  ucdr_init_buffer(&buff, &data[len], 100-len);
  //serialize data
  AsmoMessage_serialize_topic(&buff, &msg);

  auto writer = getWriter(measurement->messageId);
  if(writer != NULL)
    writer->newChange(rtps::ChangeKind_t::ALIVE, data, ucdr_buffer_length(&buff) + len);
  else
    return (msg_t)1;

  return MSG_OK;
}

/*
 * send a simple message
 * \param id
 * \param message
 * \param timeout (currently ignored)
 */
msg_t sendSimpleRTPSMessageRefTimeout(uint32_t id, uint64_t *message,
                                 uint32_t timeout){
    AsmoMessage msg;

    msg.value       = *message;
    msg.id          = id;
    msg.seconds     = 0;
    msg.nanoseconds = 0;

    // write message type (simpleMessage, 010) into id
    msg.id &= ~(1 << 31);
    msg.id |= (1 << 30);
    msg.id &= ~(1 << 29);

    ucdrBuffer buff;
    static uint8_t data[100]; // todo
    memset(data, 0,100);
    ucdr_init_buffer(&buff, data, 100);

    const uint16_t zero_options = 0;
    ucdr_serialize_array_uint8_t(&buff,
                                 rtps::SMElement::SCHEME_CDR_LE.data(),
                                 rtps::SMElement::SCHEME_CDR_LE.size());
    ucdr_serialize_uint16_t(&buff, zero_options);
    size_t len = ucdr_buffer_length(&buff);
    //Reinit buffer to avoid alignment issues
    ucdr_init_buffer(&buff, &data[len], 100-len );
    AsmoMessage_serialize_topic(&buff, &msg);
    auto writer = getWriter(id);

    if(writer != NULL)
      writer->newChange(rtps::ChangeKind_t::ALIVE, data, ucdr_buffer_length(&buff) + len);
    else
      return (msg_t)1;

    return MSG_OK;
}

void ASMO_initRTPS(void)
{
  // these variables are constant after initialization
  rtps::Config::IP_ADDRESS = {192, 168, (DEVICE_ID >> 16), DEVICE_NUMBER};
  rtps::Config::BASE_GUID_PREFIX = {1, 2, 3, 4, 5, 6, 7, 8, 9, (DEVICE_ID >> 16), DEVICE_NUMBER};

  // Domain is static so that it is initialized now, not earlier
  static rtps::Domain domain;
  part = domain.createParticipant();

  // Create writer for all streams we acquire,
  // and reader for all streams we acquire or use (like eth_comm does it)
#if ASMO_ACQUIRE_STREAM_INTERNAL_MAIN_0
  INTERNAL_MAIN_0_writer = domain.createWriter(*part, "INTERNAL_MAIN_0","AsmoMessage", false);
#endif
#if ASMO_TOUCH_STREAM_INTERNAL_MAIN_0
  INTERNAL_MAIN_0_reader = domain.createReader(*part, "INTERNAL_MAIN_0", "AsmoMessage", false, { .addr = IP_ADDR_INT(239, 188, 0, 1) });
  INTERNAL_MAIN_0_reader->registerCallback(message_callback_RTPS, NULL);
#endif
#if ASMO_ACQUIRE_STREAM_N560_MAIN_0
  N560_MAIN_0_writer = domain.createWriter(*part, "N560_MAIN_0","AsmoMessage", false);
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_0
  N560_MAIN_0_reader = domain.createReader(*part, "N560_MAIN_0", "AsmoMessage", false, { .addr = IP_ADDR_INT(239, 188, 7, 1) });
  N560_MAIN_0_reader->registerCallback(message_callback_RTPS, NULL);
#endif
#if ASMO_ACQUIRE_STREAM_N560_MAIN_1
  N560_MAIN_1_writer = domain.createWriter(*part, "N560_MAIN_1","AsmoMessage", false);
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_1
  N560_MAIN_1_reader = domain.createReader(*part, "N560_MAIN_1", "AsmoMessage", false, { .addr = IP_ADDR_INT(239, 188, 7, 2) });
  N560_MAIN_1_reader->registerCallback(message_callback_RTPS, NULL);
#endif
#if ASMO_ACQUIRE_STREAM_OTHERS_MAIN_0
  OTHERS_MAIN_0_writer = domain.createWriter(*part, "OTHERS_MAIN_0","AsmoMessage", false);
#endif
#if ASMO_TOUCH_STREAM_OTHERS_MAIN_0
  OTHERS_MAIN_0_reader = domain.createReader(*part, "OTHERS_MAIN_0", "AsmoMessage", false, { .addr = IP_ADDR_INT(239, 188, 255, 1) });
  OTHERS_MAIN_0_reader->registerCallback(message_callback_RTPS, NULL);
#endif

  //Generate remaining writers and readers
  domain.completeInit();
}

