/**
 * \file ASMO_datastorageGetTS.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#ifndef MATLAB_MEX_FILE
#include "ASMO_datastorageGetTS.h"
#endif

ASMO_Timeseries ds_TSArray = {
    #ifdef USE_TS_ID_PERFORMANCE_COUNTER
        .performance_counter = 0,
        .num_performance_counter = 0,
    #endif
    #ifdef USE_TS_ID_TIME_BEACON
        .time_beacon = 0,
        .num_time_beacon = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_LOW
        .n560_0_measurement_too_low = 0,
        .num_n560_0_measurement_too_low = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_LOW
        .n560_1_measurement_too_low = 0,
        .num_n560_1_measurement_too_low = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_HIGH
        .n560_0_measurement_too_high = 0,
        .num_n560_0_measurement_too_high = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_HIGH
        .n560_1_measurement_too_high = 0,
        .num_n560_1_measurement_too_high = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_ACQUIRE_MISSING
        .n560_0_acquire_missing = 0,
        .num_n560_0_acquire_missing = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_ACQUIRE_MISSING
        .n560_1_acquire_missing = 0,
        .num_n560_1_acquire_missing = 0,
    #endif
    #ifdef USE_TS_ID_SAFETY_SET
        .safety_set = 0,
        .num_safety_set = 0,
    #endif
    #ifdef USE_TS_ID_SAFETY_TIMEOUT
        .safety_timeout = 0,
        .num_safety_timeout = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_STARTUP
        .n560_0_startup = 0,
        .num_n560_0_startup = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_STARTUP
        .n560_1_startup = 0,
        .num_n560_1_startup = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_CARD_DISABLED
        .n560_0_card_disabled = 0,
        .num_n560_0_card_disabled = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_CARD_DISABLED
        .n560_1_card_disabled = 0,
        .num_n560_1_card_disabled = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_STATUS_REPLY
        .n560_0_status_reply = 0,
        .num_n560_0_status_reply = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_STATUS_REPLY
        .n560_1_status_reply = 0,
        .num_n560_1_status_reply = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_ALIVE
        .n560_0_alive = 0,
        .num_n560_0_alive = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_ALIVE
        .n560_1_alive = 0,
        .num_n560_1_alive = 0,
    #endif
    #ifdef USE_TS_ID_RESEND_STARTUP
        .resend_startup = 0,
        .num_resend_startup = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_USED_IDS
        .n560_0_used_ids = 0,
        .num_n560_0_used_ids = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_USED_IDS
        .n560_1_used_ids = 0,
        .num_n560_1_used_ids = 0,
    #endif
    #ifdef USE_TS_ID_SEND_USED_IDS
        .send_used_ids = 0,
        .num_send_used_ids = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_SPO2
        .n560_0_spo2 = 0,
        .num_n560_0_spo2 = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_SPO2
        .n560_1_spo2 = 0,
        .num_n560_1_spo2 = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_BPM
        .n560_0_bpm = 0,
        .num_n560_0_bpm = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_BPM
        .n560_1_bpm = 0,
        .num_n560_1_bpm = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_PA
        .n560_0_pa = 0,
        .num_n560_0_pa = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_PA
        .n560_1_pa = 0,
        .num_n560_1_pa = 0,
    #endif
    #ifdef USE_TS_ID_N560_0_STATUS
        .n560_0_status = 0,
        .num_n560_0_status = 0,
    #endif
    #ifdef USE_TS_ID_N560_1_STATUS
        .n560_1_status = 0,
        .num_n560_1_status = 0,
    #endif
};

uint8_t ASMO_getTSMeasurement(uint32_t id, uint8_t index, ds_TSReg *reg) {
    uint8_t i=0;
    switch (id) {
    #ifdef USE_TS_ID_PERFORMANCE_COUNTER
            case ID_PERFORMANCE_COUNTER:
                //! Performance Messungen (ID: 0x00000010)
                if (index < ds_TSArray.num_performance_counter) {
                    reg = ds_TSArray.performance_counter;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_TIME_BEACON
            case ID_TIME_BEACON:
                //! Beacon for time synchronization (ID: 0x00000100)
                if (index < ds_TSArray.num_time_beacon) {
                    reg = ds_TSArray.time_beacon;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
                //! Given measurement is too low (ID: 0x02070100)
                if (index < ds_TSArray.num_n560_0_measurement_too_low) {
                    reg = ds_TSArray.n560_0_measurement_too_low;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
                //! Given measurement is too low (ID: 0x02071100)
                if (index < ds_TSArray.num_n560_1_measurement_too_low) {
                    reg = ds_TSArray.n560_1_measurement_too_low;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
                //! Given measurement is too high (ID: 0x02070101)
                if (index < ds_TSArray.num_n560_0_measurement_too_high) {
                    reg = ds_TSArray.n560_0_measurement_too_high;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
                //! Given measurement is too high (ID: 0x02071101)
                if (index < ds_TSArray.num_n560_1_measurement_too_high) {
                    reg = ds_TSArray.n560_1_measurement_too_high;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
                //! Acquire define missing for Message-ID (ID: 0x02070f00)
                if (index < ds_TSArray.num_n560_0_acquire_missing) {
                    reg = ds_TSArray.n560_0_acquire_missing;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
                //! Acquire define missing for Message-ID (ID: 0x02071f00)
                if (index < ds_TSArray.num_n560_1_acquire_missing) {
                    reg = ds_TSArray.n560_1_acquire_missing;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_SAFETY_SET
            case ID_SAFETY_SET:
                //! Set command for safety layer (ID: 0x07000001)
                if (index < ds_TSArray.num_safety_set) {
                    reg = ds_TSArray.safety_set;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_SAFETY_TIMEOUT
            case ID_SAFETY_TIMEOUT:
                //! Timeout in safety state machine (ID: 0x07000002)
                if (index < ds_TSArray.num_safety_timeout) {
                    reg = ds_TSArray.safety_timeout;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_STARTUP
            case (ID_N560_X_STARTUP | (0 << 12)):
                //! Startup sequence send out by  (ID: 0x0c070000)
                if (index < ds_TSArray.num_n560_0_startup) {
                    reg = ds_TSArray.n560_0_startup;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_STARTUP
            case (ID_N560_X_STARTUP | (1 << 12)):
                //! Startup sequence send out by  (ID: 0x0c071000)
                if (index < ds_TSArray.num_n560_1_startup) {
                    reg = ds_TSArray.n560_1_startup;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (0 << 12)):
                //! Card of MCU full:  (ID: 0x0c070010)
                if (index < ds_TSArray.num_n560_0_card_disabled) {
                    reg = ds_TSArray.n560_0_card_disabled;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (1 << 12)):
                //! Card of MCU full:  (ID: 0x0c071010)
                if (index < ds_TSArray.num_n560_1_card_disabled) {
                    reg = ds_TSArray.n560_1_card_disabled;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (0 << 12)):
                //! Status message of MCU in response of the time beacon:  (ID: 0x0c070020)
                if (index < ds_TSArray.num_n560_0_status_reply) {
                    reg = ds_TSArray.n560_0_status_reply;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (1 << 12)):
                //! Status message of MCU in response of the time beacon:  (ID: 0x0c071020)
                if (index < ds_TSArray.num_n560_1_status_reply) {
                    reg = ds_TSArray.n560_1_status_reply;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_ALIVE
            case (ID_N560_X_ALIVE | (0 << 12)):
                //! Alive message (ID: 0x0c070030)
                if (index < ds_TSArray.num_n560_0_alive) {
                    reg = ds_TSArray.n560_0_alive;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_ALIVE
            case (ID_N560_X_ALIVE | (1 << 12)):
                //! Alive message (ID: 0x0c071030)
                if (index < ds_TSArray.num_n560_1_alive) {
                    reg = ds_TSArray.n560_1_alive;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_RESEND_STARTUP
            case ID_RESEND_STARTUP:
                //! resend startup (ID: 0x0cffffff)
                if (index < ds_TSArray.num_resend_startup) {
                    reg = ds_TSArray.resend_startup;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_USED_IDS
            case (ID_N560_X_USED_IDS | (0 << 12)):
                //! Used Ids send out by  (ID: 0x0d070000)
                if (index < ds_TSArray.num_n560_0_used_ids) {
                    reg = ds_TSArray.n560_0_used_ids;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_USED_IDS
            case (ID_N560_X_USED_IDS | (1 << 12)):
                //! Used Ids send out by  (ID: 0x0d071000)
                if (index < ds_TSArray.num_n560_1_used_ids) {
                    reg = ds_TSArray.n560_1_used_ids;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_SEND_USED_IDS
            case ID_SEND_USED_IDS:
                //! Devices should send out their used IDS (ID: 0x0dffffff)
                if (index < ds_TSArray.num_send_used_ids) {
                    reg = ds_TSArray.send_used_ids;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_SPO2
            case (ID_N560_X_SPO2 | (0 << 12)):
                //! SpO2 (ID: 0x10070000)
                if (index < ds_TSArray.num_n560_0_spo2) {
                    reg = ds_TSArray.n560_0_spo2;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_SPO2
            case (ID_N560_X_SPO2 | (1 << 12)):
                //! SpO2 (ID: 0x10071000)
                if (index < ds_TSArray.num_n560_1_spo2) {
                    reg = ds_TSArray.n560_1_spo2;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_BPM
            case (ID_N560_X_BPM | (0 << 12)):
                //! Pulse rate (ID: 0x10070001)
                if (index < ds_TSArray.num_n560_0_bpm) {
                    reg = ds_TSArray.n560_0_bpm;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_BPM
            case (ID_N560_X_BPM | (1 << 12)):
                //! Pulse rate (ID: 0x10071001)
                if (index < ds_TSArray.num_n560_1_bpm) {
                    reg = ds_TSArray.n560_1_bpm;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_PA
            case (ID_N560_X_PA | (0 << 12)):
                //! Pulse amplitude (ID: 0x10070002)
                if (index < ds_TSArray.num_n560_0_pa) {
                    reg = ds_TSArray.n560_0_pa;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_PA
            case (ID_N560_X_PA | (1 << 12)):
                //! Pulse amplitude (ID: 0x10071002)
                if (index < ds_TSArray.num_n560_1_pa) {
                    reg = ds_TSArray.n560_1_pa;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_0_STATUS
            case (ID_N560_X_STATUS | (0 << 12)):
                //! Status (ID: 0x10070003)
                if (index < ds_TSArray.num_n560_0_status) {
                    reg = ds_TSArray.n560_0_status;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    #ifdef USE_TS_ID_N560_1_STATUS
            case (ID_N560_X_STATUS | (1 << 12)):
                //! Status (ID: 0x10071003)
                if (index < ds_TSArray.num_n560_1_status) {
                    reg = ds_TSArray.n560_1_status;
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    }

    (void)reg;
    (void)index;
    (void)i;

    return 0;
}
