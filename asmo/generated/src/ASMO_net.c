/**
 * \file ASMO_net.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#include "SmartECLA_allHeaders.h"
#include "ASMO_stream.h"
#include <lwip/api.h>
#include <lwip/netif.h>

struct ASMO_StreamArray {
    ASMO_Stream default_stream;
    ASMO_Stream INTERNAL_MAIN_0;
    ASMO_Stream N560_MAIN_0;
    ASMO_Stream N560_MAIN_1;
    ASMO_Stream OTHERS_MAIN_0;
};

#define IP_ADDR_INT(a, b, c, d) ((d << 24) | (c << 16) | (b << 8) | a)

struct ASMO_StreamArray streams = {
    /* default stream in case no appropriate stream could be found */
    .default_stream = {
        .ipaddr = { .addr = IP_ADDR_INT(239, 188, 0, 1) /* 239.188.0.1 */ },
        .sequence_id = 0,
        .device_instance_id = 0 /* dont care */
    },
    .INTERNAL_MAIN_0 = {
        .ipaddr = { .addr = IP_ADDR_INT(239, 188, 0, 1) /* 239.188.0.1 */ },
        .sequence_id = 0,
        .device_instance_id = 0
    },
    .N560_MAIN_0 = {
        .ipaddr = { .addr = IP_ADDR_INT(239, 188, 7, 1) /* 239.188.7.1 */ },
        .sequence_id = 0,
        .device_instance_id = 0
    },
    .N560_MAIN_1 = {
        .ipaddr = { .addr = IP_ADDR_INT(239, 188, 7, 2) /* 239.188.7.2 */ },
        .sequence_id = 0,
        .device_instance_id = 1
    },
    .OTHERS_MAIN_0 = {
        .ipaddr = { .addr = IP_ADDR_INT(239, 188, 255, 1) /* 239.188.255.1 */ },
        .sequence_id = 0,
        .device_instance_id = 0
    },
};

ASMO_Stream* asmo_net_get_stream_for_message_id(uint32_t id)
{
    // assign stream by looking at device id & device nr
    switch((id & 0xFFF000) >> 12) {
        case 0x000:
            return &streams.INTERNAL_MAIN_0;
        case 0x070:
            return &streams.N560_MAIN_0;
        case 0x071:
            return &streams.N560_MAIN_1;
        case 0xff0:
            return &streams.OTHERS_MAIN_0;
    }

    return &streams.default_stream;
}


static uint32_t extract_messageId(uint8_t *msg, uint8_t deviceId, uint8_t deviceInstanceId)
{
    return ((msg[0] & 0x1F) << 24) |
            (deviceId << 16) |
           ((deviceInstanceId & 0x0F) << 12) |
           ((msg[1] & 0x0F) << 8) |
             msg[2];
}

static timestamp_t extract_measurement_timestamp(uint8_t *msg)
{
    struct ptp_timestamp ts;
    ts.secondsField = (msg[5] << 24) |
                      (msg[6] << 16) |
                      (msg[7] << 8) |
                       msg[8];
    ts.nanosecondsField = 100 * 1000 * (
                      (msg[9] << 8) |
                       msg[10]);

    return ts;
}

static inline uint64_t peek_uint64(const uint8_t *packet)
{
    uint64_t res = 0;

    const size_t bytes_to_read = 8; /* 64-bit integer */
    for (size_t i = 0; i < bytes_to_read; i++) {
        /* the index of the byte to read in the given packet */
        const size_t byte_to_read = i;
        /* the bit index where the byte is written to in res */
        const size_t dest_bit_index =
            /* data is big endian, so lowest byte contains most significant byte */
            (bytes_to_read - i - 1) * 8;
        res |= ((uint64_t) packet[byte_to_read]) << dest_bit_index;
    }

    return res;
}

static uint32_t extract_measurement_value(uint8_t *msg)
{
    return (msg[11] << 24) |
           (msg[12] << 16) |
           (msg[13] << 8) |
           msg[14];
}

static uint64_t extract_simplemessage_value(uint8_t *msg)
{
    return peek_uint64(msg + 5);
}

static void dispatch_message(uint8_t *msg, size_t msgsz, uint8_t deviceId, uint8_t deviceInstanceId)
{
    uint8_t msgtype;
    ASMO_Measurement measurement;
    uint64_t command;
    uint32_t blockId;
    uint32_t messageId;

    if (msgsz < 1) {
        return;
    }

    msgtype = (msg[0] & 0xE0) >> 5; /* 0xE0 = 0b1110 0000 */
    switch(msgtype) {
        case ASMO_NET_MESSAGE_TYPE_MEASUREMENT:
            measurement.messageId = extract_messageId(msg, deviceId, deviceInstanceId);
            measurement.timestamp = extract_measurement_timestamp(msg);
            measurement.value = extract_measurement_value(msg);

            ASMO_storeMeasurement(&measurement);

#ifdef ETHPERF_LONG_MEASUREMENT_LENGTH
            if(msgsz < ETHPERF_LONG_MEASUREMENT_LENGTH) {
              chSysHalt("long msg size fail");
            }
            // to ensure the validity of the message, we wrote the same value at the
            // beginning and end of the payload
            if(msg[ETHPERF_LONG_MEASUREMENT_LENGTH - 1] != msg[11]) {
              chSysHalt("long msg check fail");
            }
#endif
            // BC: To accurately identify the number of new measurements,
            //     we count them here instead of waiting for changes in the struct

            // Disable until fixed, since not every project supports this function yet
            // models_dispatchMeasurement(&measurement);

#if ETH_FORWARD_TO_CAN
            sendLocalMeasurementToCAN(&measurement);
#endif
        break;
        case ASMO_NET_MESSAGE_TYPE_SIMPLECAN:
            messageId = extract_messageId(msg, deviceId, deviceInstanceId);
            blockId = asmo_block_id_from_msgid(messageId) << 24;
            command = extract_simplemessage_value(msg);

            MessageCommand cmd = {
                .messageId = messageId,
                .command = command
            };

            switch(blockId) {
                // BC: I don't know how medical alerts are usually handled,
                //     so dispatch them here
                case BLOCK_ID_MEDICAL_ALERT:
                    models_dispatchCommand(&cmd);
                    break;
                case BLOCK_ID_MODEL_COMMAND:
                    models_dispatchCommand(&cmd);
                    break;
                case BLOCK_ID_SAFETY_COMMAND:
                    cmd.command &= 0xFFFFFFFF;
                    ASMO_safetyDispatch(&cmd);
                    break;
            }
#if ETH_FORWARD_TO_CAN
            sendSimpleCANMessage(messageId, command);
#endif

        break;
    }
}

static err_t recv_data(struct netconn *conn, uint8_t deviceId, uint8_t deviceInstanceId)
{
    err_t err = ERR_OK;
    struct netbuf *buf;
    uint8_t *data;
    uint16_t data_len;

    err = netconn_recv(conn, &buf);

    if (err == ERR_OK) {
      err = netbuf_data(buf, (void **)&data, &data_len);

      if (err == ERR_OK) {
        dispatch_message(data, data_len, deviceId, deviceInstanceId);
        err = ERR_OK;
      }
      netbuf_delete(buf);
    }
    return err;
}

#if ASMO_TOUCH_STREAM_INTERNAL_MAIN_0
static THD_WORKING_AREA(waNet_Rx_INTERNAL_MAIN_0, 512);
static THD_FUNCTION(thd_net_rx_INTERNAL_MAIN_0, arg)
{
    struct netconn *conn;
    err_t err;
    ip_addr_t addr;
    IP4_ADDR(&addr, 239, 188, 0, 1);

    (void)arg;

    chRegSetThreadName("ETH_Rx_INTERNAL_MAIN_0");

    conn = netconn_new(NETCONN_UDP);
    if (!conn) {
        return;
    }

    err = netconn_bind(conn, &addr, ASMO_NET_PORT);
    if (err == ERR_OK) {
        err = netconn_join_leave_group(conn, &addr, IP_ADDR_ANY, NETCONN_JOIN);
        if (err == ERR_OK) {
            while(true) {
                err = recv_data(conn, 0, 0);

                if (err != ERR_OK) {
#if (USE_USB_SERIAL)
                    asmoSerialPut("error receiving data for INTERNAL_MAIN_0: %d\n", err);
#endif /* USE_USB_SERIAL */
                    chThdSleepSeconds(1);
                }
            }
        } else {
#if (USE_USB_SERIAL)
            asmoSerialPut("error joining IGMP group for INTERNAL_MAIN_0: %d\n", err);
#endif /* USE_USB_SERIAL */
        }
    }
    netconn_delete(conn);
}
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_0
static THD_WORKING_AREA(waNet_Rx_N560_MAIN_0, 512);
static THD_FUNCTION(thd_net_rx_N560_MAIN_0, arg)
{
    struct netconn *conn;
    err_t err;
    ip_addr_t addr;
    IP4_ADDR(&addr, 239, 188, 7, 1);

    (void)arg;

    chRegSetThreadName("ETH_Rx_N560_MAIN_0");

    conn = netconn_new(NETCONN_UDP);
    if (!conn) {
        return;
    }

    err = netconn_bind(conn, &addr, ASMO_NET_PORT);
    if (err == ERR_OK) {
        err = netconn_join_leave_group(conn, &addr, IP_ADDR_ANY, NETCONN_JOIN);
        if (err == ERR_OK) {
            while(true) {
                err = recv_data(conn, 7, 0);

                if (err != ERR_OK) {
#if (USE_USB_SERIAL)
                    asmoSerialPut("error receiving data for N560_MAIN_0: %d\n", err);
#endif /* USE_USB_SERIAL */
                    chThdSleepSeconds(1);
                }
            }
        } else {
#if (USE_USB_SERIAL)
            asmoSerialPut("error joining IGMP group for N560_MAIN_0: %d\n", err);
#endif /* USE_USB_SERIAL */
        }
    }
    netconn_delete(conn);
}
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_1
static THD_WORKING_AREA(waNet_Rx_N560_MAIN_1, 512);
static THD_FUNCTION(thd_net_rx_N560_MAIN_1, arg)
{
    struct netconn *conn;
    err_t err;
    ip_addr_t addr;
    IP4_ADDR(&addr, 239, 188, 7, 2);

    (void)arg;

    chRegSetThreadName("ETH_Rx_N560_MAIN_1");

    conn = netconn_new(NETCONN_UDP);
    if (!conn) {
        return;
    }

    err = netconn_bind(conn, &addr, ASMO_NET_PORT);
    if (err == ERR_OK) {
        err = netconn_join_leave_group(conn, &addr, IP_ADDR_ANY, NETCONN_JOIN);
        if (err == ERR_OK) {
            while(true) {
                err = recv_data(conn, 7, 1);

                if (err != ERR_OK) {
#if (USE_USB_SERIAL)
                    asmoSerialPut("error receiving data for N560_MAIN_1: %d\n", err);
#endif /* USE_USB_SERIAL */
                    chThdSleepSeconds(1);
                }
            }
        } else {
#if (USE_USB_SERIAL)
            asmoSerialPut("error joining IGMP group for N560_MAIN_1: %d\n", err);
#endif /* USE_USB_SERIAL */
        }
    }
    netconn_delete(conn);
}
#endif
#if ASMO_TOUCH_STREAM_OTHERS_MAIN_0
static THD_WORKING_AREA(waNet_Rx_OTHERS_MAIN_0, 512);
static THD_FUNCTION(thd_net_rx_OTHERS_MAIN_0, arg)
{
    struct netconn *conn;
    err_t err;
    ip_addr_t addr;
    IP4_ADDR(&addr, 239, 188, 255, 1);

    (void)arg;

    chRegSetThreadName("ETH_Rx_OTHERS_MAIN_0");

    conn = netconn_new(NETCONN_UDP);
    if (!conn) {
        return;
    }

    err = netconn_bind(conn, &addr, ASMO_NET_PORT);
    if (err == ERR_OK) {
        err = netconn_join_leave_group(conn, &addr, IP_ADDR_ANY, NETCONN_JOIN);
        if (err == ERR_OK) {
            while(true) {
                err = recv_data(conn, 255, 0);

                if (err != ERR_OK) {
#if (USE_USB_SERIAL)
                    asmoSerialPut("error receiving data for OTHERS_MAIN_0: %d\n", err);
#endif /* USE_USB_SERIAL */
                    chThdSleepSeconds(1);
                }
            }
        } else {
#if (USE_USB_SERIAL)
            asmoSerialPut("error joining IGMP group for OTHERS_MAIN_0: %d\n", err);
#endif /* USE_USB_SERIAL */
        }
    }
    netconn_delete(conn);
}
#endif

void asmo_net_init(void)
{
#if ASMO_TOUCH_STREAM_INTERNAL_MAIN_0
    chThdCreateStatic(waNet_Rx_INTERNAL_MAIN_0, sizeof(waNet_Rx_INTERNAL_MAIN_0), NORMALPRIO + 1, thd_net_rx_INTERNAL_MAIN_0, NULL);
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_0
    chThdCreateStatic(waNet_Rx_N560_MAIN_0, sizeof(waNet_Rx_N560_MAIN_0), NORMALPRIO + 1, thd_net_rx_N560_MAIN_0, NULL);
#endif
#if ASMO_TOUCH_STREAM_N560_MAIN_1
    chThdCreateStatic(waNet_Rx_N560_MAIN_1, sizeof(waNet_Rx_N560_MAIN_1), NORMALPRIO + 1, thd_net_rx_N560_MAIN_1, NULL);
#endif
#if ASMO_TOUCH_STREAM_OTHERS_MAIN_0
    chThdCreateStatic(waNet_Rx_OTHERS_MAIN_0, sizeof(waNet_Rx_OTHERS_MAIN_0), NORMALPRIO + 1, thd_net_rx_OTHERS_MAIN_0, NULL);
#endif
}

