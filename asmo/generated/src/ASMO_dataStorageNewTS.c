/**
 * \file ASMO_dataStorageNewTS.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#ifndef MATLAB_MEX_FILE
#include "ASMO_datastorageNewTS.h"
#endif

uint8_t ASMO_newTSMeasurement(ASMO_Measurement *m) {
    ds_TSReg *reg;
    uint8_t num = 0;
    switch (m->messageId) {
        #ifdef USE_TS_ID_PERFORMANCE_COUNTER
            case ID_PERFORMANCE_COUNTER:
                //! Performance Messungen (ID: 0x00000010)
                reg = ds_TSArray.performance_counter;
                num = ds_TSArray.num_performance_counter;
            break;
        #endif
        #ifdef USE_TS_ID_TIME_BEACON
            case ID_TIME_BEACON:
                //! Beacon for time synchronization (ID: 0x00000100)
                reg = ds_TSArray.time_beacon;
                num = ds_TSArray.num_time_beacon;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
                //! Given measurement is too low (ID: 0x02070100)
                reg = ds_TSArray.n560_0_measurement_too_low;
                num = ds_TSArray.num_n560_0_measurement_too_low;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
                //! Given measurement is too low (ID: 0x02071100)
                reg = ds_TSArray.n560_1_measurement_too_low;
                num = ds_TSArray.num_n560_1_measurement_too_low;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
                //! Given measurement is too high (ID: 0x02070101)
                reg = ds_TSArray.n560_0_measurement_too_high;
                num = ds_TSArray.num_n560_0_measurement_too_high;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
                //! Given measurement is too high (ID: 0x02071101)
                reg = ds_TSArray.n560_1_measurement_too_high;
                num = ds_TSArray.num_n560_1_measurement_too_high;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
                //! Acquire define missing for Message-ID (ID: 0x02070f00)
                reg = ds_TSArray.n560_0_acquire_missing;
                num = ds_TSArray.num_n560_0_acquire_missing;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
                //! Acquire define missing for Message-ID (ID: 0x02071f00)
                reg = ds_TSArray.n560_1_acquire_missing;
                num = ds_TSArray.num_n560_1_acquire_missing;
            break;
        #endif
        #ifdef USE_TS_ID_SAFETY_SET
            case ID_SAFETY_SET:
                //! Set command for safety layer (ID: 0x07000001)
                reg = ds_TSArray.safety_set;
                num = ds_TSArray.num_safety_set;
            break;
        #endif
        #ifdef USE_TS_ID_SAFETY_TIMEOUT
            case ID_SAFETY_TIMEOUT:
                //! Timeout in safety state machine (ID: 0x07000002)
                reg = ds_TSArray.safety_timeout;
                num = ds_TSArray.num_safety_timeout;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_STARTUP
            case (ID_N560_X_STARTUP | (0 << 12)):
                //! Startup sequence send out by  (ID: 0x0c070000)
                reg = ds_TSArray.n560_0_startup;
                num = ds_TSArray.num_n560_0_startup;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_STARTUP
            case (ID_N560_X_STARTUP | (1 << 12)):
                //! Startup sequence send out by  (ID: 0x0c071000)
                reg = ds_TSArray.n560_1_startup;
                num = ds_TSArray.num_n560_1_startup;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (0 << 12)):
                //! Card of MCU full:  (ID: 0x0c070010)
                reg = ds_TSArray.n560_0_card_disabled;
                num = ds_TSArray.num_n560_0_card_disabled;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (1 << 12)):
                //! Card of MCU full:  (ID: 0x0c071010)
                reg = ds_TSArray.n560_1_card_disabled;
                num = ds_TSArray.num_n560_1_card_disabled;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (0 << 12)):
                //! Status message of MCU in response of the time beacon:  (ID: 0x0c070020)
                reg = ds_TSArray.n560_0_status_reply;
                num = ds_TSArray.num_n560_0_status_reply;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (1 << 12)):
                //! Status message of MCU in response of the time beacon:  (ID: 0x0c071020)
                reg = ds_TSArray.n560_1_status_reply;
                num = ds_TSArray.num_n560_1_status_reply;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_ALIVE
            case (ID_N560_X_ALIVE | (0 << 12)):
                //! Alive message (ID: 0x0c070030)
                reg = ds_TSArray.n560_0_alive;
                num = ds_TSArray.num_n560_0_alive;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_ALIVE
            case (ID_N560_X_ALIVE | (1 << 12)):
                //! Alive message (ID: 0x0c071030)
                reg = ds_TSArray.n560_1_alive;
                num = ds_TSArray.num_n560_1_alive;
            break;
        #endif
        #ifdef USE_TS_ID_RESEND_STARTUP
            case ID_RESEND_STARTUP:
                //! resend startup (ID: 0x0cffffff)
                reg = ds_TSArray.resend_startup;
                num = ds_TSArray.num_resend_startup;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_USED_IDS
            case (ID_N560_X_USED_IDS | (0 << 12)):
                //! Used Ids send out by  (ID: 0x0d070000)
                reg = ds_TSArray.n560_0_used_ids;
                num = ds_TSArray.num_n560_0_used_ids;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_USED_IDS
            case (ID_N560_X_USED_IDS | (1 << 12)):
                //! Used Ids send out by  (ID: 0x0d071000)
                reg = ds_TSArray.n560_1_used_ids;
                num = ds_TSArray.num_n560_1_used_ids;
            break;
        #endif
        #ifdef USE_TS_ID_SEND_USED_IDS
            case ID_SEND_USED_IDS:
                //! Devices should send out their used IDS (ID: 0x0dffffff)
                reg = ds_TSArray.send_used_ids;
                num = ds_TSArray.num_send_used_ids;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_SPO2
            case (ID_N560_X_SPO2 | (0 << 12)):
                //! SpO2 (ID: 0x10070000)
                reg = ds_TSArray.n560_0_spo2;
                num = ds_TSArray.num_n560_0_spo2;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_SPO2
            case (ID_N560_X_SPO2 | (1 << 12)):
                //! SpO2 (ID: 0x10071000)
                reg = ds_TSArray.n560_1_spo2;
                num = ds_TSArray.num_n560_1_spo2;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_BPM
            case (ID_N560_X_BPM | (0 << 12)):
                //! Pulse rate (ID: 0x10070001)
                reg = ds_TSArray.n560_0_bpm;
                num = ds_TSArray.num_n560_0_bpm;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_BPM
            case (ID_N560_X_BPM | (1 << 12)):
                //! Pulse rate (ID: 0x10071001)
                reg = ds_TSArray.n560_1_bpm;
                num = ds_TSArray.num_n560_1_bpm;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_PA
            case (ID_N560_X_PA | (0 << 12)):
                //! Pulse amplitude (ID: 0x10070002)
                reg = ds_TSArray.n560_0_pa;
                num = ds_TSArray.num_n560_0_pa;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_PA
            case (ID_N560_X_PA | (1 << 12)):
                //! Pulse amplitude (ID: 0x10071002)
                reg = ds_TSArray.n560_1_pa;
                num = ds_TSArray.num_n560_1_pa;
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_STATUS
            case (ID_N560_X_STATUS | (0 << 12)):
                //! Status (ID: 0x10070003)
                reg = ds_TSArray.n560_0_status;
                num = ds_TSArray.num_n560_0_status;
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_STATUS
            case (ID_N560_X_STATUS | (1 << 12)):
                //! Status (ID: 0x10071003)
                reg = ds_TSArray.n560_1_status;
                num = ds_TSArray.num_n560_1_status;
            break;
        #endif
    }

	if (num>0) {
		while(reg) {
			ds_newTS(m, reg);
            reg = reg->next;
        }
        return 1;
    }
    return 0;
}

int16_t ASMO_registerTS(ds_TSReg *reg) {
	reg->head = reg->storage - 1;
	chSemObjectInit((reg->sem), 1); /* Semaphore initialization before use */
	switch (reg->messageID) {
        #ifdef USE_TS_ID_PERFORMANCE_COUNTER
            case ID_PERFORMANCE_COUNTER:
                if (ds_TSArray.num_performance_counter == 0) {
                    ds_TSArray.performance_counter = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.performance_counter;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_performance_counter;
                return ds_TSArray.num_performance_counter - 1;
                break;
        #endif
        #ifdef USE_TS_ID_TIME_BEACON
            case ID_TIME_BEACON:
                if (ds_TSArray.num_time_beacon == 0) {
                    ds_TSArray.time_beacon = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.time_beacon;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_time_beacon;
                return ds_TSArray.num_time_beacon - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
                if (ds_TSArray.num_n560_0_measurement_too_low == 0) {
                    ds_TSArray.n560_0_measurement_too_low = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_measurement_too_low;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_measurement_too_low;
                return ds_TSArray.num_n560_0_measurement_too_low - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
                if (ds_TSArray.num_n560_1_measurement_too_low == 0) {
                    ds_TSArray.n560_1_measurement_too_low = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_measurement_too_low;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_measurement_too_low;
                return ds_TSArray.num_n560_1_measurement_too_low - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
                if (ds_TSArray.num_n560_0_measurement_too_high == 0) {
                    ds_TSArray.n560_0_measurement_too_high = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_measurement_too_high;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_measurement_too_high;
                return ds_TSArray.num_n560_0_measurement_too_high - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
                if (ds_TSArray.num_n560_1_measurement_too_high == 0) {
                    ds_TSArray.n560_1_measurement_too_high = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_measurement_too_high;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_measurement_too_high;
                return ds_TSArray.num_n560_1_measurement_too_high - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
                if (ds_TSArray.num_n560_0_acquire_missing == 0) {
                    ds_TSArray.n560_0_acquire_missing = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_acquire_missing;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_acquire_missing;
                return ds_TSArray.num_n560_0_acquire_missing - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
                if (ds_TSArray.num_n560_1_acquire_missing == 0) {
                    ds_TSArray.n560_1_acquire_missing = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_acquire_missing;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_acquire_missing;
                return ds_TSArray.num_n560_1_acquire_missing - 1;
                break;
        #endif
        #ifdef USE_TS_ID_SAFETY_SET
            case ID_SAFETY_SET:
                if (ds_TSArray.num_safety_set == 0) {
                    ds_TSArray.safety_set = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.safety_set;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_safety_set;
                return ds_TSArray.num_safety_set - 1;
                break;
        #endif
        #ifdef USE_TS_ID_SAFETY_TIMEOUT
            case ID_SAFETY_TIMEOUT:
                if (ds_TSArray.num_safety_timeout == 0) {
                    ds_TSArray.safety_timeout = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.safety_timeout;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_safety_timeout;
                return ds_TSArray.num_safety_timeout - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_STARTUP
            case (ID_N560_X_STARTUP | (0 << 12)):
                if (ds_TSArray.num_n560_0_startup == 0) {
                    ds_TSArray.n560_0_startup = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_startup;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_startup;
                return ds_TSArray.num_n560_0_startup - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_STARTUP
            case (ID_N560_X_STARTUP | (1 << 12)):
                if (ds_TSArray.num_n560_1_startup == 0) {
                    ds_TSArray.n560_1_startup = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_startup;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_startup;
                return ds_TSArray.num_n560_1_startup - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (0 << 12)):
                if (ds_TSArray.num_n560_0_card_disabled == 0) {
                    ds_TSArray.n560_0_card_disabled = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_card_disabled;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_card_disabled;
                return ds_TSArray.num_n560_0_card_disabled - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (1 << 12)):
                if (ds_TSArray.num_n560_1_card_disabled == 0) {
                    ds_TSArray.n560_1_card_disabled = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_card_disabled;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_card_disabled;
                return ds_TSArray.num_n560_1_card_disabled - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (0 << 12)):
                if (ds_TSArray.num_n560_0_status_reply == 0) {
                    ds_TSArray.n560_0_status_reply = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_status_reply;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_status_reply;
                return ds_TSArray.num_n560_0_status_reply - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (1 << 12)):
                if (ds_TSArray.num_n560_1_status_reply == 0) {
                    ds_TSArray.n560_1_status_reply = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_status_reply;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_status_reply;
                return ds_TSArray.num_n560_1_status_reply - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_ALIVE
            case (ID_N560_X_ALIVE | (0 << 12)):
                if (ds_TSArray.num_n560_0_alive == 0) {
                    ds_TSArray.n560_0_alive = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_alive;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_alive;
                return ds_TSArray.num_n560_0_alive - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_ALIVE
            case (ID_N560_X_ALIVE | (1 << 12)):
                if (ds_TSArray.num_n560_1_alive == 0) {
                    ds_TSArray.n560_1_alive = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_alive;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_alive;
                return ds_TSArray.num_n560_1_alive - 1;
                break;
        #endif
        #ifdef USE_TS_ID_RESEND_STARTUP
            case ID_RESEND_STARTUP:
                if (ds_TSArray.num_resend_startup == 0) {
                    ds_TSArray.resend_startup = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.resend_startup;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_resend_startup;
                return ds_TSArray.num_resend_startup - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_USED_IDS
            case (ID_N560_X_USED_IDS | (0 << 12)):
                if (ds_TSArray.num_n560_0_used_ids == 0) {
                    ds_TSArray.n560_0_used_ids = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_used_ids;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_used_ids;
                return ds_TSArray.num_n560_0_used_ids - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_USED_IDS
            case (ID_N560_X_USED_IDS | (1 << 12)):
                if (ds_TSArray.num_n560_1_used_ids == 0) {
                    ds_TSArray.n560_1_used_ids = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_used_ids;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_used_ids;
                return ds_TSArray.num_n560_1_used_ids - 1;
                break;
        #endif
        #ifdef USE_TS_ID_SEND_USED_IDS
            case ID_SEND_USED_IDS:
                if (ds_TSArray.num_send_used_ids == 0) {
                    ds_TSArray.send_used_ids = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.send_used_ids;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_send_used_ids;
                return ds_TSArray.num_send_used_ids - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_SPO2
            case (ID_N560_X_SPO2 | (0 << 12)):
                if (ds_TSArray.num_n560_0_spo2 == 0) {
                    ds_TSArray.n560_0_spo2 = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_spo2;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_spo2;
                return ds_TSArray.num_n560_0_spo2 - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_SPO2
            case (ID_N560_X_SPO2 | (1 << 12)):
                if (ds_TSArray.num_n560_1_spo2 == 0) {
                    ds_TSArray.n560_1_spo2 = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_spo2;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_spo2;
                return ds_TSArray.num_n560_1_spo2 - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_BPM
            case (ID_N560_X_BPM | (0 << 12)):
                if (ds_TSArray.num_n560_0_bpm == 0) {
                    ds_TSArray.n560_0_bpm = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_bpm;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_bpm;
                return ds_TSArray.num_n560_0_bpm - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_BPM
            case (ID_N560_X_BPM | (1 << 12)):
                if (ds_TSArray.num_n560_1_bpm == 0) {
                    ds_TSArray.n560_1_bpm = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_bpm;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_bpm;
                return ds_TSArray.num_n560_1_bpm - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_PA
            case (ID_N560_X_PA | (0 << 12)):
                if (ds_TSArray.num_n560_0_pa == 0) {
                    ds_TSArray.n560_0_pa = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_pa;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_pa;
                return ds_TSArray.num_n560_0_pa - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_PA
            case (ID_N560_X_PA | (1 << 12)):
                if (ds_TSArray.num_n560_1_pa == 0) {
                    ds_TSArray.n560_1_pa = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_pa;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_pa;
                return ds_TSArray.num_n560_1_pa - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_0_STATUS
            case (ID_N560_X_STATUS | (0 << 12)):
                if (ds_TSArray.num_n560_0_status == 0) {
                    ds_TSArray.n560_0_status = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_0_status;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_0_status;
                return ds_TSArray.num_n560_0_status - 1;
                break;
        #endif
        #ifdef USE_TS_ID_N560_1_STATUS
            case (ID_N560_X_STATUS | (1 << 12)):
                if (ds_TSArray.num_n560_1_status == 0) {
                    ds_TSArray.n560_1_status = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.n560_1_status;
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_n560_1_status;
                return ds_TSArray.num_n560_1_status - 1;
                break;
        #endif
	}

    return -1;
}
