/**
 * \file ASMO_simulinkGet.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#ifndef MATLAB_MEX_FILE
#include "ASMO_simulinkGet.h"
#endif
bool simulink_getMeasurement(uint32_t id, double *value, timestamp_t *timestamp) {
    ASMO_Measurement m;
    uint32_t val;
    switch (id) {
#if defined(ACQUIRE_ID_PERFORMANCE_COUNTER) || defined(USE_ID_PERFORMANCE_COUNTER)
    case ID_PERFORMANCE_COUNTER:
        m = measurements.performance_counter;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_TIME_BEACON) || defined(USE_ID_TIME_BEACON)
    case ID_TIME_BEACON:
        m = measurements.time_beacon;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW) || defined(USE_ID_N560_0_MEASUREMENT_TOO_LOW)
    case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
        m = measurements.n560_0_measurement_too_low;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW) || defined(USE_ID_N560_1_MEASUREMENT_TOO_LOW)
    case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
        m = measurements.n560_1_measurement_too_low;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH) || defined(USE_ID_N560_0_MEASUREMENT_TOO_HIGH)
    case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
        m = measurements.n560_0_measurement_too_high;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH) || defined(USE_ID_N560_1_MEASUREMENT_TOO_HIGH)
    case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
        m = measurements.n560_1_measurement_too_high;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING) || defined(USE_ID_N560_0_ACQUIRE_MISSING)
    case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
        m = measurements.n560_0_acquire_missing;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING) || defined(USE_ID_N560_1_ACQUIRE_MISSING)
    case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
        m = measurements.n560_1_acquire_missing;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_SAFETY_SET) || defined(USE_ID_SAFETY_SET)
    case ID_SAFETY_SET:
        m = measurements.safety_set;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_SAFETY_TIMEOUT) || defined(USE_ID_SAFETY_TIMEOUT)
    case ID_SAFETY_TIMEOUT:
        m = measurements.safety_timeout;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_STARTUP) || defined(USE_ID_N560_0_STARTUP)
    case (ID_N560_X_STARTUP | (0 << 12)):
        m = measurements.n560_0_startup;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_STARTUP) || defined(USE_ID_N560_1_STARTUP)
    case (ID_N560_X_STARTUP | (1 << 12)):
        m = measurements.n560_1_startup;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_CARD_DISABLED) || defined(USE_ID_N560_0_CARD_DISABLED)
    case (ID_N560_X_CARD_DISABLED | (0 << 12)):
        m = measurements.n560_0_card_disabled;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_CARD_DISABLED) || defined(USE_ID_N560_1_CARD_DISABLED)
    case (ID_N560_X_CARD_DISABLED | (1 << 12)):
        m = measurements.n560_1_card_disabled;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_STATUS_REPLY) || defined(USE_ID_N560_0_STATUS_REPLY)
    case (ID_N560_X_STATUS_REPLY | (0 << 12)):
        m = measurements.n560_0_status_reply;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_STATUS_REPLY) || defined(USE_ID_N560_1_STATUS_REPLY)
    case (ID_N560_X_STATUS_REPLY | (1 << 12)):
        m = measurements.n560_1_status_reply;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_ALIVE) || defined(USE_ID_N560_0_ALIVE)
    case (ID_N560_X_ALIVE | (0 << 12)):
        m = measurements.n560_0_alive;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_ALIVE) || defined(USE_ID_N560_1_ALIVE)
    case (ID_N560_X_ALIVE | (1 << 12)):
        m = measurements.n560_1_alive;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_RESEND_STARTUP) || defined(USE_ID_RESEND_STARTUP)
    case ID_RESEND_STARTUP:
        m = measurements.resend_startup;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_USED_IDS) || defined(USE_ID_N560_0_USED_IDS)
    case (ID_N560_X_USED_IDS | (0 << 12)):
        m = measurements.n560_0_used_ids;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_USED_IDS) || defined(USE_ID_N560_1_USED_IDS)
    case (ID_N560_X_USED_IDS | (1 << 12)):
        m = measurements.n560_1_used_ids;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_SEND_USED_IDS) || defined(USE_ID_SEND_USED_IDS)
    case ID_SEND_USED_IDS:
        m = measurements.send_used_ids;
        val = m.value;
        *value = (double)val * 1;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_SPO2) || defined(USE_ID_N560_0_SPO2)
    case (ID_N560_X_SPO2 | (0 << 12)):
        m = measurements.n560_0_spo2;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_SPO2) || defined(USE_ID_N560_1_SPO2)
    case (ID_N560_X_SPO2 | (1 << 12)):
        m = measurements.n560_1_spo2;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_BPM) || defined(USE_ID_N560_0_BPM)
    case (ID_N560_X_BPM | (0 << 12)):
        m = measurements.n560_0_bpm;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_BPM) || defined(USE_ID_N560_1_BPM)
    case (ID_N560_X_BPM | (1 << 12)):
        m = measurements.n560_1_bpm;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_PA) || defined(USE_ID_N560_0_PA)
    case (ID_N560_X_PA | (0 << 12)):
        m = measurements.n560_0_pa;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_PA) || defined(USE_ID_N560_1_PA)
    case (ID_N560_X_PA | (1 << 12)):
        m = measurements.n560_1_pa;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_STATUS) || defined(USE_ID_N560_0_STATUS)
    case (ID_N560_X_STATUS | (0 << 12)):
        m = measurements.n560_0_status;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
#if defined(ACQUIRE_ID_N560_X_STATUS) || defined(USE_ID_N560_1_STATUS)
    case (ID_N560_X_STATUS | (1 << 12)):
        m = measurements.n560_1_status;
        val = m.value;
        *value = (double)val * 1.0;
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
        default:
            break;
    }

    return false;
}
