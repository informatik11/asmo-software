/**
 * \file ASMO_safetySet.c
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#include "SmartECLA_allHeaders.h"

msg_t ASMO_safetySet(ASMO_safetyConfig *sc) {
    switch (sc->id) {
        #ifdef USE_TS_ID_PERFORMANCE_COUNTER
            case ID_PERFORMANCE_COUNTER:
                //! Performance Messungen (ID: 0x00000010)
				if (sc->command == safetyCmdSetMin) {
					measurements.performance_counter_minimum = sc->value;
					if (measurements.performance_counter.value<sc->value) {
						measurements.performance_counter.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.performance_counter_maximum = sc->value;
					if (measurements.performance_counter.value>sc->value) {
						measurements.performance_counter.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_TIME_BEACON
            case ID_TIME_BEACON:
                //! Beacon for time synchronization (ID: 0x00000100)
				if (sc->command == safetyCmdSetMin) {
					measurements.time_beacon_minimum = sc->value;
					if (measurements.time_beacon.value<sc->value) {
						measurements.time_beacon.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.time_beacon_maximum = sc->value;
					if (measurements.time_beacon.value>sc->value) {
						measurements.time_beacon.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12)):
                //! Given measurement is too low (ID: 0x02070100)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_measurement_too_low_minimum = sc->value;
					if (measurements.n560_0_measurement_too_low.value<sc->value) {
						measurements.n560_0_measurement_too_low.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_measurement_too_low_maximum = sc->value;
					if (measurements.n560_0_measurement_too_low.value>sc->value) {
						measurements.n560_0_measurement_too_low.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_LOW
            case (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12)):
                //! Given measurement is too low (ID: 0x02071100)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_measurement_too_low_minimum = sc->value;
					if (measurements.n560_1_measurement_too_low.value<sc->value) {
						measurements.n560_1_measurement_too_low.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_measurement_too_low_maximum = sc->value;
					if (measurements.n560_1_measurement_too_low.value>sc->value) {
						measurements.n560_1_measurement_too_low.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12)):
                //! Given measurement is too high (ID: 0x02070101)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_measurement_too_high_minimum = sc->value;
					if (measurements.n560_0_measurement_too_high.value<sc->value) {
						measurements.n560_0_measurement_too_high.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_measurement_too_high_maximum = sc->value;
					if (measurements.n560_0_measurement_too_high.value>sc->value) {
						measurements.n560_0_measurement_too_high.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_HIGH
            case (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12)):
                //! Given measurement is too high (ID: 0x02071101)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_measurement_too_high_minimum = sc->value;
					if (measurements.n560_1_measurement_too_high.value<sc->value) {
						measurements.n560_1_measurement_too_high.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_measurement_too_high_maximum = sc->value;
					if (measurements.n560_1_measurement_too_high.value>sc->value) {
						measurements.n560_1_measurement_too_high.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (0 << 12)):
                //! Acquire define missing for Message-ID (ID: 0x02070f00)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_acquire_missing_minimum = sc->value;
					if (measurements.n560_0_acquire_missing.value<sc->value) {
						measurements.n560_0_acquire_missing.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_acquire_missing_maximum = sc->value;
					if (measurements.n560_0_acquire_missing.value>sc->value) {
						measurements.n560_0_acquire_missing.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_ACQUIRE_MISSING
            case (ID_N560_X_ACQUIRE_MISSING | (1 << 12)):
                //! Acquire define missing for Message-ID (ID: 0x02071f00)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_acquire_missing_minimum = sc->value;
					if (measurements.n560_1_acquire_missing.value<sc->value) {
						measurements.n560_1_acquire_missing.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_acquire_missing_maximum = sc->value;
					if (measurements.n560_1_acquire_missing.value>sc->value) {
						measurements.n560_1_acquire_missing.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_SAFETY_SET
            case ID_SAFETY_SET:
                //! Set command for safety layer (ID: 0x07000001)
				if (sc->command == safetyCmdSetMin) {
					measurements.safety_set_minimum = sc->value;
					if (measurements.safety_set.value<sc->value) {
						measurements.safety_set.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.safety_set_maximum = sc->value;
					if (measurements.safety_set.value>sc->value) {
						measurements.safety_set.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_SAFETY_TIMEOUT
            case ID_SAFETY_TIMEOUT:
                //! Timeout in safety state machine (ID: 0x07000002)
				if (sc->command == safetyCmdSetMin) {
					measurements.safety_timeout_minimum = sc->value;
					if (measurements.safety_timeout.value<sc->value) {
						measurements.safety_timeout.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.safety_timeout_maximum = sc->value;
					if (measurements.safety_timeout.value>sc->value) {
						measurements.safety_timeout.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_STARTUP
            case (ID_N560_X_STARTUP | (0 << 12)):
                //! Startup sequence send out by  (ID: 0x0c070000)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_startup_minimum = sc->value;
					if (measurements.n560_0_startup.value<sc->value) {
						measurements.n560_0_startup.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_startup_maximum = sc->value;
					if (measurements.n560_0_startup.value>sc->value) {
						measurements.n560_0_startup.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_STARTUP
            case (ID_N560_X_STARTUP | (1 << 12)):
                //! Startup sequence send out by  (ID: 0x0c071000)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_startup_minimum = sc->value;
					if (measurements.n560_1_startup.value<sc->value) {
						measurements.n560_1_startup.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_startup_maximum = sc->value;
					if (measurements.n560_1_startup.value>sc->value) {
						measurements.n560_1_startup.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (0 << 12)):
                //! Card of MCU full:  (ID: 0x0c070010)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_card_disabled_minimum = sc->value;
					if (measurements.n560_0_card_disabled.value<sc->value) {
						measurements.n560_0_card_disabled.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_card_disabled_maximum = sc->value;
					if (measurements.n560_0_card_disabled.value>sc->value) {
						measurements.n560_0_card_disabled.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_CARD_DISABLED
            case (ID_N560_X_CARD_DISABLED | (1 << 12)):
                //! Card of MCU full:  (ID: 0x0c071010)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_card_disabled_minimum = sc->value;
					if (measurements.n560_1_card_disabled.value<sc->value) {
						measurements.n560_1_card_disabled.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_card_disabled_maximum = sc->value;
					if (measurements.n560_1_card_disabled.value>sc->value) {
						measurements.n560_1_card_disabled.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (0 << 12)):
                //! Status message of MCU in response of the time beacon:  (ID: 0x0c070020)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_status_reply_minimum = sc->value;
					if (measurements.n560_0_status_reply.value<sc->value) {
						measurements.n560_0_status_reply.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_status_reply_maximum = sc->value;
					if (measurements.n560_0_status_reply.value>sc->value) {
						measurements.n560_0_status_reply.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_STATUS_REPLY
            case (ID_N560_X_STATUS_REPLY | (1 << 12)):
                //! Status message of MCU in response of the time beacon:  (ID: 0x0c071020)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_status_reply_minimum = sc->value;
					if (measurements.n560_1_status_reply.value<sc->value) {
						measurements.n560_1_status_reply.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_status_reply_maximum = sc->value;
					if (measurements.n560_1_status_reply.value>sc->value) {
						measurements.n560_1_status_reply.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_ALIVE
            case (ID_N560_X_ALIVE | (0 << 12)):
                //! Alive message (ID: 0x0c070030)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_alive_minimum = sc->value;
					if (measurements.n560_0_alive.value<sc->value) {
						measurements.n560_0_alive.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_alive_maximum = sc->value;
					if (measurements.n560_0_alive.value>sc->value) {
						measurements.n560_0_alive.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_ALIVE
            case (ID_N560_X_ALIVE | (1 << 12)):
                //! Alive message (ID: 0x0c071030)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_alive_minimum = sc->value;
					if (measurements.n560_1_alive.value<sc->value) {
						measurements.n560_1_alive.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_alive_maximum = sc->value;
					if (measurements.n560_1_alive.value>sc->value) {
						measurements.n560_1_alive.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_RESEND_STARTUP
            case ID_RESEND_STARTUP:
                //! resend startup (ID: 0x0cffffff)
				if (sc->command == safetyCmdSetMin) {
					measurements.resend_startup_minimum = sc->value;
					if (measurements.resend_startup.value<sc->value) {
						measurements.resend_startup.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.resend_startup_maximum = sc->value;
					if (measurements.resend_startup.value>sc->value) {
						measurements.resend_startup.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_USED_IDS
            case (ID_N560_X_USED_IDS | (0 << 12)):
                //! Used Ids send out by  (ID: 0x0d070000)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_used_ids_minimum = sc->value;
					if (measurements.n560_0_used_ids.value<sc->value) {
						measurements.n560_0_used_ids.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_used_ids_maximum = sc->value;
					if (measurements.n560_0_used_ids.value>sc->value) {
						measurements.n560_0_used_ids.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_USED_IDS
            case (ID_N560_X_USED_IDS | (1 << 12)):
                //! Used Ids send out by  (ID: 0x0d071000)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_used_ids_minimum = sc->value;
					if (measurements.n560_1_used_ids.value<sc->value) {
						measurements.n560_1_used_ids.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_used_ids_maximum = sc->value;
					if (measurements.n560_1_used_ids.value>sc->value) {
						measurements.n560_1_used_ids.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_SEND_USED_IDS
            case ID_SEND_USED_IDS:
                //! Devices should send out their used IDS (ID: 0x0dffffff)
				if (sc->command == safetyCmdSetMin) {
					measurements.send_used_ids_minimum = sc->value;
					if (measurements.send_used_ids.value<sc->value) {
						measurements.send_used_ids.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.send_used_ids_maximum = sc->value;
					if (measurements.send_used_ids.value>sc->value) {
						measurements.send_used_ids.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_SPO2
            case (ID_N560_X_SPO2 | (0 << 12)):
                //! SpO2 (ID: 0x10070000)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_spo2_minimum = sc->value;
					if (measurements.n560_0_spo2.value<sc->value) {
						measurements.n560_0_spo2.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_spo2_maximum = sc->value;
					if (measurements.n560_0_spo2.value>sc->value) {
						measurements.n560_0_spo2.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_SPO2
            case (ID_N560_X_SPO2 | (1 << 12)):
                //! SpO2 (ID: 0x10071000)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_spo2_minimum = sc->value;
					if (measurements.n560_1_spo2.value<sc->value) {
						measurements.n560_1_spo2.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_spo2_maximum = sc->value;
					if (measurements.n560_1_spo2.value>sc->value) {
						measurements.n560_1_spo2.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_BPM
            case (ID_N560_X_BPM | (0 << 12)):
                //! Pulse rate (ID: 0x10070001)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_bpm_minimum = sc->value;
					if (measurements.n560_0_bpm.value<sc->value) {
						measurements.n560_0_bpm.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_bpm_maximum = sc->value;
					if (measurements.n560_0_bpm.value>sc->value) {
						measurements.n560_0_bpm.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_BPM
            case (ID_N560_X_BPM | (1 << 12)):
                //! Pulse rate (ID: 0x10071001)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_bpm_minimum = sc->value;
					if (measurements.n560_1_bpm.value<sc->value) {
						measurements.n560_1_bpm.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_bpm_maximum = sc->value;
					if (measurements.n560_1_bpm.value>sc->value) {
						measurements.n560_1_bpm.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_PA
            case (ID_N560_X_PA | (0 << 12)):
                //! Pulse amplitude (ID: 0x10070002)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_pa_minimum = sc->value;
					if (measurements.n560_0_pa.value<sc->value) {
						measurements.n560_0_pa.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_pa_maximum = sc->value;
					if (measurements.n560_0_pa.value>sc->value) {
						measurements.n560_0_pa.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_PA
            case (ID_N560_X_PA | (1 << 12)):
                //! Pulse amplitude (ID: 0x10071002)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_pa_minimum = sc->value;
					if (measurements.n560_1_pa.value<sc->value) {
						measurements.n560_1_pa.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_pa_maximum = sc->value;
					if (measurements.n560_1_pa.value>sc->value) {
						measurements.n560_1_pa.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_0_STATUS
            case (ID_N560_X_STATUS | (0 << 12)):
                //! Status (ID: 0x10070003)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_0_status_minimum = sc->value;
					if (measurements.n560_0_status.value<sc->value) {
						measurements.n560_0_status.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_0_status_maximum = sc->value;
					if (measurements.n560_0_status.value>sc->value) {
						measurements.n560_0_status.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        #ifdef USE_TS_ID_N560_1_STATUS
            case (ID_N560_X_STATUS | (1 << 12)):
                //! Status (ID: 0x10071003)
				if (sc->command == safetyCmdSetMin) {
					measurements.n560_1_status_minimum = sc->value;
					if (measurements.n560_1_status.value<sc->value) {
						measurements.n560_1_status.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.n560_1_status_maximum = sc->value;
					if (measurements.n560_1_status.value>sc->value) {
						measurements.n560_1_status.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
        default:
            (void)sc;
            break;
    }

    return 0;
}



