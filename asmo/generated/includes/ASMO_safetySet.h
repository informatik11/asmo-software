/**
 * \file ASMO_safetySet.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#ifndef ASMO_SAFETYSET_H
#define ASMO_SAFETYSET_H

msg_t ASMO_safetySet(ASMO_safetyConfig *sc);

#endif /* ASMO_SAFETYSET_H */
