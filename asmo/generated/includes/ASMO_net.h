/**
 * \file ASMO_net.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#ifndef ASMO_NET_H_
#define ASMO_NET_H_

#include "SmartECLA_allHeaders.h"

ASMO_Stream* asmo_net_get_stream_for_message_id(uint32_t id);
void asmo_net_init(void);

#endif /* ASMO_NETWORKING_H_ */
