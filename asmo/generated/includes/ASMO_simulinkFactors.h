/**
 * \file ASMO_simulinkFactors.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#ifndef ASMO_SM_FACTORS_H
#define ASMO_SM_FACTORS_H

#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

uint32_t simulink_getFactor(uint32_t id, double value);

#endif /* ASMO_SM_FACTORS_H */
