/**
 * \file ASMO_simulinkGet.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#ifndef ASMO_SIMULINK_GET_H
#define ASMO_SIMULINK_GET_H

#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

bool simulink_getMeasurement(uint32_t id, double *value,
                             timestamp_t *timestamp);

#endif /* ASMO_SIMULINK_GET_H */
