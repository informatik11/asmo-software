/**
 * \file ASMO_RTPS.hpp
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

/*
 *  Created on: Aug 2021-Aug 2022
 *      Author: sven, Benedikt Conze
 */

#ifndef ASMO_RTPS_HPP
#define ASMO_RTPS_HPP

#ifdef __cplusplus
extern "C" {
#endif

#include "SmartECLA_allHeaders.h"

typedef struct {
  uint8_t type;
  uint32_t id;
  uint64_t msg;
  asmo_eth_prio prio;
  timestamp_t ts;
} asmo_rtps_message;

struct asmo_rtps_simple_msg {
  uint32_t id;
  uint64_t msg;
};

union asmo_rtps_msg_union {
  struct asmo_rtps_simple_msg simple_msg;
  ASMO_Measurement measurement_msg;
};

struct asmo_rtps_msg {
  union asmo_rtps_msg_union msg;
  asmo_eth_prio prio;
  uint8_t type;
};

void ASMO_initRTPS(void);

msg_t sendLocalMeasurementToRTPS(ASMO_Measurement* measurement);

msg_t sendSimpleRTPSMessage(uint32_t id, uint64_t message);

msg_t sendSimpleRTPSMessageRef(uint32_t id, uint64_t *message);

msg_t sendSimpleRTPSMessageTimeout(uint32_t id, uint64_t message, uint32_t timeout);

msg_t sendSimpleRTPSMessageRefTimeout(uint32_t id, uint64_t *message, uint32_t timeout);

#ifdef __cplusplus
}
#endif

#endif /* ASMO_RTPS_HPP */

