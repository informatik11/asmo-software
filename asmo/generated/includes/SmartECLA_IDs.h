/**
 * \file SmartECLA_IDs.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#ifndef SMARTECLA_IDS_H_
#define SMARTECLA_IDS_H_

// Block IDs for filtering received messages
#define BLOCK_ID_MASK			(0xFF << 24)
#define BLOCK_ID_MEDICAL_ALERT (0x02 << 24)
#define BLOCK_ID_LOW_PRIO_ALERT (0x03 << 24)
#define BLOCK_ID_DEVICE_COMMAND (0x05 << 24)
#define BLOCK_ID_MODEL_COMMAND (0x06 << 24)
#define BLOCK_ID_SAFETY_COMMAND (0x07 << 24)
#define BLOCK_ID_CONTROL_VALUES (0x08 << 24)
#define BLOCK_ID_MMU_NOTIFY (0x0a << 24)
#define BLOCK_ID_MODEL_NOTIFY (0x0b << 24)
#define BLOCK_ID_NETWORK_NOTIFY (0x0c << 24)
#define BLOCK_ID_USED_ID (0x0d << 24)
#define BLOCK_ID_MEASUREMENTS (0x10 << 24)
#define BLOCK_ID_PARAMETERS (0x11 << 24)

// Device IDs for filtering received messages
#define DEVICE_ID_MASK			(0xFF << 16)
#define DEVICE_ID_INTERNAL (0x00 << 16)
#define DEVICE_ID_N560 (0x07 << 16)
#define DEVICE_ID_OTHERS (0xff << 16)

// one default shifter
#define DEFAULT_DEVNUMBER_SHIFT		12
// safety defines
#define SAFETY_TOO_LOW			(0x100)
#define SAFETY_TOO_HIGH			(0x101)

// shifter values (all are 12)
#define DEVICE_N560_SHIFT 12

#define MESSAGE_STARTUP                         0x0C000000  // indicates how the masked bits have to be set
#define MESSAGE_DISABLED_CARDS                  0x0C000010  // Needed for building the card disabled message ID in MCU code
#define MESSAGE_STATUS_REPLY                    0x0C000020  // Needed for building the status message ID in MCU code


//! Macro to read the device number out of an ID
#define getDeviceNumber(id,shifter)			((id >> shifter) & 0xF)
//! Macro to add the device number to an id. The preset bits at the device number's position in the IDs are discarded.
#define addDeviceNumber(devNum,id,shifter)	((id & ~(0xF << shifter)) | (devNum << shifter))
enum message_id { 
    //! Performance Messungen
    ID_PERFORMANCE_COUNTER = 0x00000010, 
    //! Beacon for time synchronization
    ID_TIME_BEACON = 0x00000100, 
    //! Given measurement is too low
    ID_N560_X_MEASUREMENT_TOO_LOW = 0x02070100, 
    //! Given measurement is too high
    ID_N560_X_MEASUREMENT_TOO_HIGH = 0x02070101, 
    //! Acquire define missing for Message-ID
    ID_N560_X_ACQUIRE_MISSING = 0x02070f00, 
    //! Set command for safety layer
    ID_SAFETY_SET = 0x07000001, 
    //! Timeout in safety state machine
    ID_SAFETY_TIMEOUT = 0x07000002, 
    //! Startup sequence send out by 
    ID_N560_X_STARTUP = 0x0c070000, 
    //! Card of MCU full: 
    ID_N560_X_CARD_DISABLED = 0x0c070010, 
    //! Status message of MCU in response of the time beacon: 
    ID_N560_X_STATUS_REPLY = 0x0c070020, 
    //! Alive message
    ID_N560_X_ALIVE = 0x0c070030, 
    //! resend startup
    ID_RESEND_STARTUP = 0x0cffffff, 
    //! Used Ids send out by 
    ID_N560_X_USED_IDS = 0x0d070000, 
    //! Devices should send out their used IDS
    ID_SEND_USED_IDS = 0x0dffffff, 
    //! SpO2
    ID_N560_X_SPO2 = 0x10070000, 
    //! Pulse rate
    ID_N560_X_BPM = 0x10070001, 
    //! Pulse amplitude
    ID_N560_X_PA = 0x10070002, 
    //! Status
    ID_N560_X_STATUS = 0x10070003, 
};
#endif /* SMARTECLA_IDS_H_ */
