/**
 * \file ASMO_datastorageGetTS.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */
#ifndef ASMO_DATASTORAGE_GET_TS_H
#define ASMO_DATASTORAGE_GET_TS_H
#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

uint8_t ASMO_getTSMeasurement(uint32_t id, uint8_t index, ds_TSReg *reg);

typedef struct {
    #ifdef USE_TS_ID_PERFORMANCE_COUNTER
        //! Performance Messungen (ID: 0x00000010)
        ds_TSReg *performance_counter;
        uint8_t num_performance_counter;
    #endif
    #ifdef USE_TS_ID_TIME_BEACON
        //! Beacon for time synchronization (ID: 0x00000100)
        ds_TSReg *time_beacon;
        uint8_t num_time_beacon;
    #endif
    #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_LOW
        //! Given measurement is too low (ID: 0x02070100)
        ds_TSReg *n560_0_measurement_too_low;
        uint8_t num_n560_0_measurement_too_low;
    #endif
    #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_LOW
        //! Given measurement is too low (ID: 0x02071100)
        ds_TSReg *n560_1_measurement_too_low;
        uint8_t num_n560_1_measurement_too_low;
    #endif
    #ifdef USE_TS_ID_N560_0_MEASUREMENT_TOO_HIGH
        //! Given measurement is too high (ID: 0x02070101)
        ds_TSReg *n560_0_measurement_too_high;
        uint8_t num_n560_0_measurement_too_high;
    #endif
    #ifdef USE_TS_ID_N560_1_MEASUREMENT_TOO_HIGH
        //! Given measurement is too high (ID: 0x02071101)
        ds_TSReg *n560_1_measurement_too_high;
        uint8_t num_n560_1_measurement_too_high;
    #endif
    #ifdef USE_TS_ID_N560_0_ACQUIRE_MISSING
        //! Acquire define missing for Message-ID (ID: 0x02070f00)
        ds_TSReg *n560_0_acquire_missing;
        uint8_t num_n560_0_acquire_missing;
    #endif
    #ifdef USE_TS_ID_N560_1_ACQUIRE_MISSING
        //! Acquire define missing for Message-ID (ID: 0x02071f00)
        ds_TSReg *n560_1_acquire_missing;
        uint8_t num_n560_1_acquire_missing;
    #endif
    #ifdef USE_TS_ID_SAFETY_SET
        //! Set command for safety layer (ID: 0x07000001)
        ds_TSReg *safety_set;
        uint8_t num_safety_set;
    #endif
    #ifdef USE_TS_ID_SAFETY_TIMEOUT
        //! Timeout in safety state machine (ID: 0x07000002)
        ds_TSReg *safety_timeout;
        uint8_t num_safety_timeout;
    #endif
    #ifdef USE_TS_ID_N560_0_STARTUP
        //! Startup sequence send out by  (ID: 0x0c070000)
        ds_TSReg *n560_0_startup;
        uint8_t num_n560_0_startup;
    #endif
    #ifdef USE_TS_ID_N560_1_STARTUP
        //! Startup sequence send out by  (ID: 0x0c071000)
        ds_TSReg *n560_1_startup;
        uint8_t num_n560_1_startup;
    #endif
    #ifdef USE_TS_ID_N560_0_CARD_DISABLED
        //! Card of MCU full:  (ID: 0x0c070010)
        ds_TSReg *n560_0_card_disabled;
        uint8_t num_n560_0_card_disabled;
    #endif
    #ifdef USE_TS_ID_N560_1_CARD_DISABLED
        //! Card of MCU full:  (ID: 0x0c071010)
        ds_TSReg *n560_1_card_disabled;
        uint8_t num_n560_1_card_disabled;
    #endif
    #ifdef USE_TS_ID_N560_0_STATUS_REPLY
        //! Status message of MCU in response of the time beacon:  (ID: 0x0c070020)
        ds_TSReg *n560_0_status_reply;
        uint8_t num_n560_0_status_reply;
    #endif
    #ifdef USE_TS_ID_N560_1_STATUS_REPLY
        //! Status message of MCU in response of the time beacon:  (ID: 0x0c071020)
        ds_TSReg *n560_1_status_reply;
        uint8_t num_n560_1_status_reply;
    #endif
    #ifdef USE_TS_ID_N560_0_ALIVE
        //! Alive message (ID: 0x0c070030)
        ds_TSReg *n560_0_alive;
        uint8_t num_n560_0_alive;
    #endif
    #ifdef USE_TS_ID_N560_1_ALIVE
        //! Alive message (ID: 0x0c071030)
        ds_TSReg *n560_1_alive;
        uint8_t num_n560_1_alive;
    #endif
    #ifdef USE_TS_ID_RESEND_STARTUP
        //! resend startup (ID: 0x0cffffff)
        ds_TSReg *resend_startup;
        uint8_t num_resend_startup;
    #endif
    #ifdef USE_TS_ID_N560_0_USED_IDS
        //! Used Ids send out by  (ID: 0x0d070000)
        ds_TSReg *n560_0_used_ids;
        uint8_t num_n560_0_used_ids;
    #endif
    #ifdef USE_TS_ID_N560_1_USED_IDS
        //! Used Ids send out by  (ID: 0x0d071000)
        ds_TSReg *n560_1_used_ids;
        uint8_t num_n560_1_used_ids;
    #endif
    #ifdef USE_TS_ID_SEND_USED_IDS
        //! Devices should send out their used IDS (ID: 0x0dffffff)
        ds_TSReg *send_used_ids;
        uint8_t num_send_used_ids;
    #endif
    #ifdef USE_TS_ID_N560_0_SPO2
        //! SpO2 (ID: 0x10070000)
        ds_TSReg *n560_0_spo2;
        uint8_t num_n560_0_spo2;
    #endif
    #ifdef USE_TS_ID_N560_1_SPO2
        //! SpO2 (ID: 0x10071000)
        ds_TSReg *n560_1_spo2;
        uint8_t num_n560_1_spo2;
    #endif
    #ifdef USE_TS_ID_N560_0_BPM
        //! Pulse rate (ID: 0x10070001)
        ds_TSReg *n560_0_bpm;
        uint8_t num_n560_0_bpm;
    #endif
    #ifdef USE_TS_ID_N560_1_BPM
        //! Pulse rate (ID: 0x10071001)
        ds_TSReg *n560_1_bpm;
        uint8_t num_n560_1_bpm;
    #endif
    #ifdef USE_TS_ID_N560_0_PA
        //! Pulse amplitude (ID: 0x10070002)
        ds_TSReg *n560_0_pa;
        uint8_t num_n560_0_pa;
    #endif
    #ifdef USE_TS_ID_N560_1_PA
        //! Pulse amplitude (ID: 0x10071002)
        ds_TSReg *n560_1_pa;
        uint8_t num_n560_1_pa;
    #endif
    #ifdef USE_TS_ID_N560_0_STATUS
        //! Status (ID: 0x10070003)
        ds_TSReg *n560_0_status;
        uint8_t num_n560_0_status;
    #endif
    #ifdef USE_TS_ID_N560_1_STATUS
        //! Status (ID: 0x10071003)
        ds_TSReg *n560_1_status;
        uint8_t num_n560_1_status;
    #endif
} ASMO_Timeseries;

extern ASMO_Timeseries ds_TSArray;

#endif // ASMO_DATASTORAGE_GET_TS_H
