/**
 * \file ASMO_measurements.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#ifndef ASMO_MEASUREMENTS_H_
#define ASMO_MEASUREMENTS_H_

#include "SmartECLA_allHeaders.h"

//! Mask for message-Mailbox. It fits on all used IDs.
extern unsigned long message_mailbox_mask;

//! Stores the received ASMO_Measurement in ::measurements
void ASMO_storeMeasurement(ASMO_Measurement *measurement);

//! Returns true, if copied the stored ASMO_Measurement in ::measurements
bool ASMO_getMeasurement(uint32_t ID, ASMO_Measurement **measurement, ASMO_MeasurementConfig **config);

#if !defined(USE_ID_PERFORMANCE_COUNTER) && defined(ACQUIRE_ID_PERFORMANCE_COUNTER)
    #define USE_ID_PERFORMANCE_COUNTER
#endif
#if !defined(USE_ID_TIME_BEACON) && defined(ACQUIRE_ID_TIME_BEACON)
    #define USE_ID_TIME_BEACON
#endif
#if defined(USE_ID_N560_X_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
    #define USE_ID_N560_0_MEASUREMENT_TOO_LOW
    #define USE_ID_N560_1_MEASUREMENT_TOO_LOW
#endif
#if defined(USE_ID_N560_X_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
    #define USE_ID_N560_0_MEASUREMENT_TOO_HIGH
    #define USE_ID_N560_1_MEASUREMENT_TOO_HIGH
#endif
#if defined(USE_ID_N560_X_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
    #define USE_ID_N560_0_ACQUIRE_MISSING
    #define USE_ID_N560_1_ACQUIRE_MISSING
#endif
#if !defined(USE_ID_SAFETY_SET) && defined(ACQUIRE_ID_SAFETY_SET)
    #define USE_ID_SAFETY_SET
#endif
#if !defined(USE_ID_SAFETY_TIMEOUT) && defined(ACQUIRE_ID_SAFETY_TIMEOUT)
    #define USE_ID_SAFETY_TIMEOUT
#endif
#if defined(USE_ID_N560_X_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
    #define USE_ID_N560_0_STARTUP
    #define USE_ID_N560_1_STARTUP
#endif
#if defined(USE_ID_N560_X_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
    #define USE_ID_N560_0_CARD_DISABLED
    #define USE_ID_N560_1_CARD_DISABLED
#endif
#if defined(USE_ID_N560_X_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
    #define USE_ID_N560_0_STATUS_REPLY
    #define USE_ID_N560_1_STATUS_REPLY
#endif
#if defined(USE_ID_N560_X_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
    #define USE_ID_N560_0_ALIVE
    #define USE_ID_N560_1_ALIVE
#endif
#if !defined(USE_ID_RESEND_STARTUP) && defined(ACQUIRE_ID_RESEND_STARTUP)
    #define USE_ID_RESEND_STARTUP
#endif
#if defined(USE_ID_N560_X_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
    #define USE_ID_N560_0_USED_IDS
    #define USE_ID_N560_1_USED_IDS
#endif
#if !defined(USE_ID_SEND_USED_IDS) && defined(ACQUIRE_ID_SEND_USED_IDS)
    #define USE_ID_SEND_USED_IDS
#endif
#if defined(USE_ID_N560_X_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
    #define USE_ID_N560_0_SPO2
    #define USE_ID_N560_1_SPO2
#endif
#if defined(USE_ID_N560_X_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
    #define USE_ID_N560_0_BPM
    #define USE_ID_N560_1_BPM
#endif
#if defined(USE_ID_N560_X_PA) || defined(ACQUIRE_ID_N560_X_PA)
    #define USE_ID_N560_0_PA
    #define USE_ID_N560_1_PA
#endif
#if defined(USE_ID_N560_X_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
    #define USE_ID_N560_0_STATUS
    #define USE_ID_N560_1_STATUS
#endif

typedef struct {
    #if defined(USE_ID_PERFORMANCE_COUNTER) || defined(ACQUIRE_ID_PERFORMANCE_COUNTER)
        ASMO_Measurement performance_counter;
        unsigned int performance_counter_minimum;
        unsigned int performance_counter_maximum;
    #endif
    #if defined(USE_ID_TIME_BEACON) || defined(ACQUIRE_ID_TIME_BEACON)
        ASMO_Measurement time_beacon;
        unsigned int time_beacon_minimum;
        unsigned int time_beacon_maximum;
    #endif
    #if defined(USE_ID_N560_0_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
        ASMO_Measurement n560_0_measurement_too_low;
        unsigned int n560_0_measurement_too_low_minimum;
        unsigned int n560_0_measurement_too_low_maximum;
    #endif
    #if defined(USE_ID_N560_1_MEASUREMENT_TOO_LOW) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW)
        ASMO_Measurement n560_1_measurement_too_low;
        unsigned int n560_1_measurement_too_low_minimum;
        unsigned int n560_1_measurement_too_low_maximum;
    #endif
    #if defined(USE_ID_N560_0_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
        ASMO_Measurement n560_0_measurement_too_high;
        unsigned int n560_0_measurement_too_high_minimum;
        unsigned int n560_0_measurement_too_high_maximum;
    #endif
    #if defined(USE_ID_N560_1_MEASUREMENT_TOO_HIGH) || defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH)
        ASMO_Measurement n560_1_measurement_too_high;
        unsigned int n560_1_measurement_too_high_minimum;
        unsigned int n560_1_measurement_too_high_maximum;
    #endif
    #if defined(USE_ID_N560_0_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
        ASMO_Measurement n560_0_acquire_missing;
        unsigned int n560_0_acquire_missing_minimum;
        unsigned int n560_0_acquire_missing_maximum;
    #endif
    #if defined(USE_ID_N560_1_ACQUIRE_MISSING) || defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING)
        ASMO_Measurement n560_1_acquire_missing;
        unsigned int n560_1_acquire_missing_minimum;
        unsigned int n560_1_acquire_missing_maximum;
    #endif
    #if defined(USE_ID_SAFETY_SET) || defined(ACQUIRE_ID_SAFETY_SET)
        ASMO_Measurement safety_set;
        unsigned int safety_set_minimum;
        unsigned int safety_set_maximum;
    #endif
    #if defined(USE_ID_SAFETY_TIMEOUT) || defined(ACQUIRE_ID_SAFETY_TIMEOUT)
        ASMO_Measurement safety_timeout;
        unsigned int safety_timeout_minimum;
        unsigned int safety_timeout_maximum;
    #endif
    #if defined(USE_ID_N560_0_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
        ASMO_Measurement n560_0_startup;
        unsigned int n560_0_startup_minimum;
        unsigned int n560_0_startup_maximum;
    #endif
    #if defined(USE_ID_N560_1_STARTUP) || defined(ACQUIRE_ID_N560_X_STARTUP)
        ASMO_Measurement n560_1_startup;
        unsigned int n560_1_startup_minimum;
        unsigned int n560_1_startup_maximum;
    #endif
    #if defined(USE_ID_N560_0_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
        ASMO_Measurement n560_0_card_disabled;
        unsigned int n560_0_card_disabled_minimum;
        unsigned int n560_0_card_disabled_maximum;
    #endif
    #if defined(USE_ID_N560_1_CARD_DISABLED) || defined(ACQUIRE_ID_N560_X_CARD_DISABLED)
        ASMO_Measurement n560_1_card_disabled;
        unsigned int n560_1_card_disabled_minimum;
        unsigned int n560_1_card_disabled_maximum;
    #endif
    #if defined(USE_ID_N560_0_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
        ASMO_Measurement n560_0_status_reply;
        unsigned int n560_0_status_reply_minimum;
        unsigned int n560_0_status_reply_maximum;
    #endif
    #if defined(USE_ID_N560_1_STATUS_REPLY) || defined(ACQUIRE_ID_N560_X_STATUS_REPLY)
        ASMO_Measurement n560_1_status_reply;
        unsigned int n560_1_status_reply_minimum;
        unsigned int n560_1_status_reply_maximum;
    #endif
    #if defined(USE_ID_N560_0_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
        ASMO_Measurement n560_0_alive;
        unsigned int n560_0_alive_minimum;
        unsigned int n560_0_alive_maximum;
    #endif
    #if defined(USE_ID_N560_1_ALIVE) || defined(ACQUIRE_ID_N560_X_ALIVE)
        ASMO_Measurement n560_1_alive;
        unsigned int n560_1_alive_minimum;
        unsigned int n560_1_alive_maximum;
    #endif
    #if defined(USE_ID_RESEND_STARTUP) || defined(ACQUIRE_ID_RESEND_STARTUP)
        ASMO_Measurement resend_startup;
        unsigned int resend_startup_minimum;
        unsigned int resend_startup_maximum;
    #endif
    #if defined(USE_ID_N560_0_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
        ASMO_Measurement n560_0_used_ids;
        unsigned int n560_0_used_ids_minimum;
        unsigned int n560_0_used_ids_maximum;
    #endif
    #if defined(USE_ID_N560_1_USED_IDS) || defined(ACQUIRE_ID_N560_X_USED_IDS)
        ASMO_Measurement n560_1_used_ids;
        unsigned int n560_1_used_ids_minimum;
        unsigned int n560_1_used_ids_maximum;
    #endif
    #if defined(USE_ID_SEND_USED_IDS) || defined(ACQUIRE_ID_SEND_USED_IDS)
        ASMO_Measurement send_used_ids;
        unsigned int send_used_ids_minimum;
        unsigned int send_used_ids_maximum;
    #endif
    #if defined(USE_ID_N560_0_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
        ASMO_Measurement n560_0_spo2;
        unsigned int n560_0_spo2_minimum;
        unsigned int n560_0_spo2_maximum;
    #endif
    #if defined(USE_ID_N560_1_SPO2) || defined(ACQUIRE_ID_N560_X_SPO2)
        ASMO_Measurement n560_1_spo2;
        unsigned int n560_1_spo2_minimum;
        unsigned int n560_1_spo2_maximum;
    #endif
    #if defined(USE_ID_N560_0_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
        ASMO_Measurement n560_0_bpm;
        unsigned int n560_0_bpm_minimum;
        unsigned int n560_0_bpm_maximum;
    #endif
    #if defined(USE_ID_N560_1_BPM) || defined(ACQUIRE_ID_N560_X_BPM)
        ASMO_Measurement n560_1_bpm;
        unsigned int n560_1_bpm_minimum;
        unsigned int n560_1_bpm_maximum;
    #endif
    #if defined(USE_ID_N560_0_PA) || defined(ACQUIRE_ID_N560_X_PA)
        ASMO_Measurement n560_0_pa;
        unsigned int n560_0_pa_minimum;
        unsigned int n560_0_pa_maximum;
    #endif
    #if defined(USE_ID_N560_1_PA) || defined(ACQUIRE_ID_N560_X_PA)
        ASMO_Measurement n560_1_pa;
        unsigned int n560_1_pa_minimum;
        unsigned int n560_1_pa_maximum;
    #endif
    #if defined(USE_ID_N560_0_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
        ASMO_Measurement n560_0_status;
        unsigned int n560_0_status_minimum;
        unsigned int n560_0_status_maximum;
    #endif
    #if defined(USE_ID_N560_1_STATUS) || defined(ACQUIRE_ID_N560_X_STATUS)
        ASMO_Measurement n560_1_status;
        unsigned int n560_1_status_minimum;
        unsigned int n560_1_status_maximum;
    #endif
} ASMO_Data;

typedef struct {
    #ifdef ACQUIRE_ID_PERFORMANCE_COUNTER
        //! Performance Messungen (ID: 0x00000010)
        ASMO_MeasurementConfig performance_counter_config;
    #endif
    #ifdef ACQUIRE_ID_TIME_BEACON
        //! Beacon for time synchronization (ID: 0x00000100)
        ASMO_MeasurementConfig time_beacon_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW
        //! Given measurement is too low (ID: 0x02070100)
        ASMO_MeasurementConfig n560_0_measurement_too_low_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW
        //! Given measurement is too low (ID: 0x02071100)
        ASMO_MeasurementConfig n560_1_measurement_too_low_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH
        //! Given measurement is too high (ID: 0x02070101)
        ASMO_MeasurementConfig n560_0_measurement_too_high_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH
        //! Given measurement is too high (ID: 0x02071101)
        ASMO_MeasurementConfig n560_1_measurement_too_high_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_ACQUIRE_MISSING
        //! Acquire define missing for Message-ID (ID: 0x02070f00)
        ASMO_MeasurementConfig n560_0_acquire_missing_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_ACQUIRE_MISSING
        //! Acquire define missing for Message-ID (ID: 0x02071f00)
        ASMO_MeasurementConfig n560_1_acquire_missing_config;
    #endif
    #ifdef ACQUIRE_ID_SAFETY_SET
        //! Set command for safety layer (ID: 0x07000001)
        ASMO_MeasurementConfig safety_set_config;
    #endif
    #ifdef ACQUIRE_ID_SAFETY_TIMEOUT
        //! Timeout in safety state machine (ID: 0x07000002)
        ASMO_MeasurementConfig safety_timeout_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_STARTUP
        //! Startup sequence send out by  (ID: 0x0c070000)
        ASMO_MeasurementConfig n560_0_startup_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_STARTUP
        //! Startup sequence send out by  (ID: 0x0c071000)
        ASMO_MeasurementConfig n560_1_startup_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_CARD_DISABLED
        //! Card of MCU full:  (ID: 0x0c070010)
        ASMO_MeasurementConfig n560_0_card_disabled_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_CARD_DISABLED
        //! Card of MCU full:  (ID: 0x0c071010)
        ASMO_MeasurementConfig n560_1_card_disabled_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_STATUS_REPLY
        //! Status message of MCU in response of the time beacon:  (ID: 0x0c070020)
        ASMO_MeasurementConfig n560_0_status_reply_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_STATUS_REPLY
        //! Status message of MCU in response of the time beacon:  (ID: 0x0c071020)
        ASMO_MeasurementConfig n560_1_status_reply_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_ALIVE
        //! Alive message (ID: 0x0c070030)
        ASMO_MeasurementConfig n560_0_alive_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_ALIVE
        //! Alive message (ID: 0x0c071030)
        ASMO_MeasurementConfig n560_1_alive_config;
    #endif
    #ifdef ACQUIRE_ID_RESEND_STARTUP
        //! resend startup (ID: 0x0cffffff)
        ASMO_MeasurementConfig resend_startup_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_USED_IDS
        //! Used Ids send out by  (ID: 0x0d070000)
        ASMO_MeasurementConfig n560_0_used_ids_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_USED_IDS
        //! Used Ids send out by  (ID: 0x0d071000)
        ASMO_MeasurementConfig n560_1_used_ids_config;
    #endif
    #ifdef ACQUIRE_ID_SEND_USED_IDS
        //! Devices should send out their used IDS (ID: 0x0dffffff)
        ASMO_MeasurementConfig send_used_ids_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_SPO2
        //! SpO2 (ID: 0x10070000)
        ASMO_MeasurementConfig n560_0_spo2_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_SPO2
        //! SpO2 (ID: 0x10071000)
        ASMO_MeasurementConfig n560_1_spo2_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_BPM
        //! Pulse rate (ID: 0x10070001)
        ASMO_MeasurementConfig n560_0_bpm_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_BPM
        //! Pulse rate (ID: 0x10071001)
        ASMO_MeasurementConfig n560_1_bpm_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_PA
        //! Pulse amplitude (ID: 0x10070002)
        ASMO_MeasurementConfig n560_0_pa_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_PA
        //! Pulse amplitude (ID: 0x10071002)
        ASMO_MeasurementConfig n560_1_pa_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_STATUS
        //! Status (ID: 0x10070003)
        ASMO_MeasurementConfig n560_0_status_config;
    #endif
    #ifdef ACQUIRE_ID_N560_X_STATUS
        //! Status (ID: 0x10071003)
        ASMO_MeasurementConfig n560_1_status_config;
    #endif
} ASMO_Data_Config;

//! Holds the current medical data.
extern ASMO_Data measurements;
extern ASMO_Data_Config acquire_measurements;

// Find any used ID:
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_PERFORMANCE_COUNTER)
#define ANY_USED_MESSAGE_ID ID_PERFORMANCE_COUNTER
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_TIME_BEACON)
#define ANY_USED_MESSAGE_ID ID_TIME_BEACON
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_MEASUREMENT_TOO_LOW)
#define ANY_USED_MESSAGE_ID (ID_N560_X_MEASUREMENT_TOO_LOW | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_MEASUREMENT_TOO_LOW)
#define ANY_USED_MESSAGE_ID (ID_N560_X_MEASUREMENT_TOO_LOW | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_MEASUREMENT_TOO_HIGH)
#define ANY_USED_MESSAGE_ID (ID_N560_X_MEASUREMENT_TOO_HIGH | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_MEASUREMENT_TOO_HIGH)
#define ANY_USED_MESSAGE_ID (ID_N560_X_MEASUREMENT_TOO_HIGH | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_ACQUIRE_MISSING)
#define ANY_USED_MESSAGE_ID (ID_N560_X_ACQUIRE_MISSING | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_ACQUIRE_MISSING)
#define ANY_USED_MESSAGE_ID (ID_N560_X_ACQUIRE_MISSING | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_SAFETY_SET)
#define ANY_USED_MESSAGE_ID ID_SAFETY_SET
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_SAFETY_TIMEOUT)
#define ANY_USED_MESSAGE_ID ID_SAFETY_TIMEOUT
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_STARTUP)
#define ANY_USED_MESSAGE_ID (ID_N560_X_STARTUP | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_STARTUP)
#define ANY_USED_MESSAGE_ID (ID_N560_X_STARTUP | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_CARD_DISABLED)
#define ANY_USED_MESSAGE_ID (ID_N560_X_CARD_DISABLED | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_CARD_DISABLED)
#define ANY_USED_MESSAGE_ID (ID_N560_X_CARD_DISABLED | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_STATUS_REPLY)
#define ANY_USED_MESSAGE_ID (ID_N560_X_STATUS_REPLY | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_STATUS_REPLY)
#define ANY_USED_MESSAGE_ID (ID_N560_X_STATUS_REPLY | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_ALIVE)
#define ANY_USED_MESSAGE_ID (ID_N560_X_ALIVE | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_ALIVE)
#define ANY_USED_MESSAGE_ID (ID_N560_X_ALIVE | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_RESEND_STARTUP)
#define ANY_USED_MESSAGE_ID ID_RESEND_STARTUP
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_USED_IDS)
#define ANY_USED_MESSAGE_ID (ID_N560_X_USED_IDS | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_USED_IDS)
#define ANY_USED_MESSAGE_ID (ID_N560_X_USED_IDS | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_SEND_USED_IDS)
#define ANY_USED_MESSAGE_ID ID_SEND_USED_IDS
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_SPO2)
#define ANY_USED_MESSAGE_ID (ID_N560_X_SPO2 | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_SPO2)
#define ANY_USED_MESSAGE_ID (ID_N560_X_SPO2 | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_BPM)
#define ANY_USED_MESSAGE_ID (ID_N560_X_BPM | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_BPM)
#define ANY_USED_MESSAGE_ID (ID_N560_X_BPM | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_PA)
#define ANY_USED_MESSAGE_ID (ID_N560_X_PA | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_PA)
#define ANY_USED_MESSAGE_ID (ID_N560_X_PA | (1 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_0_STATUS)
#define ANY_USED_MESSAGE_ID (ID_N560_X_STATUS | (0 << 12))
#endif
#if !defined(ANY_USED_MESSAGE_ID) && defined(USE_ID_N560_1_STATUS)
#define ANY_USED_MESSAGE_ID (ID_N560_X_STATUS | (1 << 12))
#endif

#if !defined(ANY_USED_MESSAGE_ID)
#define ANY_USED_MESSAGE_ID 0
#endif

#endif /* ASMO_MEASUREMENTS_H_ */
