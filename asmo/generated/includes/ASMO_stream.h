/**
 * \file ASMO_stream.h
 * Created by The Next-Generation SmartECLA TOML Code Generator on 2023-03-10T12:40:27.236848
 */

#ifndef ASMO_STREAM_H
#define ASMO_STREAM_H

// include USE- and ACQUIRE-macros
#include "MDL_config.h"

/*
 * In this file we define the following macros to 1...
 * ...ASMO_USE_STREAM_<stream-name> if we USE a message from that stream
 * ...ASMO_ACQUIRE_STREAM_<stream-name> if we ACQUIRE a message from that stream
 * ...ASMO_TOUCH_STREAM_<stream-name> if we USE or ACQUIRE a message from that stream
 *
 * We define the following macros to...
 * ...ASMO_NR_USE_STREAMS: the total number of streams with messages we USE
 * ...ASMO_NR_ACQUIRE_STREAMS: the total number of streams with messages we ACQUIRE
 * ...ASMO_NR_TOUCH_STREAMS: the total number of streams with messages we USE or ACQUIRE
 */

#if defined(USE_ID_PERFORMANCE_COUNTER) ||\
    defined(USE_ID_TIME_BEACON) ||\
    defined(USE_ID_SAFETY_SET) ||\
    defined(USE_ID_SAFETY_TIMEOUT) ||\
    0
#define ASMO_USE_STREAM_INTERNAL_MAIN_0 1
#define ASMO_TOUCH_STREAM_INTERNAL_MAIN_0 1
#else
#define ASMO_USE_STREAM_INTERNAL_MAIN_0 0
#endif
#if defined(ACQUIRE_ID_PERFORMANCE_COUNTER) ||\
    defined(ACQUIRE_ID_TIME_BEACON) ||\
    defined(ACQUIRE_ID_SAFETY_SET) ||\
    defined(ACQUIRE_ID_SAFETY_TIMEOUT) ||\
    0
#define ASMO_ACQUIRE_STREAM_INTERNAL_MAIN_0 1
#define ASMO_TOUCH_STREAM_INTERNAL_MAIN_0 1
#else
#define ASMO_ACQUIRE_STREAM_INTERNAL_MAIN_0 0
#endif
#ifndef ASMO_TOUCH_STREAM_INTERNAL_MAIN_0
#define ASMO_TOUCH_STREAM_INTERNAL_MAIN_0 0
#endif

#if defined(USE_ID_N560_0_SPO2) ||\
    defined(USE_ID_N560_0_BPM) ||\
    defined(USE_ID_N560_0_PA) ||\
    defined(USE_ID_N560_0_STATUS) ||\
    defined(USE_ID_N560_0_ACQUIRE_MISSING) ||\
    defined(USE_ID_N560_0_MEASUREMENT_TOO_LOW) ||\
    defined(USE_ID_N560_0_MEASUREMENT_TOO_HIGH) ||\
    defined(USE_ID_N560_0_STARTUP) ||\
    defined(USE_ID_N560_0_CARD_DISABLED) ||\
    defined(USE_ID_N560_0_STATUS_REPLY) ||\
    defined(USE_ID_N560_0_ALIVE) ||\
    defined(USE_ID_N560_0_USED_IDS) ||\
    0
#define ASMO_USE_STREAM_N560_MAIN_0 1
#define ASMO_TOUCH_STREAM_N560_MAIN_0 1
#else
#define ASMO_USE_STREAM_N560_MAIN_0 0
#endif
#if defined(ACQUIRE_ID_N560_X_SPO2) ||\
    defined(ACQUIRE_ID_N560_X_BPM) ||\
    defined(ACQUIRE_ID_N560_X_PA) ||\
    defined(ACQUIRE_ID_N560_X_STATUS) ||\
    defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING) ||\
    defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW) ||\
    defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH) ||\
    defined(ACQUIRE_ID_N560_X_STARTUP) ||\
    defined(ACQUIRE_ID_N560_X_CARD_DISABLED) ||\
    defined(ACQUIRE_ID_N560_X_STATUS_REPLY) ||\
    defined(ACQUIRE_ID_N560_X_ALIVE) ||\
    defined(ACQUIRE_ID_N560_X_USED_IDS) ||\
    0
#define ASMO_ACQUIRE_STREAM_N560_MAIN_0 1
#define ASMO_TOUCH_STREAM_N560_MAIN_0 1
#else
#define ASMO_ACQUIRE_STREAM_N560_MAIN_0 0
#endif
#ifndef ASMO_TOUCH_STREAM_N560_MAIN_0
#define ASMO_TOUCH_STREAM_N560_MAIN_0 0
#endif

#if defined(USE_ID_N560_1_SPO2) ||\
    defined(USE_ID_N560_1_BPM) ||\
    defined(USE_ID_N560_1_PA) ||\
    defined(USE_ID_N560_1_STATUS) ||\
    defined(USE_ID_N560_1_ACQUIRE_MISSING) ||\
    defined(USE_ID_N560_1_MEASUREMENT_TOO_LOW) ||\
    defined(USE_ID_N560_1_MEASUREMENT_TOO_HIGH) ||\
    defined(USE_ID_N560_1_STARTUP) ||\
    defined(USE_ID_N560_1_CARD_DISABLED) ||\
    defined(USE_ID_N560_1_STATUS_REPLY) ||\
    defined(USE_ID_N560_1_ALIVE) ||\
    defined(USE_ID_N560_1_USED_IDS) ||\
    0
#define ASMO_USE_STREAM_N560_MAIN_1 1
#define ASMO_TOUCH_STREAM_N560_MAIN_1 1
#else
#define ASMO_USE_STREAM_N560_MAIN_1 0
#endif
#if defined(ACQUIRE_ID_N560_X_SPO2) ||\
    defined(ACQUIRE_ID_N560_X_BPM) ||\
    defined(ACQUIRE_ID_N560_X_PA) ||\
    defined(ACQUIRE_ID_N560_X_STATUS) ||\
    defined(ACQUIRE_ID_N560_X_ACQUIRE_MISSING) ||\
    defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_LOW) ||\
    defined(ACQUIRE_ID_N560_X_MEASUREMENT_TOO_HIGH) ||\
    defined(ACQUIRE_ID_N560_X_STARTUP) ||\
    defined(ACQUIRE_ID_N560_X_CARD_DISABLED) ||\
    defined(ACQUIRE_ID_N560_X_STATUS_REPLY) ||\
    defined(ACQUIRE_ID_N560_X_ALIVE) ||\
    defined(ACQUIRE_ID_N560_X_USED_IDS) ||\
    0
#define ASMO_ACQUIRE_STREAM_N560_MAIN_1 1
#define ASMO_TOUCH_STREAM_N560_MAIN_1 1
#else
#define ASMO_ACQUIRE_STREAM_N560_MAIN_1 0
#endif
#ifndef ASMO_TOUCH_STREAM_N560_MAIN_1
#define ASMO_TOUCH_STREAM_N560_MAIN_1 0
#endif

#if defined(USE_ID_RESEND_STARTUP) ||\
    defined(USE_ID_SEND_USED_IDS) ||\
    0
#define ASMO_USE_STREAM_OTHERS_MAIN_0 1
#define ASMO_TOUCH_STREAM_OTHERS_MAIN_0 1
#else
#define ASMO_USE_STREAM_OTHERS_MAIN_0 0
#endif
#if defined(ACQUIRE_ID_RESEND_STARTUP) ||\
    defined(ACQUIRE_ID_SEND_USED_IDS) ||\
    0
#define ASMO_ACQUIRE_STREAM_OTHERS_MAIN_0 1
#define ASMO_TOUCH_STREAM_OTHERS_MAIN_0 1
#else
#define ASMO_ACQUIRE_STREAM_OTHERS_MAIN_0 0
#endif
#ifndef ASMO_TOUCH_STREAM_OTHERS_MAIN_0
#define ASMO_TOUCH_STREAM_OTHERS_MAIN_0 0
#endif


/*
 *  Count the number of streams from which messages are
 *  going to be sent or received.
 */
#define ASMO_NR_USE_STREAMS (ASMO_USE_STREAM_INTERNAL_MAIN_0 +\
                            ASMO_USE_STREAM_N560_MAIN_0 +\
                            ASMO_USE_STREAM_N560_MAIN_1 +\
                            ASMO_USE_STREAM_OTHERS_MAIN_0 +\
                            0)

#define ASMO_NR_ACQUIRE_STREAMS (ASMO_ACQUIRE_STREAM_INTERNAL_MAIN_0 +\
                                ASMO_ACQUIRE_STREAM_N560_MAIN_0 +\
                                ASMO_ACQUIRE_STREAM_N560_MAIN_1 +\
                                ASMO_ACQUIRE_STREAM_OTHERS_MAIN_0 +\
                                0)

#define ASMO_NR_TOUCH_STREAMS (ASMO_TOUCH_STREAM_INTERNAL_MAIN_0 +\
                              ASMO_TOUCH_STREAM_N560_MAIN_0 +\
                              ASMO_TOUCH_STREAM_N560_MAIN_1 +\
                              ASMO_TOUCH_STREAM_OTHERS_MAIN_0 +\
                              0)

#endif /* ASMO_STREAM_H */
