/**
 * @file    hal_external_adc.c
 * @brief   DAC via SPI driver code.
 * @note    Complex drivers are drivers, that heavily rely on other drivers.
 *          Following the guidelines of ChibiOS, those drivers don't need to
 *          implement LLD functions, since the LLD part is handled by the
 *          other used driver. Hence this driver does not implement a LLD.
 * @author  Moritz Huellmann
 * @date    Jan 2021
 */

#include "SmartECLA_allHeaders.h"

#if (ASMO_USE_DAC == TRUE)

DACEDriver DACED;

thread_t *dacConversionThd;

/*
 * Connected to APB1. APB1 clock runs with 50MHz. 30MHz allowed.
 * 50 MHz / 2 = 25 MHz -> Prescaler 000 (see datasheet) -> Nothing to be done.
 */
SPIConfig dacSPIConfig = {
  FALSE,
  NULL,
  GPIOB,
  GPIOB_CS_DAC,
  SPI_CR1_CPOL,
  SPI_CR2_DS
};

uint16_t _generateFrame(dacextchannel_t ch, dacextsample_t val) {
  uint16_t frame = 0;
#if AD5317_SWITCH_A_D
  if (ch == 1 || ch == 3) {
    frame |= AD5317_ASMO_CONFIG | AD5317_DAC_ADDR(ch) | (val << AD5317_DATA_POS);
  }
  else if (ch == 0) {
    frame |= AD5317_ASMO_CONFIG | AD5317_DAC_ADDR(2) | (val << AD5317_DATA_POS);
  }
  else if (ch == 2) {
    frame |= AD5317_ASMO_CONFIG | AD5317_DAC_ADDR(0) | (val << AD5317_DATA_POS);
  }
#else
  frame |= AD5317_ASMO_CONFIG | AD5317_DAC_ADDR(ch) | (val << AD5317_DATA_POS);
#endif
  return frame;
}

/**
 * @brief   Writes data to the DAC peripheral.
 * @note    SPI needs to be started before using this function.
 *
 * @param[in] driver    SPI driver to send data over
 * @param[in] ch        which channel to write to
 * @param[in] val       the value to be written
 */
void _dacWriteChannel(SPIDriver *driver, dacextchannel_t ch, dacextsample_t val) {

  osalDbgAssert(ch < 4, "invalid channel");
  osalDbgCheck(driver != NULL);

  uint16_t frame = _generateFrame(ch, val);

  spiSelect(driver);
  spiSend(driver, 1, &frame);
  spiUnselect(driver);
}

THD_WORKING_AREA(DAC_CONV_THD_WA, 1024);
THD_FUNCTION(DAC_CONV_THD, p) {
  chRegSetThreadName("DAC EXT CONV");

  osalDbgCheck(p != NULL);

  DACEDriver *dacp = p;
  SPIDriver *spip = dacp->config->spi_driver;

  systime_t start;
  sysinterval_t ticksSpend;

  uint8_t currDepthPos;

  while (!chThdShouldTerminateX()) {

    if (dacp->state != DAC_EXT_ACTIVE) {
        chThdSleepMilliseconds(1);
        continue;
    }

    currDepthPos = 0;
    while (dacp->state == DAC_EXT_ACTIVE) {
      start = chVTGetSystemTime();

      spiAcquireBus(spip);
      spiStart(spip, &dacSPIConfig);

      _dacWriteChannel(spip, (dacp->output + currDepthPos)->ch,
                       (dacp->output + currDepthPos)->value);

      spiStop(spip);
      spiReleaseBus(spip);

      currDepthPos++;

      /* Check, if conversion should be stopped. If yes, dont do callbacks */
      if (dacp->state == DAC_EXT_STOP) {
        break;
      }

      /* Check if last conversion was done */
      if (currDepthPos == dacp->depth) {
        /* If !circular, dacp->state == DAC_EXT_READY after execution of this function */
        _dac_ext_full_code(dacp);
        currDepthPos = 0;
      }
      /* Check if half the buffer was converted */
      else if (currDepthPos == dacp->depth / 2) {
        _dac_ext_half_code(dacp);
      }

      ticksSpend = chTimeDiffX(start, chVTGetSystemTime());
      if (ticksSpend < chTimeMS2I(dacp->config->interval)) {
        chThdSleep(chTimeMS2I(dacp->config->interval) - ticksSpend);
      }
    }
  }
  chThdExit((msg_t)MSG_OK);
}

void dacExtObjectInit(DACEDriver *dacp) {
  dacp->state = DAC_EXT_STOP;
  dacp->config = NULL;
  dacp->output = NULL;
  dacp->depth = 0;
  dacp->grpp = NULL;
#if ASMO_DAC_USE_WAIT
  dacp->thread = NULL;
#endif
#if ASMO_DAC_USE_MUTUAL_EXCLUSION
  osalMutexObjectInit(&dacp->mutex);
#endif
}

void dacExtStart(DACEDriver *dacp, DACEConfig *config) {
  osalDbgCheck(dacp != NULL);

  osalDbgAssert((dacp->state == DAC_EXT_STOP) || (dacp->state == DAC_EXT_READY),
                "invalid state");

  /* Nothing else needs to be done, DAC is already ready when powered up */
  dacp->config = config;

  if (dacConversionThd == NULL) {
    dacConversionThd = chThdCreateStatic(DAC_CONV_THD_WA, sizeof(DAC_CONV_THD_WA),
                                             NORMALPRIO + 1, DAC_CONV_THD, dacp);
  }

  dacp->state = DAC_EXT_READY;
}
void dacExtStop(DACEDriver *dacp) {

  osalDbgCheck(dacp != NULL);

  osalDbgAssert((dacp->state == DAC_EXT_STOP) || (dacp->state == DAC_EXT_READY),
                "invalid state");

  dacp->state = DAC_EXT_STOP;
  chThdTerminate(dacConversionThd);
  (void) chThdWait(dacConversionThd);

  /* At this point, the conversion thread is dead */
  dacConversionThd = NULL;
  dacp->config = NULL;
}

/**
 * @brief       Set a channel to a value, independent of the conversion thread.
 * @note        If the conversion thread is dead or alive does not matter.
 *
 * @param[in]   dacp    Pointer to a DACED instance.
 * @param[in]   channel Channel to set.
 * @param[in]   sample  Value to set to channel.
 */
void dacExtPutChannelX(DACEDriver *dacp, dacextchannel_t channel,
                       dacextsample_t sample) {

  osalDbgCheck(channel < (dacextchannel_t)AD5317_CH_CNT);
  osalDbgAssert(dacp->state == DAC_EXT_READY, "invalid state");

  SPIDriver *spid = dacp->config->spi_driver;

  spiAcquireBus(spid);
  spiStart(spid, &dacSPIConfig);

  _dacWriteChannel(spid, channel, sample);

  spiStop(spid);
  spiReleaseBus(spid);
}

/**
 * @brief Start an asynchronous DA conversion.
 *
 * @param[in]   dacp    Pointer to DACED instance.
 * @param[in]   grpp    Conversion Group.
 * @param[in]   output  Pointer to a daceextouput_t variable.
 * @param[in]   depth   The depth of the array given to output. If there's no array, set to 1.
 */
void dacExtStartConversion(DACEDriver *dacp, const DACEConversionGroup *grpp,
                           dacextoutput_t *output, size_t depth) {
  osalSysLock();
  dacExtStartConversionI(dacp, grpp, output, depth);
  osalSysUnlock();
}

void dacExtStartConversionI(DACEDriver *dacp, const DACEConversionGroup *grpp,
                            dacextoutput_t *output, size_t depth) {

  osalDbgCheckClassI();
  osalDbgCheck(
      (dacp != NULL) && (grpp != NULL) && (output != NULL)
          && ((depth == 1U) || ((depth & 1U) == 0U)));
  osalDbgAssert(
      (dacp->state == DAC_EXT_READY) || (dacp->state == DAC_EXT_COMPLETE)
          || (dacp->state == DAC_EXT_ERROR),
      "not ready");

  dacp->output = output;
  dacp->depth = depth;
  dacp->grpp = grpp;
  dacp->state = DAC_EXT_ACTIVE;
}

/**
 * @brief Start an asynchronous DA conversion.
 *
 * @param[in]   dacp    Pointer to DACED instance that should be stopped.
 */
void dacExtStopConversion(DACEDriver *dacp) {

  osalDbgCheck(dacp != NULL);

  osalSysLock();
  dacExtStopConversionI(dacp);
  osalSysUnlock();
}

void dacExtStopConversionI(DACEDriver *dacp) {

  osalDbgCheckClassI();
  osalDbgAssert(
       (dacp->state == DAC_EXT_READY) || (dacp->state == DAC_EXT_ACTIVE),
       "invalid state");

   dacp->state = DAC_EXT_READY;
   dacp->grpp = NULL;
   _dac_ext_reset(dacp);
}

#if ASMO_DAC_USE_WAIT
/**
 * @brief Synchronous DA conversion
 *
 * @param[in]   dacp    DACED instance to use
 * @param[in]   grpp    The conversion group to use
 * @param[in]   output  Pointer to a daceextouput_t variable.
 * @param[in]   depth   The depth of the array given to output. If there's no array, set to 1.
 */
msg_t dacExtConvert(DACEDriver *dacp, const DACEConversionGroup *grpp,
                    dacextoutput_t *output, size_t depth) {
  msg_t msg;

  osalSysLock();

  osalDbgAssert(dacp->thread == NULL, "eDAC already waiting");

  dacExtStartConversionI(dacp, grpp, output, depth);
  msg = osalThreadSuspendS(&dacp->thread);

  osalSysUnlock();
  return msg;
}
#endif

#if ASMO_DAC_USE_MUTUAL_EXCLUSION
/**
 * @brief Block other threads from using this driver
 *
 * @param[in]   dacp    The DACED instance to reserve
 */
void dacExtAcquireBus(DACEDriver *dacp) {

  osalDbgCheck(dacp != NULL);

  osalMutexLock(&dacp->mutex);
}
/**
 * @brief Unblock other threads from using this driver
 *
 * @param[in]   dacp    The DACED instance to unblock
 */
void dacExtReleaseBus(DACEDriver *dacp) {

  osalDbgCheck(dacp != NULL);

  osalMutexUnlock(&dacp->mutex);
}
#endif

#endif /* ASMO_USE_DAC == TRUE */
