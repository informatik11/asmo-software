/**
 * \file ASMO_util.c
 * Provides some utilities for character conversion.
 * Part of the Medical Interpretation Layer (fully independent from hardware).
 *
 *  \author     Florian Sehl, Moritz Huellmann
 *  \author     Lehrstuhl fuer Informatik 11 -  RWTH Aachen
 *  \date       2008, 2021
 */

#include "SmartECLA_allHeaders.h"

#if (USE_CAN_COMM == TRUE)

char sendSimpleCANMessage(unsigned long id, unsigned long long message) {
  return sendSimpleCANMessageTimeout(id, message, TIME_INFINITE);
}

char sendSimpleCANMessageRef(unsigned long id, unsigned long long *message) {
  return sendSimpleCANMessageRefTimeout(id, message, TIME_INFINITE);
}

/*
 *  timeout in ms to wait for the semaphore
 */
char sendSimpleCANMessageTimeout(unsigned long id, unsigned long long message,
                                 uint32_t timeout) {
  CANTxFrame ctfp;
  ctfp.EID = id;
  ctfp.IDE = 1; // Extended Identifier
  ctfp.DLC = CAN_DATA_LENGTH;
  ctfp.data32[0] = (message & 0xFFFFFFFF);
  ctfp.data32[1] = (message >> 32) & 0xFFFFFFFF;
  return canTransmitTimeout(&CAND, CAN_TX_MB, &ctfp, TIME_MS2I(timeout));
}

/*
 *  timeout in ms to wait for the semaphore
 */
char sendSimpleCANMessageRefTimeout(unsigned long id,
                                    unsigned long long *message,
                                    uint32_t timeout) {
  CANTxFrame ctfp;
  ctfp.EID = id;
  ctfp.IDE = 1; // Extended Identifier
  ctfp.DLC = CAN_DATA_LENGTH;
  ctfp.data32[0] = (*message & 0xFFFFFFFF);
  ctfp.data32[1] = (((*message) >> 32) & 0xFFFFFFFF);
  return canTransmitTimeout(&CAND, CAN_TX_MB, &ctfp, TIME_MS2I(timeout));
}

char sendLocalMeasurementToCAN(ASMO_Measurement *measurement) {
  // Send with timestamp in high 32 bits and data in low 32 bits.
  return sendSimpleCANMessageTimeout(
      measurement->messageId,
      measurement->value, // No timestamp, since PTP is used
      10);
}

static THD_WORKING_AREA(CANRx_wa_Thread, 512);
/**
 * CAN Reception Thread
 *
 * DUE TO THE REDUCED NUMBER OF RX MAILBOXES (2) OF THE STM32F7, COMMANDS AND SYNCEVENTS
 * ARE PLACED IN THE SAME MAILBOX, WHILE DATA KEEPS ITS OWN MAILBOX, AS THE MOST
 * TRAFFIC IS EXPECTED TO OCCUR HERE. BLOODFLOWPROFILES ARE OMITTED. (MH)
 *
 * MB1: Commands + SyncEvents
 * MB2: Data
 *
 */
THD_FUNCTION(CANRxThread, p) {
  (void)p;
  chRegSetThreadName("CAN_RX");

  event_listener_t receiveListener;
  event_listener_t errorListener;

  eventflags_t flags;

  CANRxFrame rxFrame;
  CANConfig ASMO_CANConfig;


  /* Initialize filters. Take a look at 40.3.4ff in the reference manual for details. */

  CANFilter cfp[3] = {
    {
      .filter = 0,
      .mode = 0,         // Mask Mode
      .scale = 1,        // 32 Bit per ID
      .assignment = 0,   // Filter for MB0
      .register1 = BLOCK_ID_MODEL_COMMAND
          | BLOCK_ID_SAFETY_COMMAND,
      .register2 = BLOCK_ID_MASK
    },
    {
      .filter = 1,
      .mode = 0,           // Mask Mode
      .scale = 1,          // 32 Bit per ID
      .assignment = 0,     // Filter for MB0
      .register1 = TS_SYNCMESSAGE_ID,
      .register2 = TS_MAM
    },
    {
      .filter = 2,
      .mode = 0,         // Mask Mode
      .scale = 1,        // 32 Bit per ID
      .assignment = 1,   // Filter for MB1
      .register1 = ANY_USED_MESSAGE_ID,
      .register2 = 0x00000000
    }

  };

  /* Activate filters */
  canSTM32SetFilters(&CAND, 3, 3, cfp);

  /* 75% Sample point, AHP = 54MHz = ABP1, SJW = 1 (STM32 Default). Add CAN_BTR_LBKM to enable loopback mode for debugging. */
  ASMO_CANConfig.btr = CAN_BTR_TS2(3) | CAN_BTR_TS1(12) | CAN_BTR_BRP(2) | CAN_BTR_LBKM;
  /* Automatic WakeUp Mode (when message is received). Automatic Bus Off Management (return to normal if message to be send or received) */
  ASMO_CANConfig.mcr = CAN_MCR_AWUM | CAN_MCR_ABOM;

  canStart(&CAND, &ASMO_CANConfig);

  /* Listen to RX MB0 and MB1 */
  chEvtRegisterMask(&CAND.rxfull_event, &receiveListener, CAN_RX_EVENT_MASK);
  chEvtRegisterMask(&CAND.error_event, &errorListener, CAN_ERR_EVENT_MASK);

  /* Reset priority to normal, init is done*/
  chThdSetPriority(NORMALPRIO);
  while (!chThdShouldTerminateX()) {
    eventmask_t mask = chEvtWaitAny(CAN_RX_EVENT_MASK);

    /* Which event was triggered? */
    if (mask & CAN_RX_EVENT_MASK) {
      /* RX CAN was triggered. Get flags for further details on event: Which MB? */
      flags = chEvtGetAndClearFlags(&receiveListener);

      /* Check if MB0 received */
      if (flags & CAN_MAILBOX_TO_MASK(CAN_RX_MB0)) {
        /* Service all messages in this queue */
        while (canReceiveTimeout(&CAND, CAN_RX_MB0, &rxFrame, TIME_IMMEDIATE) == MSG_OK) {
          uint32_t data[2];
          memcpy(data, rxFrame.data32, sizeof(data)); // Index 0: low, 1: high
          uint32_t messageID = rxFrame.EID;
          uint64_t command = ((uint64_t)(data[1]) << 32) | data[0];

          MessageCommand cmd = {.messageId = messageID, .command = command};

          /* Is model command? */
          if ((messageID & BLOCK_ID_MASK) == BLOCK_ID_MODEL_COMMAND) {
            models_dispatchCommand(&cmd);
#if CAN_FORWARD_TO_ETH && USE_ETH_COMM
            sendSimpleETHMessageTimeout(messageID, command, TIME_INFINITE);
#endif
          }
          /* Is safety command? */
          else if ((messageID & BLOCK_ID_MASK) == BLOCK_ID_SAFETY_COMMAND) {
            cmd.command &= (0xffffffff);
            ASMO_safetyDispatch(&cmd);
#if CAN_FORWARD_TO_ETH && USE_ETH_COMM
            sendSimpleETHMessageTimeout(messageID, command, TIME_INFINITE);
#endif
          }
          /* Is sync message? */
          else if ((messageID & TS_SYNCMESSAGE_ID) == TS_SYNCMESSAGE_ID) {
            tsProcessSyncevent(data[1], data[0]);
          }
        }
      }

      /* Check if MB1 received */
      if (flags & CAN_MAILBOX_TO_MASK(CAN_RX_MB1)) {
        /* Service all messages in this queue */
        while (canReceiveTimeout(&CAND, CAN_RX_MB1, &rxFrame, TIME_IMMEDIATE) == MSG_OK) {
          ASMO_Measurement measurement;
          measurement.messageId = rxFrame.EID;
          measurement.value = rxFrame.data32[0];
          //measurement.timestamp = tsGenerateIncomingTimestamp(
          //    (uint32_t)(rxFrame.data32[1] & 0xFFFFFF)); //TODO(MH) Ist das hier wirklich der Zeitstempel?

          /* Deliver to model function to process the received measurement */
          ASMO_storeMeasurement(&measurement);
#if CAN_FORWARD_TO_ETH && USE_ETH_COMM
          sendSimpleETHMessageTimeout(rxFrame.EID, measurement.value, TIME_INFINITE);
#endif
        }
      }
    }

    /* Service errors */
    if (mask & CAN_ERR_EVENT_MASK) {
      flags = chEvtGetAndClearFlags(&errorListener);
      if (flags & CAN_LIMIT_WARNING) {
        return;
      }
      if (flags & CAN_LIMIT_ERROR) {
        return;
      }
      if (flags & CAN_BUS_OFF_ERROR) {
        return;
      }
      if (flags & CAN_FRAMING_ERROR) {
        return;
      }
      if (flags & CAN_OVERFLOW_ERROR) {
        return;
      }
    }
  }
  chEvtUnregister(&CAND.rxfull_event, &receiveListener);
}

/**
 * @brief   Display CAN error Counter
 * @details Writes the current CAN error counter to the four 7-Seg displays.
 *          Display 1 and 2 (left) show the Transmit error
 *          Display 3 and 4 (right) show the Receive error
 *          Both Error are divided by 4 (so errors have values 0-63)
 */
void ASMO_showCANError(void) {
  /*          Only Digits 0-9 are allowed
   *          (7-Seg driver displays HELP instead of A-F)
   *          TX Error: Bit 23:16 in ESR
   *          RX Error: Bit 31:24 in ESR
   *          divide error by 4 (>> 2), in order to be able to display "all" errors (up to 255)
   */
#if (USE_SEVEN_SEG == TRUE)
  /* TX Error*/
  uint8_t canErr = (((&CAND)->can->ESR >> (16 + 0 + 2)) & 0x3F);
  sevenSeg_setDigit(0, (canErr / 10));
  sevenSeg_setDigit(1, (canErr % 10));

  /* RX Error */
  canErr = (((&CAND)->can->ESR >> (16 + 8 + 2)) & 0x3F);
  sevenSeg_setDigit(2, (canErr / 10));
  sevenSeg_setDigit(3, (canErr % 10));

#endif
}

void ASMO_initCAN(void) {
  chThdCreateStatic(CANRx_wa_Thread, sizeof(CANRx_wa_Thread), HIGHPRIO,
                    CANRxThread, NULL);
  while (CAND.state != CAN_READY) {
    chThdSleepMilliseconds(1);
  }
}

#endif /* USE_CAN_COMM == TRUE */
