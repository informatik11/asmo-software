/**
 * \file ASMO_idWrappers.c
 * \brief Contains some wrapper functions that adapt the ChibiOS hardware functionality
 * for smartECLA issues.
 *
 * \date 24.08.2011
 * \author Florian Goebe
 */

#include "SmartECLA_allHeaders.h"

/**
 * \brief Store locally measured ASMO_Measurement package by adding
 * the device number to the measurement's id.
 * \param measurement	Measurement which is to store (call-by-value to yield side effect free execution)
 */
void ASMO_storeLocalMeasurement(ASMO_Measurement measurement) {
  ASMO_storeMeasurement(&measurement);
}

/**
 * \brief Store locally measured ASMO_Measurement package on memorycard by adding
 * the device number to the measurement's id.
 * \param measurement	Measurement which is to store (call-by-value to yield side effect free execution)
 */
void ASMO_storeLocalMeasurementOnCard(ASMO_Measurement measurement) {
  FS_putToBuffer(&measurement);
}

bool checkInterval(int64_t interval, struct ptp_timestamp *a,
                   struct ptp_timestamp *b) {
  if (interval == 0) {
    return false;
  }
  int64_t diff = tsSubtractTS(a, b);
  return diff >= interval;
}

void ASMO_newMeasurement(ASMO_Measurement measurement) {
  ASMO_Measurement *m;
  ASMO_MeasurementConfig *c;

  measurement.messageId = addDeviceNumber(DEVICE_NUMBER, measurement.messageId,
                                      DEVICE_NUMBER_SHIFTER);

  if (ASMO_getMeasurement(measurement.messageId, &m, &c)) {
    uint32_t i = 0, mean = 0;

    c->mean[c->meanCnt] = measurement.value;
    for (i = 0; i < MEAN_NUMBER; i++)
      mean += c->mean[i];
    mean = (mean >> MEAN_NUMBER_POWER);

    // save on SD card
    #if defined(BOARD_STM32_ASMO)
    if ((c->sdThreshold == 0 && c->sdInterval == 0)
        || checkThreshold(mean, m->value, c->sdThreshold)
        || checkInterval(c->sdInterval, &measurement.timestamp,
                         &c->sdTimestamp)) {

      uint32_t val = measurement.value;
      // FIXME: I have no idea.
      measurement.value = mean;
      c->sdTimestamp = measurement.timestamp;
      ASMO_storeLocalMeasurementOnCard(measurement);
      measurement.value = val;
    }
    #endif

    // send to CAN and store in Measurements struct
    if ((c->canThreshold == 0 && c->canInterval == 0)
        || checkThreshold(measurement.value, m->value, c->canThreshold)
        || checkInterval(c->canInterval, &measurement.timestamp,
                         &c->canTimestamp)) {

      c->canTimestamp = measurement.timestamp;
      sendLocalMeasurement(&measurement);
    }

    ASMO_storeLocalMeasurement(measurement);

    c->meanCnt++;
    c->meanCnt %= MEAN_NUMBER;
  }
  else {
    // send ACQUIRE_MISSING
    ASMO_Measurement tm;
    tm.timestamp = measurement.timestamp;
    tm.value = measurement.messageId;
    tm.messageId = addDeviceNumber(DEVICE_NUMBER,
                               (DEVICE_ID | ID_ACQUIRE_MISSING),
                               DEVICE_NUMBER_SHIFTER);
    sendLocalMeasurement(&tm);
  }

}

uint8_t deviceNumber = 0xFF;

uint8_t device_getNumber(void) {
  if (deviceNumber == 0xFF) {
    deviceNumber = switch_readValue();
  }
  return deviceNumber;
}

bool checkThreshold(uint32_t n, uint32_t o, uint32_t t) {
  if (t == 0) {
    return false;
  }
  return ((n > o) ? (n - o) : (o - n)) >= t;
}
