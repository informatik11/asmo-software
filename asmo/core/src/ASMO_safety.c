/*
 * ASMO_safety.c
 *
 *  Created on: Nov 21, 2016
 *      Author: embedded
 */

#include "SmartECLA_allHeaders.h"

ASMO_safetyConfig sc = {.state = safetyIdle, .command = safetyCmdNoOp};
static THD_WORKING_AREA(waSafety, 256);

#define buildSafetyID(messageID,safetyID,shifter) BLOCK_ID_MEDICAL_ALERT | (messageID & (DEVICE_ID_MASK | (0xF << shifter))) | safetyID;

bool ASMO_safetyCheck(ASMO_Measurement *m, unsigned int min, unsigned int max) {
  ASMO_Measurement msg;
  if (m->value < min) {
    msg.messageId = buildSafetyID(m->messageId, SAFETY_TOO_LOW,
                              DEVICE_NUMBER_SHIFTER)
    ;
  }
  else if (m->value > max) {
    msg.messageId = buildSafetyID(m->messageId, SAFETY_TOO_HIGH,
                              DEVICE_NUMBER_SHIFTER)
    ;
  }
  else {
    return true;
  }
  msg.value = m->messageId;
  msg.timestamp = tsGetTS();
  sendLocalMeasurement(&msg);
  return false;
}

void ASMO_safetyThread(void *p) {
  (void)p;
  timestamp_t now;

  chRegSetThreadName("SafetyThread");
  while (1) {
    if (sc.state != safetyIdle) {
      now = tsGetTS();
      if (tsSubtractTS(&now, &sc.lastUpdate)
          > (int64_t)(5 * 1000 * 1000) * (int64_t)(1000)) {
        ASMO_Measurement m;
        m.messageId = ID_SAFETY_TIMEOUT;
        m.value = sc.id;
        m.timestamp = now;
        sendLocalMeasurement(&m);
        sc.state = safetyIdle;
      }
    }
    chThdSleepSeconds(1);
  }
}

void ASMO_safetyInit(void) {
  chThdCreateStatic(waSafety, sizeof(waSafety), LOWPRIO + 5, ASMO_safetyThread,
                    NULL);
}

void ASMO_safetyDispatch(MessageCommand *cmd) {
  if (cmd->messageId == ID_SAFETY_SET) {
    switch (sc.state) {
    case safetyIdle:
      sc.command = cmd->command;
      sc.state = safetyWaitForId;
      sc.lastUpdate = tsGetTS();
      break;
    case safetyWaitForId:
      sc.id = cmd->command;
      sc.state = safetyWaitForValue;
      sc.lastUpdate = tsGetTS();
      break;
    case safetyWaitForValue:
      sc.lastUpdate = tsGetTS();
      sc.value = cmd->command;
      ASMO_safetySet(&sc);
      sc.state = safetyIdle;
      break;
    default:
      sc.state = safetyIdle;
      break;
    }
  }
}

