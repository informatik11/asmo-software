/**
 * @file    hal_led_display_spi.c
 * @brief   Led display driver over SPI.
 * @addtogroup led_display_SPI
 * @{
 */

#include "SmartECLA_allHeaders.h"

#if (ASMO_USE_LED_DISPLAY == TRUE)

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/**
 * @brief external led_display SPI driver identifier.
 *
 */
LEDDisplayDriver LEDDISPD;

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   led_display Driver over SPI initialization.
 * @note    This function is implicitly invoked by @p halInit(), there is
 *          no need to explicitly initialize the driver.
 *
 * @init
 */
void ledDisplayInit(void) {

  ledDisplayObjectInit(&LEDDISPD);
}

/**
 * @brief   Initializes the standard part of a @p LEDDisplayDriver structure.
 *
 * @param[out] leddispp     pointer to the @p LEDDisplayDriver object
 *
 * @init
 */
void ledDisplayObjectInit(LEDDisplayDriver *leddispp) {
  /* Default values for driver.*/
  leddispp->state = LED_DISPLAY_SPI_STOP;
  leddispp->config = NULL;
  leddispp->samples = NULL;
  leddispp->depth = 0;
  leddispp->currentDisplay = 0;
  leddispp->decodeMode = 0;
  leddispp->stopped = false;
  leddispp->pos_conversion = 0;
  leddispp->last = 0;
  leddispp->now = 0;
#if ASMO_LED_DISPLAY_USE_WAIT
  leddispp->thread = NULL;
#endif /* led_display_SPI_USE_WAIT */
#if ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION
  osalMutexObjectInit(&leddispp->mutex);
#endif /* led_display_SPI_USE_MUTUAL_EXCLUSION */
}

/**
 * @brief   Configures and activates the led_display over SPI peripheral.
 *
 * @param[in] leddispp      pointer to the @p LEDDisplayDriver object
 * @param[in] config            pointer to the @p LEDDisplayConfig object.
 *
 * @api
 */
void ledDisplayStart(LEDDisplayDriver *leddispp, const LEDDisplayConfig *config) {

  osalDbgCheck((leddispp != NULL) && (config != NULL));
  chSysLock();
  osalDbgAssert((leddispp->state == LED_DISPLAY_SPI_STOP) || (leddispp->state == LED_DISPLAY_SPI_READY),
              "invalid state");
  leddispp->config = config;
  leddispp->state = LED_DISPLAY_SPI_READY;
  chSysUnlock();
}

/**
 * @brief   Deactivates the led_display peripheral.
 *
 * @param[in] leddispp      pointer to the @p led_displayDriver object
 *
 * @api
 */
void ledDisplayStop(LEDDisplayDriver *leddispp) {

  osalDbgCheck(leddispp != NULL);
  chSysLock();
  osalDbgAssert((leddispp->state == LED_DISPLAY_SPI_STOP) || (leddispp->state == LED_DISPLAY_SPI_READY),
              "invalid state");
  leddispp->state = LED_DISPLAY_SPI_STOP;
  chSysUnlock();

}

/**
 * @brief   Set display test.
 *
 * @api
 */
void ledDisplaySetDisplaytest(LEDDisplayDriver *leddispp, uint16_t enabled) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  uint16_t paket;
  if(enabled) {
    paket = led_display_DisplayTestOn;
  }
  else {
    paket = led_display_DisplayTestOff;
  }
  ledDisplaySendData(leddispp, 1, &paket);
}

/**
 * @brief   Set enable.
 *
 * @api
 */
void ledDisplaySetEnabled(LEDDisplayDriver *leddispp, uint16_t enabled) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  uint16_t paket;
  if(enabled) {
    paket = led_display_DisplayOn;
  }
  else {
    paket = led_display_DisplayOff;
  }
  ledDisplaySendData(leddispp, 1, &paket);

}

/**
 * @brief   Set scan limit.
 *
 * @api
 */
void ledDisplaySetScanlimit(LEDDisplayDriver *leddispp, uint16_t maxDisplay) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  uint16_t paket;
  paket = led_display_create_data(led_display_ScanLimit, maxDisplay);
  ledDisplaySendData(leddispp, 1, &paket);

}

/**
 * @brief   Set display intensity.
 *
 * @api
 */
void ledDisplaySetIntensity(LEDDisplayDriver *leddispp, uint16_t intensity) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  uint16_t paket;
  paket = led_display_create_data(led_display_Intensity, (intensity*MAXINTENSITY)/100);
  ledDisplaySendData(leddispp, 1, &paket);
}

/**
 * @brief   Enable decode mode.
 *
 * @api
 */
void ledDisplaySetEnableDecode(LEDDisplayDriver *leddispp, uint16_t enabled) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  uint16_t paket;
  if(enabled) {
    paket = led_display_DecodeModeOn;
  }
  else {
    paket = led_display_DecodeModeOff;
  }
  ledDisplaySendData(leddispp, 1, &paket);

  leddispp->decodeMode = enabled;

}

/**
 * @brief   Clear all displays.
 *
 * @api
 */
void ledDisplayClear(LEDDisplayDriver *leddispp) {

  osalDbgCheck(leddispp != NULL);

  leddispp->currentDisplay = 0;
  ledDisplaySetEnableDecode(leddispp, FALSE);
  uint8_t empty[8] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
  ledDisplayWrite(leddispp, 8, empty);
  ledDisplaySetEnableDecode(leddispp, leddispp->decodeMode);
  leddispp->currentDisplay = 0;

}

/**
 * @brief   Change active display.
 *
 * @api
 */
void ledDisplayChangeDisplay(LEDDisplayDriver *leddispp, uint8_t display) {

  osalDbgCheck(leddispp != NULL);

  leddispp->currentDisplay = display;
}

/**
 * @brief   Change active display to next one.
 *
 * @api
 */
void ledDisplayChangeDisplayNext(LEDDisplayDriver *leddispp) {

  osalDbgCheck(leddispp != NULL);

  leddispp->currentDisplay = (leddispp->currentDisplay + 1) % MAXDISPLAY;
}

/**
 * @brief   Change active display to previous one.
 *
 * @api
 */
void ledDisplayChangeDisplayPrev(LEDDisplayDriver *leddispp) {

  osalDbgCheck(leddispp != NULL);

  leddispp->currentDisplay = ((leddispp->currentDisplay + MAXDISPLAY) - 1) % MAXDISPLAY;
}

/**
 * @brief   Generate SPI frame and send it.
 *
 * @api
 */
void ledDisplaySendData(LEDDisplayDriver *leddispp, size_t n,
                        uint16_t *data) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert((leddispp->state == LED_DISPLAY_SPI_READY) ||
      (leddispp->state == LED_DISPLAY_SPI_ACTIVE), "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  spiAcquireBus(leddispp->config->spi_driver );
  spiStart(leddispp->config->spi_driver , leddispp->config->spi_config);

  size_t i;
  for(i=0; i<n; i++) {
    spiSelect(leddispp->config->spi_driver );
    spiSend(leddispp->config->spi_driver , 1, &(data[i]));
    spiUnselect(leddispp->config->spi_driver );
  }
  spiStop(leddispp->config->spi_driver );
  spiReleaseBus(leddispp->config->spi_driver );

  leddispp->state = LED_DISPLAY_SPI_COMPLETE;
  if(leddispp->config->end_cb == NULL) {
    /* No callback given */
  }
  else {
    leddispp->config->end_cb(leddispp, NULL, 0);
  }
  leddispp->state = LED_DISPLAY_SPI_READY;
}

/**
 * @brief   Write to displays.
 *
 * @api
 */
void ledDisplayWrite(LEDDisplayDriver *leddispp, size_t n, const void *txbuf){

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;
  osalDbgCheck((leddispp != NULL) && (n > 0) && (n <= (MAXDISPLAY)) && (txbuf != NULL));

  size_t i;
  uint16_t paket[MAXDISPLAY];
  for(i=0; i<n; i++) {
    paket[i] = led_display_create_data(
        (led_display_P0andP1 | (((leddispp->currentDisplay + i) % (MAXDISPLAY))&0xF)),
        ((uint8_t *)txbuf)[i]);
  }
  ledDisplaySendData(leddispp, n, paket);
  leddispp->currentDisplay += n;
  leddispp->currentDisplay %= MAXDISPLAY;
}

/**
 * @brief   Set display.
 *
 * @api
 */
void ledDisplaySetDisplay(LEDDisplayDriver *leddispp, uint8_t data) {

  osalDbgCheck(leddispp != NULL);
  osalDbgAssert(leddispp->state == LED_DISPLAY_SPI_READY, "not ready");

  leddispp->state = LED_DISPLAY_SPI_ACTIVE;

  uint16_t packet = led_display_create_data((led_display_P0andP1 | leddispp->currentDisplay), data);
  ledDisplaySendData(leddispp, 1, &packet);

}

#if (ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION == TRUE) || defined(__DOXYGEN__)
/**
 * @brief   Gains exclusive access to the led driver.
 * @details This function tries to gain ownership to the led driver, if the
 *          led driver is already being used then the invoking thread is queued.
 * @pre     In order to use this function the option
 *          @p ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION must be enabled.
 *
 * @param[in] leddispp      pointer to the @p LEDDisplayDriver object
 *
 * @api
 */
void ledDisplayAcquireBus(LEDDisplayDriver *leddispp) {

  osalDbgCheck(leddispp != NULL);

  osalMutexLock(&leddispp->mutex);
}

/**
 * @brief   Releases exclusive access to the led driver.
 * @pre     In order to use this function the option
 *          @p ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION must be enabled.
 *
 * @param[in] leddispp      pointer to the @p LEDDisplayDriver object
 *
 * @api
 */
void ledDisplayReleaseBus(LEDDisplayDriver *leddispp) {

  osalDbgCheck(leddispp != NULL);

  osalMutexUnlock(&leddispp->mutex);
}
#endif /* ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION == TRUE */

#endif /* ASMO_USE_LED_DISPLAY == TRUE */

/** @} */
