/**
* @file    ASMO_ETH.c
* @brief   Contains function to easily send messages via Ethernet.
*
* @{
*/

#include "ASMO_ETH.h"

/*===========================================================================*/
/* Task local definitions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Task exported variables.                                                  */
/*===========================================================================*/

extern BaseSequentialStream *chibios_stdout;
extern mutex_t chibios_stdout_mtx;

/*===========================================================================*/
/* Task local variables and types.                                           */
/*===========================================================================*/

// priority queue of messages to be sent by the tx-thread
static asmo_eth_message txMsgPQueue[ASMO_ETH_MSG_BUF_SIZE];

// current size of the tx-pqueue
static uint8_t txQueueSize = 0;

// mutex for atomic access to the tx-pqueue
MUTEX_DECL(txMsgPQueueMutex);

// timer that notifies the tx-thread to start
static virtual_timer_t tx_vt;

// thread reference that the tx-thread suspends on
thread_reference_t tx_thread;

/* Allocate stack size for thread.*/
THD_WORKING_AREA(ETHTx_Thread_wa, 512);

#if (ASMO_ETH_COLLECT_ERRORS == TRUE)
/* Error counter, if messages are getting lost.*/
static int errorCount = 0;
#endif /* ASMO_ETH_COLLECT_ERRORS == TRUE */

/*===========================================================================*/
/* Task local functions.                                                     */
/*===========================================================================*/

#if (ASMO_ETH_COLLECT_ERRORS == TRUE)
/**
 * @brief   Display the errors via the seven segment display of the ASMO board.
 *
 * @param[in] r     error counter
 *
 * @notapi
 */
static void show_eth_error(uint32_t r) {
#if (USE_SEVEN_SEG == TRUE)
  sevenSeg_blankAll();
  sevenSeg_writeInt(r);

  sevenSeg_setDigit(SEVENSEG_DIGIT_1, SEVENSEG_SYMBOLS_E);
  sevenSeg_setDigit(SEVENSEG_DIGIT_2, SEVENSEG_SYMBOLS_H);
#else
  (void)r;
#endif /* USE_SEVEN_SEG == TRUE */
}
#endif /* ASMO_ETH_COLLECT_ERRORS == TRUE */

/**
 * @brief   Resets the priority queue to its default values.
 *
 * @notapi
 */
static void asmo_tx_pqueue_reset(void)
{
  memset(txMsgPQueue, -1, sizeof(txMsgPQueue));
}

/**
 * @brief   Reserves space in the global priority queue txMsgPQueue for a asmo_eth_message
 *            of priority prio
 *
 * Space is reserved in the priority queue txMsgPQueue after all elements with higher or
 *   equal priority. If all elements in the queue have higher or equal priority, no space
 *   is reserved and NULL is returned. The function does not write anything, not even the
 *   new priority, to the space that is reserved for the new message. This must be done
 *   by the caller or else the priority queue is corrupted.
 *
 * @warning Assumes that txMsgPQueueMutex is locked
 *
 * @param[in] prio      Priority of the message for which to reserve space
 *
 * @return Pointer to the position in the priority queue where the message with priority
 *           prio must now be inserted; or NULL if the priority queue is full and the
 *           new element has too low a priority to be inserted
 */
static asmo_eth_message* asmo_tx_pqueue_push(asmo_eth_prio prio)
{
  // binary search for the first element with lower priority
  // adapted from https://en.cppreference.com/w/cpp/algorithm/lower_bound
  // note: for us, lower priority value == higher priority

  // center index of current search range
  uint8_t it;
  // size of current search range
  uint8_t count = txQueueSize;
  uint8_t first = 0;

  while (count > 0) {

    it = first + count / 2;

    if (txMsgPQueue[it].prio <= prio) {
      first = ++it;
      count -= count / 2 + 1;
    }
    else
      count /= 2;
  }

  if(first == ASMO_ETH_MSG_BUF_SIZE)
  {
#if (ASMO_ETH_COLLECT_ERRORS == TRUE)
    show_eth_error(++errorCount);
#endif /* ASMO_ETH_COLLECT_ERRORS == TRUE */
    return NULL;
  }

  // if the element with the next-lowest priority has the unused value -1,
  // that means that we can safely just overwrite it
  // else, we have to move all elements with lower priority to the right
  if(txMsgPQueue[first].prio != (uint8_t)-1)
  {
#if (ASMO_ETH_COLLECT_ERRORS == TRUE)
    if (txMsgPQueue[ASMO_ETH_MSG_BUF_SIZE-1].prio != (uint8_t)-1) {
      show_eth_error(++errorCount);
    }
#endif /* ASMO_ETH_COLLECT_ERRORS == TRUE */
    // move messages to the right of first one message to the right,
    // overwriting the right-most message if the end of array is reached
    memmove(txMsgPQueue + first + 1,
            txMsgPQueue + first,
            (txQueueSize - first - ((txQueueSize == ASMO_ETH_MSG_BUF_SIZE) ? 1 : 0)) * sizeof(asmo_eth_message));
  }

  // insert new priority, leave the rest as-is to be overwritten by the caller
  txMsgPQueue[first].prio = prio;

  /* Do not increment, if already full. Message with the lowest priority has been removed
   * from queue in that case anyway.*/
  if(txQueueSize < ASMO_ETH_MSG_BUF_SIZE) {
    ++txQueueSize;
  }

  return txMsgPQueue + first;
}

static uint8_t device_instance_id_from_msgid(uint32_t id) {
#define ASMO_MESSAGE_ID_DEV_INSTANCE_ID_SHIFT 12LL
#define ASMO_MESSAGE_ID_DEV_INSTANCE_ID_MASK (0xFLL << ASMO_MESSAGE_ID_DEV_INSTANCE_ID_SHIFT)
  return (id & ASMO_MESSAGE_ID_DEV_INSTANCE_ID_MASK)
      >> ASMO_MESSAGE_ID_DEV_INSTANCE_ID_SHIFT;
}

static uint16_t measurement_id_from_msgid(uint32_t id) {
#define ASMO_MESSAGE_ID_MEASUREMENT_ID_SHIFT 0LL
#define ASMO_MESSAGE_ID_MEASUREMENT_ID_MASK  0xFFFLL
  return id & ASMO_MESSAGE_ID_MEASUREMENT_ID_MASK;
}

static uint16_t get_next_seq_id_for_stream(ASMO_Stream *stream) {
  return stream->sequence_id++;
}

static void poke_uint64(uint8_t *dest, uint64_t val) {
  for (size_t i = 0; i < 8; i++) {
    dest[i] = (val >> (8 * (7 - i))) & 0xFF;
  }
}

static void poke_uint32(uint8_t *dest, uint32_t val) {
  for (size_t i = 0; i < 4; i++) {
    dest[i] = (val >> (8 * (3 - i))) & 0xFF;
  }
}

static void poke_uint16(uint8_t *dest, uint32_t val) {
  for (size_t i = 0; i < 2; i++) {
    dest[i] = (val >> (8 * (1 - i))) & 0xFF;
  }
}

static void build_message_header_eth(ASMO_Stream *stream, asmo_eth_message *msg,
                                     uint8_t *dest, size_t destsz) {

  if (destsz < 1) {
    chSysHalt("bufSz");
    return;
  }

  dest[0] = (msg->type << 5);
  dest[0] |= asmo_block_id_from_msgid(msg->id);
  dest[1] = device_instance_id_from_msgid(msg->id) << 4;
  dest[1] |= (measurement_id_from_msgid(msg->id) >> 8) & 0x0F;
  dest[2] = measurement_id_from_msgid(msg->id) & 0xFF;
  poke_uint16(dest + 3, get_next_seq_id_for_stream(stream));
}

static void build_message_frame_eth(ASMO_Stream *stream, asmo_eth_message *msg,
                                    uint8_t *buf, size_t bufsz) {

  /* The first 4 bytes are all the same for both message types */
  build_message_header_eth(stream, msg, buf, bufsz);

  /* Difference for the message types starts at byte 5 */
  if (msg->type == ASMO_NET_MESSAGE_TYPE_MEASUREMENT) {
    poke_uint32(buf + 5, msg->ts.secondsField);
    poke_uint16(buf + 9, msg->ts.nanosecondsField / 1000 / 100);

    poke_uint32(buf + 11, msg->msg);
  }
  else if (msg->type == ASMO_NET_MESSAGE_TYPE_SIMPLECAN) {
    poke_uint64(buf + 5, msg->msg);
  }

}

static struct netbuf* build_message_eth(ASMO_Stream *stream,
                                        asmo_eth_message *msg) {

  struct netbuf* nb = NULL;
  uint8_t* buf;
  size_t sz;

  nb = netbuf_new();
  if (nb == NULL) {
    return NULL;
  }

  if (msg->type == ASMO_NET_MESSAGE_TYPE_MEASUREMENT) {
    sz = ASMO_MEASUREMENT_MESSAGE_SZ;
  }
  else if (msg->type == ASMO_NET_MESSAGE_TYPE_SIMPLECAN) {
    sz = ASMO_SIMPLE_CAN_MESSAGE_SZ;
  }
  else {
    netbuf_delete(nb);
    return NULL;
  }

  buf = netbuf_alloc(nb, sz);
  if (buf == NULL) {
    netbuf_delete(nb);
    return NULL;
  }

  build_message_frame_eth(stream, msg, buf, sz);

  nb->p->prio = msg->prio;

  return nb;

}

static asmo_eth_prio get_priority_from_ID(uint32_t id) {

  return asmo_block_id_from_msgid(id);
}

/**
 * @brief   Timer-callback that starts the tx-thread
 */
static void tx_start_send(void* param)
{
  (void)param;

  // timer-callback-function is invoked from ISR context
  chSysLockFromISR();
  chThdResumeI(&tx_thread, 0);
  chSysUnlockFromISR();
}

static err_t transmit_message_eth(asmo_eth_message *msg,
                                  struct netconn *conn) {

  struct netbuf *nb;
  struct netconn *nc;
  ASMO_Stream *stream;

  stream = asmo_net_get_stream_for_message_id(msg->id);

  nc = conn;
  nb = build_message_eth(stream, msg);

  if (nb == NULL) {
    return ERR_BUF;
  }

  if (netconn_sendto(nc, nb, &stream->ipaddr, ASMO_NET_PORT) != ERR_OK) {
    netbuf_delete(nb);
    return -4;
  }

  netbuf_delete(nb);
  return ERR_OK;
}

static __attribute__((section(".ram4_init"))) THD_FUNCTION(ETHTx_Thread, arg) {
  (void)arg;

  struct netconn *conn;

  chRegSetThreadName("eth_tx");

  conn = netconn_new(NETCONN_UDP);

  if (conn == NULL) {
    chSysHalt("netconn NULL");
  }

  // start heartbeat if enabled
#if ASMO_SEND_HEARTBEAT
  asmoStartHeartbeat(0);
#endif

  while (true) {

    chSysLock();
    chThdSuspendS(&tx_thread);
    chSysUnlock();

    chMtxLock(&txMsgPQueueMutex);

    if(macPollLinkStatus(&ETHD1))
    {
      for(uint8_t i = 0; i < txQueueSize; ++i)
      {
        transmit_message_eth(&txMsgPQueue[i], conn);
      }
    }

    // empty pqueue
    // this is done even if there is no connection, because otherwise the
    // pqueue would in that case be constantly full and the tx-thread invoked
    // after every sendLocalMeasurement
    asmo_tx_pqueue_reset();
    txQueueSize = 0;

    chMtxUnlock(&txMsgPQueueMutex);
  }
}

/*===========================================================================*/
/* Task exported functions.                                                  */
/*===========================================================================*/

void ASMO_initETH(void) {

  asmo_net_init();

  /* Wait for Ethernet connection for some time before continuing. Prevents
   * the loss of messages when board is turned on.*/
  int counterWait = 0;
  while (counterWait < ASMO_ETH_WAIT_FOR_CONNECTION) {
    if (macPollLinkStatus(&ETHD1)) {
      break;
    }
    chThdSleepMilliseconds(1);
    counterWait++;
  }

  /* Initialize tx-pqueue and virtual timer. For correct error collection it has
   * to be initialized at this stage.*/
  chMtxLock(&txMsgPQueueMutex);
  asmo_tx_pqueue_reset();
  chVTObjectInit(&tx_vt);
  chMtxUnlock(&txMsgPQueueMutex);

  chThdCreateStatic(ETHTx_Thread_wa, sizeof(ETHTx_Thread_wa), NORMALPRIO + 20,
                    ETHTx_Thread, NULL);

}

uint8_t asmo_block_id_from_msgid(uint32_t id) {
#define ASMO_MESSAGE_ID_BLOCK_ID_SHIFT 24LL
#define ASMO_MESSAGE_ID_BLOCK_ID_MASK (0x1FLL << ASMO_MESSAGE_ID_BLOCK_ID_SHIFT) /* bit 28-24 */
  return (id & ASMO_MESSAGE_ID_BLOCK_ID_MASK) >> ASMO_MESSAGE_ID_BLOCK_ID_SHIFT;
}

/**
 * @brief   Send a measurement via Ethernet
 *
 * @note    No timeout parameter, since measurements always should get send
 *
 * @param[in] measurement   pointer to measurement from which to construct the Ethernet frame
 *
 * @return Status message
 * @retval  MSG_TIMEOUT if the message wasn't enqueued
 * @retval  MSG_OK      otherwise (no guarantee that it will be sent)
 *
 * @asmo_eth
 */
msg_t sendLocalMeasurementToETH(ASMO_Measurement *measurement) {

  asmo_eth_message *eth_msg;

  // Lock access to priority queue, pqueue size and virtual timer
  chMtxLock(&txMsgPQueueMutex);

  // Reserve space for new message in pqueue
  eth_msg = asmo_tx_pqueue_push(get_priority_from_ID(measurement->messageId));

  // Write message into pqueue
  if (eth_msg != NULL)
  {
    eth_msg->type = ASMO_NET_MESSAGE_TYPE_MEASUREMENT;
    eth_msg->id = measurement->messageId;
    eth_msg->msg = measurement->value;
    eth_msg->ts = measurement->timestamp;
  }

  // Priority queue is full?
  if (txQueueSize == ASMO_ETH_MSG_BUF_SIZE)
  {
    // If the priority list is full, skip the rest of the timer and immediately invoke the tx-thread
    // The vtimer might have triggered since we locked the mutex, in which case tx_thread would be NULL
    // That is no problem since chThdResume checks for that
    chVTReset(&tx_vt);
    chMtxUnlock(&txMsgPQueueMutex);
    chThdResume(&tx_thread, 0);
  }
  else
  {
    // Start the timer if this was the first enqueued message
    if(txQueueSize == 1)
    {
      chVTSet(&tx_vt, TIME_US2I(ASMO_ETH_TX_DELAY_US), tx_start_send, NULL);
    }
    // If this was neither the first message of the pqueue nor the message that fills it,
    //   just leave the existing timer running.

    chMtxUnlock(&txMsgPQueueMutex);
  }

  if (eth_msg == NULL)
  {
    // todo: we could try to send again after resuming tx_thread
    return MSG_TIMEOUT;
  }

  return MSG_OK;
}

/**
 * @brief   Send a simple message via Ethernet, i.e. a message that does not
 *          come from measurements.
 *
 * @param[in] id        CAN ID from which to construct the Ethernet frame
 * @param[in] message   pointer to the message to be send
 * @param[in] timeout   Not used at the moment
 *
 * @return Status message
 * @retval  MSG_TIMEOUT if the message wasn't enqueued
 * @retval  MSG_OK      otherwise (no guarantee that it will be sent)
 *
 * @asmo_eth
 */
msg_t sendSimpleETHMessageRefTimeout(uint32_t id, uint64_t *message,
                                     uint32_t timeout) {
  asmo_eth_message *eth_msg;
  (void)timeout;

  // Lock access to priority queue, pqueue size and virtual timer
  chMtxLock(&txMsgPQueueMutex);

  // Reserve space for new message in pqueue
  eth_msg = asmo_tx_pqueue_push(get_priority_from_ID(id));

  // Write message into pqueue
  if (eth_msg != NULL)
  {
    eth_msg->type = ASMO_NET_MESSAGE_TYPE_SIMPLECAN;
    eth_msg->id = id;
    eth_msg->msg = *message;
    eth_msg->ts = (timestamp_t)TS_ZERO_VALUE;
  }

  // Priority queue is full?
  if (txQueueSize == ASMO_ETH_MSG_BUF_SIZE)
  {
    // If the priority list is full, skip the rest of the timer and immediately invoke the tx-thread
    // The vtimer might have triggered since we locked the mutex, in which case tx_thread would be NULL
    // That is no problem since chThdResume checks for that
    chVTReset(&tx_vt);
    chMtxUnlock(&txMsgPQueueMutex);
    chThdResume(&tx_thread, 0);
  }
  else
  {
    // Start the timer if this was the first enqueued message
    if(txQueueSize == 1)
    {
      chVTSet(&tx_vt, TIME_MS2I(1), tx_start_send, NULL);
    }
    // If this was neither the first message of the pqueue nor the message that fills it,
    //   just leave the existing timer running.

    chMtxUnlock(&txMsgPQueueMutex);
  }

  if (eth_msg == NULL)
  {
    // todo: we could try to send again after resuming tx_thread
    return MSG_TIMEOUT;
  }

  return MSG_OK;
}

msg_t sendSimpleETHMessage(uint32_t id, uint64_t message) {
  return sendSimpleETHMessageRefTimeout(id, &message, TIME_INFINITE);
}

msg_t sendSimpleETHMessageRef(uint32_t id, uint64_t *message) {
  return sendSimpleETHMessageRefTimeout(id, message, TIME_INFINITE);
}

msg_t sendSimpleETHMessageTimeout(uint32_t id, uint64_t message,
                                  uint32_t timeout) {
  return sendSimpleETHMessageRefTimeout(id, &message, timeout);
}

/** @} */
