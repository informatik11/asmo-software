/*
 * \file switch.c
 * \brief Implementation of instance-switch driver
 *
 * \author Moritz Huellmann
 * \date Oct 2020
 */

#include "SmartECLA_allHeaders.h"

void switch_init(void) {
    // Nothing to be done here, was taken care of in os/hal/boards/STM32_ASMO/board.h
}

unsigned char switch_readValue(void) {
#if defined(BOARD_STM32_ASMO)
    uint8_t bit0, bit1, bit2, bit3;
    bit0 = palReadPad(SWITCH_SIGNAL1_PORT, SWITCH_SIGNAL1_PIN);
    bit1 = palReadPad(SWITCH_SIGNAL2_PORT, SWITCH_SIGNAL2_PIN);
    bit2 = palReadPad(SWITCH_SIGNAL4_PORT, SWITCH_SIGNAL4_PIN);
    bit3 = palReadPad(SWITCH_SIGNAL8_PORT, SWITCH_SIGNAL8_PIN);
    return (bit3 << 3) | (bit2 << 2) | (bit1 << 1) | (bit0 << 0);
#else
    return 0;
#endif
}

