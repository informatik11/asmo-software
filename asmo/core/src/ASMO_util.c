/**
 * \file ASMO_util.c
 * Provides some utilities for character conversion.
 * Part of the Medical Interpretation Layer (fully independent from hardware).
 *
 *  \author     Andre Stollenwerk
 *  \author		Lehrstuhl f�r Informatik 11 -  RWTH Aachen
 *  \date		2008
 */

#include "SmartECLA_allHeaders.h"

/**
 * Returns the corresponding symbol of an unsigned 4-bit hex number.\n
 * E.g. 0x5 -> '5', 0xB -> 'B', etc.
 * \param	input	The number which is to convert.
 * \return	The corresponding ASCII character.
 */
char hex2ascii(unsigned char input) {
	char output = 0;

	switch (input) {
	case 0x00:
		output = '0';
		break;
	case 0x01:
		output = '1';
		break;
	case 0x02:
		output = '2';
		break;
	case 0x03:
		output = '3';
		break;
	case 0x04:
		output = '4';
		break;
	case 0x05:
		output = '5';
		break;
	case 0x06:
		output = '6';
		break;
	case 0x07:
		output = '7';
		break;
	case 0x08:
		output = '8';
		break;
	case 0x09:
		output = '9';
		break;
	case 0x0A:
		output = 'A';
		break;
	case 0x0B:
		output = 'B';
		break;
	case 0x0C:
		output = 'C';
		break;
	case 0x0D:
		output = 'D';
		break;
	case 0x0E:
		output = 'E';
		break;
	case 0x0F:
		output = 'F';
		break;
	default:
		output = '0';
		break;
	}

	return output;
}

/**
 * Converts a one-digit hex number from ASCII symbol into the appropriate 4-bit hex number.
 * E.g. '5' -> 0x5, 'B' -> 0xB
 * \param	input	Digit as ASCII symbol.
 * \return	The appropriate number as unsigned char.
 */
unsigned char ascii2hex(char input) {
	unsigned char output = 0;

	switch (input) {
	case '0':
		output = 0x00;
		break;
	case '1':
		output = 0x01;
		break;
	case '2':
		output = 0x02;
		break;
	case '3':
		output = 0x03;
		break;
	case '4':
		output = 0x04;
		break;
	case '5':
		output = 0x05;
		break;
	case '6':
		output = 0x06;
		break;
	case '7':
		output = 0x07;
		break;
	case '8':
		output = 0x08;
		break;
	case '9':
		output = 0x09;
		break;
	case 'A':
		output = 0x0A;
		break;
	case 'B':
		output = 0x0B;
		break;
	case 'C':
		output = 0x0C;
		break;
	case 'D':
		output = 0x0D;
		break;
	case 'E':
		output = 0x0E;
		break;
	case 'F':
		output = 0x0F;
		break;
	default:
		output = 0x00;
		break;
	}

	return output;

}

/**
 * Converts 1 ascii hex byte to an unsigned short value.
 * \param ptr	pointer to the 2 chars long string. The MSB comes first.
 * \return		the converted number
 */
unsigned char hexStringTo8Bit(char *ptr) {
	unsigned char res;
	res = (unsigned short) ascii2hex(ptr[0]) * 0x10;
	res += (unsigned short) ascii2hex(ptr[1]);
	return res;
}

/**
 * Converts 2 ascii hex bytes to an unsigned short value.
 * \param ptr	pointer to the 4 chars long string. The MSB comes first.
 * \return		the converted number
 */
unsigned short hexStringTo16Bit(char *ptr) {
	unsigned short res;
	res = (unsigned short) ascii2hex(ptr[0]) * 0x1000;
	res += (unsigned short) ascii2hex(ptr[1]) * 0x0100;
	res += (unsigned short) ascii2hex(ptr[2]) * 0x0010;
	res += (unsigned short) ascii2hex(ptr[3]) * 0x0001;
	return res;
}

/**
 * Converts a 16 bit number to a 32 bit hex-ascii value.
 * \param value The number which shall be converted.
 * \param buffer The function writes the result string in this buffer
 */
void hex16BitToAscii(unsigned short value, unsigned char *buffer) {
	buffer[0] = hex2ascii( (value >> 12) & 0xF);
	buffer[1] = hex2ascii( (value >> 8) & 0xF);
	buffer[2] = hex2ascii( (value >> 4) & 0xF);
	buffer[3] = hex2ascii( (value) & 0xF);
}

/**
 * Converts a 8 bit number to a 32 bit hex-ascii value.
 * \param value The number which shall be converted.
 * \param buffer The function writes the result string in this buffer
 */
void hex8BitToAscii(unsigned char value, unsigned char *buffer) {
	buffer[0] = hex2ascii( (value >> 4) & 0xF);
	buffer[1] = hex2ascii( (value) & 0xF);
}

/**
 * Converts a digit symbol to the appropriate number. Returns 0 on non-digit symbols.
 * \param symbol	The digit as an ASCII symbol.
 * \return			The corresponding number between 0 and 9.
 */
unsigned char a2i(unsigned char symbol) {
	if (symbol >= '0' && symbol <= '9') {
		return (symbol - '0');
	}
	else {
		return 0;
	}

}

/**
 * Parses a string to a (signed) integer value.
 * \param ptr	Pointer to the string
 */
int parseInteger(char *ptr, size_t n) {
	int result = 0;
	size_t i = 0;
	bool negative = false;

	if (*ptr == '-') {
		negative = true;
		ptr++;
		i++;
	}
	else if (*ptr == '+') {
		ptr++;
		i++;
	}

	// integral part of number
	while (*ptr >= '0' && *ptr <= '9' && i < n) {
		result *= 10;
		result += a2i(*ptr);
		ptr++;
		i++;
	}

	if (negative)
		result *= -1;
	return result;
}

/**
 * Parses a string to a (signed) double value, using decimal dot (.).
 * This function does NOT yet support the exp notation (123e45).
 * Valid strings are e.g. 0123.45, -142, -12.340, 342
 * \param ptr	Pointer to the string
 */
double parseDouble(unsigned char *ptr) {
	double decFactor = 1;
	double result = 0;
	bool negative = false;

	if (*ptr == '-') {
		negative = true;
		ptr++;
	}

	// integral part of number
	while (*ptr >= '0' && *ptr <= '9') {
		result *= 10;
		result += a2i(*ptr);
		ptr++;
	}

	// fractal part of number
	if (*ptr == '.') {
		ptr++;
		while (*ptr >= '0' && *ptr <= '9') {
			decFactor /= 10;
			result += decFactor * a2i(*ptr);
			ptr++;
		}
	}

	if (negative)
		result *= -1;
	return result;
}

const unsigned char crcTbl[] = {0x00, 0xcd, 0xd9, 0x14, 0xf1, 0x3c, 0x28, 0xe5,
								0xa1, 0x6c, 0x78, 0xb5, 0x50, 0x9d, 0x89, 0x44};

/**
 * Calculate CRC8
 * \param	data	Pointer to char array
 * \param	length	Number of chars in Array
 * \return	The CRC8 checksum
 */
uint8_t calcCRC(unsigned char* data, uint16_t length) {
	unsigned int i;
	uint8_t crc, idx;
	crc = 0;
	for (i = 0; i < length; i++) {
		idx = (crc ^ (* (data + i))) & 0x0f;
		crc = ( (crc >> 4) & 0x0f) ^ crcTbl[idx];
		idx = (crc ^ (* (data + i) >> 4)) & 0xf;
		crc = ( (crc >> 4) & 0xf) ^ crcTbl[idx];
	}
	return (crc);
}
