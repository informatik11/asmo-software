/**
 * \file ASMO_ts_layer.c
 * This file contains functions timestamping mesasurments and synchronizing clock to the clockmaster
 *
 * Created on: 24.02.2011
 * Modified on: 05.03.2011
 * Author: Mathias Obster
 */

#include "SmartECLA_allHeaders.h"

/*===========================================================================*/
/* Layer exported variables.                                                 */
/*===========================================================================*/

ts_service_status ts_status;

/*===========================================================================*/
/* Layer local variables.                                                    */
/*===========================================================================*/

/*===========================================================================*/
/* Layer local functions.                                                    */
/*===========================================================================*/

/*===========================================================================*/
/* Layer exported functions.                                                 */
/*===========================================================================*/

void ts_init(void) {
  return; //Nothing to be done here right now.
}

timestamp_t tsGetTS(void) {
  struct ptp_timestamp ts;
  ptp_get_ts(&ts);
  return ts;
}

/**
 * Receives two timestamps and calculates the difference in nanoseconds.
 * Returns that difference of a-b.
 */
int64_t tsSubtractTS(timestamp_t *a, timestamp_t *b) {
  int64_t diff_s, diff_ns;
  diff_s = ((int64_t)a->secondsField) - ((int64_t)b->secondsField);
  diff_ns = ((int64_t)a->nanosecondsField) - ((int64_t)b->nanosecondsField);

  if (diff_ns < 0 && diff_s != 0) { /* carry over underflow */
    diff_ns = 1000000000 - diff_ns;
    diff_s--;
  }

  // TODO: handle overflow?
  return diff_s * 1000 * 1000 * 1000 + diff_ns;
}

/**
 * @brief		Function to be called after receiving a measurement via CAN
 * @details		Calculates the internal tick-count value according to the sequence
 *              number + msOffset value
 *
 * @param[in]	incoming	lowest 8 bit = sequenceID
 *    				        next 16 bit = msOffset
 * 				            highest 8 bit = not used
 * @return Value to insert into ASMO_Measurment timestamp value
 */
uint32_t tsGenerateIncomingTS(timestamp_t incoming) {
  // TODO: implement
  return incoming.secondsField;
}

void tsSetZero(timestamp_t *ts) {
  ts->secondsField = 0;
  ts->nanosecondsField = 0;
}

void tsAddNanoseconds(timestamp_t *ts, uint64_t ns) {
  uint64_t countSeconds = ns / ((uint64_t)1000 * 1000 * 1000);
  ts->secondsField += countSeconds;
  ts->nanosecondsField += (ns - (countSeconds * 1000 * 1000 * 1000));
}

void tsAddMilliseconds(timestamp_t *ts, uint64_t ms) {
  uint64_t countSeconds = ms / ((uint64_t) 1000);
  ts->secondsField += countSeconds;
  ts->nanosecondsField += (ms - (countSeconds * 1000));
}

void tsAddSeconds(timestamp_t *ts, uint64_t s) {
  ts->secondsField += s;
}

uint64_t tsToEpoch(timestamp_t *ts) {
  return ts->secondsField * 1000 * 1000 * 1000 + (uint64_t)ts->nanosecondsField;
}

/**
 * Returns true, if a >= b
 */
bool tsGreaterEquals(timestamp_t *a, timestamp_t *b) {
  return tsSubtractTS(a, b) >= 0;
}

void tsGenerateIncomingTimestamp(uint32_t dummy) {
    (void)dummy;
  // There is nothing done with the data yet.
}

void tsProcessSyncevent(uint32_t highData, uint32_t lowData) {
  (void)highData;
  (void)lowData;
  // There is nothing done with the data yet.
}


/** @} */
