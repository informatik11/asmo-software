/**
 * \file ASMO_LED.c
 * \brief Encapsulates the control of the LED on the ASMO board.
 *
 *  \date 17.10.2011
 *  \author Florian Goebe
 */

#include "SmartECLA_allHeaders.h"

#if (USE_LED == TRUE)

//! Init LED module
void LED_init(void){
    // No GPIOs need to be initialized, was taken care of in os/hal/boards/<boardname>/board.h
    // switch all LEDs off
    LED_switchAll(0);
}

//! Switch a certain LED to a given state
void LED_switch(unsigned char ledNumber, unsigned char state){
    switch(ledNumber){
    case 1:
        palWriteLine(LINE_LED1, state);
        break;
    case 2:
        palWriteLine(LINE_LED2, state);
        break;
    case 3:
        palWriteLine(LINE_LED3, state);
        break;
    case 4:
      // nucleo board only has LEDs 1-3
#ifdef BOARD_STM32_ASMO
        palWriteLine(LINE_LED4, state);
#endif /* BOARD_STM32_ASMO */
        break;
    }
}

//! Toggles the state of a certain LED
void LED_toggle(unsigned char ledNumber){
    switch(ledNumber){
    case 1:
        palToggleLine(LINE_LED1);
        break;
    case 2:
        palToggleLine(LINE_LED2);
        break;
    case 3:
        palToggleLine(LINE_LED3);
        break;
    case 4:
      // nucleo board only has LEDs 1-3
#ifdef BOARD_STM32_ASMO
        palToggleLine(LINE_LED4);
#endif /* BOARD_STM32_ASMO */
        break;
    }
}

void LED_setValue(uint8_t number) {
  number &= 0x0F;
  for (uint8_t i = 0; i < 4; i++) {
    LED_switch(4 - i, (number >> i) & 0x01);
  }
}

void LED_switchMask(uint8_t mask) {
  mask &= 0x0F;
  for (uint8_t i = 0; i < 4; i++) {
    LED_switch(i + 1, (mask >> i) & 0x01);
  }
}

void LED_switchAll(uint8_t arg) {
  if (arg) {
    LED_switchMask(0x0F);
  }
  else {
    LED_switchMask(0x00);
  }
}

void LED_toggleMask(uint8_t mask) {
  mask &= 0x0F;
  for (uint8_t i = 0; i < 4; i++) {
    if (mask & (0x01 << i)) LED_toggle(i + 1);
  }
}

#endif /* USE_LED == TRUE */
