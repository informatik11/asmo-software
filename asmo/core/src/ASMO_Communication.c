/*
 *  \file   ASMO_Communication.c
 *  \brief  Provides implementations for communications API
 *
 *  \date   Nov 2020
 *  \author Moritz Huellmann
 */

#include "SmartECLA_allHeaders.h"

enum ASMO_CommunicationMethods currentMethod;

static void lwipOptionsInit(void) {
  struct ip4_addr mymask;
  struct ip4_addr mygw;
  struct ip4_addr myip;
  uint8_t macaddr[6] = {0x00, 0x13, 0xA9, 0x00, 0x00, 0x00};

  // UID_BASE is base address for unique device identifier field (96 Bit)
  memcpy(macaddr + 3, (uint8_t*)UID_BASE, 3);

  // XXX(MH) IP address resolution is handled by LwIP via AutoIP
  IP4_ADDR(&myip, 192, 168, 188, 130);

  IP4_ADDR(&mymask, 255, 255, 255, 0);
  IP4_ADDR(&mygw, 192, 168, 1, 1);

  // use the default link-up/link-down-callback
  lwipthread_opts_t lwip_opts = {.macaddress = macaddr, .netmask = mymask.addr, .address = myip.addr,
                                 .gateway = mygw.addr, .addrMode = NET_ADDRESS_AUTO,
                                 .link_up_cb = NULL, .link_down_cb = NULL};

  lwipInit(&lwip_opts);
}

/**
 * Initializes every available communication method (CAN and ETH).
 * LwIP and PTP get initializes anyway, as both are necessary for timestamping.
 */
void ASMO_CommunicationsInit(void) {
  /* May be used by PTP or LWIP, thus has to be initialized before them */
#if (USE_USB_SERIAL)
  asmoSerialInit();
#endif /* USE_USB_SERIAL */

  /* Initializes LwIP */
  lwipOptionsInit();

  /* Initializes PTP Stack */
  ptp_init();

  currentMethod = ASMO_COMM_METHOD_NONE;

#if (USE_CAN_COMM)
  ASMO_initCAN();
  currentMethod = ASMO_COMM_METHOD_CAN;
#endif /* USE_CAN_COMM */

#if (USE_ETH_COMM)
  /* Initializes ETH Communication TX */
  ASMO_initETH();
  /* Set comm method to eth */
  currentMethod = ASMO_COMM_METHOD_ETH;
#endif /* USE_ETH_COMM */

}

/**
 * Set the preferred communication method to 'method'
 *
 * Return
 *  0 on success
 *  -1 otherwise
 */
int8_t setCommMethod(enum ASMO_CommunicationMethods method) {
  if (method == ASMO_COMM_METHOD_CAN && USE_CAN_COMM) {
    currentMethod = ASMO_COMM_METHOD_CAN;
    return 0;
  }
  else if (method == ASMO_COMM_METHOD_ETH && USE_ETH_COMM) {
    currentMethod = ASMO_COMM_METHOD_ETH;
    return 0;
  }
  else {
    currentMethod = ASMO_COMM_METHOD_NONE;
    return -1;
  }
}

/**
 * Sends a simple {CAN|ETH}-Message, depending on what the preferred method of communication is.
 *
 * Returns
 *  0 on success
 *  -1 otherwise
 */
int8_t sendSimpleMessageRefTimeout(uint32_t message_id, uint64_t *message,
                                   int32_t timeout) {
  switch (currentMethod) {
#if (USE_CAN_COMM)
  case ASMO_COMM_METHOD_CAN:
    return sendSimpleCANMessageRefTimeout(
        message_id, message, timeout);
#endif
#if (USE_ETH_COMM)
  case ASMO_COMM_METHOD_ETH:
    return sendSimpleETHMessageRefTimeout(message_id, message, timeout);
#endif
  default:
    return -1;
  }
}

int8_t sendLocalMeasurement(ASMO_Measurement *meas) {
  switch (currentMethod) {
#if (USE_CAN_COMM)
  case ASMO_COMM_METHOD_CAN:
    return sendLocalMeasurementToCAN(meas);
#endif

#if (USE_ETH_COMM)
  case ASMO_COMM_METHOD_ETH:
    return sendLocalMeasurementToETH(meas);
    break;
#endif
  default:
    return -1;
  }
}

/**
 * Sends a simple {CAN|ETH}-Message, depending on what the preferred method of communication is.
 *
 * Returns
 *  0 on success
 *  -1 otherwise
 */
int8_t sendSimpleMessage(uint32_t message_id, uint64_t message) {

  return sendSimpleMessageRefTimeout(message_id, &message, TIME_INFINITE);

}

/**
 * Sends a simple {CAN|ETH}-Message, depending on what the preferred method of communication is.
 *
 * Returns
 *  0 on success
 *  -1 otherwise
 */
int8_t sendSimpleMessageRef(uint32_t message_id, uint64_t *message) {

  return sendSimpleMessageRefTimeout(message_id, message, TIME_INFINITE);
}

/**
 * Sends a simple {CAN|ETH}-Message, depending on what the preferred method of communication is.
 *
 * Returns
 *  0 on success
 *  -1 otherwise
 */
int8_t sendSimpleMessageTimeout(uint32_t message_id, uint64_t message,
                                int32_t timeout) {

  return sendSimpleMessageRefTimeout(message_id, &message, timeout);
}

