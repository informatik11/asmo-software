/**
 * @file    hal_frontboard.c
 * @brief   Frontboard driver code.
 * @note    Complex drivers are drivers, that heavily rely on other drivers.
 *          Following the guidelines of ChibiOS, those drivers don't need to
 *          implement LLD functions, since the LLD part is handled by the
 *          other used driver. Hence this driver does not implement a LLD.
 *
 * @addtogroup frontboard
 * @{
 */

#include "SmartECLA_allHeaders.h"
#include <math.h>

#if (ASMO_USE_FRONTBOARD == TRUE)

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

THD_WORKING_AREA(ROTARY_ENCODER_THD_WA, 512);

/**
 * @brief LED driver configs.
 */
LEDDisplayConfig ledDisplayRight;
LEDDisplayConfig ledDisplayLeft;
LEDDisplayConfig ledDisplayRest;

/**
 * @brief LED driver SPI configs.
 */
SPIConfig ledDisplayRightSpi;
SPIConfig ledDisplayLeftSpi;
SPIConfig ledDisplayRestSpi;

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/**
 * @brief Frontboard driver.
 */
FrontboardDriver FRONTBOARD_D;

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/**
 * @brief       Converts value into display binary representation.
 *
 * @param[in] valueInput    Value to convert
 * @param[in] digitCount    Number of digits
 * @param[out] valueBin     Array to convert to
 */
static void value2Bin(double valueInput, unsigned short digitCount,
                      uint8_t valueBin[digitCount]) {

  /* Guard: Only positive integer support yet, later add more support.*/
  if (valueInput < 0 || valueInput >= pow(10, digitCount)) {
    for (unsigned short i = 0; i < digitCount; ++i) {
      valueBin[i] = LED_DISPLAY_DASH;
    }
    return;
  }
  valueInput = (double) ((int) valueInput); // Only integers yet

  /* Start conversion.*/
  bool skipZeros = true;
  for (unsigned short i = 0; i < digitCount; ++i) {

    /* Extract the decimal digits from left (highest) to right.*/
    unsigned int valueDigit =
        (int) (valueInput / pow(10, digitCount - i - 1)) % 10;

    /* Skip leading zeros.*/
    if (skipZeros) {
      if (valueDigit == 0) {
        /* Show empty field instead.*/
        valueBin[digitCount - i - 1] = LED_DISPLAY_EMPTY;
      }
      else {
        /* First none zero value terminates the skipping of zeros.*/
        skipZeros = false;
      }
    }

    /* Only convert to binary if it is not a leading zero.*/
    if (!skipZeros) {
      /* Get the binary 7 segment representation for the extracted number.*/
      valueBin[digitCount - i - 1] = led_display_convert(valueDigit);
    }
  }

  /* If all values are zero, store one zero instead of only empty.*/
  if (skipZeros) {
    valueBin[0] = led_display_convert(0);
  }
}

/**
 * @brief       Rotary Encoder thread.
 *
 * @param[in] p     FrontboardDriver object
 */
static THD_FUNCTION(ROTARY_ENCODER_THD, arg) {
  FrontboardDriver *frontboardp = (FrontboardDriver *) arg;
  chRegSetThreadName("Rotary Encoder");

  /* Holds the rotary gpio values (high or low) from the last and current step.
   * The bits 3 and 2 hold values from previous step and
   * the bits 1 and 0 the values from the current step.*/
  uint8_t rotaryValueOldNew = 0;

  while (!chThdShouldTerminateX()) {
    /* Suspend thread and wait until the encoder is turned (gpio event is
     * waking up the thread)
     * or until the timeout occurred.*/
    chSysLock();
    chThdSuspendTimeoutS(&frontboardp->rotaryEncoderThdReference,
                         chTimeMS2I(frontboardp->config->rotary_sleep_max));
    chSysUnlock();

    /* Save rotary value of last step and remove old ones.*/
    rotaryValueOldNew = (rotaryValueOldNew << 2) & 0b1100;
    /* Check the two pins of the rotary encoder to determine position change.
     * The rotary encoder has specific positions. On each position both pins
     * are high or low.
     * However, when moving from one position to the other, one pin is always
     * triggered a very small
     * moment in time before the other pin is triggered. Consequently, it is
     * important to use interrupts.
     * When the first pin is triggered, the thread is woken up and checks
     * which pin is triggered first.*/
    if (palReadLine(LINE_FRONTBOARD_RPE_A) == PAL_HIGH) {
      rotaryValueOldNew |= 0b10;
    }
    if (palReadLine(LINE_FRONTBOARD_RPE_B) == PAL_HIGH) {
      rotaryValueOldNew |= 0b01;
    }

    /* Now determine if cw or ccw and pass to callback function.*/
    switch (rotaryValueOldNew) {
      case 0b0010:
      case 0b1101:
        frontboardp->config->rotary_cb(frontboardp, ROTARY_CCW);
        break;
      case 0b0001:
      case 0b1110:
        frontboardp->config->rotary_cb(frontboardp, ROTARY_CW);
        break;
      default:
        frontboardp->config->rotary_cb(frontboardp, ROTARY_SAME);
        break;
    }
  }
}

static void isr_rotaryWakeUp(void *arg) {
  FrontboardDriver *frontboardp = (FrontboardDriver *) arg;

  /* Wakes up the thread.*/
  chSysLockFromISR();
  chThdResumeI(&frontboardp->rotaryEncoderThdReference, MSG_OK);
  chSysUnlockFromISR();
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Frontboard driver initialization.
 * @note    This function is implicitly invoked by @p halInit(), there is
 *          no need to explicitly initialize the driver.
 *
 * @init
 */
void frontboardInit(void) {

  frontboardObjectInit(&FRONTBOARD_D);

  /* Initialize SPI driver of LED display config.*/
  ledDisplayRight.spi_driver
      = ledDisplayLeft.spi_driver
      = ledDisplayRest.spi_driver = &SPID4;

  /* Initialize SPI config of LED display configs.*/
  ledDisplayRight.spi_config = &ledDisplayRightSpi;
  ledDisplayLeft.spi_config = &ledDisplayLeftSpi;
  ledDisplayRest.spi_config = &ledDisplayRestSpi;

  ledDisplayRight.spi_config->circular
    = ledDisplayLeft.spi_config->circular
    = ledDisplayRest.spi_config->circular = false;

  ledDisplayRight.spi_config->end_cb
      = ledDisplayLeft.spi_config->end_cb
      = ledDisplayRest.spi_config->end_cb = NULL;

  ledDisplayRight.spi_config->ssport
      = ledDisplayLeft.spi_config->ssport
      = ledDisplayRest.spi_config->ssport = GPIOE;

  ledDisplayRight.spi_config->sspad = GPIOE_FRONTBOARD_SPI_CS1;
  ledDisplayLeft.spi_config->sspad = GPIOE_FRONTBOARD_SPI_CS2;
  ledDisplayRest.spi_config->sspad = GPIOE_FRONTBOARD_SPI_CS3;

  /* Clock phase and clock polarity are 0 so not set,
   * tough clock polarity can be 0 or 1, doesnt matter.
   * Other possibility would be 1 and 1, since always sample at rising edge.
   *
   * Baud rate shall be min around 40ns => max 25 MHz.
   * Calc baud rate by speed of apb bus devided by 2(BR+1)
   * Current speed is 50 MHz -> BR shall be 1 to get max 25 MHz,
   * choose BR = 2 to be safe: BR1 (Bit 2) has to be set
   * */
  ledDisplayRight.spi_config->cr1
      = ledDisplayLeft.spi_config->cr1
      = ledDisplayRest.spi_config->cr1 = SPI_CR1_BR_1;
  /* Bits per transfer are 16 bits, which is maximum so all
   * data register bits have to be set.*/
  ledDisplayRight.spi_config->cr2
      = ledDisplayLeft.spi_config->cr2
      = ledDisplayRest.spi_config->cr2
          = (SPI_CR2_DS_3 | SPI_CR2_DS_2 | SPI_CR2_DS_1 | SPI_CR2_DS_0);

}

/**
 * @brief   Initializes the standard part of a @p FrontboardDiver structure.
 *
 * @param[out] frontboardp     pointer to the @p LEDDisplayDriver object
 *
 * @init
 */
void frontboardObjectInit(FrontboardDriver *frontboardp) {

  /* Default values for driver.*/
  frontboardp->state = FRONTBOARD_STOP;
  frontboardp->config = NULL;
  frontboardp->rotaryEncoderThdReference = NULL;
  frontboardp->ledValues = 0;

}

/**
 * @brief   Configures and activates the frontboard.
 *
 * @param[in] frontboardp      pointer to the @p FrontboardDriver object
 * @param[in] config           pointer to the @p FrontboardConfig object.
 *                             Depending on the implementation the value can
 *                             be @p NULL.
 *
 * @api
 */
void frontboardStart(FrontboardDriver *frontboardp, const FrontboardConfig
    *config) {

  osalDbgCheck((frontboardp != NULL) && (config != NULL));
  osalDbgAssert((frontboardp->state == FRONTBOARD_STOP) ||
      (frontboardp->state == FRONTBOARD_READY), "invalid state");

  frontboardp->config = config;

  /* Init all segments.*/
  LEDDisplayConfig *ledConfigBuffer[] = {&ledDisplayRight,
                                         &ledDisplayLeft,
                                         &ledDisplayRest};
  int i;
  for (i = 0; i < 3; ++i) {
    /* Always acquire driver with mutex to be thread safe!*/
    ledDisplayAcquireBus(&LEDDISPD);
    ledDisplayStart(&LEDDISPD, ledConfigBuffer[i]);

    /* Initialize and prepare.*/
    ledDisplaySetScanlimit(&LEDDISPD, 7);
    ledDisplaySetIntensity(&LEDDISPD, 100);
    ledDisplaySetEnabled(&LEDDISPD, true);
    ledDisplayClear(&LEDDISPD);

    /* Release driver again.*/
    ledDisplayStop(&LEDDISPD);
    ledDisplayReleaseBus(&LEDDISPD);
  }

  if(frontboardp->config->rotary_cb != NULL) {
    /* Start rotary encoder thread.*/
    palEnableLineEvent(LINE_FRONTBOARD_RPE_A, PAL_EVENT_MODE_BOTH_EDGES);
    palSetLineCallback(LINE_FRONTBOARD_RPE_A, isr_rotaryWakeUp,
                       (void *) frontboardp);

    palEnableLineEvent(LINE_FRONTBOARD_RPE_B, PAL_EVENT_MODE_BOTH_EDGES);
    palSetLineCallback(LINE_FRONTBOARD_RPE_B, isr_rotaryWakeUp,
                       (void *) frontboardp);

    chThdCreateStatic(ROTARY_ENCODER_THD_WA, sizeof(ROTARY_ENCODER_THD_WA),
                      NORMALPRIO + 1, (tfunc_t) ROTARY_ENCODER_THD, frontboardp);
  }

  frontboardp->state = FRONTBOARD_READY;

  /* Reset all LEDs.*/
  frontboardReset(frontboardp);

}

/**
 * @brief   Deactivates the frontboard peripheral.
 *
 * @param[in] frontboardp      pointer to the @p FrontboardDriver object
 *
 * @api
 */
void frontboardStop(FrontboardDriver *frontboardp) {

  osalDbgCheck(frontboardp != NULL);
  osalDbgAssert((frontboardp->state == FRONTBOARD_STOP) ||
                (frontboardp->state == FRONTBOARD_READY), "invalid state");

  /* Reset all LEDs.*/
  frontboardReset(frontboardp);

  /* Terminate the rotary encoder thread.*/
  chThdTerminate(frontboardp->rotaryEncoderThdReference);

  frontboardp->state = FRONTBOARD_STOP;

}

/**
 * @brief       Resets all displays.
 *
 * @param[in] frontboardp      pointer to the @p FrontboardDriver object
 */
void frontboardReset(FrontboardDriver *frontboardp) {

  osalDbgCheck(frontboardp != NULL);
  osalDbgAssert((frontboardp->state == FRONTBOARD_READY), "invalid state");

  frontboardSendRight(frontboardp, -1, -1);
  frontboardSendLeft(frontboardp, -1, -1);
  frontboardSendBar(frontboardp, 0);
  frontboardSendLed(frontboardp, frontboard_led_top, false);
  frontboardSendLed(frontboardp, frontboard_led_right, false);
  frontboardSendLed(frontboardp, frontboard_led_brake, false);
  frontboardSendLed(frontboardp, frontboard_led_middle, false);
  frontboardSendLed(frontboardp, frontboard_led_left, false);

}

/**
 * @brief       Sets the LEDs of the bar.
 *
 * @param[in] fractionBars      Value to be displayed as a fraction of 1 as max
 */
void frontboardSendBar(FrontboardDriver *frontboardp, double
    fractionBars) {

  osalDbgCheck(frontboardp != NULL);
  osalDbgAssert((frontboardp->state == FRONTBOARD_READY), "invalid state");

  /* Normalize the fractionBars to a percentage of the maximum number of bars
   * of the display (8 bars).*/
  fractionBars = fractionBars * 8;
  uint8_t valueBin = 0;

  for (int i = 0; i < fractionBars; ++i) {
    /* Start on leftmost bar and light as many bars to the right as high as
     * the fractionBars is.*/
    valueBin |= (1 << (7 - i));
  }
  /* Fix order since bars (so the bits) are in this order 76543210 and have
   * to be in this order 65432107 on display.
   * I have no idea why someone has chosen this display order.
   * So it is a cycle right shift with old Bit0 has to be the new Bit7.*/
  uint8_t valueBin0 = valueBin & 1;
  valueBin = valueBin >> 1;
  valueBin |= valueBin0 << 7;

  /* Write to the 4th display (number 3) of this driver.*/
  ledDisplayAcquireBus(&LEDDISPD);
  ledDisplayStart(&LEDDISPD, &ledDisplayRest);

  ledDisplayChangeDisplay(&LEDDISPD, 3);
  ledDisplayWrite(&LEDDISPD, 1, &valueBin);

  ledDisplayStop(&LEDDISPD);
  ledDisplayReleaseBus(&LEDDISPD);

}

/**
 * @brief       Sets the LEDs on the frontboard.
 *
 * @param[in] ledName       Name of the LED
 * @param[in] valueLed      Value for the LED
 */
void frontboardSendLed(FrontboardDriver *frontboardp,
                             frontboard_display_leds ledName, bool ledValue) {

  osalDbgCheck(frontboardp != NULL);
  osalDbgAssert((frontboardp->state == FRONTBOARD_READY), "invalid state");

  /* Only change the value on the respective binary position.*/
  if (ledValue) {
    /* Set Bit.*/
    frontboardp->ledValues |= (1 << ledName);
  }
  else {
    /* Clear Bit.*/
    frontboardp->ledValues &= ~(1 << ledName);
  }

  /* Send new config to the 3rd display (number 2) of this driver.*/
  ledDisplayAcquireBus(&LEDDISPD);
  ledDisplayStart(&LEDDISPD, &ledDisplayRest);

  ledDisplayChangeDisplay(&LEDDISPD, 2);
  ledDisplayWrite(&LEDDISPD, 1, &frontboardp->ledValues);

  ledDisplayStop(&LEDDISPD);
  ledDisplayReleaseBus(&LEDDISPD);

}

/**
 * @brief       Sends values to left display.
 *
 * @param[in] valueTop      Value to display in top row
 * @param[in] valueBot      Value to display in bottom row
 */
void frontboardSendLeft(FrontboardDriver *frontboardp,
                              double valueTop, double valueBot) {

  osalDbgCheck(frontboardp != NULL);
  osalDbgAssert((frontboardp->state == FRONTBOARD_READY), "invalid state");

  /* The number of digits per row of this display.*/
  unsigned short digitsPerRow = 5;

  /* Convert both values to display driver digit array representation.*/
  uint8_t valueTopBin[digitsPerRow];
  uint8_t valueBotBin[digitsPerRow];
  value2Bin(valueTop, digitsPerRow, valueTopBin);
  value2Bin(valueBot, digitsPerRow, valueBotBin);

  /* The left most digits (the 5th) are addressed by a different display driver,
   * since only 8 digits fit to one display driver.
   * Therefore, they have to be written separately.*/
  uint8_t valueBinHighest[] = {valueTopBin[digitsPerRow - 1],
                               valueBotBin[digitsPerRow - 1]};

  /* Concatenate the first four digits of top and bot row and discard the 5th
   * one.*/
  unsigned short digitCountMain = 4;
  uint8_t valueBin[2 * digitCountMain];
  for (int i = 0; i < digitCountMain; ++i) {
    valueBin[i] = valueTopBin[i];
    valueBin[i + digitCountMain] = valueBotBin[i];
  }

  /* Write first 4 digits of top and bot row.*/
  ledDisplayAcquireBus(&LEDDISPD);
  ledDisplayStart(&LEDDISPD, &ledDisplayLeft);

  ledDisplayChangeDisplay(&LEDDISPD, 0);
  ledDisplayWrite(&LEDDISPD, digitCountMain * 2, valueBin);

  ledDisplayStop(&LEDDISPD);
  ledDisplayReleaseBus(&LEDDISPD);

  /* Write the 5th digit of top and bot row to the 1st and 2nd display of the
   * rest driver.*/
  ledDisplayAcquireBus(&LEDDISPD);
  ledDisplayStart(&LEDDISPD, &ledDisplayRest);

  ledDisplayChangeDisplay(&LEDDISPD, 0);
  ledDisplayWrite(&LEDDISPD, 2, valueBinHighest);

  ledDisplayStop(&LEDDISPD);
  ledDisplayReleaseBus(&LEDDISPD);

}

/**
 * @brief       Sends values to right display.
 *
 * @param[in] valueTop      Value to display in top row
 * @param[in] valueBot      Value to display in bottom row
 */
void frontboardSendRight(FrontboardDriver *frontboardp,
                               double valueTop, double valueBot) {

  osalDbgCheck(frontboardp != NULL);
  osalDbgAssert((frontboardp->state == FRONTBOARD_READY), "invalid state");

  /* Write to all displays of this driver.*/
  unsigned short digitsPerRow = 4;

  /* Convert both values to display driver digit array representation.*/
  uint8_t valueBin[digitsPerRow * 2];
  value2Bin(valueTop, digitsPerRow, valueBin);
  value2Bin(valueBot, digitsPerRow, &(valueBin[digitsPerRow]));

  /* Write to all displays of this driver.*/
  ledDisplayAcquireBus(&LEDDISPD);
  ledDisplayStart(&LEDDISPD, &ledDisplayRight);

  ledDisplayChangeDisplay(&LEDDISPD, 0);
  ledDisplayWrite(&LEDDISPD, digitsPerRow * 2, valueBin);

  ledDisplayStop(&LEDDISPD);
  ledDisplayReleaseBus(&LEDDISPD);

}

#endif /* ASMO_USE_FRONTBOARD == TRUE */

/** @} */
