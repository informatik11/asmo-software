/**
 * @file    hal_external_adc.c
 * @brief   ADC via SPI driver code.
 * @note    Complex drivers are drivers, that heavily rely on other drivers.
 *          Following the guidelines of ChibiOS, those drivers don't need to
 *          implement LLD functions, since the LLD part is handled by the
 *          other used driver. Hence this driver does not implement a LLD.
 * @author  Moritz Huellmann
 * @date    Jan 2021
 */

#include "SmartECLA_allHeaders.h"

#if (ASMO_USE_ADC == TRUE)

ADCEDriver ADCED;

thread_t    *adcConversionThd;

/*
 * ADC capable of 1MSPS -> 16MHz clock max. SPI2 on APB1 with 50MHz.
 * 50 MHz / 4 = 12.5MHz -> Prescaler 001 (see datasheet) -> SPI_CR1_BR_0
 */
SPIConfig adcSPIConfig = {
  FALSE,
  NULL,
  GPIOB,
  GPIOB_CS_ADC,
  SPI_CR1_BR_0,
  SPI_CR2_DS
};

THD_WORKING_AREA(ADC_EXT_CONV_THD_WA, 1024);
/**
 * @brief       This thread receives the values of the external ADC.
 * @note        It runs once, if conversion is not circular. If conversion is set to be circular,
 *              it will run continuously, filling the sample buffer specified when starting the conversion.
 *              This thread is not thread-safe.
 * @param[in]   p   A pointer to the ADCED instance to work on.
 */
THD_FUNCTION(ADC_EXT_CONV_THD, p) {
  chRegSetThreadName("ADC EXT CONV");

  osalDbgCheck(p != NULL);

  ADCEDriver *adcp = p;

  systime_t start;
  sysinterval_t ticksSpend;

  uint16_t currDepthPos;
  while (!chThdShouldTerminateX()) {
    if (adcp->state != ADC_EXT_ACTIVE) {
      chThdSleepMilliseconds(1);
      continue;
    }

    /* ADC is in normal mode at this point */

    /*
     * Send first bytes to ADC to receive conversion of first used channel in next exchange
     * SPI uses DMA by default.
     */
    spiAcquireBus(adcp->config->spi_driver);
    spiStart(adcp->config->spi_driver, &adcSPIConfig);

    spiSelect(adcp->config->spi_driver);
    spiSend(adcp->config->spi_driver, 1, adcp->grpp->send_buffer);
    spiUnselect(adcp->config->spi_driver);

    spiStop(adcp->config->spi_driver);
    spiReleaseBus(adcp->config->spi_driver);

    currDepthPos = 0;
    while (adcp->state == ADC_EXT_ACTIVE) {
      start = chVTGetSystemTime();

      spiAcquireBus(adcp->config->spi_driver);
      spiStart(adcp->config->spi_driver, &adcSPIConfig);

      /* Send the instruction to sample the next channel, get data for current one */
      spiSelect(adcp->config->spi_driver);
      spiExchange(
          adcp->config->spi_driver,
          1,
          &adcp->grpp->send_buffer[((currDepthPos + 1) / (adcp->depth / ADC_CH_COUNT))
              % ADC_CH_COUNT], adcp->samples + currDepthPos);
      adcp->samples[currDepthPos] &= 0x0FFF; // Exclude channel identifier bits
      spiUnselect(adcp->config->spi_driver);

      spiStop(adcp->config->spi_driver);
      spiReleaseBus(adcp->config->spi_driver);

      currDepthPos++;

      /*
       * Check, if thread should be killed. Do it here, so no callbacks
       * can be invoked after suspension.
       */
      if (adcp->state == ADC_EXT_STOP) {
        break;
      }

      /* Check if buffer is full */
      if (currDepthPos == adcp->depth) {
        _adc_ext_isr_full_code(adcp);
        currDepthPos = 0;
      }
      else if (currDepthPos == adcp->depth / 2) {
        _adc_ext_isr_half_code(adcp);
      }
      ticksSpend = chTimeDiffX(start, chVTGetSystemTime());
      if (ticksSpend < chTimeMS2I(adcp->config->interval)) {
        chThdSleep(chTimeMS2I(adcp->config->interval) - ticksSpend);
      }
    }
  }
  chThdExit((msg_t)MSG_OK);
}

/**
 * @brief   Initializes the external ADC driver struct.
 *
 * @param[in]   adcp    Pointer to the ADCED instance to initialize
 */
void adcExtObjectInit(ADCEDriver *adcp) {

  /* Initialize the driver struct */
  adcp->state = ADC_EXT_STOP;
  adcp->samples = NULL;
  adcp->depth = 0;
  adcp->grpp = NULL;
  adcp->config = NULL;
#if ASMO_ADC_USE_WAIT == TRUE
  adcp->thread = NULL;
#endif
#if ASMO_ADC_USE_MUTUAL_EXCLUSION == TRUE
  osalMutexObjectInit(&adcp->mutex);
#endif
}

/**
 * @brief   Configures and activates the ADC peripheral. (Enters "normal mode" of hardware)
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @api
 */
void adcExtStart(ADCEDriver *adcp, ADCEConfig *config) {

  osalDbgCheck(adcp != NULL);
  osalDbgAssert((adcp->state == ADC_EXT_STOP) || (adcp->state == ADC_EXT_READY),
                "invalid state");

  if (!adcConversionThd) {
    adcConversionThd = chThdCreateStatic(ADC_EXT_CONV_THD_WA,
                                        sizeof(ADC_EXT_CONV_THD_WA), chThdGetSelfX()->prio + 1,
                                        ADC_EXT_CONV_THD, adcp);
  }

  adcp->config = config;
#if (defined(BOARD_STM32_ASMO) || defined(BOARD_STM32_PUMP))
  if (adcp->config->spi_driver == NULL) {
    adcp->config->spi_driver = &SPID2;
  }
#endif

  osalDbgCheck(adcp->config->spi_driver != NULL);

  if (adcp->state == ADC_EXT_STOP) {
    /* Initialize the hardware */
    uint16_t dummyFrame = AD7924_DUMMY_FRAME;
    uint16_t configFrame = 0 | AD7924_CH0 | AD7924_ASMO_DEF_MODE | AD7924_WRITE(1);

    /* Send frames */
    spiAcquireBus(adcp->config->spi_driver);
    spiStart(adcp->config->spi_driver, &adcSPIConfig);

    spiSelect(adcp->config->spi_driver);
    spiSend(adcp->config->spi_driver, 1, &dummyFrame);
    spiUnselect(adcp->config->spi_driver);

    spiSelect(adcp->config->spi_driver);
    spiSend(adcp->config->spi_driver, 1, &configFrame);
    spiUnselect(adcp->config->spi_driver);

    spiStop(adcp->config->spi_driver);
    spiReleaseBus(adcp->config->spi_driver);
  }

  adcp->state = ADC_EXT_READY;
}

/**
 * @brief   Deactivates the ADC peripheral. (Enters "full shutdown" mode)
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @api
 */
void adcExtStop(ADCEDriver *adcp) {

  osalDbgCheck(adcp != NULL);

  osalDbgAssert((adcp->state == ADC_EXT_STOP) || (adcp->state == ADC_EXT_READY),
                "invalid state");

  if (adcp->state != ADC_EXT_STOP) {
    /* The conversion thread checks, if state is ADC_EXT_STOP. If it is, it kills itself */
    adcp->state = ADC_EXT_STOP;
    chThdTerminate(adcConversionThd);
    (void) chThdWait(adcConversionThd);

    /* At this point, the conversion thread was killed */
    adcConversionThd = NULL;

    /* Send shutdown frame to peripheral, saves energy when its not in use */
    uint16_t shutdownFrame = 0 | AD7924_POW_FULL_SHUT;

    spiAcquireBus(adcp->config->spi_driver);
    spiStart(adcp->config->spi_driver, &adcSPIConfig);
    spiSelect(adcp->config->spi_driver);

    spiSend(adcp->config->spi_driver, 1, &shutdownFrame);

    spiUnselect(adcp->config->spi_driver);
    spiStop(adcp->config->spi_driver);
    spiReleaseBus(adcp->config->spi_driver);

  }

  adcp->config = NULL;

}

/**
 * @brief   Starts an external ADC conversion.
 * @details Starts an asynchronous conversion operation.
 * @post    The callbacks associated to the conversion group will be invoked
 *          on buffer fill and error events.
 * @note    The buffer is organized as a matrix of M*N elements where M is the
 *          channels number configured into the conversion group and N is the
 *          buffer depth. The samples are sequentially written into the buffer
 *          with no gaps.
 *
 * @param[in]   adcp        pointer to the @p ADCEDriver object
 * @param[in]   grpp        pointer to a @p ADCEConversionGroup object
 * @param[out]  samples     pointer to the samples buffer
 * @param[in]   depth       buffer depth (matrix rows number). The buffer depth
 *                          must be one or an even number.
 *
 * @iclass
 */
void adcExtStartConversionI(ADCEDriver *adcp, ADCEConversionGroup *grpp,
                            adcextsample_t *samples, size_t depth) {

  osalDbgCheckClassI();
  osalDbgCheck(
      (adcp != NULL) && (grpp != NULL) && (samples != NULL) && (depth > 0U)
          && ((depth == 1U) || ((depth & 1U) == 0U)));
  osalDbgAssert(
      (adcp->state == ADC_EXT_READY) || (adcp->state == ADC_EXT_ERROR),
      "not ready");

  adcp->samples = samples;
  adcp->depth = depth;
  adcp->grpp = grpp;

  for (uint8_t i = 0; i < depth; i++) {
    *(adcp->samples + i) = 0;
  }

  ADCEConversionGroup *cg = adcp->grpp;

  for (uint8_t i = 0; i < ADC_CH_COUNT; i++) {
    cg->send_buffer[i] = 0;
  }

  /* Setup conversion */
#if (ADC_USE_CH0)
  cg->send_buffer[ADC_CH0_POS] = AD7924_CH0 | AD7924_ASMO_DEF_MODE
      | AD7924_WRITE(1);
#endif

#if (ADC_USE_CH1)
  cg->send_buffer[ADC_CH1_POS] = AD7924_CH1 | AD7924_ASMO_DEF_MODE
      | AD7924_WRITE(1);
#endif

#if (ADC_USE_CH2)
  cg->send_buffer[ADC_CH2_POS] = AD7924_CH2 | AD7924_ASMO_DEF_MODE
      | AD7924_WRITE(1);
#endif

#if (ADC_USE_CH3)
  cg->send_buffer[ADC_CH3_POS] = AD7924_CH3 | AD7924_ASMO_DEF_MODE
      | AD7924_WRITE(1);
#endif

  /* Start conversion */
  adcp->state = ADC_EXT_ACTIVE;
}

/**
 * @brief   Starts an external ADC conversion.
 * @details Starts an asynchronous conversion operation.
 * @note    The buffer is organized as a matrix of M*N elements where M is the
 *          channels number configured into the conversion group and N is the
 *          buffer depth. The samples are sequentially written into the buffer
 *          with no gaps.
 *
 * @param[in]   adcp        pointer to the @p ADCEDriver object
 * @param[in]   grpp        pointer to a @p ADCEConversionGroup object
 * @param[out]  samples     pointer to the samples buffer
 * @param[in]   depth       buffer depth (matrix rows number). The buffer depth
 *                          must be one or an even number.
 *
 * @api
 */
void adcExtStartConversion(ADCEDriver *adcp, ADCEConversionGroup *grpp,
                           adcextsample_t *samples, size_t depth) {

  osalSysLock();
  adcExtStartConversionI(adcp, grpp, samples, depth);
  osalSysUnlock();
}

void adcExtStopConversionI(ADCEDriver *adcp) {
  adcp->state = ADC_EXT_READY;
}

void adcExtStopConversion(ADCEDriver *adcp) {
  osalSysLock();
  adcExtStopConversionI(adcp);
  osalSysUnlock();
}

#if (ADC_USE_WAIT == TRUE) || defined(__DOXYGEN__)
/**
 * @brief   Performs an external ADC conversion.
 * @details Performs a synchronous conversion operation.
 * @note    The buffer is organized as a matrix of M*N elements where M is the
 *          channels number configured into the conversion group and N is the
 *          buffer depth. The samples are sequentially written into the buffer
 *          with no gaps.
 *
 * @param[in]   adcp        pointer to the @p ADCEDriver object
 * @param[in]   grpp        pointer to a @p ADCEConversionGroup object
 * @param[out]  samples     pointer to the samples buffer
 * @param[in]   depth       buffer depth (matrix rows number). The buffer depth
 *                          must be one or an even number.
 * @return              The operation result.
 * @retval MSG_OK       Conversion finished.
 * @retval MSG_RESET    The conversion has been stopped using
 *                      @p acdExtStopConversion() or @p acdExtStopConversionI(),
 *                      the result buffer may contain incorrect data.
 * @retval MSG_TIMEOUT  The conversion has been stopped because an hardware
 *                      error.
 *
 * @api
 */
msg_t adcExtConvert(ADCEDriver *adcp, ADCEConversionGroup *grpp,
                    adcextsample_t *samples, size_t depth) {
  msg_t msg;

  osalSysLock();
  osalDbgAssert(adcp->thread == NULL, "already waiting");
  adcExtStartConversionI(adcp, grpp, samples, depth);
  msg = osalThreadSuspendS(&adcp->thread);
  osalSysUnlock();
  return msg;
}
#endif /* ADC_USE_WAIT == TRUE */

#if (ASMO_ADC_USE_MUTUAL_EXCLUSION == TRUE) || defined(__DOXYGEN__)
/**
 * @brief   Gains exclusive access to the ADC peripheral.
 * @details This function tries to gain ownership to the ADC bus, if the bus
 *          is already being used then the invoking thread is queued.
 * @pre     In order to use this function the option
 *          @p ASMO_ADC_USE_MUTUAL_EXCLUSION must be enabled.
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @api
 */
void adcExtAcquireBus(ADCEDriver *adcp) {

  osalDbgCheck(adcp != NULL);

  osalMutexLock(&adcp->mutex);
}

/**
 * @brief   Releases exclusive access to the ADC peripheral.
 * @pre     In order to use this function the option
 *          @p ASMO_ADC_USE_MUTUAL_EXCLUSION must be enabled.
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @api
 */
void adcExtReleaseBus(ADCEDriver *adcp) {

  osalDbgCheck(adcp != NULL);

  osalMutexUnlock(&adcp->mutex);
}
#endif /* ASMO_ADC_USE_MUTUAL_EXCLUSION == TRUE */

#endif /* USE_ADC == TRUE */
