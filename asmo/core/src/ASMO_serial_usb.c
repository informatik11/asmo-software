/**
 * @file    ASMO_serial_usb.c
 * @brief   Contains function to manage messages over USB
 *
 * @{
 */

#include "SmartECLA_allHeaders.h"

#if (USE_USB_SERIAL)

/*===========================================================================*/
/* Task local definitions.                                                   */
/*===========================================================================*/

#define DEBUG_MSG_MAX_LEN   (250U)

#define SERIAL_CABLE_EVENT  EVENT_MASK(0)
#define SERIAL_SEND_EVENT   EVENT_MASK(1)

#define SERIAL_LED_INDICATORS   (FALSE)

/*===========================================================================*/
/* Task local variables and types.                                           */
/*===========================================================================*/

static binary_semaphore_t serial_bin_sem;
static event_source_t usb_send_event;
static thread_reference_t serial_thd;

static char message_buffer[DEBUG_MSG_MAX_LEN];

static THD_WORKING_AREA(serial_usb_thd_wa, (64 + DEBUG_MSG_MAX_LEN));

/*===========================================================================*/
/* Task exported variables.                                                  */
/*===========================================================================*/

/*===========================================================================*/
/* Task local functions.                                                     */
/*===========================================================================*/

/**
 * @brief   This thread is necessary, because the nature of USB is to block, if the queue is full.
 *          A simple check for the status of the USB state machine is not sufficient, as the state
 *          remains at USB_ACTIVE, even if the host is not actively requesting data.
 * @param p is ignored
 */
static THD_FUNCTION(serial_usb_thd, p) {
  (void)p;
  chRegSetThreadName("serial_usb_thd");

  eventmask_t msk;
  eventflags_t flags;
  event_listener_t sdu_event_listener;
  event_listener_t usb_send_listener;

  bool prev_disconnect = false;

  /* Hook to stream events. Connect and disconnect events included. */
  chEvtRegisterMask(&SDU2.event, &sdu_event_listener, SERIAL_CABLE_EVENT);
  chEvtRegisterMask(&usb_send_event, &usb_send_listener, SERIAL_SEND_EVENT);

  while (true) {
    msk = chEvtWaitAny(ALL_EVENTS);
    if (msk == SERIAL_CABLE_EVENT) {
      flags = chEvtGetAndClearFlags(&sdu_event_listener);
      if (flags & CHN_CONNECTED) {
        /* The cable was reconnected. */
#if (SERIAL_LED_INDICATORS)
        palClearLine(LINE_LED2);
        palSetLine(LINE_LED1);
#endif
        if (prev_disconnect) {
          /*
           * We check, if there was a previous disconnect,
           * otherwise we would possibly connect to the bus twice.
           */
          usbConnectBus(serusbcfg.usbp);
          prev_disconnect = false;
        }
      }
      if (flags & CHN_DISCONNECTED) {
        /* The cable got disconnected, set the disconnect flag */
#if (SERIAL_LED_INDICATORS)
        palClearLine(LINE_LED1);
        palSetLine(LINE_LED2);
#endif
        prev_disconnect = true;
      }
#if (SERIAL_LED_INDICATORS)
      else {
        palSetLine(LINE_LED4);
      }
#endif
    }
    else if (msk == SERIAL_SEND_EVENT) {
      /*
       * We locked the mutex, so we know, that this is the newest message.
       * Also, we cannot get interrupted. Just remember to free the mutex!
       */
      if (serusbcfg.usbp->state == USB_ACTIVE) {
#if (SERIAL_LED_INDICATORS)
        palToggleLine(LINE_LED3);
#endif
        chprintf((BaseSequentialStream*)&SDU2, "%s", message_buffer);
      }
      /*
       * These are debug messages. We want to acquire the newest ones at
       * all times. Unlock the mutex again, independently of the success
       * of the sending process.
       */
      chBSemSignal(&serial_bin_sem);
    }
  }
}

#endif /* USE_USB_SERIAL */

/*===========================================================================*/
/* Task exported functions.                                                  */
/*===========================================================================*/
int asmoSerialPutParametrized(const char *fmt, va_list ap) {
  int ret_val = 0;

#if (USE_USB_SERIAL)
  /* This one gets freed in the serial thread! */
  if (chBSemWaitTimeout(&serial_bin_sem, TIME_IMMEDIATE) != MSG_OK) {
    return -1;
  }

  ret_val = chvsnprintf((char *) &message_buffer, DEBUG_MSG_MAX_LEN, fmt, ap);
  /* Signal the USB thread */
  chEvtSignal(serial_thd, SERIAL_SEND_EVENT);
#endif /* USE_USB_SERIAL */

  return ret_val;
}

int asmoSerialPut(const char *fmt, ...) {
  int ret_val = 0;

#if (USE_USB_SERIAL)
  va_list ap;
  /* Extract the parameters and parse all that into the shared buffer variable. */
  va_start(ap, fmt);
  ret_val = asmoSerialPutParametrized(fmt, ap);
  va_end(ap);
#endif /* USE_USB_SERIAL */

  /* Return number of bytes that would have been written.*/
  return ret_val;
}

void asmoSerialInit(void) {
#if (USE_USB_SERIAL)
  chBSemObjectInit(&serial_bin_sem, true);

  serial_thd = chThdCreateStatic(&serial_usb_thd_wa, sizeof(serial_usb_thd_wa),
                                 HIGHPRIO - 15, &serial_usb_thd, NULL);

  sduObjectInit(&SDU2);
  sduStart(&SDU2, &serusbcfg);

  usbDisconnectBus(serusbcfg.usbp);
  chThdSleepMilliseconds(1500);
  usbStart(serusbcfg.usbp, &usbcfg);
  usbConnectBus(serusbcfg.usbp);

  /* In case we get interrupted by something we lock sending until this point. */
  chBSemSignal(&serial_bin_sem);
#endif /* USE_USB_SERIAL */
}
