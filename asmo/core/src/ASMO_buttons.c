/**
 * \file ASMO_buttons.c
 *
 * \brief Source file to ASMO_buttons.h
 * \author Moritz Huellmann
 * \date Nov 2020
 */

#include "SmartECLA_allHeaders.h"

#if USE_BUTTONS

#define DEFAULT_TIMEOUT ((sysinterval_t)10)

EVENTSOURCE_DECL(evtSrcButtons);

ASMO_ButtonCfg buttonCfg1 = {.buttonLine = LINE_EXTERNAL_BUTTON1, .timeout = DEFAULT_TIMEOUT};
ASMO_ButtonCfg buttonCfg2 = {.buttonLine = LINE_EXTERNAL_BUTTON2, .timeout = DEFAULT_TIMEOUT};
ASMO_ButtonCfg buttonCfg3 = {.buttonLine = LINE_EXTERNAL_BUTTON3, .timeout = DEFAULT_TIMEOUT};
ASMO_ButtonCfg buttonCfg4 = {.buttonLine = LINE_EXTERNAL_BUTTON4, .timeout = DEFAULT_TIMEOUT};


uint8_t mask;
uint32_t antiBounceTimer;

/**
 * Check whether a button is pressed or not.
 */
uint8_t isPressed(ASMO_ButtonCfg *cfg) {
  return palWaitLineTimeout(cfg->buttonLine, cfg->timeout) == MSG_OK ? 1 : 0;
}

/**
 * Mask of pressed buttons.
 * Returns: 0b0000DCBA, with Bit A: Button 1, ..., Bit D: Button 4
 */
uint8_t pressedMask(void) {
  return ((isPressed(&buttonCfg1) << 0) | (isPressed(&buttonCfg2) << 1) | (isPressed(&buttonCfg3) << 2) | (isPressed(&buttonCfg4) << 3));
}

static THD_WORKING_AREA(WA_BUTTON_LISTENER_THREAD, 128);
static THD_FUNCTION(buttonListenerThread, p) {
  (void)p;
  chRegSetThreadName("ButtonListenerThread");

  mask = 0;
  antiBounceTimer = 0;

  while (true) {
    mask = pressedMask();
    if (mask) {
      // (MH) chVTGetSystemTime() kann ueberlaufen!
      if (antiBounceTimer <= chVTGetSystemTime()) {
        chEvtBroadcastFlags(&evtSrcButtons, (eventflags_t)mask);
        antiBounceTimer = chVTGetSystemTime() + TIME_MS2I(100);
      }
    }
  }
}

/**
 * Initializes Buttons. To use buttons, implement
 *
 *      chEvtRegister(&evtSrcButtons, &evtLstButtons, BUTTONS_PRESSED_MASK);
 *
 * You also have to implement evtLstButtons yourself.
 * To listen to button presses:
 *
 *      chEvtWaitOne(BUTTONS_PRESSED_MASK);
 *
 * Example (Listening to buttons 2 and 4)
 *
 *      eventflags_t flags = chEvtGetAndClearFlags(&evtLstButtons);
 *      if (flags & (BUTTON_2_PRESSED_FLAG | BUTTON_4_PRESSED_FLAG)) { ... }
 *
 */
void ASMO_ButtonsInit(void) {
  // Input was set in board.h

  //Enable external interrupts on rising edge at button pins
  palEnableLineEvent(LINE_EXTERNAL_BUTTON1, PAL_EVENT_MODE_RISING_EDGE);
  palEnableLineEvent(LINE_EXTERNAL_BUTTON2, PAL_EVENT_MODE_RISING_EDGE);
  palEnableLineEvent(LINE_EXTERNAL_BUTTON3, PAL_EVENT_MODE_RISING_EDGE);
  palEnableLineEvent(LINE_EXTERNAL_BUTTON4, PAL_EVENT_MODE_RISING_EDGE);

  (void)chThdCreateStatic(WA_BUTTON_LISTENER_THREAD,
                          sizeof(WA_BUTTON_LISTENER_THREAD), NORMALPRIO + 20,
                          buttonListenerThread, NULL);

}

#endif // USE_BUTTONS
