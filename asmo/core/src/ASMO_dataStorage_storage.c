/*
 * ASMO_dataStorage_storage.c
 *
 *  Created on: Nov 3, 2012
 *      Author: Christoph Becker
 */

#include "ASMO_dataStorage_storage.h"

/**
 * @brief		helper to store incoming measured value; newValue and storage have identical Message-ID
 * @details		Not yet implemented
 *
 * @param[in]	newValue is latest incoming measured value
 * 				storage points to time series
 */
void ds_storeTS(ASMO_Measurement newValue, ds_TSReg* storage) {
	// place head on next place to write
	if ((unsigned int) (storage->head) - (unsigned int) storage->storage == sizeof(ASMO_Measurement) * (storage
			->windowSize - 1)) {
		//head points to last place in array -> place to write is start
		storage->head = storage->storage;
	}
	else {
		++storage->head;
	}
	* (storage->head) = newValue;

	if ((unsigned int) (storage->head) - (unsigned int) storage->storage == sizeof(ASMO_Measurement) * (storage
			->windowSize - 1)) {
		storage->filledonce = 1;
	}

	if ((storage->use_avg) || (storage->use_min) || (storage->use_max)) {
		unsigned int num = storage->windowSize;
		unsigned long avg = 0;
    ASMO_Measurement minNew = newValue;
    ASMO_Measurement maxNew = newValue;
		if (!storage->filledonce) {
			num = (storage->head - storage->storage) / sizeof(ASMO_Measurement);
		}
		for (unsigned int i = 0; i < num; ++i) {
      avg += storage->storage[i].value;
      if (storage->storage[i].value <= minNew.value) {
        minNew = storage->storage[i];
      }
      if (storage->storage[i].value >= maxNew.value) {
        maxNew = storage->storage[i];
      }
		}
		storage->avg.value = avg / num;
		storage->avg.timestamp = tsGetTS();
    storage->min = minNew;
    storage->max = maxNew;
	}

}

/**
 * @brief		add new value to timeseries
 *
 * @details		Not yet implemented
 *
 * @param[in]	configuration for time series
 */
uint8_t ds_newTS(ASMO_Measurement *m, ds_TSReg *reg) {
	if (reg->messageCounter == 0) {
		chSemWait(reg->sem);
		//Protected Code
		ds_storeTS(*m, reg);
		chSemSignal(reg->sem);
	}
	// raise counter
	reg->messageCounter = (reg->messageCounter + 1) % reg->distance;
	return 0;
}

/**
 * @brief		get timeseries
 *
 * @details		Returns the measurements of the given timeseries in array m.
 *
 * @param[in]	m	- array of measurements to store timeseries in
 * 				reg - configuration for time series
 */
uint8_t ds_getTS(ASMO_Measurement *m, ds_TSReg *reg) {
	if (reg->filledonce) {
		chSemWait(reg->sem);

		for (unsigned int i = 0; i < reg->windowSize; ++i) {
			m[i].value =
					reg->storage[ ( ( (reg->head) + i + 1) - reg->storage) % reg
							->windowSize].value;
			m[i].timestamp = reg->storage[ ( ( (reg->head) + i + 1) - reg
					->storage) % reg->windowSize].timestamp;
		}
		chSemSignal(reg->sem);
		return 0;
	}
	return 1;
}

/**
 * @brief		get timeseries aggregate
 *
 * @details		Returns the selected aggregate function
 *
 * @param[in]	a	- select aggregate
 * 				m	- measurements to store aggregate in
 * 				reg - configuration for time series
 */
uint8_t ds_getTSAggregate(ds_TSAggregate a, ASMO_Measurement *m,
							ds_TSReg *reg) {
	if (reg->filledonce) {
		uint8_t ret = 0;
		chSemWait(reg->sem);
		m->messageId = reg->messageID;
		switch (a) {
		case dsTSAggrAvg:
			if (reg->use_avg) {
				m->value = reg->avg.value;
				m->timestamp = reg->avg.timestamp;
			}
			else {
				ret = 1;
			}
			break;
		case dsTSAggrMin:
			if (reg->use_min) {
				m->value = reg->min.value;
				m->timestamp = reg->min.timestamp;
			}
			else {
				ret = 1;
			}
			break;
		case dsTSAggrMax:
			if (reg->use_max) {
				m->value = reg->max.value;
				m->timestamp = reg->max.timestamp;
			}
			else {
				ret = 1;
			}
			break;
		default:
			ret = 1;
		}

		chSemSignal(reg->sem);
		return ret;
	}
	return 1;
}
