/*
 * \file    sevenSeg.c
 * \brief   Driver for the seven segment led display on the ASMO board
 * \author  Moritz Huellmann
 * \date    Oct 2020
 */

#include "SmartECLA_allHeaders.h"

#if (USE_SEVEN_SEG == TRUE)

const SPIConfig sevenSegSpiConfig = {
  FALSE,
  NULL,
  SEVENSEG_CS_PORT,
  SEVENSEG_CS_PIN,
  SPI_CR1_BR,           // Slows down baud rate
  SPI_CR2_DS            // 16 Bit per transmission
};

void sevenSeg_init(void) {
  spiStart(&SPID2, &sevenSegSpiConfig);

  // Test Display
  sevenSeg_testMode(SEVENSEG_TEST_ENABLE);
  chThdSleepMilliseconds(300);
  sevenSeg_testMode(SEVENSEG_TEST_DISABLE);

  // Configure Display
  sevenSeg_transmit(SEVENSEG_ADDR_DECODEMODE, SEVENSEG_ENABLEDECODING);
  sevenSeg_setIntensity(SEVENSEG_INTENSITY_10);
  sevenSeg_setNumberOfDigits(SEVENSEG_DIGIT_COUNT_4);

  // Start Display and blank it
  sevenSeg_enableDisplay();
}

void sevenSeg_transmit(uint16_t address, uint8_t data) {
  switch (address) {
      case SEVENSEG_ADDR_DIGIT1:
      case SEVENSEG_ADDR_DIGIT2:
      case SEVENSEG_ADDR_DIGIT3:
      case SEVENSEG_ADDR_DIGIT4:
      case SEVENSEG_ADDR_DECODEMODE:
      case SEVENSEG_ADDR_INTENSITY:
      case SEVENSEG_ADDR_SCANLIMIT:
      case SEVENSEG_ADDR_MODE:
      case SEVENSEG_ADDR_TESTMODE:
        spiAcquireBus(&SPID2);
        spiStart(&SPID2, &sevenSegSpiConfig);
        spiSelect(&SPID2);
        uint16_t transmitBuffer = address | data;
        spiSend(&SPID2, 1, &transmitBuffer);
        spiUnselect(&SPID2);
        spiReleaseBus(&SPID2);
        break;
      default:
        return;
  }
}

void sevenSeg_setDigit(SEVENSEG_DIGIT_t digit, uint8_t symbol) {
  switch (digit) {
  case SEVENSEG_DIGIT_1:
    sevenSeg_transmit(SEVENSEG_ADDR_DIGIT1, symbol);
    break;
  case SEVENSEG_DIGIT_2:
    sevenSeg_transmit(SEVENSEG_ADDR_DIGIT2, symbol);
    break;
  case SEVENSEG_DIGIT_3:
    sevenSeg_transmit(SEVENSEG_ADDR_DIGIT3, symbol);
    break;
  case SEVENSEG_DIGIT_4:
    sevenSeg_transmit(SEVENSEG_ADDR_DIGIT4, symbol);
    break;
  default:
    return;
  }
}

void sevenSeg_writeInt(int32_t number) {
  sevenSeg_blankAll();
  int digit0, digit1, digit2;
      if (number > 9999 || number < -999) {
          sevenSeg_blankAll();
          sevenSeg_setDigit(SEVENSEG_DIGIT_2, SEVENSEG_SYMBOLS_E);
          return;
      }

      // Digit 0 (may be minus sign)
      if (number < 0) {
          digit0 = SEVENSEG_SYMBOLS_MINUS;
          number *= -1;
      }
      else if (number >= 1000) {
          digit0 = number / 1000;
          number = number - digit0 * 1000;
      }
      else {
          digit0 = SEVENSEG_SYMBOLS_BLANK;
      }

      // Digit 1
      if (number >= 100 || (digit0 != SEVENSEG_SYMBOLS_BLANK && digit0 != SEVENSEG_SYMBOLS_MINUS)) {
          digit1 = number / 100;
          number -= digit1 * 100;
      }
      else {
          digit1 = SEVENSEG_SYMBOLS_BLANK;
      }

      // Digit 2
      if (number >= 10 || digit1 != SEVENSEG_SYMBOLS_BLANK) {
          digit2 = number / 10;
          number -= digit2 * 10;
      }
      else {
          digit2 = SEVENSEG_SYMBOLS_BLANK;
      }

      // Digit 3
      sevenSeg_setDigit(SEVENSEG_DIGIT_1, digit0);
      sevenSeg_setDigit(SEVENSEG_DIGIT_2, digit1);
      sevenSeg_setDigit(SEVENSEG_DIGIT_3, digit2);
      sevenSeg_setDigit(SEVENSEG_DIGIT_4, number); // the rest
}

void sevenSeg_setNumberOfDigits(SEVENSEG_DIGIT_COUNT_t count) {
  if (((uint16_t) count) > 4) {
    return;
  }
  switch (count) {
  case SEVENSEG_DIGIT_COUNT_0:
    sevenSeg_disableDisplay();
    break;
  default:
    sevenSeg_transmit(SEVENSEG_ADDR_SCANLIMIT, count);
    break;
  }
}

void sevenSeg_setIntensity(SEVENSEG_INTENSITY_t intensity) {
  sevenSeg_transmit(SEVENSEG_ADDR_INTENSITY, intensity);
}

void sevenSeg_enableDisplay(void) {
  sevenSeg_transmit(SEVENSEG_ADDR_MODE, SEVENSEG_MODE_NORMAL);
}

void sevenSeg_disableDisplay(void) {
  sevenSeg_transmit(SEVENSEG_ADDR_MODE, SEVENSEG_MODE_SHUTDOWN);
}

void sevenSeg_testMode(SEVENSEG_TEST_t enable) {
  switch (enable) {
    case SEVENSEG_TEST_ENABLE:
      sevenSeg_transmit(SEVENSEG_ADDR_TESTMODE, SEVENSEG_TEST_ENABLE);
      break;
    case SEVENSEG_TEST_DISABLE:
      sevenSeg_transmit(SEVENSEG_ADDR_TESTMODE, SEVENSEG_TEST_DISABLE);
      break;
    default:
      return;
  }
}

void sevenSeg_blankAll(void) {
  sevenSeg_setDigit(SEVENSEG_DIGIT_1, SEVENSEG_SYMBOLS_BLANK);
  sevenSeg_setDigit(SEVENSEG_DIGIT_2, SEVENSEG_SYMBOLS_BLANK);
  sevenSeg_setDigit(SEVENSEG_DIGIT_3, SEVENSEG_SYMBOLS_BLANK);
  sevenSeg_setDigit(SEVENSEG_DIGIT_4, SEVENSEG_SYMBOLS_BLANK);
}

#endif /* USE_SEVEN_SEG == TRUE */

