/*
 * \file ASMO_simulinkWrappers.c
 * \brief Provides some Wrapper functions that are used by user defined
 * Simulink S-Functions.
 *
 *  PUT ALL CALLS OR REFERENCES TO EXTERNAL FUNCTIONS, FILES, VARS INTO:
 *  #ifndef MATLAB_MEX_FILE
 *     ...
 *  #endif
 *  ELSE THE MEX COMPILER WILL FIND ERRORS
 *
 *  When you want to create an S-Function using any of the wrapped functions,
 *  copy this file and the header to your MATLAB working folder and use legacy_tool.
 *  (Chapter 4.7 in Chate Jongdee's Master Thesis)
 *
 *  Created on: 23.08.2010
 *      Author: Florian Goebe
 */

#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

void simulink_newMeasurement(unsigned long id, unsigned long data) {
#ifndef MATLAB_MEX_FILE
  timestamp_t timestamp = tsGetTS();
  ASMO_Measurement meas = {.messageId = id, .value = data, .timestamp = timestamp};
  ASMO_newMeasurement(meas);
#endif
}

void simulink_getMeasurementWrapper(unsigned long id, double *data,
                                    timestamp_t *timestamp) {
#ifndef MATLAB_MEX_FILE
  timestamp_t ts = TS_ZERO_VALUE;
  if (!simulink_getMeasurement(id, data, &ts)) {
    *data = 0;
  }
  *timestamp = ts;
#endif
}

void simulink_getTSMeasurementWrapper(ds_TSReg *reg, unsigned int *values,
                                      timestamp_t *timestamps,
                                      unsigned int *min, unsigned int *max,
                                      unsigned int *avg) {
#ifndef MATLAB_MEX_FILE
  if (reg->filledonce) {
    chSemWait(reg->sem);

    for (unsigned int i = 0; i < reg->windowSize; ++i) {
      values[i] = reg->storage[(((reg->head) + i + 1) - reg->storage)
          % reg->windowSize].value;
      timestamps[i] = reg->storage[(((reg->head) + i + 1) - reg->storage)
          % reg->windowSize].timestamp;
    }
    *min = reg->min.value;
    *max = reg->max.value;
    *avg = reg->avg.value;
    chSemSignal(reg->sem);
  }
#endif
}

void simulink_safetySet(unsigned long id, ASMO_safetyCommand cmd,
                        unsigned long data) {
#ifndef MATLAB_MEX_FILE
  // send command (set min, set max, etc.)
  timestamp_t timestamp = tsGetTS();
  ASMO_Measurement meas = {.messageId = ID_SAFETY_SET, .value = cmd,
                           .timestamp = timestamp};
  MessageCommand ccmd = {.messageId = ID_SAFETY_SET, .command = cmd};
  sendLocalMeasurement(&meas);
  ASMO_safetyDispatch(&ccmd);
  // send ID
  timestamp = tsGetTS();
  meas.value = id;
  meas.timestamp = timestamp;
  sendLocalMeasurement(&meas);
  ccmd.command = id;
  ASMO_safetyDispatch(&ccmd);
  // send value
  timestamp = tsGetTS();
  meas.value = data;
  meas.timestamp = timestamp;
  sendLocalMeasurement(&meas);
  ccmd.command = data;
  ASMO_safetyDispatch(&ccmd);
#endif
}
