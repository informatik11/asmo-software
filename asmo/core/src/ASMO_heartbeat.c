/**
 * @file    ASMO_heartbeat.c
 * @brief   Periodically sends an ALIVE-message, used in ETH communication mode..
 * @author  Benedikt Conze
 * @date    08.07.22
 *
 * @addtogroup heartbeat
 * @{
 */

#include "SmartECLA_allHeaders.h"

#if (ASMO_SEND_HEARTBEAT == TRUE)

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

THD_WORKING_AREA(heartbeat_thread_wa, 128);

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

// 32 bits access is defacto atomic
uint32_t status;
thread_t *heartbeat_thd;

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/**
 * @brief   Sends a heartbeat message with a configurable, constant interval
 */
static THD_FUNCTION(heartbeat_thread, init_status) {
  systime_t prev;
  ASMO_Measurement alive_msg;

  chRegSetThreadName("heartbeat");
  status = (uint32_t)init_status;

  alive_msg.messageId =
      BLOCK_ID_NETWORK_NOTIFY |
      DEVICE_ID |
      (DEVICE_NUMBER << DEVICE_NUMBER_SHIFTER) |
      0x030; /* ID of alive message */

  while(!chThdShouldTerminateX())
  {
    // send a heartbeat with the given interval, taking the time it takes to send into account
    // but don't catch up on missed heartbeats
    prev = chVTGetSystemTime();

    alive_msg.value = status;
    alive_msg.timestamp = tsGetTS();
    sendLocalMeasurementToETH(&alive_msg);

    chThdSleepUntilWindowed(prev, chTimeAddX(prev, TIME_MS2I(ASMO_HEARTBEAT_INTERVAL_MS)));
  }
}

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Starts the heartbeat thread if not currently running.
 */
void asmoStartHeartbeat(uint32_t initial_status)
{
  if(!heartbeat_thd)
  {
    heartbeat_thd = chThdCreateStatic(heartbeat_thread_wa, sizeof(heartbeat_thread_wa),
                                      NORMALPRIO + 2, heartbeat_thread, (void*)initial_status);
  }
}

/**
 * @brief   Stops the heartbeat thread after the current sleep, if it is running.
 */
void asmoStopHeartbeat(void)
{
  if(heartbeat_thd)
  {
    chThdTerminate(heartbeat_thd);
    heartbeat_thd = NULL;
  }
}

void asmoSetHeartbeatStatus(uint32_t s)
{
  status = s;
}

uint32_t asmoGetHeartbeatStatus(void)
{
  return status;
}

#endif
