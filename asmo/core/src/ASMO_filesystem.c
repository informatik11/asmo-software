/**
 *  \file ASMO_filesystem.c
 *  \brief ASMO Filesystem
 *
 *  This file provides functions which adapt ChaN's Fat File System (found in ASMO_ff.c)
 *  to the needs of smartECLA. Use initFileSystem to initialize everything needed (SPI, disk, ff)
 *  and create the results file.
 *  Configure the behavior of this module in ASMO_filesystem.h
 *
 *  The structure of the MMC (multi-media card) module in this project is as follows:
 *  1. ASMO_filesystem (this file) - Top layer. Triggers initialization of everything
 *      and takes measurement values to persist on the card. It also disables the card
 *      if there are not enough free sectors left in the filesystem.
 *  2. ASMO_ff.c - File system layer. An open source implementation of a FAT filesystem by ChaN.
 *      It manages the complete file system.
 *
 *  \author		Andre Stollenwerk, Florian Goebe
 *  \author		Lehrstuhl fuer Informatik 11 -  RWTH Aachen
 *  \date		2008-2010
 */

#include "SmartECLA_allHeaders.h"

/*===========================================================================*/
/* FatFs related.                                                            */
/*===========================================================================*/

#if defined(BOARD_STM32_ASMO) /* If we use the assembled ASMO board */

#define MAX_SPI_BITRATE    100
#define MIN_SPI_BITRATE    250

/**
 * @brief FS object.
 */
FATFS MMC_FS;

/**
 * MMC driver instance.
 */
MMCDriver MMCD1;

/*===========================================================================*/
/* Ffilesystem related.                                                      */
/*===========================================================================*/

/* Working Area for mmc storage thread */
static THD_WORKING_AREA(fs_waStorageThread, 1536);
//static WORKING_AREA(fs_waRemoveCardThread, 64);

FATFS FS_fatFS;
UINT bytesWritten;

/* FS mounted and ready.*/
static bool cardEnabled = false;

// TODO(MH): TESTEN
SPIConfig ls_spicfg = {
                       FALSE,
                       NULL,
                       GPIOB,
                       GPIOB_SD_CS_MMC,
                       SPI_CR1_BR,
                       0
};
MMCDriver FS_mmc;
MMCConfig FS_mmcConfig; // empty but needed.
volatile unsigned int _debug_measurement;
volatile unsigned int _debug_buffout;

/* MMC/SD over SPI driver configuration.*/
static MMCConfig mmccfg = {&SPID1, &ls_spicfg, &ls_spicfg};

/**
 * @brief Ringbuffer that holds measurements until they are written to the disc by
 * another thread.
 *
 * The Writer may write while fsBufIn != ((fsBufOut - 1 + FS_WRITEBUFFERSIZE) % FS_WRITEBUFFERSIZE)
 * i.e. while the in buffer has not over-rounded the out buffer.
 *
 * The reader may read while fsBufOut != fsBufIn,
 * i.e. while the out buffer has reached the in buffer.
 */
ASMO_Measurement fsWriteBuffer[FS_WRITEBUFFERSIZE];
char* timestampString;
bool fsWriteTimestamp = false;

unsigned int FS_bufIn, FS_bufOut; // indices, not addresses (easier to ring-count)

//! Mutex that controls the access on the filesystem.
MUTEX_DECL (FS_Mutex);

/**
 * @brief This function returns false, if the inserted MMC is write-protected and false otherwise.
 */
bool FS_mmcIsWriteProtected(void) {
  return palReadPad(FS_WP_PORT, FS_WP_PIN); // high (open switch) when card is WP
}

unsigned char FS_debug;
/**
 * @brief This function returns true, if an MMC is inserted
 */
bool FS_mmcIsInserted(void) {
  return (FS_debug = !palReadPad(FS_INS_PORT, FS_INS_PIN)); // low when card is inserted
}

/**
 * Thread function. It is run in a separated thread by the FS_init function.
 * First, it registers for card insertion and card removal events.
 * Afterwards it starts the mmc card module and waits for such events. When a card
 * is inserted, it connects to the card and starts writing the content of the
 * write (ring) buffer to the SD card.
 */
THD_FUNCTION(FS_thdStorageThread, p) {
  (void)p;
  chRegSetThreadName("FS_thdStorage");
  FRESULT err;
  uint32_t clusters;
  FATFS *fsp;
  FIL file;

  /*
   * Initializes the MMC driver to work with SPI.
   */
  mmcObjectInit(&MMCD1);
  mmcStart(&MMCD1, &mmccfg);

  // local variable declarations
  // XXX(mh) since the LCD is not implemented (yet), those are not needed. However they will not be deleted
  // in case the LCD driver is eventually being developed.
  // const char ASMO_mmc_activatedString[] = "SD-Card: on "; // last space to overwrite "off/err"
  // const char ASMO_mmc_errorString[] = "SD-Card: err";

  while (TRUE) { // one iteration of this (outer) loop is for one card insertion
    chThdSleepMilliseconds(500);

    /*
     * On insertion MMC initialization and FS mount.
     */
    if (mmcConnect(&MMCD1) == HAL_FAILED) {
      continue;
    }

    err = f_mount(&MMC_FS, "", 0);
    if (err != FR_OK) {
      //putAlarmLCD(ASMO_mmc_errorString);
      mmcDisconnect(&MMCD1);
      continue;
    }

    err = f_getfree("/", &clusters, &fsp);
    if (err != FR_OK) {
      //putAlarmLCD(ASMO_mmc_errorString);
      mmcDisconnect(&MMCD1);
      continue;
    }
    cardEnabled = true;
    //putAlarmLCD(ASMO_mmc_activatedString);

    err = f_open(&file, FS_FILENAME, FA_OPEN_ALWAYS | FA_WRITE | FA_READ);
    FS_disableOnFail(err, &file);

    err = f_sync(&file);
    FS_disableOnFail(err, &file);

    err = f_lseek(&file, file.fptr);
    FS_disableOnFail(err, &file);

    // write header
    err = FS_putLogHeaderToFile(&file);
    FS_disableOnFail(err, &file);

    err = f_sync(&file); // flush
    FS_disableOnFail(err, &file);


    unsigned long lastSync = 0; // System time of last sync (in Sys Ticks)
    while (cardEnabled) { // One iteration of this (inner) loop is one writing cycle, flushing the ring buffer into the FS.
      // All functions, which access the FS, within this loop lock the FS_Mutex first.

      // if there is new data in the buffer, check card first:
      if (!(FS_bufOut == FS_bufIn)) {
        FS_disableIfFull(&file);

        // write whole measurement buffer to card
        while (FS_bufOut != FS_bufIn) { // as long as there is data in the buffer
          _debug_measurement = (& (fsWriteBuffer[FS_bufOut]))->messageId;
          FS_writeFATASCII(&file, & (fsWriteBuffer[FS_bufOut]));
          FS_bufOut++;
          _debug_buffout = FS_bufOut;
          FS_bufOut %= FS_WRITEBUFFERSIZE;
        }

        // sync if more than FS_SYNC_EVERY_MS are passed since last sync.
        if (chVTGetSystemTime() - lastSync > TIME_MS2I(FS_SYNC_EVERY_MS)) {
          FS_sync(&file);
          lastSync = chVTGetSystemTime();
        }
      }
      if (fsWriteTimestamp) {
        FS_writeTimestampToFile(&file);
        fsWriteTimestamp = false;
      }
      chThdSleepSeconds(2);
      // Wait a little bit (non-busy). If the card is removed while waiting,
      // disable the storing and wait for new card.
      if (FS_mmcIsInserted()) {
        /* mmc still inserted go on with inner while (after 2 sec) */
      }
      else {
        FS_disableLogging(&file);
      }
    } // inner while
  } // outer while
}

/**
 * @brief This function writes a standard log header to the file associated
 * with the delivered file handler object.
 *
 * @param	file	File handler of target file.
 * @return			Status code.
 * @retval	TRUE	Something went wrong (1)
 * @retval	FALSE	SUCCESS (0)
 */
bool FS_putLogHeaderToFile(FIL *file) {
  bool fail = FALSE;
  unsigned char separator[] = "--------------------------------";
  unsigned char marker[] = "here starts a new measurement !! ";
  unsigned char header[] = "Message-ID;value;Time;TimeOffset";
  unsigned char newLine[2] = "  ";
  newLine[0] = CARRIAGE_RETURN;
  newLine[1] = LINE_FEED;

  fail |= f_write(file, newLine, sizeof (newLine), &bytesWritten);
  fail |= f_write(file, separator, sizeof (separator) - 1, &bytesWritten);
  fail |= f_write(file, newLine, sizeof (newLine), &bytesWritten);
  fail |= f_write(file, marker, sizeof (marker) - 1, &bytesWritten);
  fail |= f_write(file, newLine, sizeof (newLine), &bytesWritten);
  fail |= f_write(file, separator, sizeof (separator) - 1, &bytesWritten);
  fail |= f_write(file, newLine, sizeof (newLine), &bytesWritten);
  fail |= f_write(file, header, sizeof (header) - 1, &bytesWritten);
  fail |= f_write(file, newLine, sizeof (newLine), &bytesWritten);
  return fail;
}

/**
 *  \fn writeTimestamp(unsigned char hour, unsigned char minute, unsigned char second)
 *  \brief writes data in timestamp format to "results.txt" file on sd card
 *  \param file file to write to.
 *  \param hour hour of timestamp
 *  \param minute minute of timestamp
 *  \param second second of timestamp
 *  \return			Status code.
 *  \retval	TRUE	Something went wrong (1)
 *  \retval	FALSE	SUCCESS (0)
 */
void FS_writeTimestamp(unsigned char hour, unsigned char minute,
                       unsigned char second) {
  timestampString[21] = hex2ascii(hour / 10);
  timestampString[22] = hex2ascii(hour % 10);
  timestampString[24] = hex2ascii(minute / 10);
  timestampString[25] = hex2ascii(minute % 10);
  timestampString[27] = hex2ascii(second / 10);
  timestampString[28] = hex2ascii(second % 10);
  timestampString[29] = CARRIAGE_RETURN;
  timestampString[30] = LINE_FEED;
  fsWriteTimestamp = true;
}

/**
 *  \fn writeTimestamp(unsigned char hour, unsigned char minute, unsigned char second)
 *  \brief writes data in timestamp format to "results.txt" file on sd card
 *  \param file file to write to.
 *  \param hour hour of timestamp
 *  \param minute minute of timestamp
 *  \param second second of timestamp
 *  \return			Status code.
 *  \retval	TRUE	Something went wrong (1)
 *  \retval	FALSE	SUCCESS (0)
 */
bool FS_writeTimestampToFile(FIL *file) {

  bool fail;

  fail = f_write(file, timestampString, sizeof (timestampString),
                 &bytesWritten);

  return fail != FR_OK; // false on success
}

/** \fn writeFATASCII(unsigned int id, unsigned int value, unsigned int time, unsigned int timeOffset)
 *  \brief writes data in ASCII format to "results.txt" file on sd card.
 *  This function is thread-safe (protected by mutex).
 *  \param file			file to write to.
 *  \param measurement	measurement object, which shall be logged.
 *  \return			Status code.
 *  \retval	TRUE	Something went wrong (1)
 *  \retval	FALSE	SUCCESS (0)
 */
bool FS_writeFATASCII(FIL *file, ASMO_Measurement *measurement) {
  unsigned char buffer[40];
  unsigned char bufferCounter = 0;
  unsigned char charBuffer = 0;
  unsigned char counter = 0;
  unsigned char virgin = 1;
  uint64_t div = 0;
  unsigned int id = measurement->messageId;
  int16_t value = measurement->value;
  uint64_t time = tsToEpoch(&(measurement->timestamp));
  unsigned int timeOffset = 0; //Flo: This was a parameter before but was always zero. What is it for?
  bool res;

  if (!cardEnabled)
    return TRUE;

  chMtxLock(&FS_Mutex);

  for (counter = 8; counter > 0; counter--) {
    charBuffer = hex2ascii( (id >> 28) & 0x0F); // used because of endianess conflict
    if (charBuffer != '0' || !virgin || counter == 1) {
      virgin = 0;
      buffer[bufferCounter++ ] = charBuffer;
    }
    id = id << 4; // used because of endianess conflict
  }
  buffer[bufferCounter++ ] = CSV_SEPARATOR;
  virgin = 1;
  div = 1000000000;
  if (value < 0) {
    buffer[bufferCounter++ ] = '-';
    value = -value;
  }
  for (counter = 10; counter > 0; counter--) {
    charBuffer = hex2ascii( (value / div) % 10);
    if (charBuffer != '0' || !virgin || counter == 1) {
      virgin = 0;
      buffer[bufferCounter++ ] = charBuffer;
    }
    div /= 10;
  }

  buffer[bufferCounter++ ] = CSV_SEPARATOR;
  virgin = 1;
  div = 1000000000000;
  for (counter = 13; counter > 0; counter--) {
    charBuffer = hex2ascii( (time / div) % 10);
    if (charBuffer != '0' || !virgin || counter == 1) {
      virgin = 0;
      buffer[bufferCounter++ ] = charBuffer;
    }
    div /= 10;
  }

  buffer[bufferCounter++ ] = CSV_SEPARATOR;
  virgin = 1;
  div = 1000000000;

  for (counter = 10; counter > 0; counter--) {
    charBuffer = hex2ascii( (timeOffset / div) % 10);
    if (charBuffer != '0' || !virgin || counter == 1) {
      virgin = 0;
      buffer[bufferCounter++ ] = charBuffer;
    }
    div /= 10;
  }

  buffer[bufferCounter++ ] = CARRIAGE_RETURN;
  buffer[bufferCounter++ ] = LINE_FEED;

  res = f_write(file, buffer, bufferCounter, &bytesWritten) != FR_OK; // false = success

  chMtxUnlock(&FS_Mutex);

  return res;
}

/**
 * Checks if there is enough free space left on the card
 * and disables it, if not (using ::disableFileSystem).
 * This function is thread-safe (protected by mutex).
 */
char FS_disableIfFull(FIL *file) {
  DWORD freeOnCard = 0;
  FRESULT fres;

  FATFS *temp; // = &FS_fatFS;

  if (!cardEnabled)
    return false;

  chMtxLock(&FS_Mutex);
  fres = f_getfree("/", &freeOnCard, &temp);
  chMtxUnlock(&FS_Mutex);

  if (fres == FR_DISK_ERR || freeOnCard < FS_FREECLUSTERS_LOWERBOUND) {
    FS_disableLogging(file);
  }

  return cardEnabled;
}

/**
 * Syncs the filesystem thread-safe (protected by mutex).
 * On an error it disables the card using ::disableFileSystem.
 * @param	file	File that shall be synchronized.
 */
void FS_sync(FIL *file) {
  FRESULT fres;

  chMtxLock(&FS_Mutex);
  fres = f_sync(file);
  chMtxUnlock(&FS_Mutex);

  if (fres == FR_DISK_ERR) {
    FS_disableLogging(file);
  }
}

/**
 * @brief Disables the card, gives notifications and EXITS THE CALLING THREAD.
 *
 * Disables the card writing and
 * triggers a notification message via CAN,
 * a notification on the display and an alarm beep.
 * When the card is disabled, any read/write request
 * through this file is ignored thus the card can be removed
 * safely when the CARD-Full message appears.
 * @param	file	Name of the used file. The function tries to close it.
 */
void FS_disableLogging(FIL *file) {
  // XXX(mh) since the LCD is not implemented (yet), this is not needed. However they will not be deleted
  // in case the LCD driver is eventually being developed.
  // const char alarmString[] = "SD-Card: off";
  (void) file;
  cardEnabled = FALSE;
  if (FS_ALARM_BEEP) {
    // not yet implemented
  }
  if (FS_ALARM_LCD) {
    //putAlarmLCD(alarmString);
  }
  if (FS_ALARM_CAN) {
    sendSimpleMessage(
        addDeviceNumber(DEVICE_NUMBER,
                        (MESSAGE_DISABLED_CARDS | DEVICE_ID),
                        DEVICE_NUMBER_SHIFTER),
                        0);
  }
  mmcDisconnect(&MMCD1);
  cardEnabled = false;
}

/**
 * Puts a measurement into the Card Buffer.
 * The Content of this Ringbuffer is written to the card
 * by another thread.
 */
void FS_putToBuffer(ASMO_Measurement *measurement) {
  if (FS_bufIn != (FS_bufOut - 1 + FS_WRITEBUFFERSIZE) % FS_WRITEBUFFERSIZE) { // if buf is not full
    fsWriteBuffer[FS_bufIn] = *measurement;
    FS_bufIn = (FS_bufIn + 1) % FS_WRITEBUFFERSIZE;
  }
}

void FS_init(void) {
  timestampString = "Timestamp on bootup: XX:XX:XX  ";
  mmccfg.spip = &SPID1;
  mmccfg.lscfg = &ls_spicfg;
  mmccfg.hscfg = &ls_spicfg;

  // init FS module variables
  FS_bufIn = 0;
  FS_bufOut = 0;

  // Create Thread that stores the measurements on the mmc card.
  chThdCreateStatic(fs_waStorageThread, sizeof(fs_waStorageThread),
                    NORMALPRIO, FS_thdStorageThread, NULL);
  chThdSleepMilliseconds(1000);
}

#else
/*
 * Just use empty functions, although they never are going to be used.
 * They return values indicating failure if they should be called.
 */

void FS_init(void) {}
void FS_writeTimestamp(unsigned char hour, unsigned char minute,
						unsigned char second)
{
  (void)hour; (void)minute; (void)second;
}
bool FS_writeTimestampToFile(FIL *file)
{
  (void)file;
  return true;
}
bool FS_writeFATASCII(FIL *file, ASMO_Measurement *measurement)
{
  (void)file; (void)measurement;
  return false;
}
char FS_disableIfFull(FIL *file)
{
  (void)file;
  return 0;
}
void FS_sync(FIL *file)
{
  (void)file;
}
void FS_disableLogging(FIL *file)
{
  (void)file;
}
void FS_putToBuffer(ASMO_Measurement *measurement)
{
  (void)measurement;
}
bool FS_mmcIsWriteProtected(void)
{
  return false;
}
bool FS_mmcIsInserted(void)
{
  return false;
}
bool FS_putLogHeaderToFile(FIL *file)
{
  (void)file;
  return true;
}

#endif /* defined(BOARD_STM32_ASMO) */

