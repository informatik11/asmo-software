/**
 * \file	SmartECLA_allHeaders.h
 * \brief	This file aggregates all C headers of the SmartECLA MCU software
 * 			in an appropriate order (considering all dependencies).
 *
 *			It shall be included in all header and source files right away after the
 *			guard define.
 *
 *			The SmartECLA header policy bases on the following include level definitions:
 *
 *			Level 0: Comments only
 *
 *			Level 1: Pure Defines, no #if, #ifdef, etc. (except guards)
 *			11 - MDL Defs		(MDL configures behaviour of other modules)
 *			12 - ASMO Defs		(ASMO configures behaviour of other modules)
 *
 *			Level 2: Defines that depend on (not stand for) foreign Level-1 defines and are relevant for other modules (just #define, #undefine, #if, #ifdef, etc. allowed).
 *			21 - MDL Conditional Defs
 *			22 - ASMO Conditional Defs
 *
 *			Level 3: Operating System (untouched or OS-conform headers)
 *			31 - Standard C libraries
 *			32 - Kernel
 *			33 - Hardware Abstraction (Drivers)
 *
 *			Level 4: Global Enums based on elementary types
 *			41 - SmartECLA IDs
 *
 *			Level 5: Declarations (just declarations and local defines, macros)
 *			51 - Headers of Non-OS-conform drivers, Non-ASMO wrappers (Level 51 shall be removed soon)
 *			52 - Model Headers (Set USE Defines)
 *			53 - ASMO Declarations, incl. wrappers (Read USE Defines)
 *			54 - MDL Declarations
 *
 *			Level 6: Included Definitions (static (inline) functions and variables)
 *
 *			Level 7: Sources (currently compiled .c file)
 *
 *			!! ALL Headers must have guards !!
 *
 * \date	Nov 2011
 * \author	Florian Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#pragma once // Include this file only once!
#ifndef SMARTECLA_ALLHEADERS_H_ // additional guard
#define SMARTECLA_ALLHEADERS_H_

//	Level 0: Comments only
#include "SmartECLA_level0.h"

//	Level 1: Pure Defines, no #if, #ifdef, etc. (except guards)
//	11 - MDL Defs		(MDL configures behaviour of other modules)
//	12 - ASMO Defs		(ASMO configures behaviour of other modules)
#include "SmartECLA_level1.h"

//	Level 2: Defines that depend on (not stand for) foreign Level-1 defines and are relevant for other modules (just #define, #undefine, #if, #ifdef, etc. allowed).
//	21 - MDL Conditional Defs
//	22 - ASMO Conditional Defs
#include "SmartECLA_level2.h"

//	Level 3: Operating System (untouched or OS-conform headers)
//	31 - Standard C libraries
//	32 - Kernel
//	33 - Hardware Abstraction (Drivers)
#include "SmartECLA_level3.h"

//	Level 4: Global Enums based on elementary types
//	41 - ASMO_typedefs
//	42 - SmartECLA IDs
//  43 - Datastorage
#include "SmartECLA_level4.h"

//	Level 5: Declarations (just declarations and local defines, macros)
//	51 - Headers of Non-OS-conform drivers, Non-ASMO wrappers (Level 51 shall be removed soon)
//	52 - Model Headers (Set USE Defines)
//	53 - ASMO Declarations, incl. wrappers (Read USE Defines)
//	54 - MDL Declarations
#include "SmartECLA_level5.h"
//	Level 6: Included Definitions (static (inline) functions and variables)
#include "SmartECLA_level6.h"

//	Level 7: Sources (currently compiled .c file)
/*
 * ###############################################################################
 * # Level 7 is not contained in this file since it just consists of the sources #
 * # that include this file.                                                     #
 *  ##############################################################################
 */

/*
 * Include-Strategie der AllHeaders Datei für Chibios:
 * in der mcuconf.h die Level 0 bis 2 includieren
 *
 * BSP Code:
 // set all headers define to not include Levels >2 here.
 #ifndef SMARTECLA_ALLHEADERS_H_
 #define SMARTECLA_ALLHEADERS_H_
 #endif
 // include levels <=2
 #include "SmartECLA_level0.h"
 #include "SmartECLA_level1.h"
 #include "SmartECLA_level2.h"
 *
 */

#endif /* SMARTECLA_ALLHEADERS_H_ */
