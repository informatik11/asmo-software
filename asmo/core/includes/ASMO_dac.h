/**
 * @file    hal_external_dac.h
 * @brief   AD via SPI macros and structures.
 * @note    Complex drivers are drivers, that heavily rely on other drivers.
 *          Following the guidelines of ChibiOS, those drivers don't need to
 *          implement LLD functions, since the LLD part is handled by the
 *          other used driver. Hence this driver does not implement a LLD.
 * @author  Moritz Huellmann
 * @date    Jan 2021
 */

#ifndef ASMO_DAC_H
#define ASMO_DAC_H

/* Default values if not defined.*/
#ifndef ASMO_USE_DAC
#ifdef BOARD_STM32_PUMP
/* Enable by default if pump board is in use.*/
#define ASMO_USE_DAC TRUE
#else
/* Disable by default for all other boards.*/
#define ASMO_USE_DAC FALSE
#endif /* BOARD_STM32_PUMP */
#endif /* ASMO_USE_DAC */

#if (ASMO_USE_DAC == TRUE)

#if (HAL_USE_SPI == FALSE)
#error "SPI needs to be enabled in order for the external DAC to work!"
#endif

#if (SPI_SELECT_MODE != SPI_SELECT_MODE_PAD)
#error "SPI select mode needs to be pad in order for the external DAC to work!"
#endif

/**
 * @name    External DAC configuration options
 * @{
 */
/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#if !defined(ASMO_DAC_USE_WAIT) || defined(__DOXYGEN__)
#define ASMO_DAC_USE_WAIT   TRUE
#endif

/**
 * @brief   Enables the @p dacAcquireBus() and @p dacReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#if !defined(ASMO_DAC_USE_MUTUAL_EXCLUSION) || defined(__DOXYGEN__)
#define ASMO_DAC_USE_MUTUAL_EXCLUSION    TRUE
#endif
/** @} */

#define AD5317_ADD_START_POS    (14)
#define AD5317_GAIN_POS         (13)
#define AD5317_BUFF_POS         (12)

/* a = 0, 1, 2, 3. where 0 = A, 1 = B, 2 = C, 3 = D */
#define AD5317_DAC_ADDR(a)        ((a) << AD5317_ADD_START_POS)

/* Switch DAC A and C outputs */
#define AD5317_SWITCH_A_D       (1)

/* LSB of data in frame */
#define AD5317_DATA_POS         (2)

#define AD5317_CH_CNT           (4)

/* b = 0,1 */
#define AD5317_GAIN(b)          ((b) << AD5317_GAIN_POS)
#define AD5317_BUFF(b)          ((b) << AD5317_BUFF_POS)

#define AD5317_ASMO_CONFIG      (AD5317_GAIN(0) | AD5317_BUFF(1))

/**
 * @brief   Type of a DAC channel index.
 */
typedef uint16_t dacextchannel_t;

/**
 * @brief   Type representing a DAC sample.
 */
typedef uint16_t dacextsample_t;

/**
 * @brief   Type representing a DAC output.
 */
typedef struct hal_dac_ext_output dacextoutput_t;

/**
 * @brief   Type of a structure representing an DAC configuration.
 */
typedef struct hal_dac_ext_config DACEConfig;

/**
 * @brief   Type of a structure representing an DAC driver.
 */
typedef struct hal_dac_ext_driver DACEDriver;

/**
 * @brief   Type of a DAC conversion group.
 */
typedef struct hal_dac_ext_conversion_group DACEConversionGroup;

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
  DAC_EXT_UNINIT = 0,                   /**< Not initialized.                   */
  DAC_EXT_STOP = 1,                     /**< Stopped.                           */
  DAC_EXT_READY = 2,                    /**< Ready.                             */
  DAC_EXT_ACTIVE = 3,                   /**< Exchanging data.                   */
  DAC_EXT_COMPLETE = 4,                 /**< Asynchronous operation complete.   */
  DAC_EXT_ERROR = 5                     /**< Error.                             */
} dacextstate_t;


/**
 * @brief   DAC notification callback type.
 *
 * @param[in] dacp      pointer to the @p DACDriver object triggering the
 */
typedef void (*dacextcallback_t)(DACEDriver *dacp);

/**
 * @brief   DAC error callback type.
 *
 * @param[in] dacp      pointer to the @p DACDriver object triggering the
 *                      callback
 * @param[in] err       DAC error code
 */
typedef void (*dacexterrorcallback_t)(DACEDriver *dacp, uint8_t err);

struct hal_dac_ext_output {
  /**
   * @brief The channel to output the following sample on.
   */
  dacextchannel_t   ch;
  /**
   * @brief The value to output
   */
  dacextsample_t    value;
};

struct hal_dac_ext_config {
  /**
   * @brief   SPI driver to use for accessing the peripheral
   */
  SPIDriver     *spi_driver;
  /**
   * @brief   Interval between two conversions
   */
  uint16_t      interval;
};

struct hal_dac_ext_conversion_group {
  /**
   * @brief   Activate circular sending.
   */
  bool                      circular;
  /**
   * @brief   Operation complete callback or @p NULL.
   */
  dacextcallback_t          end_cb;
  /**
   * @brief   Error handling callback or @p NULL.
   */
  dacexterrorcallback_t     error_cb;
};

/**
 * @brief   Structure representing a DAC driver.
 */
struct hal_dac_ext_driver {
  /**
   * @brief   Driver state.
   */
  dacextstate_t             state;
  /**
   * @brief   Conversion group.
   */
  const DACEConversionGroup *grpp;
  /**
   * @brief   Output buffer pointer.
   */
  dacextoutput_t            *output;
  /**
   * @brief   Output buffer size.
   */
  size_t                    depth;
  /**
   * @brief   Config to use.
   */
  const DACEConfig          *config;
#if (ASMO_DAC_USE_WAIT == TRUE) || defined(__DOXYGEN__)
  /**
   * @brief   Waiting thread.
   */
  thread_reference_t        thread;
#endif /* DAC_USE_WAIT */
#if (ASMO_DAC_USE_MUTUAL_EXCLUSION == TRUE) || defined(__DOXYGEN__)
  /**
   * @brief   Mutex protecting the bus.
   */
  mutex_t                   mutex;
#endif /* DAC_USE_MUTUAL_EXCLUSION */
  /* End of the mandatory fields.*/
};

#if (ASMO_DAC_USE_WAIT == TRUE) || defined(__DOXYGEN__)

/**
 * @brief   Resumes a thread waiting for a conversion completion.
 *
 * @param[in] dacp      pointer to the @p DACDriver object
 *
 * @notapi
 */
#define _dac_ext_reset(dacp) osalThreadResumeS(&(dacp)->thread, MSG_OK);


#else /* !DAC_USE_WAIT */
#define _dac_ext_reset(dacp)
#endif /* !DAC_USE_WAIT */

/**
 * @brief   Common ISR code, half buffer event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *          .
 * @note    This macro is meant to be used in the low level drivers
 *          implementation only.
 *
 * @param[in] dacp      pointer to the @p DACDriver object
 *
 * @notapi
 */
#define _dac_ext_half_code(dacp) {                                          \
  if ((dacp)->grpp->end_cb != NULL) {                                       \
    (dacp)->grpp->end_cb(dacp);                                             \
  }                                                                         \
}

/**
 * @brief   Common ISR code, full buffer event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *          - Driver state transitions.
 *          .
 * @note    This macro is meant to be used in the low level drivers
 *          implementation only.
 *
 * @param[in] dacp      pointer to the @p DACDriver object
 *
 * @notapi
 */
#define _dac_ext_full_code(dacp) {                                              \
  if ((dacp)->grpp->end_cb != NULL) {                                           \
    (dacp)->state = DAC_EXT_COMPLETE;                                           \
    (dacp)->grpp->end_cb(dacp);                                                 \
    if ((dacp)->state == DAC_EXT_COMPLETE) {                                    \
      (dacp)->state = DAC_EXT_ACTIVE;                                           \
    }                                                                           \
  }                                                                             \
  if (!(dacp)->grpp->circular) {                                                \
    (dacp)->state = DAC_EXT_READY;                                              \
    if ((dacp)->thread != NULL) {                                               \
      chSysLock();                                                              \
      _dac_ext_reset(dacp);                                                     \
      chSysUnlock();                                                            \
    }                                                                           \
  }                                                                             \
}

extern DACEDriver DACED;

#ifdef __cplusplus
extern "C" {
#endif
  void dacExtObjectInit(DACEDriver *dacp);
  void dacExtStart(DACEDriver *dacp, DACEConfig *config);
  void dacExtStop(DACEDriver *dacp);
  void dacExtPutChannelX(DACEDriver *dacp,
                      dacextchannel_t channel,
                      dacextsample_t sample);
  void dacExtStartConversion(DACEDriver *dacp, const DACEConversionGroup *grpp,
                          dacextoutput_t *output, size_t depth);
  void dacExtStartConversionI(DACEDriver *dacp, const DACEConversionGroup *grpp,
                           dacextoutput_t *output, size_t depth);
  void dacExtStopConversion(DACEDriver *dacp);
  void dacExtStopConversionI(DACEDriver *dacp);
#if ASMO_DAC_USE_WAIT
  msg_t dacExtConvert(DACEDriver *dacp, const DACEConversionGroup *grpp,
                   dacextoutput_t *output, size_t depth);
#endif
#if ASMO_DAC_USE_MUTUAL_EXCLUSION
  void dacExtAcquireBus(DACEDriver *dacp);
  void dacExtReleaseBus(DACEDriver *dacp);
#endif
#ifdef __cplusplus
}
#endif

#endif /* ASMO_USE_DAC == TRUE */

#endif /* ASMO_DAC_H */
