/*
 * switch.h
 *
 *  Created on: 20.07.2011
 *      Author: Florian Goebe
 */

#ifndef SWITCH_H_
#define SWITCH_H_

#define SWITCH_SIGNAL1_PORT GPIOG
#define SWITCH_SIGNAL2_PORT GPIOG
#define SWITCH_SIGNAL4_PORT GPIOG
#define SWITCH_SIGNAL8_PORT GPIOG

#define SWITCH_SIGNAL1_PIN  0
#define SWITCH_SIGNAL2_PIN  1
#define SWITCH_SIGNAL4_PIN  2
#define SWITCH_SIGNAL8_PIN  3

void switch_init(void);
unsigned char switch_readValue(void);

#endif /* SWITCH_H_ */
