/*
 *  \file   ASMO_Communication.h
 *  \brief  Provides public headers for communications API
 *
 *  \date   Nov 2020
 *  \author Moritz Huellmann
 */

/*
 *
 * ForceCan
 * ForceEth
 * SendMsg
 * SendMsgTimeOut ......
 *
 */

#ifndef _ASMO_COMMUNICATION_H_
#define _ASMO_COMMUNICATION_H_

enum ASMO_CommunicationMethods {
  ASMO_COMM_METHOD_NONE,
  ASMO_COMM_METHOD_CAN,
  ASMO_COMM_METHOD_ETH,
};

void ASMO_CommunicationsInit(void);

int8_t sendSimpleMessage(uint32_t message_id, uint64_t message);

int8_t sendSimpleMessageRef(uint32_t message_id, uint64_t *message);

int8_t sendSimpleMessageTimeout(uint32_t message_id, uint64_t message,
                                int32_t timeout);

int8_t sendSimpleMessageRefTimeout(uint32_t message_id, uint64_t *message,
                                   int32_t timeout);

int8_t sendLocalMeasurement(ASMO_Measurement *meas);

int8_t setCommMethod(enum ASMO_CommunicationMethods method);

#endif /* _ASMO_COMMUNICATION_H_ */

