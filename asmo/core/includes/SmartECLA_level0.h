/**
 * \file	SmartECLA_level0.h
 * \brief	Includes all SmartECLA headers of level 0.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL0_H_
#define SMARTECLA_LEVEL0_H_

/*
 *  ##################################################################
 *  # Level 0 - Included comment files (Doxygen etc.)                #
 *  ##################################################################
 */

// -- currently empty --

#endif /* SMARTECLA_LEVEL0_H_ */
