/*! \file
 *  \brief ASMO Constants
 *
 *  Constants for ASMO project that are independent from hardware
 *
 *  \author		Felix Gathmann
 *  \author		Lehrstuhl fuer Informatik 11 -  RWTH Aachen
 *  \date		2008
 */

#ifndef _ASMO_CONSTANTS_H
#define _ASMO_CONSTANTS_H

#define CSV_SEPARATOR                           0x3B // ASCII Zeichen ";"
#define LINE_FEED                               0x0A
#define CARRIAGE_RETURN                         0x0D
#define SPACE                                   0x20

#define   BIT0        0x00000001
#define   BIT1        0x00000002
#define   BIT2        0x00000004
#define   BIT3        0x00000008
#define   BIT4        0x00000010
#define   BIT5        0x00000020
#define   BIT6        0x00000040
#define   BIT7        0x00000080
#define   BIT8        0x00000100
#define   BIT9        0x00000200
#define   BIT10       0x00000400
#define   BIT11       0x00000800
#define   BIT12       0x00001000
#define   BIT13       0x00002000
#define   BIT14       0x00004000
#define   BIT15       0x00008000
#define   BIT16       0x00010000
#define   BIT17       0x00020000
#define   BIT18       0x00040000
#define   BIT19       0x00080000
#define   BIT20       0x00100000
#define   BIT21       0x00200000
#define   BIT22       0x00400000
#define   BIT23       0x00800000
#define   BIT24       0x01000000
#define   BIT25       0x02000000
#define   BIT26       0x04000000
#define   BIT27       0x08000000
#define   BIT28       0x10000000
#define   BIT29       0x20000000
#define   BIT30       0x40000000
#define   BIT31       0x80000000

#define EVT_ID_MMC_INSERTED					10
#define EVT_ID_MMC_REMOVED					11

#define SAM7_RETURN_SUCCESS_DEPRECATED       100
#define RETURN_NO_INIT       101
#define RETURN_NO_DEVICE     102
#define INIT_INCOMPLETE      200
#define INIT_SUCCESS         201

// ASMO layer needs MMC
#define HAL_USE_MMC_SPI				TRUE

// Defines for MMC / SPI communication
#define FS_WP_PORT					GPIOB
#define FS_WP_PIN					0 // Write Protection on B0
#define FS_INS_PORT					GPIOB
#define FS_INS_PIN					1 // Card-detected switch on B1

#ifndef MEAN_NUMBER_POWER
#define MEAN_NUMBER_POWER	2u
#endif
#define MEAN_NUMBER			(1 << MEAN_NUMBER_POWER)

#endif
