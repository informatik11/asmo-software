/**
 * \file ASMO_LED.h
 * \brief Encapsulates the control of the LED on the STM32-ASMO board.
 *
 *  \date Oct 2011, Oct 2020
 *  \author Florian Goebe, Moritz Huellmann
 */

#ifndef ASMO_LED_H_
#define ASMO_LED_H_

/* Default values if not defined.*/
#ifndef USE_LED
#if defined(BOARD_STM32_ASMO) || defined(BOARD_ST_NUCLEO144_F767ZI)
/* Enable by default if ASMO or Nucleo board is in use.*/
#define USE_LED           TRUE
#else
/* Disable by default for all other boards.*/
#define USE_LED           FALSE
#endif
#endif /* USE_LED */

#if (USE_LED == TRUE)
/**
 * \brief Inits the LED module. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 */
void LED_init(void);

/**
 * \brief Switches a certain LED on or off. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 * \param ledNumber     decides which LED shall be switched
 * \param state         true: switch LED on,  false: switch LED off.
 */
void LED_switch(unsigned char ledNumber, unsigned char state);

/**
 * \brief Toggles the state of a certain LED. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 * \param ledNumber     decides which LED's state shall be toggled.
 */
void LED_toggle(unsigned char ledNumber);


/**
 * \brief Sets the LEDs to represent a number from 0 to 2^(Nr of LEDs)-1 in binary. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 * \param number    the number to represent
 */
void LED_setValue(uint8_t number);

/**
 * \brief Switches all LEDs according to mask 0bXXXXYYYYY. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 * \param mask      the mask 0xXY. X: Don't care, Y: Mask. If bit is 1, switch LED on, else off.
 */
void LED_switchMask(uint8_t mask);

/**
 * \brief Switches all LEDs on or off. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 * \param arg      0: off, else on.
 */
void LED_switchAll(uint8_t arg);


/**
 * \brief Toggle all specified LEDs. Has no effect if BOARD_STM32_ASMO and BOARD_ST_NUCLEO144_F767ZI are not defined.
 * \param mask      the mask 0xXY. X: Don't care, Y: Mask. If bit is 1, toggle, else leave LED as is.
 */
void LED_toggleMask(uint8_t mask);

#endif /* USE_LED == TRUE */
#endif /* ASMO_LED_H_ */
