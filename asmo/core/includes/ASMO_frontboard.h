/**
 * @file    hal_frontboard.h
 * @brief   Frontboard driver macros and structures.
 *
 * @addtogroup frontboard
 * @{
 */

#ifndef ASMO_FRONTBOARD_H
#define ASMO_FRONTBOARD_H

/* Default values if not defined.*/
#ifndef ASMO_USE_FRONTBOARD
#ifdef BOARD_STM32_PUMP
/* Enable by default if pump board is in use.*/
#define ASMO_USE_FRONTBOARD TRUE
#else
/* Disable by default for all other boards.*/
#define ASMO_USE_FRONTBOARD FALSE
#endif /* BOARD_STM32_PUMP */
#endif /* ASMO_USE_FRONTBOARD */

#if (ASMO_USE_FRONTBOARD == TRUE)

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

#if (HAL_USE_SPI == FALSE)
#error "SPI needs to be enabled in order for the frontboard driver to work!"
#endif

#if (ASMO_USE_LED_DISPLAY == FALSE)
#error "LED Display needs to be enabled in order for the frontboard driver to "
"work!"
#endif

#if (PAL_USE_CALLBACKS == FALSE)
#error "PAL callbacks need to be enabled in order for the frontboard driver "
"to work!"
#endif

#if (PAL_USE_WAIT == FALSE)
#error "PAL wait needs to be enabled in order for the frontboard driver "
"to work!"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/**
 * @brief   Mapping of the LED numbers to the associated task.
 */
typedef enum {
  frontboard_led_middle  =        0,
  frontboard_led_top     =        3,
  frontboard_led_right   =        4,
  frontboard_led_brake   =        5,
  frontboard_led_left    =        7
} frontboard_display_leds;

/**
 * @brief   Rotary encoder state.
 */
typedef enum {
  ROTARY_SAME = 0,                 /**< No change.                  */
  ROTARY_CW = 1,                   /**< Clock wise turned.          */
  ROTARY_CCW = 2                  /**< Counter clock wise turned.  */
} frontboardrotarystate_t;

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
  FRONTBOARD_UNINIT = 0,         /**< Not initialized.                  */
  FRONTBOARD_STOP = 1,           /**< Stopped.                           */
  FRONTBOARD_READY = 2,          /**< Ready.                             */
  FRONTBOARD_ACTIVE = 3,         /**< Exchanging data.                   */
} frontboardstate_t;

/**
 * @brief   Type of a structure representing an Frontboard driver.
 */
typedef struct hal_frontboard_driver FrontboardDriver;
/**
 * @brief   Type of a Frontboard driver configuration structure.
 */
typedef struct hal_frontboard_config FrontboardConfig;

/**
 * @brief   Frontboard rotary encoder notification callback type.
 *
 * @param[in] frontboardp      pointer to the @p FrontboardDriver object
 *                             triggering the callback
 */
typedef void (*frontboardrotarycallback_t)(FrontboardDriver *frontboardp,
    frontboardrotarystate_t frontboardrotary);

/**
 * @brief   Driver configuration structure.
 */
struct hal_frontboard_config {
  /**
   * @brief   Callback for the rotary encoder.
   */
  frontboardrotarycallback_t rotary_cb;

  /**
   * @brief   Maximum sleep time for rotary thread.
   */
  time_msecs_t rotary_sleep_max;
};

/**
 * @brief   Structure representing a frontboard driver.
 */
struct hal_frontboard_driver {

  /**
   * @brief Driver state.
   */
  frontboardstate_t                state;

  /**
   * @brief Driver config.
   */
  const FrontboardConfig               *config;

  /**
   * @brief Thread reference for rotary encoder.
   */
  thread_reference_t rotaryEncoderThdReference;

  /**
   * @brief Binary array storing the current led status.
   */
  uint8_t ledValues;

};

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

extern FrontboardDriver FRONTBOARD_D;

#ifdef __cplusplus
extern "C" {
#endif
void frontboardInit(void);
void frontboardObjectInit(FrontboardDriver *frontboardp);
void frontboardStart(FrontboardDriver *frontboardp, const FrontboardConfig
*config);
void frontboardStop(FrontboardDriver *frontboardp);
void frontboardReset(FrontboardDriver *frontboardp);
void frontboardSendBar(FrontboardDriver *frontboardp, double
fractionBars);
void frontboardSendLed(FrontboardDriver *frontboardp,
                             frontboard_display_leds ledName, bool ledValue);
void frontboardSendLeft(FrontboardDriver *frontboardp,
                              double valueTop, double valueBot);
void frontboardSendRight(FrontboardDriver *frontboardp,
                               double valueTop, double valueBot);
#ifdef __cplusplus
}
#endif

#endif /* ASMO_USE_FRONTBOARD == TRUE */

#endif /* ASMO_FRONTBOARD_H */

/** @} */
