/**
 * \file	SmartECLA_level4.h
 * \brief	Includes all SmartECLA headers of level 4.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL4_H_
#define SMARTECLA_LEVEL4_H_

/*
 *  ##################################################################
 *  # Level 4 - Global Types based on elementary types               #
 *  ##################################################################
 */

/**
 * Level 41 - SmartECLA Typedefs
 */
#include "ptp.h"
#include "ASMO_typedefs.h"

/**
 * Level 42 - SmartECLA IDs
 */
#include "SmartECLA_IDs.h"

/**
 * Level 43 - Datastorage
 */
#include "ASMO_dataStorage_register.h"
#include "ASMO_dataStorage_storage.h"

#endif /* SMARTECLA_LEVEL4_H_ */
