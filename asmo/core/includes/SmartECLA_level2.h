/**
 * \file	SmartECLA_level2.h
 * \brief	Includes all SmartECLA headers of level 2.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL2_H_
#define SMARTECLA_LEVEL2_H_

/*
 *  ##################################################################
 *  # Level 2 - Public defines that depend on foreign Level-1 Defines#
 *  ##################################################################
 */

/**
 * Level 21 - MDL Conditional Defs
 */
// -- currently empty --

/**
 * Level 22 - ASMO Conditional Defs
 */
// -- currently empty --


#endif /* SMARTECLA_LEVEL2_H_ */
