/**
 * \file	SmartECLA_level1.h
 * \brief	Includes all SmartECLA headers of level 1.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL1_H_
#define SMARTECLA_LEVEL1_H_

/*
 *  ##################################################################
 *  # Level 1 - Pure Defines (Static configuration of OS/Drivers/...)#
 *  ##################################################################
 */

/**
 * Level 11 - MDL Defs
 */
#include "MDL_config.h"
#include "asmoconf_defaults.h"
#include "revisionNumberMDL.ign.h"

/**
 * Level 12 - ASMO Defs
 */
#include "ASMO_constants.h"
#include "revisionNumberIDs.ign.h"


#endif /* SMARTECLA_LEVEL1_H_ */
