/*
 * \file ASMO_simulinkWrappers.h
 * \brief Provides some Wrapper functions that are used by user defined
 * Simulink S-Functions.
 *
 *  Created on: 23.08.2010
 *      Author: Florian Goebe
 */

#ifndef ASMO_SIMULINKWRAPPERS_H_
#define ASMO_SIMULINKWRAPPERS_H_

// Store in the struct in the memory (cache for models, etc.)
void simulink_newMeasurement(unsigned long id, unsigned long data);

// Load current value of measurement
void simulink_getMeasurementWrapper(unsigned long id, double *data,
									timestamp_t *timestamp);

void simulink_getTSMeasurementWrapper(ds_TSReg *reg, unsigned int *values,
										timestamp_t *timestamps,
										unsigned int *min, unsigned int *max,
										unsigned int *avg);

void simulink_safetySet(unsigned long id, ASMO_safetyCommand cmd, unsigned long data);
#endif /* ASMO_SIMULINKWRAPPERS_H_ */
