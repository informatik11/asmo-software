/**
 * \file	SmartECLA_level3.h
 * \brief	Includes all SmartECLA headers of level 3.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL3_H_
#define SMARTECLA_LEVEL3_H_

/*
 *  ##################################################################
 *  # Level 3 - Operating System (untouched or OS-conform headers)   #
 *  ##################################################################
 */

/**
 * Level 31 - C runtime libraries
 */
#include <string.h>
#include <stdbool.h>

/**
 * Level 32 - Kernel
 */
#include "ch.h"
#include "revisionNumberChibiOS.ign.h"

/**
 * Level 33 - Hardware Abstraction (Drivers)
 */
#include "hal.h"

/**
 * Level 33 - Filesystem (Drivers)
 */
#include <lwip/api.h>
#include <lwipthread.h>
#include "../../../fatfs/source/ff.h"
#include "ASMO_usbcfg.h"

#endif /* SMARTECLA_LEVEL3_H_ */
