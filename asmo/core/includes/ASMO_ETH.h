/**
 * @file    ASMO_ETH.h
 * @brief   Contains headers for functions to easily send messages via Ethernet.
 * @{
 */

#include "SmartECLA_allHeaders.h"

#ifndef ASMO_ETH_H
#define ASMO_ETH_H

/*===========================================================================*/
/* Task constants.                                                           */
/*===========================================================================*/

#define ASMO_SIMPLE_CAN_MESSAGE_SZ          13
#define ASMO_MEASUREMENT_MESSAGE_SZ         15

#define ASMO_NET_PORT                       51111
#define ASMO_NET_MESSAGE_TYPE_MEASUREMENT   1
#define ASMO_NET_MESSAGE_TYPE_SIMPLECAN     2

/*
 * Message priorities defined for the CAN Bus.
 *
 * CAN PRIO     DESCRIPTION
 * ---------------------------------------------------
 * 0x00         Synchronization messages, performance
 * 0x02         Alarm (medical)
 * 0x03         Alarm (low prio)
 * 0x05         Control command for ASMO
 * 0x06         Control command for models on ASMO
 * 0x07         Security
 * 0x08         Data point
 * 0x0A         Status ASMO
 * 0x0B         Status model on ASMO
 * 0x0C         Status connection
 * 0x10         Measurements
 * 0x11         Parameters
 * 0x1F         Lowest Priority
 */

/*===========================================================================*/
/* Task pre-compile time settings.                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Task data structures and types.                                           */
/*===========================================================================*/

typedef uint8_t asmo_eth_prio;

typedef struct {
  ip_addr_t ipaddr;
  uint16_t sequence_id;
  bool message_received;
  uint8_t device_instance_id;
} ASMO_Stream;

typedef struct {
  uint8_t type;
  uint32_t id;
  uint64_t msg;
  asmo_eth_prio prio;
  timestamp_t ts;
} asmo_eth_message;

/*===========================================================================*/
/* Task macros.                                                              */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
void ASMO_initETH(void);
uint8_t asmo_block_id_from_msgid(uint32_t id);
msg_t sendLocalMeasurementToETH(ASMO_Measurement* measurement);
msg_t sendSimpleETHMessage(uint32_t id, uint64_t message);
msg_t sendSimpleETHMessageRef(uint32_t id, uint64_t *message);
msg_t sendSimpleETHMessageTimeout(uint32_t id, uint64_t message,
                                  uint32_t timeout);
msg_t sendSimpleETHMessageRefTimeout(uint32_t id, uint64_t *message,
                                     uint32_t timeout);
#ifdef __cplusplus
}
#endif

#endif /* ASMO_ETH_H */

/** @} */
