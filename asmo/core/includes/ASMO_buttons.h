/**
 * \file ASMO_buttons.h
 *
 * \brief Manages external buttons for STM32. There is no need to use this, you can simply use
 * palReadLine(LINE_EXTERNAL_BUTTONX), X in 1..4, too.
 */

#ifndef ASMO_BUTTONS_H_
#define ASMO_BUTTONS_H_

/* Default values if not defined.*/
#ifndef USE_BUTTONS
#define USE_BUTTONS         FALSE
#endif /* USE_BUTTONS */

#if USE_BUTTONS

#define BUTTON_1_PRESSED_FLAG   (1 << 0)
#define BUTTON_2_PRESSED_FLAG   (1 << 1)
#define BUTTON_3_PRESSED_FLAG   (1 << 2)
#define BUTTON_4_PRESSED_FLAG   (1 << 3)

#define BUTTONS_PRESSED_MASK    EVENT_MASK(0);

extern event_source_t evtSrcButtons;

typedef struct {
  ioline_t buttonLine;
  sysinterval_t timeout;
} ASMO_ButtonCfg;

#endif // USE_BUTTONS
#endif /* ASMO_BUTTONS_H_ */
