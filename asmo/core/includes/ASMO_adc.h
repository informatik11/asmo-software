/**
 * @file    hal_external_adc.h
 * @brief   AD via SPI macros and structures.
 * @author  Moritz Huellmann
 * @date    Jan 2021
 */

#ifndef ASMO_ADC_H
#define ASMO_ADC_H

/* Default values if not defined.*/
#ifndef ASMO_USE_ADC
#ifdef BOARD_STM32_PUMP
/* Enable by default if pump board is in use.*/
#define ASMO_USE_ADC TRUE
#else
/* Disable by default for all other boards.*/
#define ASMO_USE_ADC FALSE
#endif /* BOARD_STM32_PUMP */
#endif /* ASMO_USE_ADC */

#if (ASMO_USE_ADC == TRUE)

#if (HAL_USE_SPI == FALSE)
#error "SPI needs to be enabled in order for the external ADC to work!"
#endif

#if (SPI_SELECT_MODE != SPI_SELECT_MODE_PAD)
#error "SPI select mode needs to be pad in order for the external ADC to work!"
#endif

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#if !defined(ASMO_ADC_USE_WAIT) || defined(__DOXYGEN__)
#define ASMO_ADC_USE_WAIT                   TRUE
#endif

/**
 * @brief   Enables the @p adcAcquireBus() and @p adcReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#if !defined(ASMO_ADC_USE_MUTUAL_EXCLUSION) || defined(__DOXYGEN__)
#define ASMO_ADC_USE_MUTUAL_EXCLUSION       TRUE
#endif

#if !defined(ADC_USE_CH0)
#define ADC_USE_CH0             TRUE
#endif
#if !defined(ADC_USE_CH1)
#define ADC_USE_CH1             FALSE
#endif
#if !defined(ADC_USE_CH2)
#define ADC_USE_CH2             FALSE
#endif
#if !defined(ADC_USE_CH3)
#define ADC_USE_CH3             FALSE
#endif

#if ADC_USE_CH0
#define ADC_CH0_POS             (0)
#endif

#if ADC_USE_CH1
#define ADC_CH1_POS             (ADC_USE_CH0)
#endif

#if ADC_USE_CH2
#define ADC_CH2_POS             (ADC_USE_CH0 + ADC_USE_CH1)
#endif

#if ADC_USE_CH3
#define ADC_CH3_POS             (ADC_USE_CH0 + ADC_USE_CH1 + ADC_USE_CH2)
#endif

#define ADC_CH_COUNT            ((uint8_t)ADC_USE_CH0 + (uint8_t)ADC_USE_CH1 + (uint8_t)ADC_USE_CH2 + (uint8_t)ADC_USE_CH3)

#define AD7924_DUMMY_FRAME      0xFFFF

/* AD7924 ADC data frame flag positions */
#define AD7924_WRITE_POS        15
#define AD7924_SEQ1_POS         14
#define AD7924_ADD1_POS         11
#define AD7924_ADD0_POS         10
#define AD7924_PM1_POS          9
#define AD7924_PM0_POS          8
#define AD7924_SEQ0_POS         7
#define AD7924_RANGE_POS        5
#define AD7924_CODING_POS       4

/* Sequencer operation modes */
#define AD7924_SEQ_UNUSED       ((0 << AD7924_SEQ1_POS) | (0 << AD7924_SEQ0_POS))
#define AD7924_SEQ_USED         ((1 << AD7924_SEQ1_POS) | (0 << AD7924_SEQ0_POS))
#define AD7924_SEQ_CONT         ((1 << AD7924_SEQ1_POS) | (1 << AD7924_SEQ0_POS))

/* Power modes */
#define AD7924_POW_NORMAL       ((1 << AD7924_PM1_POS) | (1 << AD7924_PM0_POS))
#define AD7924_POW_FULL_SHUT    ((1 << AD7924_PM1_POS) | (0 << AD7924_PM0_POS))
#define AD7924_POW_AUTO_SHUT    ((0 << AD7924_PM1_POS) | (1 << AD7924_PM0_POS))

/* Channels */
#define AD7924_CH0              ((0 << AD7924_ADD1_POS) | (0 << AD7924_ADD0_POS))
#define AD7924_CH1              ((0 << AD7924_ADD1_POS) | (1 << AD7924_ADD0_POS))
#define AD7924_CH2              ((1 << AD7924_ADD1_POS) | (0 << AD7924_ADD0_POS))
#define AD7924_CH3              ((1 << AD7924_ADD1_POS) | (1 << AD7924_ADD0_POS))

/* Flags */
/* Set write flag. b = 1 writes data to registers */
#define AD7924_WRITE(b)         ((b) << AD7924_WRITE_POS)
/* Set range flag. Sets range to (2-b) * V_ref */
#define AD7924_RANGE(b)         ((b) << AD7924_RANGE_POS)
/* Set coding flag. b = 1 sets coding to straight binary */
#define AD7924_CODING(b)        ((b) << AD7924_CODING_POS)

#define AD7924_ASMO_DEF_MODE    (AD7924_SEQ_UNUSED | AD7924_POW_NORMAL | AD7924_CODING(1) | AD7924_RANGE(0))

typedef uint32_t adcextsample_t;

typedef enum {
  ADC_EXT_UNINIT = 0,                           /**< Not initialized.           */
  ADC_EXT_STOP = 1,                             /**< Stopped.                   */
  ADC_EXT_READY = 2,                            /**< Ready.                     */
  ADC_EXT_ACTIVE = 3,                           /**< Converting.                */
  ADC_EXT_COMPLETE = 4,                         /**< Conversion complete.       */
  ADC_EXT_ERROR = 5                             /**< Conversion error.          */
} adcextstate_t;

/**
 * @brief   Type of a structure representing the external ADC driver.
 */
typedef struct hal_adc_ext_driver ADCEDriver;

/**
 * @brief   Type of a struct representing the configuration for the external ADC driver.
 */
typedef struct hal_adc_ext_configuration ADCEConfig;

/**
 * @brief   Conversion group configuration structure.
 * @details This implementation-dependent structure describes a conversion
 *          operation.
 * @note    The use of this configuration structure requires knowledge of
 *          STM32 ADC cell registers interface, please refer to the STM32
 *          reference manual for details.
 */
typedef struct hal_adc_ext_configuration_group ADCEConversionGroup;

/**
 * @brief   Type of an ADC notification callback.
 *
 * @param[in] adcp      pointer to the @p ADCDriver object triggering the
 *                      callback
 */
typedef void (*adcextcallback_t)(ADCEDriver *adcp);

struct hal_adc_ext_configuration {
  /**
   * @brief     Interval in ms between conversions (linear, not per channel).
   */
  uint16_t                      interval;
  /**
   * @brief     The SPI driver to access the ADC with. If on STM32 ASMO board, you
   *            can leave this blank.
   */
  SPIDriver                     *spi_driver;
};

struct hal_adc_ext_configuration_group {
  /**
   * @brief   Enables the circular buffer mode for the group.
   */
  bool                          circular;
  /**
   * @brief   Number of the analog channels belonging to the conversion group.
   */
  uint16_t                      *send_buffer;
  /**
   * @brief   Callback function associated to the group or @p NULL.
   */
  adcextcallback_t              end_cb;
};

struct hal_adc_ext_driver {
  /**
   * @brief Driver state.
   */
  adcextstate_t           state;
  /**
   * @brief Current samples buffer pointer or @p NULL.
   */
  adcextsample_t          *samples;
  /**
   * @brief Current samples buffer depth or @p 0.
   */
  size_t                  depth;
  /**
   * @brief Current conversion group pointer or @p NULL.
   */
  ADCEConversionGroup     *grpp;
  /**
   * @brief Current configuration pointer.
   */
  ADCEConfig              *config;
#if (ASMO_ADC_USE_WAIT == TRUE) || defined(__DOXYGEN__)
  /**
   * @brief Waiting thread.
   */
  thread_reference_t        thread;
#endif /* ADC_USE_WAIT == TRUE */
#if (ASMO_ADC_USE_MUTUAL_EXCLUSION == TRUE) || defined(__DOXYGEN__)
  /**
   * @brief Mutex protecting the peripheral.
   */
  mutex_t                   mutex;
#endif /* ADC_USE_MUTUAL_EXCLUSION == TRUE */
};

/**
 * @brief   Wakes up the waiting thread.
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @note    Borrowed from ChibiOS 20.3.2. (MH)
 *
 * @notapi
 */
#define _adc_ext_wakeup_isr(adcp) {                                             \
  osalSysLock();                                                     \
  osalThreadResumeI(&(adcp)->thread, MSG_OK);                               \
  osalSysUnlock();                                                   \
}

/**
 * @brief   Common ISR code, half buffer event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @notapi
 */
#define _adc_ext_isr_half_code(adcp) {                                      \
  if ((adcp)->grpp->end_cb != NULL) {                                       \
    (adcp)->grpp->end_cb(adcp);                                             \
  }                                                                         \
}

/**
 * @brief   Common ISR code, full buffer event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *          - Waiting thread wakeup, if any.
 *          - Driver state transitions.
 *
 * @param[in] adcp      pointer to the @p ADCEDriver object
 *
 * @notapi
 */
#define _adc_ext_isr_full_code(adcp) {                                          \
  if ((adcp)->grpp->end_cb != NULL) {                                           \
    (adcp)->state = ADC_EXT_COMPLETE;                                           \
    (adcp)->grpp->end_cb(adcp);                                                 \
    if ((adcp)->state == ADC_EXT_COMPLETE) {                                    \
      (adcp)->state = ADC_EXT_ACTIVE;                                           \
    }                                                                           \
  }                                                                             \
  if (!(adcp)->grpp->circular) {                                                       \
    (adcp)->state = ADC_EXT_READY;                                              \
    if ((adcp)->thread != NULL) {                                               \
      _adc_ext_wakeup_isr(adcp);                                          \
    }                                                                           \
  }                                                                             \
}

extern ADCEDriver ADCED;

#ifdef __cplusplus
extern "C" {
#endif
  void adcExtObjectInit(ADCEDriver *adcp);
  void adcExtStart(ADCEDriver *adcp, ADCEConfig *config);
  void adcExtStop(ADCEDriver *adcp);
  void adcExtStartConversion(ADCEDriver *adcp,
                          ADCEConversionGroup *grpp,
                          adcextsample_t *samples,
                          size_t depth);
  void adcExtStartConversionI(ADCEDriver *adcp,
                           ADCEConversionGroup *grpp,
                           adcextsample_t *samples,
                           size_t depth);
  void adcExtStopConversion(ADCEDriver *adcp);
  void adcExtStopConversionI(ADCEDriver *adcp);
#if ASMO_ADC_USE_WAIT == TRUE
  msg_t adcExtConvert(ADCEDriver *adcp,
                   ADCEConversionGroup *grpp,
                   adcextsample_t *samples,
                   size_t depth);
#endif
#if ASMO_ADC_USE_MUTUAL_EXCLUSION == TRUE
  void adcExtAcquireBus(ADCEDriver *adcp);
  void adcExtReleaseBus(ADCEDriver *adcp);
#endif
#ifdef __cplusplus
}
#endif

#endif /* ASMO_USE_ADC == TRUE */

#endif /* ASMO_ADC_H */
