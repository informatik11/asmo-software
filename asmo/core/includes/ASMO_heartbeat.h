/**
 * @file    ASMO_heartbeat.h
 * @brief   Periodically sends an ALIVE-message, used in ETH communication mode.
 * @author  Benedikt Conze
 * @date    08.07.22
 *
 * @addtogroup heartbeat
 * @{
 */

#ifndef ASMO_HEARTBEAT_H
#define ASMO_HEARTBEAT_H

/* Default values if not defined.*/
#ifndef ASMO_SEND_HEARTBEAT
/* Enable by default only in ETH mode.*/
#define ASMO_SEND_HEARTBEAT         USE_ETH_COMM
#endif /* ASMO_SEND_HEARTBEAT */

#ifndef ASMO_HEARTBEAT_INTERVAL_MS
#define ASMO_HEARTBEAT_INTERVAL_MS  2000
#endif

#if (ASMO_SEND_HEARTBEAT == TRUE)

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

void asmoStartHeartbeat(uint32_t initial_status);
void asmoStopHeartbeat(void);
void asmoSetHeartbeatStatus(uint32_t status);
uint32_t asmoGetHeartbeatStatus(void);

#ifdef __cplusplus
}
#endif

#endif /* ASMO_SEND_HEARTBEAT == TRUE */

#endif /* ASMO_HEARTBEAT_H */
