/**
 * \file ASMO_util.h
 * Header file of ASMO_util.c
 *
 */

#ifndef _ASMO_CAN_H
#define _ASMO_CAN_H

#if (USE_CAN_COMM == TRUE)

#define CAND                CAND1
#define CAN_DATA_LENGTH     8
#define CAN_RX_EVENT_MASK   EVENT_MASK(0)
#define CAN_ERR_EVENT_MASK  EVENT_MASK(1)

#define CAN_TX_MB           0
#define CAN_RX_MB0          1
#define CAN_RX_MB1          2


void ASMO_initCAN(void);

char sendSimpleCANMessage(unsigned long id, unsigned long long message);

char sendSimpleCANMessageRef(unsigned long id, unsigned long long *message);

char sendSimpleCANMessageTimeout(unsigned long id, unsigned long long message,
                                    uint32_t timeout);

char sendSimpleCANMessageRefTimeout(unsigned long id,
                                    unsigned long long *message, uint32_t timeout);

char sendLocalMeasurementToCAN(ASMO_Measurement *measurement);

void ASMO_showCANError(void);

#endif /* USE_CAN_COMM == TRUE */

#endif /* _ASMO_CAN_H */
