/*
 * \file    sevenSeg.h
 * \brief   Driver for the seven segment led display on the STM32-ASMO board
 * \author  Moritz Huellmann
 * \date    Oct 2020
 */

#ifndef SEVENSEG_H_
#define SEVENSEG_H_

#ifndef USE_SEVEN_SEG
#ifdef BOARD_STM32_ASMO
/*
 * Controls if the seven segment display functions have an effect.
 * Defaults to TRUE on the STM32_ASMO board.
 */
#define USE_SEVEN_SEG           TRUE
#else
#define USE_SEVEN_SEG           FALSE
#endif /* BOARD_STM32_ASMO */
#endif /* USE_SEVEN_SEG */

#if (USE_SEVEN_SEG == TRUE)

// Hardware
#define SEVENSEG_SPI_DRIVER         SPID2
#define SEVENSEG_CS_PORT            GPIOB
#define SEVENSEG_CS_PIN             GPIOB_CS_SEVEN_SEG

// Operation mode
#define SEVENSEG_ENABLEDECODING     0xFF

typedef enum {
  SEVENSEG_MODE_SHUTDOWN    = 0x00,
  SEVENSEG_MODE_NORMAL      = 0x01
} SEVENSEG_MODE_t;

typedef enum {
  SEVENSEG_TEST_DISABLE     = 0x00,
  SEVENSEG_TEST_ENABLE      = 0x01
} SEVENSEG_TEST_t;

// Configuration
typedef enum {
  SEVENSEG_INTENSITY_0      = 0x00,
  SEVENSEG_INTENSITY_1      = 0x01,
  SEVENSEG_INTENSITY_2      = 0x02,
  SEVENSEG_INTENSITY_3      = 0x03,
  SEVENSEG_INTENSITY_4      = 0x04,
  SEVENSEG_INTENSITY_5      = 0x05,
  SEVENSEG_INTENSITY_6      = 0x06,
  SEVENSEG_INTENSITY_7      = 0x07,
  SEVENSEG_INTENSITY_8      = 0x08,
  SEVENSEG_INTENSITY_9      = 0x09,
  SEVENSEG_INTENSITY_10     = 0x0A,
  SEVENSEG_INTENSITY_11     = 0x0B,
  SEVENSEG_INTENSITY_12     = 0x0C,
  SEVENSEG_INTENSITY_13     = 0x0D,
  SEVENSEG_INTENSITY_14     = 0x0E,
  SEVENSEG_INTENSITY_15     = 0x0F
} SEVENSEG_INTENSITY_t;

typedef enum {
  SEVENSEG_DIGIT_COUNT_0    = 0x00,
  SEVENSEG_DIGIT_COUNT_1    = 0x01,
  SEVENSEG_DIGIT_COUNT_2    = 0x02,
  SEVENSEG_DIGIT_COUNT_3    = 0x03,
  SEVENSEG_DIGIT_COUNT_4    = 0x04
} SEVENSEG_DIGIT_COUNT_t;

typedef enum {
  SEVENSEG_DIGIT_1          = 0x01,
  SEVENSEG_DIGIT_2          = 0x02,
  SEVENSEG_DIGIT_3          = 0x03,
  SEVENSEG_DIGIT_4          = 0x04
} SEVENSEG_DIGIT_t;

// Addresses
#define SEVENSEG_ADDR_DIGIT1      ((uint16_t)0x0100)
#define SEVENSEG_ADDR_DIGIT2      ((uint16_t)0x0200)
#define SEVENSEG_ADDR_DIGIT3      ((uint16_t)0x0300)
#define SEVENSEG_ADDR_DIGIT4      ((uint16_t)0x0400)
#define SEVENSEG_ADDR_DECODEMODE  ((uint16_t)0x0900)
#define SEVENSEG_ADDR_INTENSITY   ((uint16_t)0x0A00)
#define SEVENSEG_ADDR_SCANLIMIT   ((uint16_t)0x0B00)
#define SEVENSEG_ADDR_MODE        ((uint16_t)0x0C00)
#define SEVENSEG_ADDR_TESTMODE    ((uint16_t)0x0F00)

// Symbols
#define SEVENSEG_DOT              0x80
#define SEVENSEG_SYMBOLS_MINUS    0x0A
#define SEVENSEG_SYMBOLS_E        0x0B
#define SEVENSEG_SYMBOLS_H        0x0C
#define SEVENSEG_SYMBOLS_L        0x0D
#define SEVENSEG_SYMBOLS_P        0x0E
#define SEVENSEG_SYMBOLS_BLANK    0x0F

/**
 * Blanks all digits of the display.
 */
void sevenSeg_blankAll(void);

/**
 *
 */
void sevenSeg_transmit(uint16_t address, uint8_t data);

/**
 * Initializes Display in digit-decode mode. Before it enters the test
 * mode for short. The intensity is put to maximum. 4 Digits are activated.
 * Has no effect if USE_SEVEN_SEG is not set.
 */
void sevenSeg_init(void);

/**
 * Prints a number on a certain digit. Has no effect if USE_SEVEN_SEG is not set.
 * valid number codes:
 *      0 - 9 : '0' - '9'
 *      0xA :   '-'
 *      0xB :   'E'
 *      0xC :   'H'
 *      0xD :   'L'
 *      0xE :   'P'
 *      0xF :   ' ' (blank)
 * \param digit     The digit where the number shall be shown (0-3)
 * \param number    The number to display. If 9 < n < 0xF, the corresponding letter is printed.
 */
void sevenSeg_setDigit(SEVENSEG_DIGIT_t digit, uint8_t symbol);

/**
 * Writes an int number between -999 and 9999 onto the display
 * Has no effect if USE_SEVEN_SEG is not set.
 * \param number    The number to print on the display.
 */
void sevenSeg_writeInt(int32_t number);

/**
 * Sets the number of activated digits. Has no effect if USE_SEVEN_SEG is not set.
 *  0 : disable LED display
 *  1 : Use digit 0 only
 *  2 : Use digits 0 and 1
 *  and so forth...
 *
 * \param number    number of digits to use.
 */
void sevenSeg_setNumberOfDigits(SEVENSEG_DIGIT_COUNT_t number);

/** Test mode of Display.
 * Has no effect if USE_SEVEN_SEG is not set.
 * Enables all LED segments (incl. dots).
 * \param enable 1 = enter test mode, 0 = leave test mode.
 */
void sevenSeg_testMode(SEVENSEG_TEST_t enable);

/**
 * Sets the intensitsy of the seven-segment LEDs
 * Has no effect if USE_SEVEN_SEG is not set.
 * \param intensity 0 = darkest (off), 0xF = brightest.
 */
void sevenSeg_setIntensity(SEVENSEG_INTENSITY_t intensity);

/**
 * Switches the display on (from power-saving mode)
 * Has no effect if USE_SEVEN_SEG is not set.
 */
void sevenSeg_enableDisplay(void);

/**
 * Switches the display off (power-saving).
 * All register values are kept.
 */
void sevenSeg_disableDisplay(void);

#endif /* USE_SEVEN_SEG == TRUE */
#endif /* SEVENSEG_H_ */
