/**
 * \file ASMO_util.h
 * Header file of ASMO_util.c
 *
 */

#ifndef _ASMO_TYPEDEFS_H
#define _ASMO_TYPEDEFS_H

/** Timevalue-type for a Message*/
typedef struct ptp_timestamp timestamp_t;

/**

 * Represents one measurement including its CAN ID, its

 * value and a 32-bit timestamp.

 */

typedef struct {
  uint32_t messageId;
  uint32_t value;
  timestamp_t timestamp;
} ASMO_Measurement;

/**

 * Represents the configuration for acquired measurements for sending

 * via CAN and saving to SD.

 */

typedef struct {

  unsigned int canInterval;

  timestamp_t canTimestamp;

  unsigned int canThreshold;

  unsigned int sdInterval;

  timestamp_t sdTimestamp;

  unsigned int sdThreshold;

  unsigned int mean[MEAN_NUMBER];

  unsigned int meanCnt;

} ASMO_MeasurementConfig;

typedef struct {
  unsigned long messageId; // 32 bit
  unsigned long long command; // 64 bit
} MessageCommand;

#endif
