#ifndef ASMO_SAFETY_H
#define ASMO_SAFETY_H

typedef enum {
	safetyIdle,
	safetyWaitForId,
	safetyWaitForValue,
	safetyRunCmd
} ASMO_safetyState;

typedef enum {
	safetyCmdNoOp,
	safetyCmdSetMin,
	safetyCmdSetMax
} ASMO_safetyCommand;

typedef struct {
		ASMO_safetyState state;
		ASMO_safetyCommand command;
		unsigned int id;
		unsigned int value;
		timestamp_t lastUpdate;
} ASMO_safetyConfig;

bool ASMO_safetyCheck(ASMO_Measurement *m, unsigned int min, unsigned int max);
void ASMO_safetyInit(void);
void ASMO_safetyDispatch(MessageCommand *cmd);

#endif /* ASMO_SAFETY_H */
