/**
 * @file    hal_led_display_spi.h
 * @brief   Led display driver over SPI macros and structures.
 * @addtogroup led_display_SPI
 * @{
 */

#ifndef ASMO_LED_DISPLAY_H
#define ASMO_LED_DISPLAY_H

/* Default values if not defined.*/
#ifndef ASMO_USE_LED_DISPLAY
#ifdef BOARD_STM32_PUMP
/* Enable by default if pump board is in use.*/
#define ASMO_USE_LED_DISPLAY TRUE
#else
/* Disable by default for all other boards.*/
#define ASMO_USE_LED_DISPLAY FALSE
#endif /* BOARD_STM32_PUMP */
#endif /* ASMO_USE_LED_DISPLAY */

#if (ASMO_USE_LED_DISPLAY == TRUE)

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/* Display parameters.*/
#define MAXDISPLAY		8
#define MAXINTENSITY	15

/* Binary representations of seven-segment digits or elements.*/
#define LED_DISPLAY_DP		0x80
#define LED_DISPLAY_EMPTY	0x00
#define LED_DISPLAY_DASH  0x01
#define LED_DISPLAY_0		0b1111110
#define LED_DISPLAY_1		0b0110000
#define LED_DISPLAY_2		0b1101101
#define LED_DISPLAY_3		0b1111001
#define LED_DISPLAY_4		0b0110011
#define LED_DISPLAY_5		0b1011011
#define LED_DISPLAY_6		0b1011111
#define LED_DISPLAY_7		0b1110000
#define LED_DISPLAY_8		0b1111111
#define LED_DISPLAY_9		0b1111011
#define LED_DISPLAY_A		0b1110111
#define LED_DISPLAY_B		0b0011111
#define LED_DISPLAY_C		0b1001110
#define LED_DISPLAY_D		0b0111101
#define LED_DISPLAY_E		0b1001111
#define LED_DISPLAY_F		0b1000111

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @name    led_display configuration options
 * @{
 */
/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#if !defined(ASMO_LED_DISPLAY_USE_WAIT) || defined(__DOXYGEN__)
#define ASMO_LED_DISPLAY_USE_WAIT                TRUE
#endif

/**
 * @brief   Enables the @p led_displayAcquireBus() and @p led_displayReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#if !defined(ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION) || defined(__DOXYGEN__)
#define ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION    TRUE
#endif
/** @} */

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

#if (HAL_USE_SPI == FALSE)
#error "SPI needs to be enabled in order for the led display driver to work!"
#endif

#if (SPI_SELECT_MODE != SPI_SELECT_MODE_PAD)
#error "SPI select mode needs to be pad in order for the led display driver "
"to work!"
#endif

#if ASMO_LED_DISPLAY_USE_WAIT && !(CH_CFG_USE_MUTEXES)
#error "led_display SPI driver requires CH_CFG_USE_MUTEXES when "
"ASMO_LED_DISPLAY_USE_WAIT is enabled"
#endif

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/**
 * @brief   Driver state machine possible states.
 */
typedef enum {
  LED_DISPLAY_SPI_UNINIT = 0,                           /**< Not initialized.           */
  LED_DISPLAY_SPI_STOP = 1,                             /**< Stopped.                   */
  LED_DISPLAY_SPI_READY = 2,                            /**< Ready.                     */
  LED_DISPLAY_SPI_ACTIVE = 3,                           /**< Converting.                */
  LED_DISPLAY_SPI_COMPLETE = 4,                         /**< Conversion complete.       */
  led_display_SPI_ERROR = 5                             /**< Conversion complete.       */
} led_display_state_t;

/**
 * @brief   Display commands state machine.
 */
typedef enum {
  led_display_DecodeMode = 0x01, /**< Not initialized.           */
  led_display_Intensity = 0x02, /**< Stopped.                   */
  led_display_ScanLimit = 0x03, /**< Ready.                     */
  led_display_Configuration = 0x04, /**< Converting.                */
  led_display_DisplayTest = 0x07, /**< Conversion complete.       */
  led_display_P0only = 0x20, /**< Conversion complete.       */
  led_display_P1only = 0x40, /**< Conversion complete.       */
  led_display_P0andP1 = 0x60 /**< Conversion complete.       */
}led_display_command_t;

/**
 * @brief led_display sample data type.
 * array[M][N] is a Matrix MxN with M number of samples and N Number of channel
 */
typedef uint16_t led_display_sample_t;

/**
 * @brief   Type of a structure representing an led_display driver.
 */
typedef struct hal_led_display_driver LEDDisplayDriver;

/**
 * @brief   Type of a structure representing an led_display configuration.
 */
typedef struct hal_led_display_config LEDDisplayConfig;

/**
 * @brief led_display notification callback type.
 * @param[in] pointer the led_display Driver Object
 * @param[in] buffer pointer to the most recent samples data
 * @param[in] n number of buffer rows available starting from @p buffer
 */
typedef void (*led_display_callback_t)
    (LEDDisplayDriver *led_displayp, led_display_sample_t *buffer, size_t n);

///**
// * @brief Conversion group configuration structure.
// * @details This implementation-dependent structure describes a conversion
// *          operation.
// */
//typedef struct {
//  /**
//   * @brief Callback function associated to the group or @p NULL.
//   */
//  led_display_callback_t         end_cb;
//
//  /**
//   * @brief dataframe to send over spi.
//   */
//  uint16_t					spiframe;
//  /**
//   * @brief position of the channelnumber in the spiframe (number of bits).
//   */
//  uint8_t					channelPosInFrame;
//  /**
//   * @brief number of zeros behind data (offset).
//   */
//  uint8_t					dataPosInFrame;
//  /**
//   * @brief Current samples buffer pointer or @p NULL.
//   */
//  led_display_sample_t			*sendbuffer;
//  /* End of the mandatory fields.*/
//} led_displaySConversionGroup;

/**
 * @brief Driver configuration structure.
 */
struct hal_led_display_config {
  /**
   * @brief Current spi driver.
   */
  SPIDriver *spi_driver;

  /**
   * @brief Current spi configuration.
   */
  SPIConfig *spi_config;

  /**
   * @brief Callback function associated to the group or @p NULL.
   */
  led_display_callback_t end_cb;

};

/**
 * @brief   Structure representing an led_display driver.
 * @note    Implementations may extend this structure to contain more,
 *          architecture dependent, fields.
 */
struct hal_led_display_driver {
  /**
   * @brief Driver state.
   */
  led_display_state_t state;

  /**
   * @brief Current configuration data.
   */
  const LEDDisplayConfig *config;

  /**
   * @brief Current samples buffer pointer or @p NULL.
   */
  led_display_sample_t *samples;

  /**
   * @brief Current samples buffer depth or @p 0.
   */
  size_t depth;

  /**
   * @brief Current display.
   */
  uint8_t currentDisplay;

  /**
   * @brief Current decode mode.
   */
  uint8_t decodeMode;

  /**
   * @brief flag if conversion is stopped.
   */
  uint8_t stopped;

  /**
   * @brief Current samples buffer position or @p 0.
   */
  size_t pos_conversion;

  /**
   * @brief store time(system ticks) to guarantee the frequency or @p 0.
   */
  systime_t last;

  /**
   * @brief store time(system ticks) to guarantee the frequency @p 0.
   */
  systime_t now;

  /**
   * @brief Current conversion group pointer or @p NULL.
   */
//  const led_displaySConversionGroup  *grpp

#if (ASMO_LED_DISPLAY_USE_WAIT == TRUE) || defined(__DOXYGEN__)
  /**
   * @brief Waiting thread.
   */
  thread_reference_t        thread;
#endif

#if (ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION == TRUE) || defined(__DOXYGEN__)
  /**
   * @brief Mutex protecting the peripheral.
   */
  mutex_t                   mutex;
#endif

};

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/* Useful predefined display setups.*/
#define led_display_DisplayTestOn led_display_create_data \
(led_display_DisplayTest, 1)
#define led_display_DisplayTestOff led_display_create_data \
(led_display_DisplayTest, 0)
#define led_display_DisplayOn led_display_create_data \
(led_display_Configuration, 0x01)
#define led_display_DisplayOff led_display_create_data \
(led_display_Configuration, 0x00)
#define led_display_DecodeModeOn led_display_create_data \
(led_display_DecodeMode, 0xFF)
#define led_display_DecodeModeOff led_display_create_data \
(led_display_DecodeMode, 0x00)

#define led_display_create_data(command,value) \
(((command)&0xFF)<<8 | ((value) & 0xFF))

#define led_display_convert(i) ({ \
        unsigned short __ret=0;\
        switch (i){\
          case 0x0:\
                __ret = LED_DISPLAY_0;\
                break;\
          case 0x1:\
                __ret = LED_DISPLAY_1;\
                break;\
          case 0x2:\
                __ret = LED_DISPLAY_2;\
                break;\
          case 0x3:\
                __ret = LED_DISPLAY_3;\
                break;\
          case 0x4:\
                __ret = LED_DISPLAY_4;\
                break;\
          case 0x5:\
                __ret = LED_DISPLAY_5;\
                break;\
          case 0x6:\
                __ret = LED_DISPLAY_6;\
                break;\
          case 0x7:\
                __ret = LED_DISPLAY_7;\
                break;\
          case 0x8:\
                __ret = LED_DISPLAY_8;\
                break;\
          case 0x9:\
                __ret = LED_DISPLAY_9;\
                break;\
          case 0xa:\
                __ret = LED_DISPLAY_A;\
                break;\
          case 0xb:\
                __ret = LED_DISPLAY_B;\
                break;\
          case 0xc:\
                __ret = LED_DISPLAY_C;\
                break;\
          case 0xd:\
                __ret = LED_DISPLAY_D;\
                break;\
          case 0xe:\
                __ret = LED_DISPLAY_E;\
                break;\
          case 0xf:\
                __ret = LED_DISPLAY_F;\
                break;\
          default:\
                __ret = 0x00;\
        }\
        __ret; \
      })


/**
 * @name    Low Level driver helper macros
 * @{
 */
#if ASMO_LED_DISPLAY_USE_WAIT || defined(__DOXYGEN__)
/**
 * @brief   Resumes a thread waiting for a conversion completion.
 *
 * @param[in] led_displayp      pointer to the @p LEDDisplayDriver object
 *
 * @notapi
 */
#define led_display_spi_reset_i(led_displayp) { \
  if ((led_displayp)->thread != NULL) {         \
    Thread *tp = (led_displayp)->thread;        \
    (led_displayp)->thread = NULL;              \
    tp->p_u.rdymsg  = RDY_RESET;                \
    chSchReadyI(tp);                            \
  }                                             \
}

/**
 * @brief   Resumes a thread waiting for a conversion completion.
 *
 * @param[in] led_displayp      pointer to the @p led_displayDriver object
 *
 * @notapi
 */
#define led_display_spi_reset_s(led_displayp) { \
  if ((led_displayp)->thread != NULL) {         \
    Thread *tp = (led_displayp)->thread;        \
    (led_displayp)->thread = NULL;              \
    chSchWakeupS(tp, RDY_RESET);                \
  }                                             \
}

/**
 * @brief   Wakes up the waiting thread.
 *
 * @param[in] led_displayp      pointer to the @p led_displayDriver object
 *
 * @notapi
 */
#define led_display_spi_wakeup_isr(led_displayp) { \
  if ((led_displayp)->thread != NULL) {            \
    Thread *tp;                                    \
    tp = (led_displayp)->thread;                   \
    (led_displayp)->thread = NULL;                 \
    tp->p_u.rdymsg = RDY_OK;                       \
    chSysLockFromIsr();														 \
    chSchReadyI(tp);                               \
    chSysUnlockFromIsr();													 \
  }                                                \
}

/**
 * @brief   Wakes up the waiting thread with a timeout message.
 *
 * @param[in] led_displayp      pointer to the @p led_displayDriver object
 *
 * @notapi
 */
#define led_display_spi_timeout_isr(led_displayp) { \
  chSysLockFromIsr();                               \
  if ((led_displayp)->thread != NULL) {             \
    Thread *tp;                                     \
    tp = (led_displayp)->thread;                    \
    (led_displayp)->thread = NULL;                  \
    tp->p_u.rdymsg = RDY_TIMEOUT;                   \
    chSchReadyI(tp);                                \
  }                                                 \
  chSysUnlockFromIsr();                             \
}

#else /* !led_display_USE_WAIT */
#define led_display_spi_reset_i(led_displayp)
#define led_display_spi_reset_s(led_displayp)
#define led_display_spi_wakeup_isr(led_displayp)
#define led_display_spi_timeout_isr(led_displayp)
#endif /* !led_display_USE_WAIT */

/**
 * @brief   Common ISR code, half buffer event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *          .
 * @note    This macro is meant to be used in the low level drivers
 *          implementation only.
 *
 * @param[in] led_displayp      pointer to the @p led_displayDriver object
 *
 * @notapi
 */
#define led_display_spi_isr_half_code(led_displayp) {                                      \
  if ((led_displayp)->grpp->end_cb != NULL) {                                       \
    (led_displayp)->grpp->end_cb(led_displayp, (led_displayp)->samples, (led_displayp)->depth / 2);         \
  }                                                                         \
}

/**
 * @brief   Common ISR code, full buffer event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *          - Waiting thread wakeup, if any.
 *          - Driver state transitions.
 *          .
 * @note    This macro is meant to be used in the low level drivers
 *          implementation only.
 *
 * @param[in] led_displayp      pointer to the @p led_displayDriver object
 *
 * @notapi
 */
#define led_display_spi_isr_full_code(led_displayp) {					\
/* End conversion.*/													\
led_display_spi_lld_stop_conversion(led_displayp);						\
if ((led_displayp)->grpp->end_cb != NULL) {								\
  (led_displayp)->state = led_display_SPI_COMPLETE;						\
  if ((led_displayp)->depth > 1) {										\
	/* Invokes the callback passing the 2nd half of the buffer.*/		\
	size_t half = (led_displayp)->depth / 2;							\
	(led_displayp)->grpp->end_cb(led_displayp, (led_displayp)->samples + half, half);	\
  }																		\
  else {																\
	/* Invokes the callback passing the whole buffer.*/					\
	(led_displayp)->grpp->end_cb(led_displayp, (led_displayp)->samples, (led_displayp)->depth);	\
  }																		\
  if ((led_displayp)->state == led_display_SPI_COMPLETE)				\
	(led_displayp)->state = led_display_SPI_READY;						\
}																		\
else																	\
  (led_displayp)->state = led_display_SPI_READY;						\
(led_displayp)->grpp = NULL;											\
led_display_spi_wakeup_isr(led_displayp);								\
																		\
}

/**
 * @brief   Common ISR code, error event.
 * @details This code handles the portable part of the ISR code:
 *          - Callback invocation.
 *          - Waiting thread timeout signaling, if any.
 *          - Driver state transitions.
 *          .
 * @note    This macro is meant to be used in the low level drivers
 *          implementation only.
 *
 * @param[in] led_displayp      pointer to the @p led_displayDriver object
 * @param[in] err       platform dependent error code
 *
 * @notapi
 */
#define led_display_spi_isr_error_code(led_displayp, err) {                                \
  led_display_spi_lld_stop_conversion(led_displayp);                                        \
  if ((led_displayp)->grpp->error_cb != NULL) {                                     \
    (led_displayp)->state = led_display_SPI_ERROR;                                          \
    (led_displayp)->grpp->error_cb(led_displayp, err);                                      \
    if ((led_displayp)->state == led_display_SPI_ERROR)                                     \
      (led_displayp)->state = led_display_SPI_READY;                                        \
  }                                                                         \
  (led_displayp)->grpp = NULL;                                                      \
  led_display_spi_timeout_isr(led_displayp);                                               \
}
/** @} */

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

extern LEDDisplayDriver LEDDISPD;

#ifdef __cplusplus
extern "C" {
#endif
void ledDisplayInit(void);
void ledDisplayObjectInit(LEDDisplayDriver *led_displayp);
void ledDisplayStart(LEDDisplayDriver *led_displayp, const LEDDisplayConfig *config);
void ledDisplayStop(LEDDisplayDriver *led_displayp);
void ledDisplaySetEnabled(LEDDisplayDriver *led_displayp, uint16_t enabled);
void ledDisplaySetDisplaytest(LEDDisplayDriver *led_displayp, uint16_t enabled);
void ledDisplaySetScanlimit(LEDDisplayDriver *led_displayp, uint16_t maxDisplay);
void ledDisplaySetIntensity(LEDDisplayDriver *led_displayp, uint16_t intensity);
void ledDisplaySetEnableDecode(LEDDisplayDriver *led_displayp, uint16_t enabled);
void ledDisplayClear(LEDDisplayDriver *led_displayp);
void ledDisplaySetDisplay(LEDDisplayDriver *led_displayp, uint8_t data);
void ledDisplayChangeDisplay(LEDDisplayDriver *led_displayp, uint8_t display);
void ledDisplayChangeDisplayNext(LEDDisplayDriver *led_displayp);
void ledDisplayChangeDisplayPrev(LEDDisplayDriver *led_displayp);
void ledDisplaySendData(LEDDisplayDriver *led_displayp, size_t n, uint16_t *data);
void ledDisplayWrite(LEDDisplayDriver *led_displayp, size_t n, const void *txbuf);
#if ASMO_LED_DISPLAY_USE_MUTUAL_EXCLUSION == TRUE
void ledDisplayAcquireBus(LEDDisplayDriver *led_displayp);
void ledDisplayReleaseBus(LEDDisplayDriver *led_displayp);
#endif
#ifdef __cplusplus
}
#endif

#endif /* ASMO_USE_LED_DISPLAY == TRUE */

#endif /* ASMO_LED_DISPLAY_H */

/** @} */
