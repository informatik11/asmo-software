/*
 * ASMO_dataStorage_register.h
 *
 *  Created on: Nov 3, 2012
 *      Author: embedded (?? Christoph Becker ??)
 */

#ifndef ASMO_DATASTORAGE_REGISTER_H_
#define ASMO_DATASTORAGE_REGISTER_H_

typedef struct ds_TSReg ds_TSReg;
#include "SmartECLA_allHeaders.h"

/**
 * Enum of aggregate functions of a time series
 */
typedef enum {
	dsTSAggrAvg,
	dsTSAggrMin,
	dsTSAggrMax
} ds_TSAggregate;

/**
 * Structure to store the configuration of a time series
 */
struct ds_TSReg {
		unsigned int messageID;
		unsigned int windowSize; // amount of values stored
		unsigned int distance; // size of gap between two stored values: skipped values = distance -1
		unsigned int messageCounter; // count incoming values to meet gap
		semaphore_t *sem; // for mutual access of safety model and framework
		ASMO_Measurement* storage; // circular storage of time series
		ASMO_Measurement* head; // points to latest added value
		uint8_t filledonce; // init = 0, set to 1 when #windowSize values were added -> prevent that models can fetch incomplete window
		uint8_t use_min;
		uint8_t use_max;
		uint8_t use_avg;
		ASMO_Measurement min;
		ASMO_Measurement max;
		ASMO_Measurement avg;
		ds_TSReg *next;
};

/**
 * Structure to store the configuration of a extremum
 */
typedef struct {
		unsigned int messageID;
		ASMO_Measurement* extremum;
		unsigned char type; // 1 = MAX ; 0 = MIN
		unsigned char* overwrite; // 1 = model wants new extremum to be recorded
		unsigned char* filledonce; // init = 0, set to 1 when #windowSize values were added -> prevent that models can fetch incomplete window
} ds_EXReg;

/**
 * register time series
 */
int ds_registerTS(ds_TSReg request);

/**
 * register extremum
 */
int ds_registerEX(ds_EXReg request);

#endif /* ASMO_DATASTORAGE_REGISTER_H_ */
