/**
 * \file ASMO_ts_layer.h
 * Header File of the ASMO TimeStamp (TS) Layer
 *
 * Created on: 24.02.2011
 * Modified on: 05.03.2011
 * Author: Mathias Obster
 */

#ifndef _ASMO_TIMESTAMP_H_
#define _ASMO_TIMESTAMP_H_

/*===========================================================================*/
/* Data structures and types.                                                */
/*===========================================================================*/

/**
 * @brief Status of the timeservice - since it might not be enabled at first
 */
typedef enum {
	TS_INITIALIZED = 0, //System just started without any syncevent received yet
	TS_RUNNING = 1      //At least one syncmessage has been received
} ts_service_status;

/*===========================================================================*/
/* Macros.                                                                   */
/*===========================================================================*/

/**
 * @brief A zeroed out timestamp.
 */
#define TS_ZERO_VALUE {0,0}

#define TS_SYNCMESSAGE_ID   ID_TIME_BEACON
#define TS_MAM              0xFFFFFFFF

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

	uint32_t tsGenerateIncomingTS(timestamp_t incoming);

    int64_t tsSubtractTS(timestamp_t *a, timestamp_t *b);

    void tsAddNanoseconds(timestamp_t *ts, uint64_t ms);

    void tsAddMilliseconds(timestamp_t *ts, uint64_t ms);

    void tsAddSeconds(timestamp_t *ts, uint64_t s);

    timestamp_t tsGetTS(void);

    void tsSetZero(timestamp_t *ts);

    bool tsGreaterEquals(timestamp_t *a, timestamp_t *b);

    uint64_t tsToEpoch(timestamp_t *ts);


    /* LEGACY FUNCTIONS */

    void tsProcessSyncevent(uint32_t highData, uint32_t lowData);
    void tsGenerateIncomingTimestamp(uint32_t dummy);

#ifdef __cplusplus
}
#endif

#endif /* _ASMO_TIMESTAMP_H_ */

/** @} */
