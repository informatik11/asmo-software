/*! \file SAM7_lcd.h
 *  \brief Header File of SAM7_lcd.c
 *
 *  \author     James P. Lynch
 *  \author		Felix Gathmann
 *  \author		Lehrstuhl f�r Informatik 11 -  RWTH Aachen
 *  \date		2008
 */

#ifndef _SAM7_LCD_H
#define _SAM7_LCD_H

// Begin of file -------------------------------------------------------------
//constants
#define BIT2  0x00000004
#define BIT12 0x00001000
#define BIT20 0x00100000

#define DISON 0xAF // Display on
#define DISOFF 0xAE // Display off
#define DISNOR 0xA6 // Normal display
#define DISINV 0xA7 // Inverse display
#define COMSCN 0xBB // Common scan direction
#define DISCTL 0xCA // Display control
#define SLPIN 0x95 // Sleep in
#define SLPOUT 0x94 // Sleep out
#define PASET 0x75 // Page address set
#define CASET 0x15 // Column address set
#define DATCTL 0xBC // Data scan direction, etc.
#define RGBSET8 0xCE // 256-color position set
#define RAMWR 0x5C // Writing to memory
#define RAMRD 0x5D // Reading from memory
#define PTLIN 0xA8 // Partial display in
#define PTLOUT 0xA9 // Partial display out
#define RMWIN 0xE0 // Read and modify write
#define RMWOUT 0xEE // End
#define ASCSET 0xAA // Area scroll set
#define SCSTART 0xAB // Scroll start set
#define OSCON 0xD1 // Internal oscillation on
#define OSCOFF 0xD2 // Internal oscillation off
#define PWRCTR 0x20 // Power control
#define VOLCTR 0x81 // Electronic volume control
#define VOLUP 0xD6 // Increment electronic control by 1
#define VOLDOWN 0xD7 // Decrement electronic control by 1
#define TMPGRD 0x82 // Temperature gradient set
#define EPCTIN 0xCD // Control EEPROM
#define EPCOUT 0xCC // Cancel EEPROM control
#define EPMWR 0xFC // Write into EEPROM
#define EPMRD 0xFD // Read from EEPROM
#define EPSRRD1 0x7C // Read register 1
#define EPSRRD2 0x7D // Read register 2
#define NOP 0x25 // NOP instruction
#define BKLGHT_LCD_ON 1
#define BKLGHT_LCD_OFF 2

// Booleans
#define NOFILL 0
#define FILL 1

// 12-bit color definitions
#define WHITE 0xFFF
#define BLACK 0x000
#define RED 0xF00
#define GREEN 0x0F0
#define BLUE 0x00F
#define CYAN 0x0FF
#define MAGENTA 0xF0F
#define YELLOW 0xFF0
#define BROWN 0xB22
#define ORANGE 0xFA0
#define PINK 0xF6A

// Font sizes
#define SMALL 0
#define MEDIUM 1
#define LARGE 2
// hardware definitions
#define SPI_SR_TXEMPTY

// defines for SPI driver
// SPID1(chibiOS) = SPI0(SAM7). CS: Pin A12
#define LCD_SPI_DRIVER				SPID1
#define	LCD_SPI_CS_PORT				AT91C_BASE_PIOA
#define	LCD_SPI_CS_PIN				12

// digital I/O lines for lcd control
#define LCD_RESET_PORT				AT91C_BASE_PIOA
#define LCD_RESET_PIN				2
#define LCD_BACKLIGHT_PORT			AT91C_BASE_PIOB
#define LCD_BACKLIGHT_PIN			20

//extern SPIConfig lcdConfig;


//prototypes
/*! \fn SmartECLALCDBackground(void)
    \brief Creates a screen with some default information about the project
           and built number on the LCD for SmartECLA project.
*/
void SmartECLALCDBackground(void);


/*! \fn initLCD(void)
    \brief initializes SPI, adds LCD device and initializes LC Display
    \return RETURN_SUCCESS
*/
void lcd_init(void);

/*! \fn backlightLCD(unsigned char state)
    \brief switches LCD backlight on
    \param state 1 = on, other values = off
*/
void backlightLCD(unsigned char state);

/*! \fn write130x130BmpLCD(void)
    \brief shows bitmap saved in global variable bmp[] on LCD
*/
void write130x130BmpLCD(void);

/*! \fn clearScreenLCD(void)
    \brief sets complete display to black
*/
void clearScreenLCD(void);

/*! \fn setPixelLCD(int x, int y, int color)
    \brief sets one pixel of display to specified colour
    \param x x-coordinate of pixel
    \param y y-coordinate of pixel
    \param color one of the defined colour constants
*/
void setPixelLCD(int x, int y, int color);

/*! \fn setLineLCD(int x0, int y0, int x1, int y1, int color)
    \brief draws one line onto the display
    \param x0 x-coordinate of pixel to start from
    \param y0 y-coordinate of pixel to start from
    \param x1 x-coordinate of pixel to end in 
    \param y1 y-coordinate of pixel to end in
    \param color one of the defined colour constants
*/
void setLineLCD(int x0, int y0, int x1, int y1, int color);

/*! \fn setRectLCD(int x0, int y0, int x1, int y1, unsigned char fill, int color)
    \brief draws one rect defined by two corners onto the display
    \param x0 x-coordinate of first corner
    \param y0 y-coordinate of first corner
    \param x1 x-coordinate of second corner
    \param y1 y-coordinate of second corner
    \param fill FILL = filled, other values = only outline
    \param color one of the defined colour constants
*/
void setRectLCD(int x0, int y0, int x1, int y1, unsigned char fill, int color);

/*! \fn setCircleLCD(int x0, int y0, int radius, int color)
    \brief draws one circle onto the display
    \param x0 x-coordinate of centre
    \param y0 y-coordinate of centre
    \param radius radius of the circle
    \param color one of the defined colour constants
*/
void setCircleLCD(int x0, int y0, int radius, int color);

/*! \fn putCharLCD(char c, int x, int y, int size, int fColor, int bColor)
    \brief draws one character onto the display
    \param c character to be drawn
    \param x x-coordinate
    \param y y-coordinate
    \param size font size, possible values are SMALL or MEDIUM or LARGE
    \param fcolor font color: one of the defined colour constants
    \param bcolor background color: one of the defined colour constants
*/
void putCharLCD(char c, int x, int y, int size, int fColor, int bColor);

/*! \fn putStrLCD(char *pString, int x, int y, int Size, int fColor, int bColor)
    \brief draws a complete string onto the display
    \param pString string to be drawn
    \param x x-coordinate
    \param y y-coordinate
    \param size font size, possible values are SMALL or MEDIUM or LARGE
    \param fcolor font color: one of the defined colour constants
    \param bcolor background color: one of the defined colour constants
*/
void putStrLCD(char *pString, int x, int y, int size, int fColor, int bColor);

/*! \fn putAlarmLCD(char *pString)
 * \brief puts alarm string in large red font on lcd.
 * \param pString The alarm string.
 */
void putAlarmLCD(char *pString);

/*! \fn delay(unsigned long a)
    \brief very simple delay function
    \param a variable that is decremented until reaching zero
*/
void delay(unsigned long a);

extern const unsigned char bmp[];
// End of file ---------------------------------------------------------------

#endif
