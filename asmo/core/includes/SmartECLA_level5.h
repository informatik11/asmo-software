/**
 * \file	SmartECLA_level5.h
 * \brief	Includes all SmartECLA headers of level 5.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL5_H_
#define SMARTECLA_LEVEL5_H_

/*
 *  ##################################################################
 *  # Level 5 - Declarations                                         #
 *  ##################################################################
 */

/**
 * Level 50 - Headers of Non-OS-conform drivers, Non-ASMO wrappers
 * (Level 50 shall be removed soon)
 */

/**
 * Level 52 - Model Headers (Setting USE Defines)
 */
#include "models.h"

/**
 * Level 51 - ASMO declarations / wrappers (Reading USE Defines)
 */
#include "ASMO_safety.h"
#include "ASMO_measurements.h"				// Great data provisioning struct
#include "ASMO_safetySet.h"
#include "ASMO_simulinkGet.h"
#include "ASMO_simulinkFactors.h"
#include "ASMO_datastorageGetTS.h"
#include "ASMO_datastorageNewTS.h"
#include "ASMO_ETH.h"
#include "ASMO_heartbeat.h"
#include "ASMO_buttons.h"
#include "ASMO_filesystem.h"
#include "ASMO_idWrappers.h"
#include "ASMO_sevenSeg.h"
#include "ASMO_simulinkWrappers.h"
#include "ASMO_switch.h"
#include "ASMO_timestamp.h"
#include "ASMO_util.h"
#include "ASMO_canUtils.h"
#include "ASMO_net.h"
#include "ASMO_LED.h"
#include "ASMO_Communication.h"
#include "ASMO_adc.h"
#include "ASMO_dac.h"
#include "ASMO_ledDisplay.h"
#include "ASMO_frontboard.h"
#include "ASMO_serial_usb.h"
// #include "ASMO_lcd.h"                    //XXX: MH: Excluded

/**
 * Level 53 - MDL declarations.
 */
#include "MDL_allHeaders.h"

#endif /* SMARTECLA_LEVEL5_H_ */
