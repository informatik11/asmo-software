/**
 * \file ASMO_idWrappers.h
 * \brief Contains some wrapper functions that adapt the chibi os hardware functionality
 * for smartECLA issues.
 *
 * \date 24.08.2011
 * \author Florian Goebe
 */

#ifndef ASMO_IDWRAPPER_H_
#define ASMO_IDWRAPPER_H_

#define ID_ACQUIRE_MISSING	0x02000F00

char ASMO_sendLocalMeasurementToCan(ASMO_Measurement *measurement);
void ASMO_storeLocalMeasurement(ASMO_Measurement measurement);
void ASMO_storeLocalMeasurementOnCard(ASMO_Measurement measurement);

void ASMO_newMeasurement(ASMO_Measurement measurement);
uint8_t device_getNumber(void);

bool checkThreshold(uint32_t n, uint32_t o, uint32_t t);

#endif /* ASMO_IDWRAPPER_H_ */
