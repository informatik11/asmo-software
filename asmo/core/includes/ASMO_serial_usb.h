/**
 *
 * @file	ASMO_serial_usb.h
 * @brief 	Header file for everything serial over USB related
 * @author 	Moritz Huellmann
 * @date 	24.05.22
 *
 */

#ifndef _SRC_INCLUDES_ASMO_SERIAL_USB_H_
#define _SRC_INCLUDES_ASMO_SERIAL_USB_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief   Puts out a formatized string over USB
 *
 * @param fmt   Format string
 * @param ...   Variables to populate the format string in @p fmt with
 *
 * @return Error code or number of printed chars
 * @retval -1 on error
 * @retval 0..251 otherwise
 */
int asmoSerialPutParametrized(const char *fmt, va_list ap);

/**
 * @brief   Puts out a formatized string over USB
 * 
 * @param fmt   Format string
 * @param ...   Variables to populate the format string in @p fmt with
 * 
 * @return Error code or number of printed chars
 * @retval -1 on error
 * @retval 0..251 otherwise
 */
int asmoSerialPut(const char *fmt, ...);

/**
 * @brief Initializes the Serial via USB system.
 */
void asmoSerialInit(void);

#ifdef __cplusplus
}
#endif

#endif /* _SRC_INCLUDES_ASMO_SERIAL_USB_H_ */
