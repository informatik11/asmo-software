/**
 * \file	SmartECLA_level6.h
 * \brief	Includes all SmartECLA headers of level 6.
 *
 * \date	01.12.2011
 * \author	Goebe, Embedded Software Laboratory, RWTH Aachen University, Germany
 */

#ifndef SMARTECLA_LEVEL6_H_
#define SMARTECLA_LEVEL6_H_

/*
 *  ##################################################################
 *  # Level 6 - Included Definitions                                 #
 *  ##################################################################
 */
// -- currently empty --
// some sources include special definitions on theirselves (e.g. getter headers of Simulink models)

/**
 * Level 61 - Guards that check for errors and incompabilities in the macro defines
 */
#include "guards.h"


#endif /* SMARTECLA_LEVEL6_H_ */
