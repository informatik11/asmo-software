/**
 * \file ASMO_util.h
 * Header file of ASMO_util.c
 *
 */

#ifndef _ASMO_UTIL_H
#define _ASMO_UTIL_H

char hex2ascii(unsigned char input);

unsigned char ascii2hex(char input);

unsigned char hexStringTo8Bit(char *ptr);

unsigned short hexStringTo16Bit(char *ptr);

void hex16BitToAscii(unsigned short value, unsigned char *buffer);

void hex8BitToAscii(unsigned char value, unsigned char *buffer);

unsigned char a2i(unsigned char a);

int parseInteger(char *ptr, size_t n);

double parseDouble(unsigned char *ptr);

uint8_t calcCRC(unsigned char *data, uint16_t length);

#endif
