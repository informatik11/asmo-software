/**
 *  \file ASMO_filesystem.h
 *  \brief ASMO Filesystem
 *
 *  Header file for ASMO_filesystem.c.
 *
 *  \author		Florian G�be, Andre Stollenwerk
 *  \author		Lehrstuhl f�r Informatik 11 -  RWTH Aachen
 *  \date		2008 - 2012
 */

#ifndef _ASMO_FILESYSTEM_H
#define _ASMO_FILESYSTEM_H
// Begin of file -------------------------------------------------------------

// Defines for alarm on full disk
#define FS_FREECLUSTERS_LOWERBOUND	125 //clusters, this is 500 kB at a clustersize of 4096B
#define FS_SYNC_EVERY_MS			500
#define FS_WRITEBUFFERSIZE			300 // ASMO_Measurements: 12 Byte

// 1: on, 0: off
#define FS_ALARM_BEEP				0 // not yet implemented
#define FS_ALARM_CAN				1
#define FS_ALARM_LCD				1

#define FS_FILENAME					"results.txt"

// Defines for MMC / SPI communication
#define FS_SPI_DRIVER				SPID1 // = SPI0 on STM32F7 (ChibiOS starts counting at 1)
#define FS_SPI_CS_PORT				GPIOB
#define FS_SPI_CS_PIN				2 // MMC CS is Pin B2

// Makros
#define FS_disableOnFail(fres,file)	if(fres){FS_disableLogging(file);}

/*###################################################
 # Function Declarations                           #
 ###################################################  */

void FS_init(void);
void FS_writeTimestamp(unsigned char hour, unsigned char minute,
						unsigned char second);
bool FS_writeTimestampToFile(FIL *file);
bool FS_writeFATASCII(FIL *file, ASMO_Measurement *measurement);
char FS_disableIfFull(FIL *file);
void FS_sync(FIL *file);
void FS_disableLogging(FIL *file);
void FS_putToBuffer(ASMO_Measurement *measurement);
bool FS_mmcIsWriteProtected(void);
bool FS_mmcIsInserted(void);
bool FS_putLogHeaderToFile(FIL *file);

/**
 * MMC driver object (Defined in ASMO_filesystem.c)
 */
extern MMCDriver FS_mmc;

//! Flag that indicates, if the FS has been initialized.
extern char fileSystemInitialized;

/**
 * @brief Mutex that protects access to the FAT FS module.
 */
extern mutex_t FS_Mutex;

extern ASMO_Measurement fsWriteBuffer[];
extern unsigned char fsTimestampString[];
extern bool fsWriteTimestamp;

extern unsigned int FS_bufIn, FS_bufOut; // indices, not addresses (easier to ring-count)

#endif

