/*
 * ASMO_dataStorage_storage.h
 *
 *  Created on: Nov 3, 2012
 *      Author: embedded (?? Christoph Becker ??)
 */

#ifndef ASMO_DATASTORAGE_STORAGE_H_
#define ASMO_DATASTORAGE_STORAGE_H_
#include "SmartECLA_allHeaders.h"

// Header to provide access to storage for hooks in ASMO_CAN

uint8_t ds_newTS(ASMO_Measurement *m, ds_TSReg *reg);

uint8_t ds_getTS(ASMO_Measurement *m, ds_TSReg *reg);

uint8_t ds_getTSAggregate(ds_TSAggregate a, ASMO_Measurement *m, ds_TSReg *reg);

#endif /* ASMO_DATASTORAGE_STORAGE_H_ */
