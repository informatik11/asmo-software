ASMOSRCDIR 		:= $(ASMODIR)/core/src
ASMOINCDIR 		:= $(ASMODIR)/core/includes
ASMOGENSRCDIR   := $(ASMODIR)/generated/src
ASMOGENINCDIR   := $(ASMODIR)/generated/includes

include $(MDLDIR)/mdl.mk
include $(MODELSDIR)/models.mk

ASMOCSRC := \
	$(ASMOSRCDIR)/ASMO_buttons.c \
	$(ASMOSRCDIR)/ASMO_canUtils.c \
	$(ASMOSRCDIR)/ASMO_dataStorage_storage.c \
	$(ASMOSRCDIR)/ASMO_idWrappers.c \
	$(ASMOSRCDIR)/ASMO_safety.c \
	$(ASMOSRCDIR)/ASMO_simulinkWrappers.c \
	$(ASMOSRCDIR)/ASMO_switch.c \
	$(ASMOSRCDIR)/ASMO_timestamp.c \
	$(ASMOSRCDIR)/ASMO_LED.c \
	$(ASMOSRCDIR)/ASMO_util.c \
	$(ASMOSRCDIR)/ASMO_ETH.c \
	$(ASMOSRCDIR)/ASMO_heartbeat.c \
	$(ASMOSRCDIR)/ASMO_serial_usb.c \
	$(ASMOSRCDIR)/ASMO_sevenSeg.c \
	$(ASMOSRCDIR)/ASMO_Communication.c \
	$(ASMOSRCDIR)/ASMO_filesystem.c \
	$(ASMOSRCDIR)/ASMO_adc.c \
	$(ASMOSRCDIR)/ASMO_dac.c \
	$(ASMOSRCDIR)/ASMO_ledDisplay.c \
	$(ASMOSRCDIR)/ASMO_frontboard.c \
	$(ASMOSRCDIR)/ASMO_usbcfg.c
	# $(ASMOSRCDIR)/ASMO_lcd.c
	

ASMOGENSRC := \
	$(ASMOGENSRCDIR)/ASMO_measurements.c \
	$(ASMOGENSRCDIR)/ASMO_datastorageGetTS.c \
	$(ASMOGENSRCDIR)/ASMO_dataStorageNewTS.c \
	$(ASMOGENSRCDIR)/ASMO_net.c \
	$(ASMOGENSRCDIR)/ASMO_safetySet.c \
	$(ASMOGENSRCDIR)/ASMO_simulinkFactors.c \
	$(ASMOGENSRCDIR)/ASMO_simulinkGet.c

# Shared variables.
ALLCSRC += 	$(ASMOCSRC) \
			$(ASMOGENSRC)
ALLINC  += 	$(ASMOINCDIR) \
			$(ASMOGENINCDIR)
