classdef(Enumeration) message_id < Simulink.IntEnumType
    enumeration
    {% for m in messages_expanded %}
        {{ m.get_build_name() }}({{ m.get_build_id() }}),
    {% endfor %}
		EMPTY(0)
	end
	methods( Static = true)
		function retVal = addClassNameToEnumNames()
			retVal = true;
		end
		function retVal = getDefaultValue()
			retVal = message_id.EMPTY;
		end
	end
end
