{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}

#ifndef ASMO_MEASUREMENTS_H_
#define ASMO_MEASUREMENTS_H_

#include "SmartECLA_allHeaders.h"

//! Mask for message-Mailbox. It fits on all used IDs.
extern unsigned long message_mailbox_mask;

//! Stores the received ASMO_Measurement in ::measurements
void ASMO_storeMeasurement(ASMO_Measurement *measurement);

//! Returns true, if copied the stored ASMO_Measurement in ::measurements
bool ASMO_getMeasurement(uint32_t ID, ASMO_Measurement **measurement, ASMO_MeasurementConfig **config);

{% for mname, m in messages %}
{% if m.is_expandable() %}
#if defined({{ m.get_use_name() }}) || defined({{ m.get_acquire_name() }})
    {% for n in m.get_expanded_msgs() %}
    #define {{ n.get_use_name() }}
    {% endfor %}
#endif
{% else %}
#if !defined({{ m.get_use_name() }}) && defined({{ m.get_acquire_name() }})
    #define {{ m.get_use_name() }}
#endif
{% endif %}
{% endfor %}

typedef struct {
{% for n in messages_expanded %}
    {% set lower_name = n.get_name_without_prefix().lower() %}
    {# if either expanded ID is USE-d or non-expanded id is ACUQUIRE-d #}
    #if defined({{ n.get_use_name() }}) || defined({{ n.get_acquire_name() }})
        ASMO_Measurement {{ lower_name }};
        unsigned int {{ lower_name }}_minimum;
        unsigned int {{ lower_name }}_maximum;
    #endif
{% endfor %}
} ASMO_Data;

typedef struct {
{% for m in messages_expanded %}
    {% set lower_name = m.get_name_without_prefix().lower() %}
    #ifdef {{ m.get_acquire_name() }}
        {{ macros.msg_description_with_id(m) }}
        ASMO_MeasurementConfig {{ lower_name}}_config;
    #endif
{% endfor %}
} ASMO_Data_Config;

//! Holds the current medical data.
extern ASMO_Data measurements;
extern ASMO_Data_Config acquire_measurements;

// Find any used ID:
{% for n in messages_expanded %}
#if !defined(ANY_USED_MESSAGE_ID) && defined({{ n.get_use_name() }})
#define ANY_USED_MESSAGE_ID {{ macros.expand_id(n) }}
#endif
{% endfor %}

#if !defined(ANY_USED_MESSAGE_ID)
#define ANY_USED_MESSAGE_ID 0
#endif

#endif /* ASMO_MEASUREMENTS_H_ */
{% endblock %}
