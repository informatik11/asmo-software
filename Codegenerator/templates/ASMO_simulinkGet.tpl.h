{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}

#ifndef ASMO_SIMULINK_GET_H
#define ASMO_SIMULINK_GET_H

#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

bool simulink_getMeasurement(uint32_t id, double *value,
                             timestamp_t *timestamp);

#endif /* ASMO_SIMULINK_GET_H */
{% endblock %}
