{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#ifndef ASMO_DATASTORAGE_GET_TS_H
#define ASMO_DATASTORAGE_GET_TS_H
#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

uint8_t ASMO_getTSMeasurement(uint32_t id, uint8_t index, ds_TSReg *reg);

typedef struct {
{% for m in messages_expanded %}
    {% set lower_name = m.get_name_without_prefix().lower() %}
    #ifdef {{ m.get_ts_name() }}
        {{ macros.msg_description_with_id(m) }}
        ds_TSReg *{{ lower_name }};
        uint8_t num_{{ lower_name }};
    #endif
{% endfor %}
} ASMO_Timeseries;

extern ASMO_Timeseries ds_TSArray;

#endif // ASMO_DATASTORAGE_GET_TS_H
{% endblock %}
