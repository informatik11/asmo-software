{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#ifndef MATLAB_MEX_FILE
#include "ASMO_datastorageGetTS.h"
#endif

ASMO_Timeseries ds_TSArray = {
{% for m in messages_expanded %}
    {% set lower_name = m.get_name_without_prefix().lower() %}
    #ifdef {{ m.get_ts_name() }}
        .{{ lower_name }} = 0,
        .num_{{ lower_name }} = 0,
    #endif
{% endfor %}
};

uint8_t ASMO_getTSMeasurement(uint32_t id, uint8_t index, ds_TSReg *reg) {
    uint8_t i=0;
    switch (id) {
    {% for m in messages_expanded %}
    #ifdef {{ m.get_ts_name() }}
        {% set lower_name = m.get_name_without_prefix().lower() %}
            case {{ macros.expand_id(m) }}:
                {{ macros.msg_description_with_id(m) }}
                if (index < ds_TSArray.num_{{ lower_name }}) {
                    reg = ds_TSArray.{{ lower_name }};
                    while(reg->next && i<index) {
                        reg = reg->next;
                        ++i;
                    }
                    return i==index;
                }
                return 0;
            break;
    #endif
    {% endfor %}
    }

    (void)reg;
    (void)index;
    (void)i;

    return 0;
}
{% endblock %}
