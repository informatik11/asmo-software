{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#include "SmartECLA_allHeaders.h"

msg_t ASMO_safetySet(ASMO_safetyConfig *sc) {
    switch (sc->id) {
    {% for m in messages_expanded %}
        {% set lower_name = m.get_name_without_prefix().lower() %}
        #ifdef {{ m.get_ts_name() }}
            case {{ macros.expand_id(m) }}:
                {{ macros.msg_description_with_id(m) }}
				if (sc->command == safetyCmdSetMin) {
					measurements.{{ lower_name }}_minimum = sc->value;
					if (measurements.{{ lower_name }}.value<sc->value) {
						measurements.{{ lower_name }}.value=sc->value;
						return 1;
					}
				} else if (sc->command == safetyCmdSetMax) {
					measurements.{{ lower_name }}_maximum = sc->value;
					if (measurements.{{ lower_name }}.value>sc->value) {
						measurements.{{ lower_name }}.value=sc->value;
						return 1;
					}
				}
            break;
        #endif
    {% endfor %}
        default:
            (void)sc;
            break;
    }

    return 0;
}



{% endblock %}
