{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#ifndef MATLAB_MEX_FILE
#include "ASMO_datastorageNewTS.h"
#endif

uint8_t ASMO_newTSMeasurement(ASMO_Measurement *m) {
    ds_TSReg *reg;
    uint8_t num = 0;
    switch (m->messageId) {
    {% for m in messages_expanded %}
        #ifdef {{ m.get_ts_name() }}
            {% set lower_name = m.get_name_without_prefix().lower() %}
            case {{ macros.expand_id(m) }}:
                {{ macros.msg_description_with_id(m) }}
                reg = ds_TSArray.{{ lower_name }};
                num = ds_TSArray.num_{{ lower_name }};
            break;
        #endif
    {% endfor %}
    }

	if (num>0) {
		while(reg) {
			ds_newTS(m, reg);
            reg = reg->next;
        }
        return 1;
    }
    return 0;
}

int16_t ASMO_registerTS(ds_TSReg *reg) {
	reg->head = reg->storage - 1;
	chSemObjectInit((reg->sem), 1); /* Semaphore initialization before use */
	switch (reg->messageID) {
    {% for m in messages_expanded %}
        {% set lower_name = m.get_name_without_prefix().lower() %}
        #ifdef {{ m.get_ts_name() }}
            case {{ macros.expand_id(m) }}:
                if (ds_TSArray.num_{{ lower_name}} == 0) {
                    ds_TSArray.{{ lower_name }} = reg;
                } else {
                    ds_TSReg *r = ds_TSArray.{{ lower_name }};
                    while(r->next) r = r->next;
                    r->next = reg;
                }
                ++ds_TSArray.num_{{ lower_name }};
                return ds_TSArray.num_{{ lower_name }} - 1;
                break;
        #endif
    {% endfor %}
	}

    return -1;
}
{% endblock %}
