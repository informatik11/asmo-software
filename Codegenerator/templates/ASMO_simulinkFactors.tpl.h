{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}

#ifndef ASMO_SM_FACTORS_H
#define ASMO_SM_FACTORS_H

#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

uint32_t simulink_getFactor(uint32_t id, double value);

#endif /* ASMO_SM_FACTORS_H */
{% endblock %}
