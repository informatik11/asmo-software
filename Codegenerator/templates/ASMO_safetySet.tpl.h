{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}

#ifndef ASMO_SAFETYSET_H
#define ASMO_SAFETYSET_H

msg_t ASMO_safetySet(ASMO_safetyConfig *sc);

#endif /* ASMO_SAFETYSET_H */
{% endblock %}
