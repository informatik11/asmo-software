{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#ifndef MATLAB_MEX_FILE
#include "ASMO_simulinkGet.h"
#endif
bool simulink_getMeasurement(uint32_t id, double *value, timestamp_t *timestamp) {
    ASMO_Measurement m;
    uint32_t val;
    switch (id) {
{% for m in messages_expanded %}
{{ macros.if_msg_in_use_expand(m) }}
    case {{ macros.expand_id(m) }}:
        m = measurements.{{ m.get_name_without_prefix().lower() }};
        val = m.value;
        *value = (double)val * {{ m.scale|defaultIfNone(1.0) }};
        //*timestamp = m.timestamp; // TODO: port
        return true;
#endif
{% endfor %}
        default:
            break;
    }

    return false;
}
{% endblock %}
