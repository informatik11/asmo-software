{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#ifndef SMARTECLA_IDS_H_
#define SMARTECLA_IDS_H_

// Block IDs for filtering received messages
#define BLOCK_ID_MASK			(0xFF << 24)
{% for p in priority %}
#define BLOCK_ID_{{ p.get_name() }} ({{p.get_hex_id()}} << {{p.shift}})
{% endfor %}

// Device IDs for filtering received messages
#define DEVICE_ID_MASK			(0xFF << 16)
{% for d in devices %}
#define DEVICE_ID_{{ d.get_name() }} ({{d.get_hex_id()}} << {{d.shift}})
{% endfor %}

// one default shifter
#define DEFAULT_DEVNUMBER_SHIFT		12
// safety defines
#define SAFETY_TOO_LOW			(0x100)
#define SAFETY_TOO_HIGH			(0x101)

// shifter values (all are 12)
{% for d in devices_multiple %}
#define DEVICE_{{ d.get_name() }}_SHIFT 12
{% endfor %}

#define MESSAGE_STARTUP                         0x0C000000  // indicates how the masked bits have to be set
#define MESSAGE_DISABLED_CARDS                  0x0C000010  // Needed for building the card disabled message ID in MCU code
#define MESSAGE_STATUS_REPLY                    0x0C000020  // Needed for building the status message ID in MCU code


//! Macro to read the device number out of an ID
#define getDeviceNumber(id,shifter)			((id >> shifter) & 0xF)
//! Macro to add the device number to an id. The preset bits at the device number's position in the IDs are discarded.
#define addDeviceNumber(devNum,id,shifter)	((id & ~(0xF << shifter)) | (devNum << shifter))
enum message_id { 
    {% for mname, m in messages %}
    {{ macros.msg_description(m) }}
    {{ m.get_build_name() }} = {{ m.get_hex_id() }}, 
    {% endfor %}
};
#endif /* SMARTECLA_IDS_H_ */
{% endblock %}
