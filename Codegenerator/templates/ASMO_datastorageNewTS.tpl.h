#ifndef ASMO_DATASTORAGE_NEW_TS_H
#define ASMO_DATASTORAGE_NEW_TS_H

#ifndef MATLAB_MEX_FILE
#include "SmartECLA_allHeaders.h"
#endif

uint8_t ASMO_newTSMeasurement(ASMO_Measurement *m);
int16_t ASMO_registerTS(ds_TSReg *reg);

#endif /* ASMO_DATASTORAGE_NEW_TS_H */