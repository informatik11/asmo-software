{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#ifndef MATLAB_MEX_FILE
#include "ASMO_simulinkFactors.h"
#endif
uint32_t simulink_getFactor(uint32_t id, double value) {
    switch (id) {
{% for mname, m in messages %}
{{ macros.if_msg_in_use(m) }}
{{ macros.case_msg(m)|indent(4, first=True) }}
            return (unsigned long)(value / {{ m.scale|defaultIfNone(1.0) }});
#endif
{% endfor %}
        default:
            break;
    }
    return 0;
}
{% endblock %}
