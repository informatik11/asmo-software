{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}

#ifndef ASMO_STREAM_H
#define ASMO_STREAM_H

// include USE- and ACQUIRE-macros
#include "MDL_config.h"

/*
 * In this file we define the following macros to 1...
 * ...ASMO_USE_STREAM_<stream-name> if we USE a message from that stream
 * ...ASMO_ACQUIRE_STREAM_<stream-name> if we ACQUIRE a message from that stream
 * ...ASMO_TOUCH_STREAM_<stream-name> if we USE or ACQUIRE a message from that stream
 *
 * We define the following macros to...
 * ...ASMO_NR_USE_STREAMS: the total number of streams with messages we USE
 * ...ASMO_NR_ACQUIRE_STREAMS: the total number of streams with messages we ACQUIRE
 * ...ASMO_NR_TOUCH_STREAMS: the total number of streams with messages we USE or ACQUIRE
 */

{% for s in stream_instances %}
#if {% for m in s.messages %}defined({{ m.get_use_name() }}){{" ||\\
    "}}{% endfor %}0
#define ASMO_USE_STREAM_{{ s.get_name() }} 1
#define ASMO_TOUCH_STREAM_{{ s.get_name() }} 1
#else
#define ASMO_USE_STREAM_{{ s.get_name() }} 0
#endif
#if {% for m in s.messages %}defined({{ m.get_acquire_name() }}){{" ||\\
    "}}{% endfor %}0
#define ASMO_ACQUIRE_STREAM_{{ s.get_name() }} 1
#define ASMO_TOUCH_STREAM_{{ s.get_name() }} 1
#else
#define ASMO_ACQUIRE_STREAM_{{ s.get_name() }} 0
#endif
#ifndef ASMO_TOUCH_STREAM_{{ s.get_name() }}
#define ASMO_TOUCH_STREAM_{{ s.get_name() }} 0
#endif

{% endfor %}

/*
 *  Count the number of streams from which messages are
 *  going to be sent or received.
 */
#define ASMO_NR_USE_STREAMS ({% for s in stream_instances %}ASMO_USE_STREAM_{{ s.get_name() }} +{{"\\
                            "}}{% endfor %}0)

#define ASMO_NR_ACQUIRE_STREAMS ({% for s in stream_instances %}ASMO_ACQUIRE_STREAM_{{ s.get_name() }} +{{"\\
                                "}}{% endfor %}0)

#define ASMO_NR_TOUCH_STREAMS ({% for s in stream_instances %}ASMO_TOUCH_STREAM_{{ s.get_name() }} +{{"\\
                              "}}{% endfor %}0)

#endif /* ASMO_STREAM_H */
{% endblock %}
