{# Return a #if macro that evaluated to true when the message or
    an expanded version of this message is used (USE/ACQUIRE defined) #}
{% macro _or_use_acquire(m) %}
    {%- set exp = m.get_expanded_msgs() %}
    {%- set n = exp[0] -%}
    defined ({{ n.get_use_name() }}) 
    {%- for n in exp[1:] -%} || defined ({{ n.get_use_name() }}) {% endfor -%}
{%- endmacro %}
{% macro if_msg_in_use(m) -%}
#if defined({{ m.get_acquire_name()}}) || {{ _or_use_acquire(m) }}
{%- endmacro %}
{% macro if_msg_in_use_expand(m) -%}
#if defined({{ m.get_acquire_name()}}) || defined({{ m.get_use_name() }})
{%- endmacro %}

{# Return a #if macro that evaluates to true when the message itself
    (but not an expanded version) is used (USE/ACQUIRE defined) #}
{% macro if_only_msg_in_use(m) -%}
#if defined ({{ m.use_define_name }}) || defined({{ m.acquire_define_name }}) 
{%- endmacro %}

{# Return C code that calculates the expanded ID of the given ExpandedMessage #}
{% macro expand_id(m) -%}
    {%- if m.is_expandable() -%}
        ({{ m.get_name_unexpanded() }} | ({{ m.instanceNumber}} << 12))
    {%- else %}
        {{- m.get_build_name() }} 
    {%- endif -%}
{%- endmacro %}

{# Return C code that calculates the expanded ID of the given ExpandedMessage #}
{% macro expand_stream_message_id(s,m) -%}
    {%- if m.is_expandable() -%}
        ({{ m.get_name_unexpanded() }} | ({{ s.instance}} << 12))
    {%- else %}
        {{- m.get_build_name() }} 
    {%- endif -%}
{%- endmacro %}

{# Return a case block that matches for all Ids of the given mesage #}
{% macro case_msg(m) -%}
    {%- for n in m.get_expanded_msgs() -%}
    case {{ expand_id(n) }}:
    {% endfor -%}
{%- endmacro %}

{# Return a C comment with description of message #}
{% macro msg_description(m) -%}
//! {{ m.description|defaultIfNone("(no description)") }} 
{%- endmacro %}

{# Return a C comment with description of message #}
{% macro msg_description_with_id(m) -%}
//! {{ m.description|defaultIfNone("(no description)") }} (ID: {{ m.get_hex_id() }})
{%- endmacro %}

{# Print an IP address as IP_ADDR_INT macro #}
{% macro print_ip(ip) -%}
IP_ADDR_INT({{ ip[0]|int }}, {{ ip[1]|int }}, {{ ip[2]|int }}, {{ ip[3]|int }})
{%- endmacro %}

{# Print an IP address as LWIP IP4_ADDR macro #}
{% macro print_lwip_ip(destptr, ip) -%}
IP4_ADDR({{ destptr }}, {{ ip[0]|int }}, {{ ip[1]|int }}, {{ ip[2]|int }}, {{ ip[3]|int }})
{%- endmacro %}
