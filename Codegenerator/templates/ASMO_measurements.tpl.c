{% import 'macros.tpl' as macros %}
{% extends "base.tpl" %}
{% block content %}
#include "SmartECLA_allHeaders.h"

//! Holds the current medical data.
ASMO_Data measurements = {
    {% for m in messages_expanded %}
        #if defined({{ m.get_use_name() }}) || defined({{ m.get_acquire_name() }})
        { {{ macros.expand_id(m) }}, 0, TS_ZERO_VALUE },
        {{ m.get_min()|defaultIfNone(0)|int }},
        {{ m.get_max()|defaultIfNone(4294967295)|int }},
        #endif
    {% endfor %}
};

//! Holds the current medical data with Message configuration.
ASMO_Data_Config acquire_measurements = {
    {% for m in messages_expanded %}
        #ifdef {{ m.get_acquire_name() }}
        { 
            TIME_MS2I({{ m.intervalSending|defaultIfNone(0)|int }}),
            TS_ZERO_VALUE,
            {{ m.get_thresholdSending()|defaultIfNone(0)|int }},
            TIME_MS2I({{ m.intervalSaving|defaultIfNone(0)|int }}),
            TS_ZERO_VALUE,
            {{ m.get_thresholdSaving()|defaultIfNone(0)|int }},
            {},
            0
        },
        #endif
    {% endfor %}
};

//! Stores the received ASMO_Measurement in ::measurements.
void ASMO_storeMeasurement(ASMO_Measurement *measurement) {

    switch(measurement->messageId) {
    {% for m in messages_expanded %}
        {% set lower_name = m.get_name_without_prefix().lower() %}
        #if defined({{ m.get_use_name() }}) || defined({{ m.get_acquire_name() }})
            case {{ macros.expand_id(m) }}:
                if(ASMO_safetyCheck(measurement, measurements.{{ lower_name}}_minimum, measurements.{{ lower_name }}_maximum)) {
                    measurements.{{ lower_name }} = *measurement;
                }
            break;
        #endif
    {% endfor %}
        default:
            break;
    }
    ASMO_newTSMeasurement(measurement);
}

//! Returns true, if copied the stored ASMO_Measurement in ::measurements.
bool ASMO_getMeasurement(uint32_t ID, ASMO_Measurement **measurement, ASMO_MeasurementConfig **config) {

    bool result = false;

    switch(ID) {
    {% for mname, m in messages %}
        #ifdef {{ m.get_acquire_name() }}
        {% if m.is_expandable() %}
            {% for n in m.get_expanded_msgs() %}
            {% set lower_name = n.get_name_without_prefix().lower() %}
            case {{ macros.expand_id(n) }}:
                *measurement = &measurements.{{ lower_name }};
                *config = &acquire_measurements.{{ lower_name }}_config;
                result = true;
                break;
            {% endfor %}
        {% else %}
            {% set lower_name = m.get_name_without_prefix().lower() %}
            case {{ m.get_hex_id() }}:
                *measurement = &measurements.{{ lower_name }};
                *config = &acquire_measurements.{{ lower_name }}_config;
                result = true;
                break;
        {% endif %}
        #endif
    {% endfor %}
    }
    
    return result;
}

// No longer used. Just here for compatibility.
unsigned long message_mailbox_mask = 0;

{% endblock %}
