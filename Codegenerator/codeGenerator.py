#!/usr/bin/python3
#coding utf-8 
"""
codegenerator.py

Generate C Files and Headers from the XML configuration of the
communication system.
"""

import toml
import Parser

import argparse
import datetime
import os, sys
from jinja2 import Environment, PackageLoader


def generate(parser, output_dir):
    env = Environment(
        loader=PackageLoader("codeGenerator", "templates"),
        trim_blocks=True,
        lstrip_blocks=True
    )

    def filterDefaultIfNone(value, default=1):
        if value is None:
            return default
        return value
    env.filters['defaultIfNone'] = filterDefaultIfNone


    # sort messages by canID
    messages_sorted = sorted(parser.messages.items(), key=lambda m: m[1].get_build_id())
    
    # priorities for SmartECLA_IDs.h defines
    prios = []
    for prio in parser.priorities:
        prios.append(parser.priorities[prio])
        
    
    devices_multiple = []
    for dev in parser.devices:
        if dev.maxCount >1:
            devices_multiple.append(dev)
            

    # generate list of all expanded messages for convenience in templates
    messages_expanded = []
    for mname, m in messages_sorted:
        messages_expanded += m.get_expanded_msgs()
        



    templates = [
        "ASMO_measurements.tpl.c",
        "ASMO_measurements.tpl.h",
        "ASMO_datastorageGetTS.tpl.c",
        "ASMO_datastorageGetTS.tpl.h",
        "ASMO_dataStorageNewTS.tpl.c",
        "ASMO_datastorageNewTS.tpl.h",
        "ASMO_net.tpl.h",
        "ASMO_net.tpl.c",
        "ASMO_safetySet.tpl.c",
        "ASMO_safetySet.tpl.h",
        "ASMO_simulinkFactors.tpl.c",
        "ASMO_simulinkFactors.tpl.h",
        "ASMO_simulinkGet.tpl.c",
        "ASMO_simulinkGet.tpl.h",
        "ASMO_stream.tpl.h",
        "ASMO_RTPS.tpl.hpp",
        "ASMO_RTPS.tpl.cpp",
        "message_id.tpl.m",
        "SmartECLA_IDs.h",
    ]
    timestamp = datetime.datetime.now().isoformat()

    for tname in templates:
        output_filename = tname.replace(".tpl.", ".")

        # put files of different types in their respective directory
        _, extension = os.path.splitext(tname)
        if extension == '.m':
            intermediate_dir_name = ""
        elif extension == '.c' or extension == '.cpp':
            intermediate_dir_name = "src"
        elif extension ==  '.h' or extension == '.hpp':
            intermediate_dir_name = "includes"
        else:
            raise argparse.ArgumentTypeError("Unknown file ending of file " + tname)

        output_path = os.path.join(output_dir, intermediate_dir_name, output_filename)
        tpl = env.get_template(tname)
        # create directory, if it doesn't exist already
        os.makedirs(os.path.dirname(output_path), exist_ok=True)
        with open(output_path, "w") as outf:
            outf.write(tpl.render(
                messages=messages_sorted,
                messages_expanded=messages_expanded,
                timestamp=timestamp,
                stream_instances=parser.streams,
                priority = prios,
                devices=parser.devices,
                devices_multiple=devices_multiple,
                output_filename=output_filename
            ))

if __name__ == "__main__":
    def readable_dir(mydir):
        if not os.path.isdir(mydir):
            if os.path.exists(mydir):
                raise argparse.ArgumentTypeError("Given path is not a directory")
            try:
                os.mkdir(mydir)
            except Exception as e:
                raise argparse.ArgumentTypeError("Given path does not exist and failed to create directory") from e
        if not os.access(mydir, os.W_OK):
            raise argparse.ArgumentTypeError("Given path is not writeable")
        return mydir

    parser = argparse.ArgumentParser()
    parser.add_argument("input_directory", help="Where the input files (config.toml and toml files for devices) are stored", type=readable_dir)
    parser.add_argument("output_directory", help="Where the generated files will be stored", type=readable_dir)
    args = parser.parse_args()

    try:
        parser = Parser.Parser()
        config = toml.load(args.input_directory + "config.toml")
        parser.parse_dictionary(config, args.input_directory)
    except Parser.ParseError as err:
        print (err)
        sys.exit(1)
    generate(parser, args.output_directory)
