#!/usr/bin/python3
#coding utf-8 

import toml
import sys

class ParseError(Exception):
    pass


#check if naming convenitons are satisfied (alphanumeric capital letters or underscore allowed
def check_name(name):
    if not type(name) == str:
        raise ParseError("names have to be stored as string")
    filename = name.lower() # only to test for capital letters
    res_str = name.replace('_', '') # help to check if name is alphanumeric, underscores are allowed
        
    if not name == filename.upper(): #Test if letters are capital
        raise ParseError("Incorrect naming of %s, no lower letters are allowed" %name)
    elif not len(res_str)>0: #Test if there is at least one alphanumeric character
        raise ParseError("Incorrect naming of %s, has to contain at least one alphanumeric character" %name)
    elif not res_str.isalnum(): #Test if the string only contains alphanumeric characters or underscore
        raise ParseError("Incorrect naming of %s, only use alphanumeric character or underscore" %name)
    elif not res_str[0] == name[0]: #Test if the name starts with an underscore (not allowed)
        raise ParseError("Incorrect naming of %s, don't start with an underscore" %name)
    
#check if id exceeds maximal bit length and id is an integer
def check_id(identifier, length):
    if not type(identifier) == int:
        raise ParseError("id has to be a number that can be parsed to an int (ids are expected to be hexadecimal)")
    if len(bin(identifier)) > (length + 2): #Add 2 to number of bits allowed because 0b is added to the bit string
        raise ParseError("The ID is too long, maximum %s Bit allowed" %(length))
    
def check_unique(name, checked):
    if name in checked:
        if type(name) == int:
            pre = "ID "
        else:
            pre = "Name "
        raise ParseError(pre + "%s appears at least twice" %(name))

def parseTime(attribute):
    time = 0
    if type(attribute) == int or attribute.get("unit") == None:
        return attribute
    if not type(attribute.get("time")) in [int, float]:
        raise ParseError("time value needs to be integer or float")
    if not type(attribute.get("unit")) == str:
        raise ParseError("time unit needs to be a string")
    if not attribute.get("unit") in ["sec", "milli", "micro", "nano"]:
        raise ParseError("unknown unit, has to be 'sec', 'milli', 'mikro' or 'nano'")
    if attribute.get("unit") == "sec":
        if attribute.get("time") > 4.294:
            raise ParseError("max value for seconds is 4.294 otherwise it cannot be parsed to integer nanoseconds")
        time = attribute["time"]*pow(10,9)
    if attribute.get("unit") == "milli":
        if attribute.get("time") > 4294.967:
            raise ParseError("max value for milliseconds is 4294.967 otherwise it cannot be parsed to integer nanoseconds")
        time = attribute["time"]*pow(10,6)
    if attribute.get("unit") == "micro":
        if attribute.get("time") > 4294967.295:
            raise ParseError("max value for microseconds is 4294967.295 otherwise it cannot be parsed to integer nanoseconds")
        time = attribute["time"]*pow(10,3)
    if attribute.get("unit") == "nano":
        time= attribute["time"]
    if time == int(time):
        return int(time)
    else:
        raise ParseError("too many decimal places, needs to be an integer when transformed to nanoseconds")
    
class Priority(object):
    shift = 24
    def __init__(self, kwargs):
        self._name = kwargs["name"]
        self._id = kwargs["id"]
        self.description = kwargs["description"]
    
    def __str__(self):
        return "Priority(name = {}, id = {}, description = {})".format(self._name, self._id, self.description)
    
    def get_name(self):
        return self._name
    
    def get_id(self):
        return self._id
    
    def get_hex_id(self):
        return "{:#04x}".format(self._id)
    
    def get_shift_id(self):
        shifted = "{:#07b}".format(self._id)
        shifted = shifted + Priority.shift * "0"
        return int(shifted,2)

class Message(object):
    PREFIX = "ID_"
    ACQUIRE_PREFIX = "ACQUIRE_ID_"
    USE_PREFIX = "USE_ID_"
    USE_TS_PREFIX = "USE_TS_ID_"
    
    
    def __init__(self, kwargs):
        if "copy_from" in kwargs:
            msg = kwargs["copy_from"]
            self._name = msg._name
            self._id = msg._id
            self.description = msg.description
            self._priority = msg._priority
            self._device = msg._device
            self.scale = msg.scale
            self.unit = msg.unit
            self.intervalSending = msg.intervalSending
            self.intervalSaving = msg.intervalSaving
            self._thresholdSending = msg._thresholdSending
            self._thresholdSaving = msg._thresholdSaving
            self._min = msg._min
            self._max = msg._max
            self.build = msg.build
        else:
            self._name = kwargs["name"]
            self._id = kwargs["id"]
            self.description = kwargs["description"]
            self._priority = Priority
            self._device = Device 
            if kwargs.get("scale") == None:
                self.scale = 1
            else:
                self.scale = kwargs.get("scale")
            self.unit = kwargs.get("unit")
            self.intervalSending = kwargs.get("intervalSending")
            self.intervalSaving = kwargs.get("intervalSaving")
            self._thresholdSending = kwargs.get("thresholdSending")
            self._thresholdSaving = kwargs.get("thresholdSaving")
            self._min = kwargs.get("min")
            self._max = kwargs.get("max")
            self.build = True
        
        
    def __str__(self):
        return "Message(name = {}, id = {}, description = {})".format(self.get_build_name(), self._id, self.description)
    
       
    def _set_Priority(self, prio):
        self._priority = prio
        
    def _set_Device(self, dev):
        self._device = dev
        
    def _build_name(self):
        build = Message.PREFIX
        if self.build:
            build += self._device.get_build_name() + "_"
        build += self._name
        return build
    
    def _build_id(self):
        build = self._id 
        if self.build:
            build = build + self._device.get_shift_id() 
            build = build + self._priority.get_shift_id()
        return build
    
    def get_hex_id(self):
        return "{:#010x}".format(self._build_id())
    
    def get_build_id(self):
        return self._build_id()
    
    def get_acquire_name(self):
        return self._build_name().replace(Message.PREFIX, Message.ACQUIRE_PREFIX,1)
    
    def get_use_name(self):
        return self._build_name().replace(Message.PREFIX, Message.USE_PREFIX,1)
    
    def get_ts_name(self):
        return self._build_name().replace(Message.PREFIX, Message.USE_TS_PREFIX,1)
    
    def get_name_without_prefix(self):
        return self._build_name().replace(Message.PREFIX, "",1)
    
    def get_build_name(self):
        return self._build_name()
        
    def get_name(self):
        return self._name
    
    def get_id(self):
        return self._id
    
    def get_Device(self):
        return self._device
    
    def is_expandable(self):
        if self._device == None or self._device.maxCount == 1:
            return False
        else:
            return True
    
    def get_expanded_msg(self, deviceInstanceNumber):
        """
        Get expanded Message for the given device instance number.
        """
        if deviceInstanceNumber >= self._device.maxCount:
            raise ValueError("requested deviceInstanceNumber {:d} but device only has {:d}".format(deviceInstanceNumber, self.device.instanceCount))
        args = dict()
        args["copy_from"] = self
        args["instance_no"] = deviceInstanceNumber
        return ExpandedMessage(args)

    def get_expanded_msgs(self):
        """ Get expanded Message. Each device instance needs a separate ID for
        messages. This works by having an ID template containing "_X_"
        in the configuration file. The "_X_" is then replaced by the
        respective device ID, resulting in an expanded ID.

        ID expansion is only performed if more than one instance exists.
        If no ID expansion is performed, a list containing only the
        unexpanded message is returned.
        """
        res = []
        if self._device == None or self._device.maxCount == 1:
            return [self]

        for i in range(0, self._device.maxCount):
            res.append(self.get_expanded_msg(i))
        return res
    
    def get_hex_id(self):
        return "{:#010x}".format(self._build_id())
    
    def get_min(self):
        if self._min == None:
            return self._min
        else:
            return (int)(self._min/self.scale)
    
    def get_max(self):
        if self._max == None:
            return self._max
        else:
            return (int)(self._max/self.scale)
        
    def get_thresholdSending(self):
        if self._thresholdSending == None:
            return self._thresholdSending
        else:
            return (int)(self._thresholdSending/self.scale)
    
    def get_thresholdSaving(self):
        if self._thresholdSaving == None:
            return self._thresholdSaving
        else:
            return (int)(self._thresholdSaving/self.scale)
    
        
        
class ExpandedMessage(Message):
    shift = 12
    def __init__(self, kwargs):
        super().__init__(kwargs)
        instance_no = kwargs["instance_no"]
        
        if instance_no >= self._device.maxCount:
            raise ValueError("instance ID {:d} greater than count {:d}".format(instance_no, self.device.maxCount))
        
        self.instanceNumber = instance_no
        
    def get_build_id(self):
        shifted = "{:#06b}".format(self.instanceNumber)
        shifted = shifted + ExpandedMessage.shift * "0" 
        my_id = self._build_id() + int(shifted,2)
        return my_id
    
    def get_hex_id(self):
        return "{:#010x}".format(self.get_build_id())
        
    def get_build_name(self):
        name = self._build_name().replace("_X_", "_" + str(self.instanceNumber) + "_")
        return name
    
    def get_acquire_name(self):
        return self._build_name().replace(Message.PREFIX, Message.ACQUIRE_PREFIX,1)
    
    def get_use_name(self):
        return self.get_build_name().replace(Message.PREFIX, Message.USE_PREFIX,1)
    
    def get_ts_name(self):
        return self.get_build_name().replace(Message.PREFIX, Message.USE_TS_PREFIX,1)
    
    def get_name_without_prefix(self):
        return self.get_build_name().replace(Message.PREFIX, "",1)
    
    def get_name_unexpanded(self):
        return self._build_name()
        
    def __str__(self):
        return "Message(name = {}, id = {}, description = {})".format(self.get_build_name(), self._id, self.description)
     
    
class Device(object):
    shift = 16
    def __init__(self, kwargs):
        # since name, id and description are mandatory and checked before we can use squared brackets
        # for non optional attributes not set before with default value we have to use the get function (returns None if not set)
        self._name = kwargs["name"]
        self._id = kwargs["id"]
        self.description = kwargs["description"]
        self.maxCount = kwargs["maxCount"]
        self.messages = []
        
    def __str__(self):
        return "Device(name = {}, id = {}, description = {})".format(self._name, self._id, self.description)

    def __lt__(self, other):
        return self._id < other._id
           
        
    def get_name(self):
        return self._name
        
    def get_id(self):
        return self._id
    
    def get_build_name(self):
        name = self._name
        if self.maxCount > 1:
            name += "_X"
        return name
    
    def get_hex_id(self):
        return "{:#04x}".format(self._id)
    
    def get_shift_id(self):
        shifted = "{:#010b}".format(self._id)
        shifted = shifted + Device.shift * "0"
        return int(shifted,2)
    

class Stream(object):
    def __init__(self, **kwargs):
        self.device = kwargs["device"]
        self.instance = kwargs["instance"]
        self.messages = []
        for msg in self.device.messages:
            self.messages.append(msg.get_expanded_msg(self.instance))
        self.multiAddr= [239, 188, self.device.get_id(), self.instance + 1]
        self.multicastAddr = "239.188." + str(self.device.get_id()) + "." + str(self.instance + 1)
        
    def get_name(self):
        return self.device.get_name() + "_MAIN_" + str(self.instance)
    

class Parser(object):
    def __init__(self):
        #create dictionaries for different object attributes
        self._message_parse = dict()
        self._message_parse["intervalSending"] = parseTime
        self._message_parse["intervalSaving"] = parseTime
        #list all not mandatory attributes with allowed types and other attributes (in string)
        #even if not used yet: {max: "attr"} is allowed where attr is the name of another message attribute
        #{int: scale} means attribute divided by scale needs to be an integer
        #{def: value} indicates the default set (value)
        self._message_attributes = dict()
        self._message_attributes["scale"] = [float, int, {min: 0, "def": 1}]
        self._message_attributes["unit"] = [str]
        self._message_attributes["intervalSending"] = [int, {min: 0}]
        self._message_attributes["intervalSaving"] = [int, {min: 0}]
        self._message_attributes["thresholdSending"] = [int, float, {min: 0, int: "scale"}]
        self._message_attributes["thresholdSaving"] = [int, float, {min: 0, int: "scale"}]
        self._message_attributes["min"] = [int, float, {min: 0, int: "scale"}]
        self._message_attributes["max"] = [int, float, {min: "min", int: "scale"}]
    
        #list all mandatory attributes with allowed types and other attributes (in string)
        self._device_attributes = dict()
        self._device_attributes["maxCount"] = [int, {min:  1, max: 16, "def": 1}]
        self._device_attributes["general"] = [bool, {"def": True}]
        self._device_attributes["Messages"] = [list]
        
    
        #list mandatory attributes for devices, messages and priorities
        self._mandatory_attributes = ["name", "id", "description"]
    
        #list all keys in config that are not declared by Priorities
        self._config_keys = ["devices", "namePrefix", "Priority"]
        
    #name, id and description are always mandatory for Priorities, Devices and Messages, names for messages not given as attribute
    def check_mandatory(self, obj):
        try: 
            for man in self._mandatory_attributes:
                obj[man]
        except KeyError as err:
            raise ParseError("you need to set " + str(err))


    #append general messages to device before parsing the message
    def _append_general_messages(self, device):
        for key in self._general:
            for msg in self._general[key]:
                if msg.get("addName") == True:
                    msg["description"] = msg["description"] + device["name"]
                    msg.pop("addName")
            if not device.get(key) == None:
                for msg in self._general[key]:
                    device[key].append(msg)
            else:
                device[key] = self._general[key]
                
    def _check_message_attributes(self, message):
        try:
            self.check_mandatory(message)
        except ParseError as err:
            ParseError("for message: " + str(err))
            
        try: 
            check_name(message.get("name"))
            check_id(message["id"], 12)
        except ParseError as err:
            raise ParseError("for message %s: " %(message.get("name")) + str(err))
            
 
        if not type(message["description"]) == str or len(message["description"]) == 0:
            raise ParseError("for message %s: description has to be a not empty string" %(message["name"]))
        
        
        #parse attributes if needed
        for attr in self._message_parse:
            if not message.get(attr) == None:
                message[attr] = self._message_parse[attr](message[attr])
                
        
        #check if optional attributes are set correctly if set, allowed attributes are listed in self._message_attributes with allowed types and additional properties (min or max in dicts as key)
        for attr in self._message_attributes:
            if not message.get(attr) == None:
                if not type(message[attr]) in self._message_attributes[attr]:
                    raise ParseError("Message %s: %s has the wrong type" %(message["name"], attr))
                
                #check for additional properties(min, max, ...)
                for bounds in self._message_attributes[attr]:
                    if type(bounds) == dict:
                        if min in bounds:
                            #min refered by other attribute or by default)
                            if type(bounds[min]) == str:
                                if not message.get(bounds[min]) == None:
                                    if not message.get(attr) >= message.get(bounds[min]):
                                        raise ParseError("Message %s: %s is too small (has a min value)"%(message["name"], attr))
                            else:
                                if not message.get(attr) >= bounds[min]:
                                    raise ParseError("message %s: %s is too small (has a min value)"%(message["name"], attr))
                        if max in bounds:
                            if type(bounds[max]) == str:
                                if not  message.get(bounds[max]) == None:
                                    if not message.get(attr) <= message.get(bounds[max]):
                                        raise ParseError("Message %s: %s is too large (has a max value)"%(message["name"], attr))
                            else:
                                if not message.get(attr) <= bounds[max]:
                                    raise ParseError("message %s: %s is too large (has a max value)"%(message["name"], attr))
                        if int in bounds:
                            if type(bounds[int]) == str:
                                if not message.get(bounds[int]) == None:
                                    if not (int)(message.get(attr)/ message.get(bounds[int])) == message.get(attr)/ message.get(bounds[int]):
                                        raise ParseError("Message %s: %s is not an int when divided by %s"%(message["name"], attr, bounds[int]))
                            else:
                                if not (int)(message.get(attr)/bounds[int]) == message.get(attr)/bounds[int]:
                                    raise ParseError("message %s: %s is too large (has a max value)"%(message["name"], attr))
                            
            else:
                for bounds in self._message_attributes[attr]:
                    if type(bounds) == dict:
                        if "def" in bounds:
                            message[attr] = bounds["def"]
        
    def _check_device_attributes(self, device):
        #check if mandatory attributes are set
        try: 
            self.check_mandatory(device)
        except ParseError as err:
            raise ParseError("for device: " + str(err))
           
        #check if name and id are set correctly (Type, min/ max value, other conventions)
        try: 
            check_name(device.get("name"))
            check_id(device["id"], 8)
        except ParseError as err:
            raise ParseError("for device: " + str(err))
        
        
        #check if other attributes have correct type and set default values
        if not type(device["description"]) == str or len(device["description"]) == 0:
            raise ParseError("for device: description has to be a not empty string")
        
        #check if optional attributes are set correctly if set, allowed attributes are listed in self._device_attributes with allowed types and additional properties (min or max in dicts as key)
        for attr in self._device_attributes:
            if not device.get(attr) == None:
                if not type(device[attr]) in self._device_attributes[attr]:
                    #print(type(device[attr])
                    raise ParseError("for device: %s has the wrong type" %(attr))
                
                for bounds in self._device_attributes[attr]:
                    if type(bounds) == dict:
                        if min in bounds:
                            if type(bounds[min]) == str:
                                if not device.get(bounds[min]) == None:
                                    if not device.get(attr) >= device.get(bounds[min]):
                                        raise ParseError("for device: %s is too small (has a min value)"%(attr))
                            else:
                                if not device.get(attr) >= bounds[min]:
                                    raise ParseError("for device: %s is too small (has a min value)"%(attr))
                        if max in bounds:
                            if type(bounds[max]) == str:
                                if not device.get(bounds[max]) == None:
                                    if not device.get(attr) <= device.get(bounds[max]):
                                        raise ParseError("for device: %s is too large (has a max value)"%(attr))
                            else:
                                if not device.get(attr) <= bounds[max]:
                                    raise ParseError("for device: %s is too large (has a max value)"%(attr))
            
            #set default values
            else:
                for bounds in self._device_attributes[attr]:
                    if type(bounds) == dict:
                        if "def" in bounds:
                            device[attr] = bounds["def"]
            
            
       
    
    def _parse_message(self, message, device, priority):
        self._check_message_attributes(message)
            
        
        msg = Message(message)
        msg._set_Device(device)
        msg._set_Priority(priority)
        self.messages[msg.get_build_name()] = msg
        return msg
    
    
    # get the parsed toml dictionary for a device
    def _parse_device(self, device):
        self._check_device_attributes(device)
        if not device.get("general") == False:
            self._append_general_messages(device)
                
        dev = Device(device)
        
        m_names = []
        for key in device.keys():
            #check for unknown keys
            #if not (key in self._device_attributes or key in self.priorities or key in self._mandatory_attributes):
            #   raise ParseError ("with Device: unknown key: %s" %(key))
            
            #parse all messages
            if key in self.priorities:
                m_ids = []
                for msg in range (0, len(device[key])):
                    new_msg = self._parse_message(device[key][msg], dev, self.priorities[key])
                    dev.messages.append(new_msg)
                    try:
                        check_unique(new_msg.get_id(), m_ids)
                        m_ids.append(new_msg.get_id())
                    except ParseError as err:
                        raise ParseError(" with messages in priority %s: "%(key) + str(err))
                    
                    try:
                        check_unique(new_msg.get_name(), m_names)
                        m_names.append(new_msg.get_name())
                    except ParseError as err:
                        raise ParseError(" with messages: " + str(err))
            
            #parse special messages
            if key == "Messages":
                if not dev.maxCount == 1:
                    raise ParseError("with general messages: device can not have multiple instances")
                for msg in range (0, len(device[key])):
                    message = device[key][msg]
                    message["id"] = message.get("completeID")
                    try:
                        self.check_mandatory(message)
                        check_name(message["name"])
                        check_id(message["id"], 29)
                    except ParseError as err:
                        raise ParseError(" with general messages: " + str(err))
                    if not (type(message["description"]) == str and len(message["description"])>1):
                        raise ParseError("with general message %s: description has to be a not empty string" %(message["name"]))
                    new_msg = Message(message)
                    new_msg._set_Device(dev)
                    new_msg.build = False
                    dev.messages.append(new_msg)
                    self.messages[new_msg.get_build_name()] = new_msg
                    
        self.devices.append(dev)
        


    def print_devices(self):
        for d in self.devices:
            print("Device name = {}".format(d._name))
            print("\t attributes: id = {}, description = {}, maxCount = {}".format(d._id, d.description, d.maxCount))
            #TODO ? Add messages and streams
            
    
    def parse_dictionary(self, config, source):
        self.messages = dict()
        self.devices = []
        self.priorities = dict()
        self._general = dict()
        self.streams = []
        
        
        #create dictionary with parsed toml as input for all devices named in config
        device = config.get('devices')
        deviceDict = dict()
        for ix in device:
            src = source + ix + ".toml"
            deviceDict[ix] = toml.load(src)
            
        #set message name prefix if set in config
        if not config.get("namePrefix") == None:
            Message.PREFIX = config.get("namePrefix")
        
        #check Priorities and parse to object Priority
        prioIDs = []
        for prio in config["Priority"]:
            try:
                self.check_mandatory(prio)
                check_name(prio["name"])
                check_id(prio["id"], 5)
                check_unique(prio["name"], self.priorities)
                check_unique(prio["id"], prioIDs)
                for key in prio.keys():
                    if not key in self._mandatory_attributes:
                        raise ParseError("unknown key " + key)
            except ParseError as err:
                raise ParseError("Error in config.toml with Priorities: " + str(err))
            prioIDs.append(prio["id"])
            priority = Priority(prio)
            self.priorities[prio["name"]] = priority
            
        
        
        for key in config.keys():
            #check for unknown keys
            if not (key in self.priorities or key in self._config_keys):
                raise ParseError("Error in config.toml with undefined key: " + key)
            
            #dictionary containing all Messages that should be generated for all devices
            if key in self.priorities:
                for msg in range(0, len(config[key])):
                    try:
                        self.check_mandatory(config[key][msg])
                        self._check_message_attributes(config[key][msg])
                    except ParseError as err:
                        raise ParseError ("Error in config.toml with general Messages: " + str(err))
                self._general[key] = config[key]

        
        #parse all devices (and their messages)
        for dev in deviceDict:
            try:
                self._parse_device(deviceDict[dev])
            except ParseError as err:
                raise ParseError("Error in file %s.toml "%(dev) + str(err))
        
        
        #check devices for unique name and id
        names = []
        ids = []
        for dev in self.devices:
            try:
                check_unique(dev.get_name(), names)
                check_unique(dev.get_id(), ids)
                names.append(dev.get_name())
                ids.append(dev.get_id())
            except ParseError as err:
                raise ParseError ("Error with devices: " + str(err))

        self.devices.sort()
        
        #create stream for each device instance
        for dev in self.devices:
            for inst in range(0, dev.maxCount):
                self.streams.append(Stream(device=dev, instance=inst))
    
        
if __name__ == "__main__":
    config = toml.load("src/config.toml")
    try:
        parser = Parser()
        parser.parse_dictionary(config, "src/")
        parser.print_devices()
    except ParseError as err:
        sys.exit(err)
    
    
     
