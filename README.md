# ASMO Software

This repository contains an implementation of the ASMO software architecture. The external libraries and operating system are included as submodules. The repository is structured in multiple folders:

### ASMO

Contains the data provisioning layer, including the safety, data retention and synchronization layers. These files can be generated from the communication matrix with the provided code generator.

### Simulink Models

Contains a Simulink model template and Matlab wrapper functions. These can be used for model-based applications, where C code is generated from Matlab Simulink models.

### Code generator

Contains the code generator to generate the data provisioning layer from the communication matrix. 

### TOML

Contains example TOML files for the code generation, which include the information from the communication matrix. As an example device the OxiMax N-560 pulse oximeter is used. 

### Submodules

The software architecture is built on the ChibiOS operating system, the DDS implementation embeddedRTPS and uses the lwip TCP/IP stack. These are integrated as submodules.
