# TOML files

The TOML files contain a global listing of all possible measurement and internally generated values. Besides a unique identifier and name the listing also contains additional information, for example unit and scaling factor. This repository contains example files for the OxiMax N-560 pulse oxymeter and internal values.